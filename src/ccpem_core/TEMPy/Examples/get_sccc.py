import sys
import os
import json
import pandas as pd
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
from TEMPy.MapParser import MapParser
from TEMPy.RigidBodyParser import RBParser
from TEMPy.ShowPlot import Plot
from TEMPy.ScoringFunctions import ScoringFunctions

def main():
    job_location = None
    score_name = None
    if len(sys.argv) > 1:
        # For use with CCP-EM
        json_path = sys.argv[1]
        if os.path.basename(json_path) == 'args.json':
            args = json.load(open(json_path, 'r'))
            m = args['map_path']
            p = args['pdb_path']
            r = args['map_resolution']
            rb_file = args['rigid_body_path']
            job_location = args['job_location']
            score_name = args['score_name']
        else:
            #map file
            m = sys.argv[1]
            #prot file
            p = sys.argv[2]
            #resolution
            r = float(sys.argv[3])
            # rigid body file indicating segments to be scored
            rb_file = sys.argv[4]
        
    else:
        m = 'Test_Files/1akeA_10A.mrc'
        p = 'Test_Files/1ake_mdl1.pdb'
        r = 10.0
        rb_file = 'Test_Files/1ake_mdl1_rigid.txt'
    
    # the sigma factor determines the width of the Gaussian distribution used to describe each atom
    sim_sigma_coeff = 0.187
    
    # make class instances for density simulation (blurring), scoring and plot scores
    blurrer = StructureBlurrer()
    scorer = ScoringFunctions()
    plot = Plot()

    # read map file
    emmap = MapParser.readMRC(m)

    # read PDB file
    structure_instance=PDBParser.read_PDB_file('pdbfile',
                                               p,
                                               hetatm=False,
                                               water=False)

    # generate atom density and blur to required resolution
    sim_map = blurrer.gaussian_blur(structure_instance,
                                    r,
                                    densMap=emmap,
                                    sigma_coeff=sim_sigma_coeff,
                                    normalise=True)
    SCCC_list_structure_instance=[]

    # read rigid body file and generate structure instances for each segment
    listRB = RBParser.read_FlexEM_RIBFIND_files(rb_file,
                                                structure_instance)

    # score each rigid body segment
    rb_sccc = {}
    for n, RB in enumerate(listRB):
        # sccc score
        score_SCCC = scorer.SCCC(emmap,
                                 6.6,
                                 sim_sigma_coeff,
                                 structure_instance,
                                 RB)
        SCCC_list_structure_instance.append(score_SCCC)
        print ('\nRigid Body: {0}'.format(n+1))
        print RB
        print ('SCCC: {:.3f}'.format(score_SCCC))
        rb_sccc[n] = score_SCCC

    # generate chimera attribute file for coloring segments based on sccc score 
    listRB = RBParser.RBfileToRBlist(rb_file)
    if job_location is None:
        attribute_path = None
    else:
        attribute_path = os.path.join(
            job_location,
            'sccc_atribute.txt')
    plot.PrintOutChimeraAttributeFileSCCC_Score(
        p,
        SCCC_list_structure_instance,
        listRB,
        out_path=attribute_path)

    # Output scores
    scf = None
    if score_name is not None:
        pName = os.path.basename(os.path.abspath(p)).split('.')[0]
        scf_path = os.path.join(job_location, score_name)
        scf = open(scf_path, 'w')
    elif os.path.isfile(os.path.abspath(p)):
        pName = os.path.basename(os.path.abspath(p)).split('.')[0]
        scf = open(os.path.join(
            os.path.dirname(os.path.abspath(p)),'sccc_'+pName),'w')

    if scf is not None:
        print 'Output SCCC scores: ', scf.name
        scf.write('Rigid Body : SCCC\n\n')
        for rb, sccc in rb_sccc.iteritems():
            score = '{0:5d} : {1:.3f}\n'.format(rb+1, sccc)
            scf.write(score)
        scf.close()

    # CCP-EM Jsrview output
    # Check in ccpem is available
    try:
        from ccpem_core.tasks.tempy.sccc import sccc_results
        df = pd.DataFrame.from_dict(rb_sccc, orient='index')
        df.columns = ['SCCC']
        print df
#         sccc_results.SCCCResultsViewer(
#             dataframe=df,
#             directory=job_location)
    except ImportError:
        pass

if __name__ == '__main__':
#     test = {}
#     test[1]=1.00
#     test[2]=2131.12312
#     print test
#     print df
    main()
