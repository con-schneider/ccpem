#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


'''
Task window for Buccaneer.
'''
import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_core.tasks.buccaneer import  buccaneer_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types

class BuccaneerWindow(window_utils.CCPEMTaskWindow):
    '''
    Buccaneer window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(BuccaneerWindow, self).__init__(task=task,
                                              parent=parent)
        self.rv_timer = QtCore.QTimer()
        self.rv_timer.timeout.connect(self.set_rv_ui)
        self.rv_timer.start(1500)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(map_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.1,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Map sharpen
        map_sharpen_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=-1000,
            maximum=1000,
            arg_name='map_sharpen',
            args=self.args)
        self.args_widget.args_layout.addWidget(map_sharpen_input)

        # Input seq
        self.seq_input = window_utils.SequenceArgInput(
            parent=self,
            arg_name='input_seq',
            args=self.args,
            label='Input sequence',
            required=True)
        self.args_widget.args_layout.addWidget(self.seq_input)

        # Initial model input
        extend_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='extend_pdb',
            args=self.args,
            label='Extend model',
            file_types=ccpem_file_types.pdb_ext,
            required=False)
        self.args_widget.args_layout.addWidget(extend_model_input)

        # Number of Buccaneer cycles
        ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle',
            args=self.args)
        self.args_widget.args_layout.addWidget(ncycle_input)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Keywords
        self.keyword_entry = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args)
        extended_options_frame.add_extension_widget(self.keyword_entry)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='input_seq',
            file_type='standard',
            description=self.args.input_seq.help,
            selected=False)
        self.launcher.add_file(
            arg_name='extend_pdb',
            file_type='standard',
            description=self.args.extend_pdb.help,
            selected=True)

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        if hasattr(self.task, 'job_location'):
            if self.task.job_location is not None:
                report = os.path.join(self.task.job_location,
                                      'report/index.html')
                if os.path.exists(report):
                    self.rv_view = QtWebKit.QWebView()
                    self.rv_view.load(QtCore.QUrl(report))
                    self.results_dock = QtGui.QDockWidget('Results',
                                                           self,
                                                           QtCore.Qt.Widget)
                    self.results_dock.setToolTip('Results overview')
                    self.results_dock.setWidget(self.rv_view)
                    self.tabifyDockWidget(self.setup_dock, self.results_dock)
                    self.results_dock.raise_()
                    self.rv_timer.stop()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        # Set launcher files
        if hasattr(self.task, 'process_maptomtz'):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.process_maptomtz.hklout_path,
                file_type='mtz',
                description='Structure factors from input map',
                selected=False)
        if hasattr(self.task, 'process_buccaneer_pipeline'):
            if hasattr(self.task, 'process_refine'):
                self.launcher.add_file(
                    path=os.path.join(os.path.dirname(
                        self.task.process_refine.pdbout_path),
                        'buccaneer.pdb'),
                    file_type='pdb',
                    description='Refined PDB file',
                    selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()

def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
#         args_json='./test_data/unittest_args.json',
        task=buccaneer_task.Buccaneer,
        window=BuccaneerWindow)

if __name__ == '__main__':
    main()
