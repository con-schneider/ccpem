#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import pytest
import os
import argparse
import ccpem_core


'''
N.B. Three levels of unit tests marked by tags in the class / function name.

   (1) Unit test (to execute in < 1sec, run always)
       class TestFoo
           or
       def test_foo

   (2) Integration tests (to execute in < 1min, run frequently)
       class TestIntegration
           or
       def test_integration

   (3) Performance tests (to execute in > 1min, run rarely / for development)
       class TestPerformance
           or
       def test_performance
'''
# Coverage settings
unit_test_only = ('not (performance or integration '
                  'or Performance or Integration)')
unit_and_integration = 'not performance'
unit_integration_performance = ''


ignore = []
# XXX ignore EMDB sfftk unittests for now
from ccpem_progs import emdb_sfftk
ignore.append(os.path.dirname(emdb_sfftk.__file__))

from ccpem_progs import emdb_xml_translator
ignore.append(os.path.dirname(emdb_xml_translator.__file__))

# XXX ignore Beeby lab symm unittests for now
from ccpem_progs import symm
ignore.append(os.path.dirname(symm.__file__))

# # XXX Debug
# try:
#     import xdist
#     print 'Xdist path : ', xdist.__file__
# except ImportError:
#     print 'Xdist not available'

def unittest_args():
    '''
    Unit test option parser.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-n',
                        '--ncpu',
                        help='number of CPUs',
                        type=int,
                        default=1)
    parser.add_argument('-c',
                        '--coverage',
                        help='coverage of test',
                        choices=['unit', 'integration', 'performance'],
                        type=str,
                        default='integration')
    parser.add_argument('-p',
                        '--path',
                        help='root path (will test recursively from this path)',
                        type=str,
                        default=None)
    parser.add_argument('-xml',
                        '--xml_output',
                        help='output results.xml',
                        type=bool,
                        default=False)
    return parser.parse_args()

def main(args):
    '''
    See pytest docs for usage and invocations:
       http://doc.pytest.org/en/latest/usage.html
       pytest.main(sys.argv[1:])
    '''
    args = unittest_args()
    # Set test coverage
    coverage = unit_and_integration
    if args.coverage == 'unit':
        coverage = unit_test_only
    if args.coverage == 'performance':
        coverage = unit_integration_performance
    # Set number of CPUs available
    if args.path is None:
        ccpem_core_path = os.path.dirname(ccpem_core.__file__)
        # Move to parent to include ccpem_core and ccpem_progs
        path = os.path.join(ccpem_core_path,
                            os.pardir)
        print path
    else:
        path = args.path
    # Run pytest
    #     N.B. no doctest
    run_args = [path,
                 '-p', 'no:doctest',
                 '-k', coverage]
    for path in ignore:
        run_args += ['--ignore', path]
    if args.xml_output:
        run_args += ['--junitxml', 'results.xml']
    pytest.main(run_args)

if __name__ == '__main__':
    main(sys.argv)

