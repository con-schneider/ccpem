#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
import normalise_nan


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.npz = np.load('unittests/testdata/make_ac_samples_exp_results.npz')

    def test_normalise_nan(self):
        result = normalise_nan.nan_norm(self.npz["sample_lm"][:,  0:2, 0:2])
        exp = np.load("unittests/testdata/expected_nan_norm.npy")
        np.testing.assert_array_equal(result, exp, "Normalised array with removed nan values did not match expected.")

    def test_nan_in_array(self):
        result = normalise_nan.nan_norm(self.npz["sample_lm"][:, 0:2, 0:2])
        self.assertEqual(True in np.isnan(result), False, "NaN in output array after running normalise_nan.")

if __name__ == "__main__":
        unittest.main()
