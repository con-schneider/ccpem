#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtGui, QtCore
from ccpem_gui.utils import ccpem_widgets
from ccpem_core import process_manager
import window_utils
from ccpem_core import settings


class LaunchFile(object):
    '''
    View types can be either a specific program or the following general types
        standard -> open with users default application via QDesktopServices
                    uses xdg-open on linux
        mol_graphics -> default to users preference of mol graphics
    '''
    file_types = ['map', 'mtz', 'pdb', 'standard']

    def __init__(self,
                 arg_name,
                 description,
                 file_type='standard',
                 path=None,
                 group='Files',
                 selected=True,
                 display_from='running'):
        self.arg_name = arg_name
        assert file_type in self.file_types
        self.file_type = file_type
        self.description = description
        self.path = path
        self.group = group
        if selected:
            self.selected = 2
        else:
            self.selected = 0
        self.display_from = display_from
        assert self.display_from in process_manager.statuses

    def get_path(self, args=None):
        if self.path is not None:
            return self.path
        elif self.arg_name is not None and args is not None:
            if hasattr(args, self.arg_name):
                return getattr(args, self.arg_name).value
            else:
                return None
        else:
            return None


class CCPEMLauncher(QtGui.QGroupBox):
    '''
    View / launch job files.
    '''
    all_mg_progs = {'ccp4mg':'CCP4mg',
                    'coot':'Coot',
                    'chimera':'Chimera'}
    mg_progs = []
    for command, prog in all_mg_progs.iteritems():
        if settings.which(command) is not None:
            mg_progs.append(prog)


    def __init__(self,
                 parent,
                 mg_default=None):
        super(CCPEMLauncher, self).__init__(parent)
        self.task_window = parent
        self.files = []
        if mg_default is not None:
            if mg_default not in self.mg_progs:
                mg_default = None

        self.mg_files = {'map': [],
                         'mtz': [],
                         'pdb': [],
                         'cif': []}

        # Setup layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        # Setup button box
        self.button_box = QtGui.QDialogButtonBox()
        self.layout.addWidget(self.button_box)

        # Set MG program
        self.mg_button = QtGui.QPushButton()
        self.mg_button.setToolTip('Select default MG program')
        menu = QtGui.QMenu(self)
        for prog in self.mg_progs:
            menu.addAction(prog, self.set_coot_default)
        self.mg_button.setMenu(menu)
        self.button_box.addButton(self.mg_button, self.button_box.ApplyRole)
        # Get default MG program from Qt global settings
        if mg_default is None:
            q_settings = QtCore.QSettings()
            mg_default = q_settings.value('MG_DEFAULT_KEY').toString()
            if mg_default == '':
                mg_default = 'Coot'

        self.set_mg_default(mg_default)

        # Open selected button
        self.open_button = QtGui.QPushButton('&Open selected')
        self.setToolTip('Open in default program(s).  Use toolbar to launch '
                        'in alternative viewer.')
        self.open_button.setDefault(True)
        self.open_button.clicked.connect(self.launch_viewers)
        self.button_box.addButton(self.open_button, self.button_box.ApplyRole)

        # Add info message
        self.message_label = QtGui.QLabel('')
        self.layout.addWidget(self.message_label)
        self.message_label.hide()

        # Setup tree widget
        self.tree_widget = QtGui.QTreeWidget()
        self.tree_widget.setToolTip('Select files to open.')
        self.layout.addWidget(self.tree_widget)
        self.set_tree_view()
        self.tree_widget.doubleClicked.connect(self.on_double_click)

    def set_message_label(self, message):
        self.message_label.setText(message)
        self.message_label.show()

    def set_mg_default(self, mg_prog):
        if mg_prog in self.mg_progs:
            self.mg_default = mg_prog
            self.mg_button.setText(mg_prog)
            # Save to global Qt settings
            q_settings = QtCore.QSettings()
            q_settings.setValue('MG_DEFAULT_KEY',
                                mg_prog)

    def set_coot_default(self):
        self.set_mg_default('Coot')

    def set_chimera_default(self):
        self.set_mg_default('Chimera')

    def set_ccp4mg_default(self):
        self.set_mg_default('CCP4mg')

    def add_file(self,
                 description='',
                 file_type='standard',
                 arg_name=None,
                 path=None,
                 display_from='running',
                 selected=True,
                 group='Files'):
        lf = LaunchFile(
            arg_name=arg_name,
            file_type=file_type,
            path=path,
            description=description,
            display_from=display_from,
            selected=selected,
            group=group)
        append = True

        # Path or arg name should be unique
        for f in self.files:
            if lf.arg_name is not None:
                if f.arg_name == lf.arg_name:
                    append = False
            if lf.path is not None:
                if f.path == lf.path:
                    append = False
        if append:
            self.files.append(lf)
            self.set_tree_view()

    def set_tree_view(self):
        '''
        Updates tree view.
        '''
        self.tree_widget.clear()
        self.tree_widget.setColumnCount(3)
        self.tree_widget.setHeaderLabels(['Name',
                                          'Description',
                                          'Path'])
        self.tree_widget.setItemsExpandable(True)

        # Display data in file groups
        for ccpem_file in self.files:
            # Check status
            i_stat = process_manager.statuses.index(
                self.task_window.status)
            i_disp = process_manager.statuses.index(
                ccpem_file.display_from)
            if not i_disp <= i_stat:
                continue

            # Check if group already present
            find_group = self.tree_widget.findItems(
                ccpem_file.group,
                QtCore.Qt.MatchContains)
            if len(find_group) == 0:
                group_item = QtGui.QTreeWidgetItem(
                    self.tree_widget,
                    [ccpem_file.group])
            else:
                group_item = find_group[0]

            # Get file info
            path = ccpem_file.get_path(args=self.task_window.args)
            if path is not None:
                if os.path.exists(path):
                    file_info = [os.path.basename(path),
                                 ccpem_file.description,
                                 path]

                    # Set items
                    file_item = QtGui.QTreeWidgetItem(
                        group_item,
                        file_info)
                    file_item.ccpem_file = ccpem_file
                    file_item.setCheckState(0, ccpem_file.selected)
                    self.tree_widget.expandItem(file_item)
                    self.tree_widget.expandItem(group_item)

        # Connect item changed to update ccpem_file dictionary
        self.tree_widget.itemChanged.connect(self.set_selected)
        self.tree_widget.resizeColumnToContents(0)
        self.tree_widget.resizeColumnToContents(1)

    @QtCore.pyqtSlot(int)
    def on_double_click(self, index):
        item = self.tree_widget.itemFromIndex(index)
        path = item.ccpem_file.get_path(args=self.task_window.args)
        if path is not None:
            if os.path.exists(path=path):
                if item.ccpem_file.file_type == 'standard':
                    ccpem_widgets.launch_desktop_services(path=path)
                else:
                    self.clear_mg_files()
                    if item.ccpem_file.file_type in self.mg_files.keys():
                        self.mg_files[item.ccpem_file.file_type].append(path)
                    self.launch_mg_viewers(select_files=False)

    @QtCore.pyqtSlot(int)
    def set_selected(self, col):
        '''
        Set selection state in ccpem_file dictionary
        '''
        col.ccpem_file.selected = int(col.checkState(0))

    def launch_viewers(self):
        '''
        Launch external view programs for selected files.
        '''
        self.launch_standard_file_viewers()
        self.launch_mg_viewers()

    def launch_standard_file_viewers(self):
        '''
        Launch selected file in standard application.
        '''
        for ccpem_file in self.files:
            if ccpem_file.file_type == 'standard':
                if ccpem_file.selected:
                    path = ccpem_file.get_path(args=self.task_window.args)
                    if path is not None:
                        ccpem_widgets.launch_desktop_services(path=path)

    def clear_mg_files(self):
        self.mg_files = {'map': [],
                         'mtz': [],
                         'pdb': [],
                         'cif': []}

    def launch_mg_viewers(self,
                          mg_viewer='Default',
                          select_files=True,
                          launch_mg_without_file=False):
        assert (mg_viewer == 'Default' or mg_viewer in self.mg_progs)
        if select_files:
            self.clear_mg_files()
            find_mg_file = False
            for ccpem_file in self.files:
                if ccpem_file.file_type != 'standard':
                    if ccpem_file.selected:
                        path = ccpem_file.get_path(args=self.task_window.args)
                        if path is not None:
                            if ccpem_file.file_type in self.mg_files.keys():
                                self.mg_files[ccpem_file.file_type].append(path)
                                find_mg_file = True
        else:
            find_mg_file = True
        if find_mg_file or launch_mg_without_file:
            if mg_viewer == 'Default':
                mg_viewer = self.mg_default
            if mg_viewer == 'Coot':
                self.run_selected_coot()
            elif mg_viewer == 'Chimera':
                self.run_selected_chimera()
            elif mg_viewer == 'CCP4mg':
                self.run_selected_ccp4mg()

    def run_selected_coot(self):
        if self.task_window is not None:
            if self.task_window.run_coot_custom() != NotImplemented:
                return
        args = []
        for pdb in self.mg_files['pdb']:
            args += ['--pdb', pdb]
        for map_ in self.mg_files['map']:
            args += ['--map', map_]
        for mtz in self.mg_files['mtz']:
            args += ['--data', mtz]
        for cif in self.mg_files['cif']:
            args += ['--cif', cif]
        window_utils.run_coot(args=args)

    def run_selected_chimera(self):
        args = []
        for pdb in self.mg_files['pdb']:
            args.append(pdb)
        for map_ in self.mg_files['map']:
            args.append(map_)
        for mtz in self.mg_files['mtz']:
            args.append(mtz)
        for cif in self.mg_files['cif']:
            args.append(cif)
        if self.task_window is not None:
            if self.task_window.run_chimera_custom() != NotImplemented:
                return
        window_utils.run_chimera(args=args)

    def run_selected_ccp4mg(self):
        # No restore arg stops ccp4mg asking to restore previous state
        if self.task_window is not None:
            if self.task_window.run_ccp4mg_custom() != NotImplemented:
                return
        args = ['-norestore']
        for pdb in self.mg_files['pdb']:
            args.append(pdb)
        for map_ in self.mg_files['map']:
            args.append(map_)
        for mtz in self.mg_files['mtz']:
            args.append(mtz)
        for cif in self.mg_files['cif']:
            args.append(cif)
        window_utils.run_ccp4mg(args=args)