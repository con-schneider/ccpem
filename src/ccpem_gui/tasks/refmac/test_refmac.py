#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test refmac task
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.refmac import refmac_task
from ccpem_gui.tasks.refmac import refmac_window
from ccpem_core.test_data.tasks import refmac as refmac_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app = QtGui.QApplication(sys.argv)


class RefmacTest(unittest.TestCase):
    '''
    Unit test for Refmac5 (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(refmac_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_refmac_window_integration(self):
        '''
        Test refmac refinement pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Refmac')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = refmac_task.Refmac(job_location=self.test_output,
                                      args_json=args_path)
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = refmac_window.Refmac5Window(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        timeout = 0
        # Global refine stdout (i.e. last job in pipeline)
        stdout = run_task.pipeline.pipeline[-1][-1].stdout
        assert os.path.basename(stdout) == 'refmacrefineglobal_stdout.txt'
        delay = 5.0
        timeout_limit = 200
        while not job_completed and timeout < timeout_limit:
            print 'Refmac running for {0} secs (timeout = 500)'.format(
                timeout,
                timeout_limit)
            time.sleep(delay)
            timeout += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        job_completed = True
        # Check timeout
        assert timeout < timeout_limit
        # Check job completed
        assert job_completed


    def test_refmac_dac_window_integration(self):
        '''
        Test refmac DAC refinement pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Refmac DaC mode')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args_dac.json')
        run_task = refmac_task.Refmac(job_location=self.test_output,
                                      args_json=args_path)
        print run_task.args.output_args_as_text()
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = refmac_window.Refmac5Window(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        timeout = 0
        # Global refine stdout (i.e. last job in pipeline)
        stdout = run_task.pipeline.pipeline[-1][-1].stdout
        assert os.path.basename(stdout) == 'refinechainbychain_stdout.txt'
        delay = 5.0
        timeout_limit = 200
        while not job_completed and timeout < timeout_limit:
            print 'Refmac DaC running for {0} secs (timeout = 500)'.format(
                timeout,
                timeout_limit)
            time.sleep(delay)
            timeout += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        job_completed = True
        # Check timeout
        assert timeout < timeout_limit
        # Check job completed
        assert job_completed


if __name__ == '__main__':
    unittest.main()
