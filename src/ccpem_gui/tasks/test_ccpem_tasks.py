#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
from ccpem_gui.tasks import ccpem_tasks
from ccpem_core.tasks.refmac import refmac_task


class Test(unittest.TestCase):
    '''
    Unit test for ccpem tasks.
    '''
    # Get task and assert returns correct class
    tasks = ccpem_tasks.CCPEMTasks()
    task = tasks.get_task_class(program='Refmac5')()
    assert isinstance(task, refmac_task.Refmac)


if __name__ == '__main__':
    unittest.main()
