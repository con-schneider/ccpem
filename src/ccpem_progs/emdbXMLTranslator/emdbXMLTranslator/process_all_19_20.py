#!/usr/bin/env python
"""
process_all_19_20.py

Wrapper script for emdb_xml_translate.py for converting v 1.9 files
in EMDB to v2.0.

TODO:

Version history:


                

Copyright [2014-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""


__author__ = 'Ardan Patwardhan'
__email__ = 'ardan@ebi.ac.uk'
__date__ = '2014-11-30'

import glob
import os
import logging
import subprocess
from optparse import OptionParser
from emdb_settings import emdb_settings

logging.basicConfig(level=emdb_settings.log_level, format=emdb_settings.log_format)


def process_all_19_20(filePathTemplate, outDir):
    commandListBase= ['python', './emdb_xml_translate.py', '-f'] 
    emdbFiles = glob.glob(filePathTemplate)
    numErrors = 0
    numSuccess = 0
    errorList = []
    for f in emdbFiles:
        inf = os.path.basename(f)
        outf = os.path.join(outDir,inf)
        logging.info("Input file: %s, output file: %s" % (f, outf))
        commandList = list(commandListBase)
        commandList.append(outf)
        commandList.append(f)
        cmdText = ' '.join(commandList)
        logging.info('Executing: %s' % cmdText)
        exitCode = subprocess.call(commandList)
        if exitCode != 0:
            numErrors += 1
            errorList.append(inf)
        else:
            numSuccess += 1
    logging.warning('%d files successfully processed!' % numSuccess)
    if numErrors > 0:
        logging.warning('%d errors!' % numErrors)
        logging.warning('List of entries that were not translated')
        for entry in errorList:
            logging.warning(entry)
        
def main():
    """
    Convert all EMDB XML 1.9 header files to XML 2.0 files
    
    """
    
    defaultFilePathTemplate=emdb_settings.archiveHeaderTemplate
    defaultOutDir=emdb_settings.emdb20Dir

    # Handle command line options
    usage = """
            process_all_19_20.py [options] 
            Convert all EMDB XML 1.9 header files to XML 2.0 files.

            Examples:
            python process_all_19_20.py

            Typical run:
            python process_all_19_20.py -t '/data/emstaging/EMD-*/header/emd-*.xml' -o '/data/emdb20'
            /data/emstaging/EMD-*/header/emd-*.xml is the template used to glob all input 1.9 header files
            /data/emdb20 is the output directory with the EMDB XML 2.0 files
            
               
            """
    version = "0.1"
    parser = OptionParser(usage = usage, version = version)
    parser.add_option("-t", "--template", action="store", type="string", metavar="TEMPLATE", dest="filePathTemplate", default = defaultFilePathTemplate, help="Template used to glob all input 1.9 header files [default: %default]")
    parser.add_option("-o", "--out-dir", action="store", type="string", metavar="DIR", dest="outDir", default = defaultOutDir, help="Directory for EMDB XML 2.0 files [default: %default]")
    (options, args) = parser.parse_args()
    process_all_19_20(options.filePathTemplate, options.outDir)


    
if __name__ == "__main__":
    main()
    