#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtCore, QtGui
import RemoteSettings_ui
import json
from ccpem_core.gui.job_manager import longbow
from ccpem_core.gui.job_manager.longbow import *

'''
Remote setting class previously used in Flex widget.  Moved here for possible 
use in future.
'''

class RemoteSettings(QtGui.QDialog):
    '''
    Creates the remote settings window (i.e. for running jobs on a cluster)
    '''
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.ui = RemoteSettings_ui.Ui_remote_settings()
        self.ui.setupUi(self)
        self.server_settings = {}
        self.ui.buttonBox.accepted.connect(self.accept)
        self.ui.buttonBox.rejected.connect(self.close)
      #  if os.path.isfile('serverdetails.json'):
      #      data = json.load(open('serverdetails.json', 'r'))
        self.ui.userEdit.setText("")
        self.ui.portSpinBox.setValue(22)
        self.ui.hostNameEdit.setText("")
        self.ui.remoteworkdirEdit.setText("")
      #  self.ui.commandlineEdit.setText("")
        self.ui.queueEdit.setText("")
        self.ui.coresEdit.setText("")
        self.ui.maxtimeEdit.setText("")
        self.ui.accountflagEdit.setText("")
        self.ui.accountEdit.setText("")
        self.ui.clusterEdit.setText("")
        self.ui.frequencyEdit.setText("")
        self.ui.corespernodeEdit.setText("")
        self.ui.memoryEdit.setText("")
        self.ui.handlerEdit.setText("")




        # if the save details checkbox is checked, save the server settings
        self.ui.saveDetailsCheckBox.stateChanged.connect(self.save_details)





    def save_details(self):
        self.server_settings = {
            'host' : str(self.ui.hostNameEdit.text()),
            'port' : int(self.ui.portSpinBox.text()),
            'user' : str(self.ui.userEdit.text()),
         #   'password' : str(self.ui.passwordEdit.text()),  # not possible in Longbow
            'scheduler' : str(self.ui.schedulerComboBox.currentText()),
            'remoteworkdir' : str(self.ui.remoteworkdirEdit.text())
            }

        if self.ui.saveDetailsCheckBox.isChecked():
            with open('serverdetails.json', 'w+') as file_data:
                file_data.write(json.dumps(self.server_settings))
        return self.server_settings


# if the user clicks OK, load data containing input files and save server settings and job settings (additional parameters required by Longbow that need to be written in hosts.conf)
    def accept(self):
        with open('flexemdetails.json') as data_file:    
             data = json.load(data_file)

        em = data["em_map_file"]
        pdb = data["input_pdb_file"]
        rigid = data["rigid_filename"]
 

        '''
        Actions to take if the user clicks the ok button in the remote settings
        popup window
        '''
        self.server_settings = {
            'host' : str(self.ui.hostNameEdit.text()),
            'port' : int(self.ui.portSpinBox.text()),
            'user' : str(self.ui.userEdit.text()),
         #   'password' : str(self.ui.passwordEdit.text()), # not possible in longbow
            'scheduler' : str(self.ui.schedulerComboBox.currentText()),
            'remoteworkdir' : str(self.ui.remoteworkdirEdit.text()),
            'download-include' :  str("flex-em.log"),
            'upload-include' : str("CG.py, MD.py, rigid.py, %s, %s, %s" %(em,pdb,rigid))
            }


        self.job_settings = {
            'executable' : str("python"),
          #  'commandline' : str(self.ui.commandlineEdit.text()), # to be removed from GUI: commandline is fixed and includes all the files to be staged
            'queue' : str(self.ui.queueEdit.text()),
            'cores' : str(self.ui.coresEdit.text()),
            'maxtime' : str(self.ui.maxtimeEdit.text()),
           # 'localworkdir' : str(self.ui.localworkdirEdit.text()),   # localworkdir is separately written , taken from job.location
            'accountflag' : str(self.ui.accountflagEdit.text()),
            'account' : str(self.ui.accountEdit.text()),
            'cluster' : str(self.ui.clusterEdit.text()),
            'frequency' : str(self.ui.frequencyEdit.text()),
            'corespernode' : str(self.ui.corespernodeEdit.text()),
            'memory' : str(self.ui.memoryEdit.text()),
            'handler' : str(self.ui.handlerEdit.text()),
            'executableargs': str("flex-em.py > flex-em.log"   ) # includes all the input files we have written and the 3 input files selected from GUI

            }
  


# save server settings and job settings in dictionary files
        if self.ui.saveDetailsCheckBox.isChecked():
            with open('serverdetails.json', 'w+') as file_data:
                file_data.write(json.dumps(self.server_settings))
        
            with open('jobdetails.json', 'w+') as file_data2:
                file_data2.write(json.dumps(self.job_settings))


        self.close()
        return self.server_settings
        return self.job_settings
#



