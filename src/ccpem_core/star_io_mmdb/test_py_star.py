#
#     Copyright (C) 2014 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from collections import OrderedDict
import random
from ccpem_core.star_io_mmdb import proto_py_star


class Test(unittest.TestCase):
    '''
    Unit test for star file i/o (uses MMDB via SWIG bindings)
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_py_star(self):
        # Read star file
        star_filename = os.path.join(self.test_data,
                                     'rln_postprocess_run1.star')
        assert os.path.exists(star_filename)
        star_parser = proto_py_star.MetaData(filename=star_filename)
   
        # Check all expected data blocks present
        for block in ['general', 'fsc', 'guinier']:
            assert block in star_parser.metadata.keys()
   
        # Check expected pair value
        res = '14.160000'
        assert star_parser.metadata['general']['rlnFinalResolution'] == res
   
        # Check muliline string ok
        ml = ('    yyyyyyyyyyy\n'
              '  xxxxxxxxx\n'
              'asdasd\n')
        assert star_parser.metadata['guinier']['multiline'] == ml
   
        # Check loop
        val = star_parser.metadata[
            'guinier']['loop_1']['rlnLogAmplitudesWeighted'][9]
        assert val == '-7.275771'
        val = star_parser.metadata[
            'guinier']['loop_1']['rlnLogAmplitudesSharpened'][-1]
        assert val == '-10.239869'
  
        # Save and reload
        star_filename2 = os.path.join(self.test_output, 'test.star')
        star_parser.write_star(filename=star_filename2)
        star_parser2 = proto_py_star.MetaData(filename=star_filename2)
        assert star_parser2.metadata['guinier']['multiline'] == ml
  
        # Speed test
        del star_filename
        del star_filename2
        star_create = os.path.join(self.test_output, 'create.star')
        md = proto_py_star.MetaData()
        block_name = 'test_block'
        md.add_block(block_name)
        loop = md.add_loop(data_block=block_name)
        rows = 100000
        # 3 float x 100K lines = create, write in ~1.0sec, using numpy as txt
        # is .72sec
        print 'Random'
        dict = OrderedDict({
            'col1': list(range(1, rows+1)),
            'col2': [random.uniform(0, 10) for r in xrange(rows)],
            'col3': [random.uniform(0, 10) for r in xrange(rows)]})
        print 'Set'
        md.set_loop(data_block=block_name,
                    data_loop=loop,
                    loop_dict=dict)
        print 'Write'
        md.write_star(filename=star_create)
        print star_create

        # Re-load
        # Read 100K line read = 0.8 sec to read
        del md
        star_parser = proto_py_star.MetaData(filename=star_create)

if __name__ == '__main__':
    unittest.main()
