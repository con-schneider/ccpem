#!/usr/bin/python
import sys, time

results = """
#################################################################
## Divide and Conquer - refinement of multiple PDBs for cryoEM ##
#################################################################
############### Version alpha ########### Dec 2016 ##############
#################################################################
## Copyright (C) 2016  Oleg Kovalevskiy, Robert Nicholls       ##
##                     and Garib Murshudov                     ##
##                                                             ##
## This program runs other programs:                           ##
## REFMAC5 by Garib Murshudov                                  ##
## PrepEM by Robert Nicholls                                   ##
## collect_pdbs by Garib Murshudov                             ##
## extract_and_collect by Garib Murshudov                      ##
##                                                             ##
## Program is distributed in the hope that it will be useful,  ##
## but WITHOUT ANY WARRANTY; without even the implied warranty ##
## of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.     ##
## See the GNU Lesser General Public License (v3) for details. ##
## The authors accept no liability whatsoever for any damage   ##
## caused by the use or misuse of this software.               ##
##                                                             ##
## E-mail: okovalev@mrc-lmb.cam.ac.uk                          ##
##                                                             ##
#################################################################


Working in: DaC_13/

Program received following parameters:

List of PDBs and maps: /lmb/home/okovalev/Work/DivideAndConquer/play/5me2.txt
Running refinements on 4 CPUs
Number of refinement cycles: 1


Reading correspondence between PDBs and maps (with resolution) from file /lmb/home/okovalev/Work/DivideAndConquer/play/5me2.txt :

PDB file '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2_ab.pdb' (id '01') corresponds to map '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map' with resolution 3.20
PDB file '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2_cd.pdb' (id '02') corresponds to map '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map' with resolution 3.20

2 PDB files and 1 maps will be processed

Preparing PDB files for refinement...

Collecting input PDBs into one PDB to analyse chain contacts...

Analysing chain contacts...

For segment 01A following surrounding segments written out to file '/lmb/home/okovalev/Work/DivideAndConquer/play/DaC_13/chainsForRefinement/collectedBeforeRef_segment01A.pdb':
   MAPIN1, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 01A 01B 
   MAPIN2, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 02C 02D 

For segment 01B following surrounding segments written out to file '/lmb/home/okovalev/Work/DivideAndConquer/play/DaC_13/chainsForRefinement/collectedBeforeRef_segment01B.pdb':
   MAPIN1, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 01A 01B 
   MAPIN2, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 02C 

For segment 02C following surrounding segments written out to file '/lmb/home/okovalev/Work/DivideAndConquer/play/DaC_13/chainsForRefinement/collectedBeforeRef_segment02C.pdb':
   MAPIN1, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 01A 01B 
   MAPIN2, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 02C 02D 

For segment 02D following surrounding segments written out to file '/lmb/home/okovalev/Work/DivideAndConquer/play/DaC_13/chainsForRefinement/collectedBeforeRef_segment02D.pdb':
   MAPIN1, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 01A 
   MAPIN2, map file: '/lmb/home/okovalev/Work/DivideAndConquer/5me2/5me2.map', segments: 02C 02D 


4 PDB files containing target chain plus surrounding chains prepared for refinement.

Generating scripts for refinement...

Queuing 4 refinements on 4 CPUs...

4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
4 refinements are running, 0 refinements finished (0 succesfully), 0 refinements to run
Waiting for 5 seconds...
2 refinements are running, 2 refinements finished (2 succesfully), 0 refinements to run
Waiting for 5 seconds...

All 4 refinement jobs finished; 4 - succesfully.

Regenerating original PDB structure from refined chains...


Generating '5me2_ab_ref.pdb'...
Calculating FSC and R-factor for '5me2_ab.pdb'...
Calculating FSC and R-factor for '5me2_ab_ref.pdb'...

Generating '5me2_cd_ref.pdb'...
Calculating FSC and R-factor for '5me2_cd.pdb'...
Calculating FSC and R-factor for '5me2_cd_ref.pdb'...

Generating 'collected.pdb' containing all chains altogether...

Please find following refined PDB files in the output directory DaC/ :
5me2_ab_ref.pdb
Overall R-factor      (before/after): 0.478 / 0.387
Average FSC           (before/after): 0.634 / 0.842
Ramachandran outliers (before/after): 0.00 / 0.00 %
Ramachandran favoured (before/after): 97.13 / 96.77 %
RMS bonds             (before/after): 0.009 / 0.016
RMS angles            (before/after): 1.140 / 1.640

5me2_cd_ref.pdb
Overall R-factor      (before/after): 0.473 / 0.393
Average FSC           (before/after): 0.624 / 0.831
Ramachandran outliers (before/after): 0.00 / 0.00 %
Ramachandran favoured (before/after): 97.13 / 96.77 %
RMS bonds             (before/after): 0.009 / 0.015
RMS angles            (before/after): 1.150 / 1.600

collected.pdb



"""



def main():
    '''
    Run task
    '''

    stdin = []

    for line in sys.stdin:
        stdin.append(line.strip())
        if line.strip().lower() == 'end':
            break


    print 'Dummy DaC'
    print 'Received parameters:'
    print sys.argv
    print 'Received stdin:'
    for line in stdin:
        print line
    print
    print 'Pretending of doing something useful for 10 seconds...'
    time.sleep(10)
    print
    print 'Printing some standard DaC output, so it could be parsed by results viewer'
    print
    print results




if __name__ == '__main__':
    main()


