#!/usr/bin/env python

import sys
from helper_functions.nptomo import open_tom

from helper_functions.VolumeData.arraygrid import Array_Grid_Data
from helper_functions.writemrc import write_mrc2000_grid_data as write_mrc


def save_npy_as_mrc_file(ofilepath, npy_array):

    data = Array_Grid_Data(npy_array)
    write_mrc(data, ofilepath)
    return


def npy2mrc(ifilepath, ofilepath):

    with open_tom(ifilepath) as tom:

        data = tom.get_all_tom_density_values()
        save_npy_as_mrc_file(ofilepath, data)

    return


if __name__ == '__main__':

    in_filepath = sys.argv[1]
    out_filepath = in_filepath[:-4] + ".mrc"

    sys.exit(npy2mrc(in_filepath, out_filepath))
