#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Refmac5.
'''
import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_core.tasks.refmac import refmac_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.project_database import sqlite_project_database


class RefmacDataset(object):
    def __init__(self,
                 pdb_path=None,
                 map_path=None,
                 resolution=None):
        self.pdb_path = None
        self.map_path = None
        self.resolution = None
        self.set_pdb_path(pdb_path)
        self.set_map_path(map_path)
        self.set_resolution(resolution)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = str(pdb_path)

    def set_map_path(self, map_path):
        if map_path is not None:
            self.map_path = str(map_path)

    def set_resolution(self, resolution):
        if resolution is not None:
            self.resolution = float(resolution)

    def validate(self):
        if [self.pdb_path, self.map_path, self.resolution].count(None) == 0:
            return True
        else:
            return False


class extRestraintDataset(object):
    def __init__(self,
                 restraints_file=None,
                 external_weight_scale=None,
                 external_weight_gmwt=None):
        self.restraints_file = None
        self.external_weight_scale = 10.0
        self.external_weight_gmwt = 0.02
        self.set_restraints_file(restraints_file)
        self.set_external_weight_scale(external_weight_scale)
        self.set_external_weight_gmwt(external_weight_gmwt)

    def set_restraints_file(self, restraints_file):
        if restraints_file is not None:
            self.restraints_file = str(restraints_file)

    def set_external_weight_scale(self, external_weight_scale):
        if external_weight_scale is not None:
            self.external_weight_scale = float(external_weight_scale)

    def set_external_weight_gmwt(self, external_weight_gmwt):
        if external_weight_gmwt is not None:
            self.external_weight_gmwt = float(external_weight_gmwt)

    def validate(self):
        if [self.restraints_file, self.external_weight_scale, self.external_weight_gmwt].count(None) == 0:
            return True
        else:
            return False


class RefmacDatasetInput(QtGui.QWidget):
    def __init__(self,
                 pdb_path=None,
                 map_path=None,
                 resolution=None):
        super(RefmacDatasetInput, self).__init__()
        self.dataset = RefmacDataset()

        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        self.pdb_label = QtGui.QLabel('PDB')
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        pdb_select = QtGui.QPushButton('Select')
        pdb_select.setFixedWidth(button_width)
        pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finised_pdb)
        pdb_box.addWidget(self.pdb_value)
        self.layout.addLayout(pdb_box)

        # Map
        map_box = QtGui.QHBoxLayout()
        self.map_label = QtGui.QLabel('Map')
        self.map_label.setFixedWidth(label_width)
        map_box.addWidget(self.map_label)
        map_select = QtGui.QPushButton('Select')
        map_select.setFixedWidth(button_width)
        map_select.clicked.connect(self.get_map_file)
        map_box.addWidget(map_select)
        self.map_value = QtGui.QLineEdit()
        map_box.addWidget(self.map_value)
        self.layout.addLayout(map_box)

        # Resolution
        res_box = QtGui.QHBoxLayout()
        self.res_label = QtGui.QLabel('Resolution')
        self.res_label.setFixedWidth(label_width)
        res_box.addWidget(self.res_label)
        self.res_value = QtGui.QDoubleSpinBox()
        self.res_value.setDecimals(2)
        self.res_value.setFixedWidth(button_width)
        self.res_value.setSpecialValueText('None')
        self.res_value.setSingleStep(0.1)
        self.res_value.valueChanged.connect(self.set_resolution)
        res_box.addWidget(self.res_value)
        res_box.setAlignment(self.res_value, QtCore.Qt.AlignLeft)

        # Remove
        remove_button = QtGui.QPushButton('Remove')
        remove_button.setFixedWidth(button_width)
        remove_button.setToolTip('Remove dataset')
        remove_button.clicked.connect(self.remove)
        res_box.addWidget(remove_button)
        res_box.setAlignment(res_box, QtCore.Qt.AlignRight)
        self.layout.addLayout(res_box)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)
        # Map
        self.set_map_file(path=map_path)
        # Resolution
        if resolution is None:
            resolution = 0
        self.res_value.setValue(resolution)
        self.set_resolution(resolution)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        self.set_pdb_file(path=path)

    def edit_finised_pdb(self):
        path = self.pdb_value.text()
        self.set_pdb_file(path=path)

    def get_map_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.mrc_ext)
        self.set_map_file(path=path)

    def edit_finised_map(self):
        path = self.map_value.text()
        self.set_pdb_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)

    def set_map_file(self, path):
        self.set_shading(self.map_label, shade=True)
        self.set_shading(self.map_value, shade=True)
        self.dataset.map_path = None
        if path is not None:
            if os.path.exists(path):
                self.map_value.setText(path)
                self.set_shading(self.map_label, shade=False)
                self.set_shading(self.map_value, shade=False)
                self.dataset.set_map_path(map_path=path)

    @QtCore.pyqtSlot(int)
    def set_resolution(self, value):
        if value < 0.1:
            self.set_shading(self.res_value, shade=True)
            self.set_shading(self.res_label, shade=True)
            self.dataset.resolution = None
        else:
            self.set_shading(self.res_value, shade=False)
            self.set_shading(self.res_label, shade=False)
            self.dataset.set_resolution(resolution=value)

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        self.deleteLater()


class RefmacDatasetsInput(QtGui.QWidget):
    def __init__(self,
                 pdbs_arg=None,
                 maps_arg=None,
                 resolutions_arg=None,
                 parent=None):
        super(RefmacDatasetsInput, self).__init__()
        self.parent = parent
        width = 100

        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Label
        ds_label = QtGui.QLabel('Input Datasets')
        self.layout.addWidget(ds_label)

        # Add dataset
        add_button = QtGui.QPushButton('Add')
        add_button.setFixedWidth(width)
        add_button.setToolTip('Add another dataset')
        add_button.clicked.connect(self.handle_add_button)
        self.layout.addWidget(add_button)

        # Set args
        pdbs = pdbs_arg()
        maps = maps_arg()
        resolutions = resolutions_arg()
        # Add black dataset
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n],
                    map_path=maps[n],
                    resolution=resolutions[n])
        else:
            self.add_dataset(
                pdb_path=pdbs,
                map_path=maps,
                resolution=resolutions)

    def handle_add_button(self):
        '''
        Handle dataset button; stop bool signal transfer
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    map_path=None,
                    resolution=None):
        dataset = RefmacDatasetInput(pdb_path=pdb_path,
                                     map_path=map_path,
                                     resolution=resolution)
        self.layout.addWidget(dataset)

    def validate_datasets(self):
        children = self.findChildren(RefmacDatasetInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires PDB, map and resolution)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self, pdbs_arg, maps_arg, resolutions_arg):
        children = self.findChildren(RefmacDatasetInput)
        pdbs = []
        maps = []
        resolutions = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
                maps.append(child.dataset.map_path)
                resolutions.append(child.dataset.resolution)
            pdbs_arg.value = pdbs
            maps_arg.value = maps
            resolutions_arg.value = resolutions
        else:
            child = children[0]
            pdbs_arg.value = child.dataset.pdb_path
            maps_arg.value = child.dataset.map_path
            resolutions_arg.value = child.dataset.resolution


class extRestraintInput(QtGui.QWidget):
    def __init__(self,
                 restraints_file=None,
                 external_weight_scale=None,
                 external_weight_gmwt=None):
        super(extRestraintInput, self).__init__()
        self.dataset = extRestraintDataset()

        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        # restraints_file
        restraints_file_box = QtGui.QHBoxLayout()
        self.restraints_file_label = QtGui.QLabel('Restraints file')
        self.restraints_file_label.setFixedWidth(label_width)
        restraints_file_box.addWidget(self.restraints_file_label)
        restraints_file_select = QtGui.QPushButton('Select')
        restraints_file_select.setFixedWidth(button_width)
        restraints_file_select.clicked.connect(self.get_restraints_file)
        restraints_file_box.addWidget(restraints_file_select)
        self.restraints_file_value = QtGui.QLineEdit()
        self.restraints_file_value.editingFinished.connect(self.edit_finished_restraints_file)
        restraints_file_box.addWidget(self.restraints_file_value)
        self.layout.addLayout(restraints_file_box)


        # external_weight_scale
        external_weight_scale_box = QtGui.QHBoxLayout()
        self.external_weight_scale_label = QtGui.QLabel('Weight')
        self.external_weight_scale_label.setFixedWidth(label_width)
        external_weight_scale_box.addWidget(self.external_weight_scale_label)
        self.external_weight_scale_value = QtGui.QDoubleSpinBox()
        self.external_weight_scale_value.setDecimals(2)
        self.external_weight_scale_value.setFixedWidth(button_width)
        self.external_weight_scale_value.setValue(10.0)
        self.external_weight_scale_value.setSingleStep(0.1)
        self.external_weight_scale_value.valueChanged.connect(self.set_external_weight_scale)
        external_weight_scale_box.addWidget(self.external_weight_scale_value)
        external_weight_scale_box.setAlignment(self.external_weight_scale_value, QtCore.Qt.AlignLeft)
        self.layout.addLayout(external_weight_scale_box)


        # external_weight_gmwt
        external_weight_gmwt_box = QtGui.QHBoxLayout()
        self.external_weight_gmwt_label = QtGui.QLabel('GMWT')
        self.external_weight_gmwt_label.setFixedWidth(label_width)
        external_weight_gmwt_box.addWidget(self.external_weight_gmwt_label)
        self.external_weight_gmwt_value = QtGui.QDoubleSpinBox()
        self.external_weight_gmwt_value.setDecimals(2)
        self.external_weight_gmwt_value.setFixedWidth(button_width)
        self.external_weight_gmwt_value.setValue(0.02)
        self.external_weight_gmwt_value.setSingleStep(0.01)
        self.external_weight_gmwt_value.valueChanged.connect(self.set_external_weight_gmwt)
        external_weight_gmwt_box.addWidget(self.external_weight_gmwt_value)
        external_weight_gmwt_box.setAlignment(self.external_weight_gmwt_value, QtCore.Qt.AlignLeft)


        # Remove
        remove_button = QtGui.QPushButton('Remove')
        remove_button.setFixedWidth(button_width)
        remove_button.setToolTip('Remove restraints set')
        remove_button.clicked.connect(self.remove)
        external_weight_gmwt_box.addWidget(remove_button)
        external_weight_gmwt_box.setAlignment(external_weight_gmwt_box, QtCore.Qt.AlignRight)
        self.layout.addLayout(external_weight_gmwt_box)

        # Set initial values
        # restraints_file
        self.set_restraints_file(path=restraints_file)
        # external_weight_scale
        self.set_external_weight_scale(external_weight_scale)
        # external_weight_gmwt
        self.set_external_weight_gmwt(external_weight_gmwt)

    def get_restraints_file(self):
        path = self.get_file_from_browser(file_types='')
        self.set_restraints_file(path=path)

    def edit_finished_restraints_file(self):
        path = self.restraints_file_value.text()
        self.set_restraints_file(path=path)

    def set_restraints_file(self, path):
        self.set_shading(self.restraints_file_label, shade=True)
        self.set_shading(self.restraints_file_value, shade=True)
        self.dataset.restraints_file_path = None
        if path is not None:
            if os.path.exists(path):
                self.restraints_file_value.setText(path)
                self.set_shading(self.restraints_file_label, shade=False)
                self.set_shading(self.restraints_file_value, shade=False)
                self.dataset.set_restraints_file(path)

    @QtCore.pyqtSlot(int)
    def set_external_weight_scale(self, value):
        self.set_shading(self.external_weight_scale_value, shade=False)
        self.set_shading(self.external_weight_scale_label, shade=False)
        self.dataset.set_external_weight_scale(value)

    def set_external_weight_gmwt(self, value):
        self.set_shading(self.external_weight_gmwt_value, shade=False)
        self.set_shading(self.external_weight_gmwt_label, shade=False)
        self.dataset.set_external_weight_gmwt(value)


    def get_file_from_browser(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        self.deleteLater()


class extRestraintsInput(QtGui.QWidget):
    def __init__(self,
                 restraints_files_arg=None,
                 external_weight_scales_arg=None,
                 external_weight_gmwts_arg=None,
                 parent=None):
        super(extRestraintsInput, self).__init__()
        self.parent = parent
        width = 100

        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        # Label
        ds_label = QtGui.QLabel('Input External restraints')
        self.layout.addWidget(ds_label)

        # Add dataset
        add_button = QtGui.QPushButton('Add')
        add_button.setFixedWidth(width)
        add_button.setToolTip('Add another dataset')
        add_button.clicked.connect(self.handle_add_button)
        self.layout.addWidget(add_button)

        # Set args
        restraints_files = restraints_files_arg()
        external_weight_scales = external_weight_scales_arg()
        external_weight_gmwts = external_weight_gmwts_arg()
        # Add black dataset
        if isinstance(restraints_files, list):
            for n in xrange(len(restraints_files)):
                self.add_dataset(
                    restraints_files=restraints_files[n],
                    external_weight_scales=external_weight_scales[n],
                    external_weight_gmwts=external_weight_gmwts[n])
        else:
            self.add_dataset(
                restraints_files=restraints_files,
                external_weight_scales=external_weight_scales,
                external_weight_gmwts=external_weight_gmwts)

    def handle_add_button(self):
        '''
        Handle dataset button; stop bool signal transfer
        '''
        self.add_dataset()

    def add_dataset(self,
                    restraints_files=None,
                    external_weight_scales=None,
                    external_weight_gmwts=None):
        dataset = extRestraintInput(restraints_file=restraints_files,
                                     external_weight_scale=external_weight_scales,
                                     external_weight_gmwt=external_weight_gmwts)
        self.layout.addWidget(dataset)

    def validate_datasets(self):
        children = self.findChildren(extRestraintInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires external restraints file, weight and GMWT)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self, restraints_files_arg, external_weight_scales_arg, external_weight_gmwts_arg):
        children = self.findChildren(extRestraintInput)
        restraints_files = []
        external_weight_scales = []
        external_weight_gmwts = []
        for child in children:
            restraints_files.append(child.dataset.restraints_file)
            external_weight_scales.append(child.dataset.external_weight_scale)
            external_weight_gmwts.append(child.dataset.external_weight_gmwt)
        restraints_files_arg.value = restraints_files
        external_weight_scales_arg.value = external_weight_scales
        external_weight_gmwts_arg.value = external_weight_gmwts


class Refmac5Window(window_utils.CCPEMTaskWindow):
    '''
    Refmac window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(Refmac5Window, self).__init__(task=task,
                                            parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Set dc mode
        self.dc_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='dc_mode',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.dc_input)
        self.dc_input.value_line.currentIndexChanged.connect(self.set_dc_mode)

        # XXX Oleg option for dc prototyping
        if not self.args.dc_proto():
            self.dc_input.hide()

        # Find in map (i.e. run Molrep)
        self.find_in_map_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='find_in_map',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.find_in_map_input)

        # Refinement options frame
        refinement_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Refinement options',
            button_tooltip='Specify refinement parameters')
        self.args_widget.args_layout.addLayout(refinement_options_frame)

        # -> Ncycle
        ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle',
            args=self.args)
        refinement_options_frame.add_extension_widget(ncycle_input)

        # -> LIBG restraints for nucleic acids
        self.libg_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='libg',
            args=self.args)
        refinement_options_frame.add_extension_widget(self.libg_input)
        self.libg_input.value_line.currentIndexChanged.connect(self.set_libg_selection)

        # -> Keyword entry for libg nucleotide range selection
        self.libg_selection_input = window_utils.KeywordArgInput(
            parent=self,
            arg_name='libg_selection',
            args=self.args,
            label='Nucleotide range')
        refinement_options_frame.add_extension_widget(self.libg_selection_input)
        self.set_libg_selection()

        # -> Weight auto
        self.weight_auto_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='weight_auto',
            args=self.args)
        refinement_options_frame.add_extension_widget(self.weight_auto_input)
        self.weight_auto_input.value_line.stateChanged.connect(self.set_weight_auto)

        # -> Weight
        self.weight_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='weight',
            args=self.args,
            decimals = 5)
        refinement_options_frame.add_extension_widget(self.weight_input)
        self.set_weight_auto()

        # -> Map sharpen
        map_sharpen_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=-999,
            arg_name='map_sharpen',
            args=self.args)
        refinement_options_frame.add_extension_widget(map_sharpen_input)

        # -> Jelly body
        jelly_body_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='jelly_body',
            args=self.args)
        refinement_options_frame.add_extension_widget(jelly_body_input)

        # -> Hydrogens
        hydrogens_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='hydrogens',
            args=self.args)
        refinement_options_frame.add_extension_widget(hydrogens_input)

        # -> Input cig
        lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        refinement_options_frame.add_extension_widget(lib_input)


        # B-factor refinement
        bfactor_frame = window_utils.CCPEMExtensionFrame(
            button_name='Edit input model',
            button_tooltip='Set b-factor options')
        refinement_options_frame.add_extension_frame(bfactor_frame)

        # ->-> Set bfactor on
        bfactor_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='set_bfactor',
            args=self.args)

        bfactor_on = window_utils.CheckArgInput(
            parent=self,
            arg_name='set_bfactor_on',
            dependants=[bfactor_input],
            args=self.args)
        bfactor_frame.add_extension_widget(bfactor_on)

        # ->-> Set bfactor value
        bfactor_frame.add_extension_widget(bfactor_input)

        # -> Local refinement options
        local_refinement_frame = window_utils.CCPEMExtensionFrame(
            button_name='Local refinement options',
            button_tooltip='Refine around radius of supplied molecule only')
        refinement_options_frame.add_extension_frame(local_refinement_frame)

        # ->-> Local refinement on
        mask_radius_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='mask_radius',
            args=self.args,
            required=False,
            label='Mask radius')
        self.local_refinement_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='local_refinement_on',
            dependants=[mask_radius_input],
            args=self.args)
        local_refinement_frame.add_extension_widget(
            self.local_refinement_input)

        # ->-> Mask Radius
        local_refinement_frame.add_extension_widget(mask_radius_input)

        # External restraints frame
        self.external_restraints_frame = window_utils.CCPEMExtensionFrame(
            button_name='External restraints',
            button_tooltip='Set external restraints')
        self.args_widget.args_layout.addLayout(self.external_restraints_frame)

        # -> Use external restraints
#        input_prosmart = window_utils.FileArgInput(
#            parent=self,
#            arg_name='input_restraints_path',
#            args=self.args)

        # Files with restraints
        self.restraints =  extRestraintsInput(
            restraints_files_arg =self.args.restraints_files,
            external_weight_scales_arg = self.args.external_weight_scales,
            external_weight_gmwts_arg = self.args.external_weight_gmwts,
            parent=self)
        self.args_widget.args_layout.addWidget(self.restraints)


        self.restraints_button = window_utils.CheckArgInput(
            parent=self,
            arg_name='external_restraints_on',
            dependants=[self.restraints],
            args=self.args)
        self.external_restraints_frame.add_extension_widget(self.restraints_button)

        # -> ProSMART restraints
        self.external_restraints_frame.add_extension_widget(self.restraints)

        # -> External restraints weights
        for arg in ['external_weight_dmx']:
            restraints_menu = window_utils.NumberArgInput(
                parent=self,
                arg_name=arg,
                args=self.args)
            self.external_restraints_frame.add_extension_widget(restraints_menu)
        # -> Main chain only option
        input_main_chain = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='external_main_only',
            args=self.args)
        self.external_restraints_frame.add_extension_widget(input_main_chain)

        # Validation frame
        self.validation_frame = window_utils.CCPEMExtensionFrame(
            button_name='Validate options',
            button_tooltip='Show validation options')
        self.args_widget.args_layout.addLayout(self.validation_frame)

        self.half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            required=True)

        self.half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            required=True)

        # -> Validation on
        self.validation_button = window_utils.CheckArgInput(
            parent=self,
            arg_name='validate_on',
            dependants=[self.half_map_1_input, self.half_map_2_input],
            args=self.args)
        self.validation_frame.add_extension_widget(self.validation_button)

        # -> Half map 1
        self.validation_frame.add_extension_widget(self.half_map_1_input)

        # -> Half map 2
        self.validation_frame.add_extension_widget(self.half_map_2_input)

        # -> Reference map
        self.reference_map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='reference_map',
            args=self.args,
            label='Reference map',
            tooltip_text='Optional: for map sharpening',
            required=False)
        if self.args.reference_map() is None:
            input_map = self.args.input_map()
            if input_map is not None:
                if isinstance(input_map, list):
                    input_map = input_map[0]
                self.reference_map_input.set_arg_value(input_map)
        self.validation_frame.add_extension_widget(self.reference_map_input)

        # Keyword frame
        keyword_frame = window_utils.CCPEMExtensionFrame(
            button_name='Keywords',
            button_tooltip='Optional user-supplied keywords')
        self.args_widget.args_layout.addLayout(keyword_frame)

        # -> Keyword entry
        self.keyword_entry_new = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args,
            label='Keywords')
        keyword_frame.add_extension_widget(self.keyword_entry_new)

        # Change map / pdb / resolution input depending on DC mode
        self.set_dc_mode()

    def set_reference_map(self):
        '''
        Reference map should automatically pick up input structure by default
        '''
        self.reference_map_input.set_arg_value(self.map_input.action.value)

    def set_weight_auto(self):
        if self.args.weight_auto.value:
            self.weight_input.hide()
        else:
            self.weight_input.show()

    def set_libg_selection(self):
        if self.args.libg.value:
            self.libg_selection_input.show()
        else:
            self.libg_selection_input.hide()

    def set_dc_mode(self):
        if self.args.dc_mode():
            if hasattr(self, 'pdb_input'):
                self.pdb_input.deleteLater()
            if hasattr(self, 'map_input'):
                self.map_input.deleteLater()
            if hasattr(self, 'resolution_input'):
                self.resolution_input.deleteLater()

            # Set map, pdb and resolution datasets
            # Input datasets
            self.datasets_input = RefmacDatasetsInput(
                pdbs_arg=self.args.start_pdb,
                maps_arg=self.args.input_map,
                resolutions_arg=self.args.resolution,
                parent=self)
            self.args_widget.args_layout.insertWidget(3, self.datasets_input)

###
            self.use_cluster = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='use_cluster',
                args=self.args)
            self.args_widget.args_layout.insertWidget(4, self.use_cluster)

            self.numberCPUsOrNodes = window_utils.NumberArgInput(
                parent=self,
                arg_name='numberCPUsOrNodes',
                args=self.args)
            self.args_widget.args_layout.insertWidget(5, self.numberCPUsOrNodes)
###



            # Resolution cutoff for Refmac DC
#            self.cutoff_input = window_utils.NumberArgInput(
#                parent=self,
#                arg_name='cutoff_res',
#                args=self.args,
#                required=True)
#            self.args_widget.args_layout.insertWidget(4, self.cutoff_input)

###
            self.find_in_map_input.hide()
            self.reference_map_input.hide()
            self.external_restraints_frame.hide()
            self.validation_frame.hide()
###
        else:
            if hasattr(self, 'datasets_input'):
                self.datasets_input.deleteLater()
            if hasattr(self, 'use_cluster'):
                self.use_cluster.deleteLater()
            if hasattr(self, 'numberCPUsOrNodes'):
                self.numberCPUsOrNodes.deleteLater()


#            if hasattr(self, 'cutoff_input'):
#                self.cutoff_input.deleteLater()
            # Input pdb
            self.pdb_input = window_utils.FileArgInput(
                parent=self,
                arg_name='start_pdb',
                args=self.args,
                label='Input PDB',
                file_types=ccpem_file_types.pdb_ext,
                required=True)
            self.args_widget.args_layout.insertWidget(3, self.pdb_input)
            # Input map
            self.map_input = window_utils.FileArgInput(
                parent=self,
                arg_name='input_map',
                args=self.args,
                label='Input map',
                file_types=ccpem_file_types.mrc_ext,
                required=True)
            self.map_input.value_line.textChanged.connect(
                self.set_reference_map)
            self.args_widget.args_layout.insertWidget(4, self.map_input)
            # Resolution
            self.resolution_input = window_utils.NumberArgInput(
                    parent=self,
                    arg_name='resolution',
                    args=self.args,
                    required=True)
            self.args_widget.args_layout.insertWidget(5, self.resolution_input)
###
            self.find_in_map_input.show()
            self.reference_map_input.show()
            self.external_restraints_frame.button.show()
            self.validation_frame.button.show()
###

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.raise_()

    def handle_toolbar_run(self):
        '''
        Over ride from baseclass to allow validation of multiple datasets
        '''
        if self.database_path is not None:
            db = sqlite_project_database.CCPEMDatabase(
                database_path=self.database_path)
            job_id = db.insert_new_job(program=self.task.task_info.name,
                                       job_location='None')
        else:
            job_id=None

        if self.check_input():
            if self.args.dc_mode():
                # Valdited multiple datasets
                if not self.datasets_input.validate_datasets():
                    return
                else :
                    self.datasets_input.set_args(
                        pdbs_arg=self.args.start_pdb,
                        maps_arg=self.args.input_map,
                        resolutions_arg=self.args.resolution)

            if self.args.external_restraints_on.value:
                if not self.restraints.validate_datasets():
                    return
                else:
                    self.restraints.set_args(
                      restraints_files_arg=self.args.restraints_files,
                      external_weight_scales_arg=self.args.external_weight_scales,
                      external_weight_gmwts_arg=self.args.external_weight_gmwts)

            self.run_detached_process(job_id=job_id)


    def set_on_job_running_custom(self):
        if not self.args.dc_mode():
            # Set inputs for launcher
            self.launcher.add_file(
                arg_name='input_map',
                file_type='map',
                description=self.args.input_map.help,
                selected=True)
            self.launcher.add_file(
                arg_name='start_pdb',
                file_type='pdb',
                description=self.args.start_pdb.help,
                selected=True)
#            self.launcher.add_file(
#                arg_name='input_restraints_path',
#                file_type='standard',
#                description=self.args.input_restraints_path.help,
#                selected=True)
        self.launcher_dock.show()


    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        # Refined model
        if self.task.args.local_refinement_on.value:
            if hasattr(self.task, 'process_shift_back'):
                self.launcher.add_file(
                    path=self.task.process_shift_back.pdbout_path,
                    file_type='pdb',
                    description='Refined PDB file',
                    selected=True)
        else:
            if hasattr(self.task, 'process_refine'):
                self.launcher.add_file(
                    path=self.task.process_refine.pdbout_path,
                    file_type='pdb',
                    description='Refined PDB file',
                    selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        # Set results viewer
        self.set_rv_ui()


def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=refmac_task.Refmac,
        window=Refmac5Window)

if __name__ == '__main__':
    main()
