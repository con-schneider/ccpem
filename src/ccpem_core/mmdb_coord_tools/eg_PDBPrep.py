#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core.mmdb_coord_tools.coord_tools import manager
from ccpem_core import test_data
from ccpem_core.map_io import numpy_mrc_io


class Test(unittest.TestCase):
    '''
    Unit test for mmdb_coord_tools.
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_PDB_prep(self):
        # Load model
        pdb_path = os.path.join(self.test_data,
                                'pdb/1AKE_cha_molrep_ns.pdb')
        assert os.path.exists(pdb_path)
        model = manager(pdb_path)

        # Load map get unit cell info
        print self.test_data
        map_path = os.path.join(self.test_data,
                                'map/mrc/1ake_10A_molrep.mrc')
        assert os.path.exists(map_path)
        header, data = numpy_mrc_io.read_mrc(map_path)
        print header['ispg']
        print header['alpha']
        print header['beta']
        print header['gamma']
        print header['mx']
        print header['my']
        print header['mz']

        # Remove solvent
        model.delete_solvent()

        # Remove alt locs
        model.delete_alternative_locations()

        # Set space group
        pdb_out_path = os.path.join(self.test_output,
                                    'out.pdb')
        model.write_pdb_file(pdb_out_path)
        print 'Done'

if __name__ == '__main__':
    unittest.main()
