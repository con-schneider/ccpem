#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for DockEM program. For advanced form from the terminal
use as python shapem_em_window.py -adv
'''

import sys
import os
import shutil
from PyQt4 import QtGui, QtCore
import ccpem_core.gui_ext as window_utils
from ccpem_core.mrc_map_io_clipper import read_mrc_header
import ccpem_core.gui_ext as text_browser
import shapem
import picture
import ccpem_core.ccpem_utils as ccpem_utils
import ccpem_core.gui_ext as icons
from ccpem_utils.spherical_angles_distribution import angle_distn
from ccpem_utils import which


class ShapemMainWindow(window_utils.CCPEMTaskWindow):
    '''
    Inherits from CCPEMTaskWindow.
    '''
    def __init__(self,
                 parent=None,
                 database_path=None,
                 args=None,
                 job_manager=False):
        super(ShapemMainWindow, self).__init__(parent)
        self.parent = parent
        self.database_path = database_path
        self.args = args
        self.job_manager = job_manager
        self.set_program_info(prog_info=shapem.ProgramInfo())
        self.set_args()
        avail_geo = QtGui.QDesktopWidget().availableGeometry(self).size()
        self.resize(QtCore.QSize(avail_geo.width() * 0.35,
                                 avail_geo.height() * 0.7))
        self.set_second_dialog()
        self.analyzer_args()
        self.tb_run_button.setEnabled(False)

    def set_setup_ui(self):
        '''
        Setup UI for input arguments.
        '''
        self.setup_ui = QtGui.QWidget()
        self.setup_ui_vbox_layout = QtGui.QVBoxLayout()
        # Add program information
        self.prog_label = QtGui.QLabel()
        self.prog_label.setWordWrap(True)
        self.prog_label.setFrameStyle(QtGui.QFrame.StyledPanel |
                                      QtGui.QFrame.Sunken)
        self.setup_ui_vbox_layout.addWidget(self.prog_label)
        self.adv_button = QtGui.QCheckBox('Show me advanced options')
        self.setup_ui_vbox_layout.addWidget(self.adv_button)
        # Add args dialog
        self.args_widget = window_utils.CCPEMArgsDialog()
        self.setup_ui_vbox_layout.addWidget(self.args_widget)
        self.setup_ui.setLayout(self.setup_ui_vbox_layout)
        # Add second dialog
        self.second_dialog = window_utils.CCPEMArgsDialog()
        self.setup_ui_vbox_layout.addWidget(self.second_dialog)
        self.setup_ui.setLayout(self.setup_ui_vbox_layout)
        return self.setup_ui

    def set_args(self):
        '''
        Set input arguments: stage1
        '''
        if self.args is None:
            self.args = shapem.shapem_parser().generate_arguments()
        # Input target mrc
        self.targetmap_arg_input = window_utils.FileArgInput(
            parent=self,
            arg_name='targetmap',
            args=self.args,
            label='Target Map',
            tooltip_text='target molecular density of macromolecular complex',
            path_remember=True,
            file_type='MRC(*.mrc *.map *.ccp4)')
        self.args_widget.args_layout.addWidget(self.targetmap_arg_input)
        filename = self.args.targetmap.value
        # Update map header when target map uploaded
        self.targetmap_arg_input.input_file_edit.textChanged.connect(
            lambda: self.map_header.header_values(
                str(self.targetmap_arg_input.input_file_edit.text())))
        # Sets up names for filtered map
        self.targetmap_arg_input.input_file_edit.textChanged.connect(
            lambda: self.map_output_files(
                str(self.targetmap_arg_input.input_file_edit.text())))
        # Target Map header details
        self.header_button = QtGui.QCheckBox(
            'Show Target Map Header Information')
        self.header_button.setCheckable(True)
        self.header_button.setFixedWidth(300)
        self.args_widget.args_layout.addWidget(self.header_button)
        self.map_header = window_utils.HeaderInfo(filename)
        self.HeaderFrame = self.map_header.HeaderFrame
        self.line = self.map_header.line
        self.args_widget.args_layout.addWidget(self.HeaderFrame)
        self.args_widget.args_layout.addWidget(self.line)
        self.args_widget.args_layout.addStretch()
        self.header_button.toggled.connect(self.HeaderFrame.setVisible)

        # Input search density and its header
        self.searchmap_arg_input = window_utils.FileArgInput(
            parent=self,
            arg_name='searchmap',
            args=self.args,
            label='Search Map',
            tooltip_text='search object/shape to be searched in target map',
            path_remember=True,
            file_type='MRC(*.mrc *.map *.ccp4)')
        self.args_widget.args_layout.addWidget(self.searchmap_arg_input)
        sfilename = self.args.searchmap.value

        # Search Map header details
        self.sheader_button = QtGui.QCheckBox(
            'Show Search Map Header Information')
        self.sheader_button.setCheckable(True)
        self.sheader_button.setFixedWidth(300)
        self.args_widget.args_layout.addWidget(self.sheader_button)
        self.smap_header = window_utils.HeaderInfo(sfilename)
        self.sHeaderFrame = self.smap_header.HeaderFrame
        self.sLine = self.smap_header.line
        self.args_widget.args_layout.addWidget(self.sHeaderFrame)
        self.args_widget.args_layout.addWidget(self.sLine)
        self.args_widget.args_layout.addStretch()
        self.sheader_button.toggled.connect(self.sHeaderFrame.setVisible)

        # Mask file upload option
        self.mask_up_label = QtGui.QLabel('Do you have mask file to upload?')
        self.args_widget.args_layout.addWidget(self.mask_up_label)
        self.yes_bt = QtGui.QRadioButton('Yes')
        self.no_bt = QtGui.QRadioButton('No')
        self.yes_bt.setAutoExclusive(True)
        self.no_bt.setAutoExclusive(True)
        self.no_bt.setChecked(True)
        self.args_widget.args_layout.addWidget(self.yes_bt)
        self.args_widget.args_layout.addWidget(self.no_bt)
        self.yes_bt.toggled.connect(self.yes_clicked)
        self.no_bt.toggled.connect(self.no_clicked)
        self.MaskC = True

        # Mask file
        self.mask_uploaded = window_utils.FileArgInput(
            parent=self,
            arg_name='smap_mask_up',
            args=self.args,
            label='Search Mask',
            tooltip_text='',
            path_remember=True,
            file_type='MRC(*.mrc *.map *.ccp4)')
        self.args_widget.args_layout.addWidget(self.mask_uploaded)
        self.mask_uploaded.setVisible(False)
        self.mask_uploaded.input_file_edit.textChanged.connect(self.maskfile)

        # Sets up names for filtered map, search object and mask filenames
        self.searchmap_arg_input.input_file_edit.textChanged.connect(
            lambda: self.search_output_files(
                str(self.searchmap_arg_input.input_file_edit.text())))
        self.searchmap_arg_input.input_file_edit.textChanged.connect(
            lambda: self.smap_header.header_values(
                str(self.searchmap_arg_input.input_file_edit.text())))

        # Input other args
        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setSpacing(0)
        self.map_res_input = window_utils.SimpleArgInput(
            parent=self,
            arg_name='targetmap_res',
            args=self.args,
            label='Target map Resolution',
            width=200)
        self.grid_layout.addWidget(self.map_res_input, 1, 0)
        self.map_res_input.input_edit.textChanged.connect(
            self.mask_radius_value)

        # Currently its value is set to same as target map resolution
        self.high_res_input = window_utils.SimpleArgInput(
            parent=self,
            arg_name='rhigh',
            args=self.args,
            label='Highest resolution',
            width=200,
            tooltip_text='low pass (high resolution cuttoff) filter')
        self.grid_layout.addWidget(self.high_res_input, 2, 1)
        self.high_res_input.setVisible(False)

        # Mask radius
        self.mask_radius = window_utils.SimpleArgInput(
            parent=self,
            arg_name= 'mask_radius',
            args=self.args,
            label='Mask radius (A)',
            width=200,
            tooltip_text='Radius of the sphere to convolute with')
        self.grid_layout.addWidget(self.mask_radius, 1, 1)
        self.mask_radius.setVisible(False)

        # Low resolution
        self.low_res_input = window_utils.SimpleArgInput(
            parent=self,
            arg_name='rlow',
            args=self.args,
            label='Lowest resolution',
            width=200,
            tooltip_text='high pass (low resolution cuttoff) filter')
        self.grid_layout.addWidget(self.low_res_input, 2, 0)
        label_prepare = QtGui.QLabel(
            'Please select the preliminary step(s) you wish to run')
        self.grid_layout.addWidget(label_prepare,4,0)
        self.mapfmap_btn = QtGui.QCheckBox('Filter target map')
        self.pdbf_btn = QtGui.QCheckBox('Filter search map')
        self.button_list = [self.mapfmap_btn, self.pdbf_btn]
        col = 0
        for every_btn in self.button_list:
            every_btn.setCheckable(True)
            every_btn.setFixedWidth(300)
            self.grid_layout.addWidget(every_btn, 5, col)
            col += 1
        self.pdbf_btn.setChecked(True)
        self.mapfmap_btn.setChecked(True)
        self.mapfmap_btn.toggled.connect(self.check_filter_target)
        self.pdbf_btn.toggled.connect(self.check_filter_search)
        self.TargetMap_filter = True
        self.SearchMap_filter = True
        self.prepare_button = window_utils.ButtonWithLabel(
            parent=self,
            label='Prepare input files',
            width=200,
            tooltip_text=(
                'Runs Mapfilter on target map and search object and creates '
                'mask for the search object'))
        self.grid_layout.addWidget(self.prepare_button, 6, 0)
        self.prepare_button.SimpleButton.setEnabled(False)
        self.prepare_button.SimpleButton.clicked.connect(
            self.run_check_detached_process)
        self.prepare_timer = QtCore.QTimer()
        self.prepare_timer.timeout.connect(self.check_processed_input_button)
        self.prepare_timer.timeout.connect(self.write_log)
        self.prepare_timer.start(1000)
        self.args_widget.args_layout.addLayout(self.grid_layout)

        # To store the path of current file before changing directory to
        # job_location
        self.gui_path = os.path.realpath(__file__)

    def set_second_dialog(self):
        '''
        Set input arguments: stage2
        '''
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setSpacing(2)

        # Input output name of the filtered map
        self.fftarget_map = window_utils.SimpleArgInput(
            parent=self,
            arg_name='FilteredMap',
            args=self.args,
            label='Filtered Target Map',
            width=200,
            tooltip_text=(
                'Fourier filter of target map, and set up the unit cell '
                'dimensions'))
        self.fftarget_map.input_edit.setFixedWidth(500)
        self.gridLayout.addWidget(self.fftarget_map)

        # Input output name of the filtered search object
        self.ffsearch_object = window_utils.SimpleArgInput(
            parent=self,
            arg_name='FilteredSmap',
            args=self.args,
            label='Filtered Search Object',
            width=200,
            tooltip_text=(
                'Fourier filter of search density at the same resolution as '
                'the map'))
        self.ffsearch_object.input_edit.setFixedWidth(500)
        self.gridLayout.addWidget(self.ffsearch_object)

        # PDB header button
        self.smapff_button = QtGui.QCheckBox(
            'Show filtered search object map header information')
        self.smapff_button.setCheckable(True)
        self.smapff_button.setFixedWidth(300)
        self.gridLayout.addWidget(self.smapff_button)
        self.smapff_gridlayout = QtGui.QGridLayout()
        self.smapffFrame = QtGui.QFrame()
        self.smapffFrame.setFrameStyle(QtGui.QFrame.Plain | QtGui.QFrame.Box)
        smapff_labels = ['Number of pixels (before filtering)',
                         'Number of pixels (after filtering)']
        nrow = 0
        for label in smapff_labels:
            self.smapff_gridlayout.addWidget(QtGui.QLabel(label), nrow, 0)
            nrow +=2
        self.smapffFrame.setLayout(self.smapff_gridlayout)
        self.gridLayout.addWidget(self.smapffFrame)
        self.smapffFrame.setVisible(False)
        self.smapff_button.toggled.connect(self.smapffFrame.setVisible)

        # Input output name of the filtered mask
        self.mask_search_object = window_utils.SimpleArgInput(
            parent=self,
            arg_name='smap_mask',
            args=self.args,
            label='Search Object Mask',
            width=200)
        self.mask_search_object.input_edit.setFixedWidth(500)
        self.gridLayout.addWidget(self.mask_search_object)

        # Add search parameters for DockEM 
        self.search_params_label = QtGui.QLabel('Search parameters')
        self.search_params_label_font = QtGui.QFont(12)
        self.search_params_label_font.setBold(True)
        self.search_params_label.setFont(self.search_params_label_font)
        self.search_params_label.setAlignment(QtCore.Qt.AlignLeft)
        self.gridLayout.addWidget(self.search_params_label)

        # Input angular sampling
        self.angular_sampling = window_utils.SimpleArgInput(
            parent=self,
            arg_name='angl_samp',
            args=self.args,
            label='Angular sampling (in degrees)',
            width=200,
            tooltip_text='same as used to create the angles file')
        self.gridLayout.addWidget(self.angular_sampling)
        self.angular_sampling.input_edit.editingFinished.connect(
            self.set_theta)

        # Input Mask Threshold
        self.mask_threshold = window_utils.SimpleArgInput(
            parent=self,
            arg_name='mask_threshold',
            args=self.args,
            label='Threshold for Mask',
            width=200,
            tooltip_text=(
                'isosurface that includes approximately the expected volume '
                'of the domain'))
        self.gridLayout.addWidget(self.mask_threshold)

        # Input Extent search
        self.origin_rotation = window_utils.SimpleArgInput(
            parent=self,
            arg_name='ori_rot',
            args=self.args,
            label='Origin of rotation',
            width=200,
            tooltip_text=(
                'Either at Center-of-mass of search object, or the box '
                'centre. Type C or B'))
        self.gridLayout.addWidget(self.origin_rotation)

        # Input theta step-advanced option
        self.theta_step = window_utils.SimpleArgInput(
            parent=self,
            arg_name='theta_step',
            args=self.args,
            label='Theta step (degrees)',
            width=200,
            tooltip_text=(
                'Lies between 0-180, used to generate angular distribution '
                'on sphere'))
        self.gridLayout.addWidget(self.theta_step)
        self.theta_step.setVisible(False)
        self.theta_step.input_edit.editingFinished.connect(self.set_angl_sampl)

        # Add full second dialog with all parameters to the interface
        self.second_dialog.args_layout.addLayout(self.gridLayout)

        # For advanced form from the terminal: python shapem_em_window.py -adv
        self.adv_button.stateChanged.connect(self.show_adv_options)
        if self.args.advanced_option.value is True:
            self.adv_button.setCheckState(2)

    def analyzer_args(self):
        if self.args is None:
            self.args = shapem.shapem_parser().generate_arguments()

        # Input number of solutions required
        self.num_hits = window_utils.SimpleArgInput(
            parent=self,
            arg_name='num_hits',
            args=self.args,
            label='Number of solutions',
            width=200,
            tooltip_text=(
                'Range of solutions to produce (input a number between '
                '1-10000)'))
        self.an_gridLayout.addWidget(self.num_hits,0,0)

        # Input peak radius
        self.peak_radius = window_utils.SimpleArgInput(
            parent=self,
            arg_name='peak_radius',
            args=self.args,
            label='Peak Radius',
            width=200,
            tooltip_text='only one solution within this radius will be chosen')
        self.an_gridLayout.addWidget(self.peak_radius, 3, 0, 1, 1)

        # Input solution range
        self.soln_range = window_utils.SimpleArgInput(
            parent=self,
            arg_name='soln_range',
            args=self.args,
            label='Number of Top solutions',
            width=200,
            tooltip_text=(
                'solution range (e.g. 1,10 for the top ten solutions)'))
        self.an_gridLayout.addWidget(self.soln_range, 3, 1, 1, 1)
        self.soln_range.input_edit.textChanged.connect(self.hits_display)
        self.an_gridLayout.setAlignment(QtCore.Qt.AlignTop)

        # Run button for analyser
        self.run_analyzer_button = QtGui.QPushButton('Re-run Analyzer')
        self.run_analyzer_button.setFixedWidth(200)
        self.run_analyzer_button.setStyleSheet('background-color: lightgreen')
        self.run_analyzer_button.clicked.connect(self.re_analyzer_button)

        # For top ten pdb solutions to view/save
        self.button_gridlayout = QtGui.QGridLayout()
        self.buttonFrame = QtGui.QFrame()
        self.buttonFrame.setFrameStyle(QtGui.QFrame.Plain | QtGui.QFrame.Box)
        self.buttonFrame.setLayout(self.button_gridlayout)
        self.hits_timer = QtCore.QTimer()
        self.hits_timer.timeout.connect(self.show_button_frame)
        self.hits_timer.start(1000)
        self.solutions_list_lbl = QtGui.QLabel(
            'Results: Top solutions to visualise in Chimera')
        self.button_gridlayout.addWidget(self.solutions_list_lbl)

        # Add the quick link buttons to visualize upto 10 solutions in Chimera
        self.hits_display()
        self.an_gridLayout.addWidget(self.buttonFrame, 6, 1)
        self.job_process = QtGui.QLabel('Please wait the job is running')
        label_font = self.job_process.font()
        label_font.setPointSize(20)
        self.job_process.setFont(label_font)
        self.job_process.setStyleSheet('background-color: red')
        self.analyzer_ui_vbox_layout.addLayout(self.an_gridLayout)
        self.buttonFrame.hide()

    def set_output_ui(self):
        '''
        Output UI, shows text browser of log file.
        '''
        self.output_ui = QtGui.QWidget()
        self.output_ui_vbox_layout = QtGui.QVBoxLayout()
        # Setup layout
        if self.job_manager is not None:
            output_file = self.job_manager.stdout_file
        else:
            output_file = None
        self.ouput_tb = text_browser.TextBrowser(filepath=output_file,
                                                 parent=self)
        self.ouput_tb.text_browser.setText(
            'Log file will be displayed once job is running')
        self.output_ui_vbox_layout.addWidget(self.ouput_tb)
        self.output_ui.setLayout(self.output_ui_vbox_layout)
        self.output_dock.setWidget(self.output_ui)

    def mask_radius_value(self):
        '''
        Upon input of target map resolution, sets the mask radius to use as 
        1.5*resolution
        '''
        mask_rad = 1.5 * self.args.targetmap_res.value
        self.mask_radius.input_edit.setText(str(mask_rad))
        print 'mask radius used:', mask_rad

    def show_adv_options(self):
        '''
        Sets up the advanced form of the GUI, when the advanced button is 
        checked of from the terminal script is used as 
            python dock_em_window.py -adv
        '''
        adv_widget_list = [self.high_res_input,
                           self.theta_step,
                           self.mask_radius]
        for widget in adv_widget_list:
            if self.adv_button.checkState() == 0:
                self.args.advanced_option.value = False
                widget.setVisible(False)
            if self.adv_button.checkState() == 2:
                widget.setVisible(True)
                self.args.advanced_option.value = True

    def set_analyzer_ui(self):
        '''
        Analyzer UI, to view the output of ShapEM more interactively.
        '''
        self.analyzer_ui = QtGui.QWidget()
        self.analyzer_ui_vbox_layout = QtGui.QVBoxLayout()
        if self.job_manager is not None:
            ccc_output_file = self.job_manager.stdout_file
        else:
            ccc_output_file = None
        self.analyzer_tb = picture.Picture(filepath=ccc_output_file,
                                           parent=self)
        self.analyzer_dock = QtGui.QDockWidget('ShapeEM Analyzer',
                                               self, QtCore.Qt.Widget)
        self.tabifyDockWidget(self.output_dock, self.analyzer_dock)
        self.an_gridLayout = QtGui.QGridLayout()
        self.analyzer_ui.setLayout(self.analyzer_ui_vbox_layout)
        self.analyzer_dock.setWidget(self.analyzer_ui)

    def map_output_files(self, uploaded_str):
        '''
        Sets the argument names for stage2 for target map outputs
        '''
        self.targetmap_arg_input.input_file_edit.setText(uploaded_str)
        outfile_name = os.path.splitext(os.path.basename(uploaded_str))[0]
        self.fftarget_map.input_edit.setFixedWidth(500)
        self.fftarget_map.input_edit.setText(str(outfile_name+'_ff.mrc'))
        try:
            self.args.grid_samp.value = \
                self.map_header.map_data.voxx_voxy_voxz[2]
        except AttributeError:
            pass
        self.args.FilteredMap.value = (str(outfile_name + '_ff.mrc'))

    def set_angl_sampl(self):
        '''
        Theta and angular sampling set to same. If user changes either value
        other value gets updated
        '''
        self.angular_sampling.input_edit.setText(
            str(self.args.theta_step.value))
        self.args.angl_samp.value = self.args.theta_step.value

    def set_theta(self):
        '''
        Theta and angular sampling set to same. If user changes either value
        other value gets updated
        '''
        self.theta_step.input_edit.setText(str(self.args.angl_samp.value))
        self.args.theta_step.value = self.args.angl_samp.value

    def search_output_files(self, uploaded_str):
        '''
        Sets the argument names for stage 2 for search object outputs
        '''
        self.searchmap_arg_input.input_file_edit.setText(uploaded_str)
        outfile_name = os.path.splitext(os.path.basename(uploaded_str))[0]
        self.ffsearch_object.input_edit.setFixedWidth(500)
        self.mask_search_object.input_edit.setFixedWidth(500)
        self.ffsearch_object.input_edit.setText(str(outfile_name+'_ff.mrc'))
        self.mask_search_object.input_edit.setText(
            str(outfile_name+'_mask.mrc'))
        self.args.FilteredSmap.value = (str(outfile_name + '_ff.mrc'))

    def maskfile(self):
        '''
        If mask file uploaded sets the name for the file
        '''
        uploaded_mask = str(self.mask_uploaded.input_file_edit.text())
        self.mask_data = read_mrc_header.MRCMapHeaderData(uploaded_mask)
        self.mask_search_object.input_edit.setText(
            str(os.path.split(uploaded_mask)[1]))

    def yes_clicked(self):
        '''
        Yes will remove MaskConvolute from job list
        '''
        self.mask_uploaded.setVisible(True)
        self.MaskC = False

    def no_clicked(self):
        '''
        Mask file will be generated using MaskConvolute
        '''
        self.mask_uploaded.setVisible(False)
        self.MaskC = True
        self.mask_uploaded.input_file_edit.setText('Optional input')
        search_obj = str(self.searchmap_arg_input.input_file_edit.text())
        self.mask_search_object.input_edit.setText(
            str(os.path.split(search_obj)[1]))

    def check_filter_target(self):
        '''
        Checks if to execute Mapfilter on target map and sets the output name
        '''
        if self.mapfmap_btn.isChecked():
            self.TargetMap_filter = True
            self.fftarget_map.input_edit.setDisabled(False)
        else:
            self.TargetMap_filter = False
            self.fftarget_map.input_edit.setDisabled(True)
            self.target_uploaded_str = str(
                self.targetmap_arg_input.input_file_edit.text())
            filter_map = os.path.basename(self.target_uploaded_str)
            self.fftarget_map.input_edit.setText(str(filter_map))

    def check_filter_search(self):
        '''
        Checks if to execute Mapfilter on search map and sets the output name
        '''
        if self.pdbf_btn.isChecked():
            self.SearchMap_filter = True
            self.ffsearch_object.input_edit.setDisabled(False)
        else:
            self.SearchMap_filter = False
            self.ffsearch_object.input_edit.setDisabled(True)
            self.search_uploaded_str = str(
                self.searchmap_arg_input.input_file_edit.text())
            filter_smap = os.path.basename(self.search_uploaded_str)
            self.ffsearch_object.input_edit.setText(str(filter_smap))

    def show_button_frame(self):
        '''
        Sets up the buttonframe containing the hit list and visualise option
        in chimera
        '''
        if os.path.exists('stdout_shapemb.txt'):
            self.buttonFrame.show()
            self.visualise_button.setEnabled(True)
            self.an_gridLayout.addWidget(self.analyzer_tb, 5, 0, 4, 1)
            self.an_gridLayout.addWidget(self.run_analyzer_button, 10, 1)
            self.job_process.setVisible(False)
            self.status_widget.set_finished()
            self.hits_timer.stop()

    def check_processed_input_button(self):
        '''
        If form is filled enables the prepare button
        '''
        if (self.args.targetmap.value is not 'MRC'
                and self.args.targetmap_res.value is not None
                and self.args.searchmap.value is not 'MRC'
                and self.args.rlow.value is not 'None'):
            self.prepare_button.SimpleButton.setStyleSheet(
                'background-color: lightgreen')
            self.prepare_button.SimpleButton.setEnabled(True)
            self.check_button = window_utils.ButtonWithLabel(
                parent=self,
                label='Check input files in Chimera',
                width=200,
                tooltip_text=(
                    'Visualise files in Chimera and set Mask threshold'))
            self.grid_layout.addWidget(self.check_button, 5, 4)
            self.check_button.SimpleButton.clicked.connect(self.show_chimera)
            self.tb_run_button.setEnabled(True)
            self.status_widget.set_ready()

    def show_chimera(self):
        '''
        launches chimera as a detached process to view input files
        and set mask threshold
        '''
        mask_level = str(os.path.split(self.gui_path)[0]) + '/chimera_mask.py'
        chimera_command = '%s %s %s %s' % (
            str(mask_level),
            str(self.args.smap_mask.value),
            str(self.args.FilteredMap.value),
            str(self.args.FilteredSmap.value))
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        self.chimera_bin_command = which('chimera')
        process = QtCore.QProcess()
        process.startDetached(self.chimera_bin_command, arg_list_chimera)

    def hits_display(self):
        '''
        Controls the number of button up to 10 as per user input to
        visualise solutions
        '''
        for i in reversed(range(self.button_gridlayout.count())):
            self.button_gridlayout.itemAt(i).widget().setParent(None)
        self.button_gridlayout.addWidget(self.solutions_list_lbl, 0, 0)
        num_sol = int(self.args.soln_range.value)
        hits_to_display = min(num_sol, 10)
        first_row = 1
        second_row = 1
        for item in xrange(1, hits_to_display+1):
            text = 'Hit' + str(item)
            self.hit_button = QtGui.QRadioButton(text)
            if item > 5:
                second_col = 1
                self.button_gridlayout.addWidget(self.hit_button,
                                                 second_row,
                                                 second_col)
                second_row += 1
            else:
                first_col = 0
                self.button_gridlayout.addWidget(self.hit_button,
                                                 first_row,
                                                 first_col)
            first_row += 1
            self.hit_button.setAutoExclusive(False)
        text_label = (
            'Note: This is a quick link to view upto top-ten solutions, '
            'beyond ten please refer to the job folder')

        self.note = QtGui.QLabel(text_label)
        self.font = QtGui.QFont('Sans Serif', 7)
        self.note.setFont(self.font)
        self.button_gridlayout.addWidget(self.note, 6, 0)
        self.visualise_button = QtGui.QPushButton('Visualize')
        self.visualise_button.setEnabled(False)
        self.button_gridlayout.addWidget(self.visualise_button, 6, 1)
        self.visualise_button.clicked.connect(self.chimera_view)

    def chimera_view(self):
        '''
        View upto top-ten solutions in Chimera loads target map, sets its
        transparency and contour and open the selected solutions
        '''
        map_ff = self.args.FilteredMap.value
        curr_path1 = self.gui_path
        chimera_prog = str(os.path.dirname(curr_path1)) + '/open_chimera.py'
        list_pdb_sols = []
        for i in range(self.button_gridlayout.count()):
            widget = self.button_gridlayout.itemAt(i).widget()
            if (widget != 0) and (type(widget) is QtGui.QRadioButton):
                if widget.isChecked():
                    if len(str(i)) == 1:
                        soln = '0' + str(i)
                    else:
                        soln = str(i)
                    pdb_sol = 'maphit-run123-0' + soln + '.mrc'
                    list_pdb_sols.append(pdb_sol)
        ids_checked = '_'.join(list_pdb_sols)
        chimera_command = '%s %s %s' % (
            chimera_prog,
            map_ff,
            ids_checked)
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        self.chimera_bin = which('chimera')
        process = QtCore.QProcess()
        process.startDetached(self.chimera_bin, arg_list_chimera)

    def re_analyzer_button(self):
        '''
        Re-Runs shapema and shapemb and generate new ccc plot and hits
        '''
        # Remove the solutions and old stdin files
        self.status_widget.set_running()
        curr_dir = os.getcwd()
        all_files = os.listdir(curr_dir)
        for files in all_files:
            if files.startswith('maphit'):
                os.remove(files)
        list_remove = ['stdin_shapemb.txt',
                       'stdin_shapema.txt',
                       'stdout_shapema.txt',
                       'stdout_shapemb.txt',
                       'searchdocIP123.dkm']
        for dockem_file in list_remove:
            if os.path.exists(dockem_file):
                os.remove(dockem_file)
        #
        run_task = shapem.RunTask_Analyzer(
            job_id=None,
            stdin_file_shapema=os.path.join(self.job_location,
                                            'stdin_shapema.txt'),
            stdin_file_shapemb=os.path.join(self.job_location,
                                            'stdin_shapemb.txt'),
            args=self.args,
            parent=self)
        self.status_widget.set_finished()
        self.analyzer_tb.set_filepath(
            filepath=os.path.join(self.job_location,
                                  'searchdocIP123.dkm'),
            job_location=self.job_location,
            job_id=self.job_id)
        self.filePath = self.job_location

    def run_check_detached_process(self):
        '''
        Launch detached job upto MaskConvolute (includes Mapfilterand
        MaskConvolute)
        '''
        self.status_widget.set_running()
        os.chdir(os.path.dirname(self.gui_path))
        #Database paths: Commented out for now 
#         if self.parent is not None:
#             database_path = self.parent.database_path
#             job_id, job_location = job_wrapper.create_job_database_entry(
#                 database_path=self.parent.database_path,
#                 project_location=self.parent.project_location,
#                 program=tasks.shapem.shapem.ProgramInfo().program_id,
#                 job_location=None)
#         else:
        job_id = None
        job_location = core_utils.check_directory_and_make(
            os.path.join(os.getcwd(),
                         'shapem_job_1'),
            verbose=True,
            auto_suffix=True)
        self.job_location = job_location
        self.job_id = job_id
        database_path = None
        os.chdir(job_location)

        if self.TargetMap_filter is False:
            shutil.copy(self.args.targetmap.value, self.job_location)
        if self.SearchMap_filter is False:
            shutil.copy(self.args.searchmap.value, self.job_location)
        if self.MaskC is False:
            uploaded_mask = str(self.mask_uploaded.input_file_edit.text())
            shutil.copy(uploaded_mask,self.job_location)

        self.job_num = os.path.splitext(os.path.basename(self.job_location))[0]
        self.setWindowTitle('CCP-EM | ShapeEM ' + self.job_num)

        run_task = shapem.RunTask_Check(
            job_id=job_id,
            job_location=job_location,
            database_path=database_path,
            stdin_file_mapfilter=os.path.join(job_location,
                                              'stdin_mapfilter.txt'),
            stdin_file_smapff=os.path.join(job_location,
                                           'stdin_smapff.txt'),
            stdin_file_maskcon=os.path.join(job_location,
                                            'stdin_maskcon.txt'),
            args=self.args,
            parent=self,
            mapf=self.TargetMap_filter,
            pdbf=self.SearchMap_filter,
            maskc=self.MaskC)
        self.ouput_tb.set_filepath(os.path.join(self.job_location,
                                                'shapem.log'))
        self.filePath = job_location

    def write_log(self):
        '''
        writes output tab display file
        '''
        if (os.path.exists('stdout_mapfmap.txt')
                or os.path.exists('stdout_smapff.txt')
                or os.path.exists('stdout_maskc.txt')):
            try:
                shapem_log = open(os.path.join(self.job_location,
                                               'shapem.log'),
                                  'w')
                mapf = open(os.path.join(self.job_location,
                                         'stdout_mapfmap.txt'),
                            'rb').readlines()
                out_log = '*******MapFilter on Target Map*******\n'
                mapf_out1 = mapf[2:17]
                mapf_out2 = mapf[-4:-1]
                out_log += ' '.join(mapf_out1)
                out_log += ' '.join(mapf_out2)
                shapem_log.write(out_log)
                shapem_log.close()
            except:
                pass
            try:
                shapem_log = open(os.path.join(self.job_location,
                                               'shapem.log'),
                                  'w')
                out_log1 = '*******MapFilter on Search object*******\n'
                smapff = open(os.path.join(self.job_location,
                                           'stdout_smapff.txt'),
                              'rb').readlines()
                smapff_out1 = smapff[2:17]
                smapff_out2 = smapff[-4:-1]
                out_log1 += ' '.join(smapff_out1)
                out_log1 += ' '.join(smapff_out2)
                shapem_log.write(out_log1)
                shapem_log.close()
            except:
                pass
            try:
                shapem_log = open(os.path.join(self.job_location,
                                               'shapem.log'),
                                  'w')
                out_log2 = '*******MaskConvolute on Search object*******\n'
                maskc = open(os.path.join(self.job_location,
                                          'stdout_maskc.txt'),
                             'rb').readlines()
                maskc_out1 = maskc[2:17]
                maskc_out2 = maskc[-4:-1]
                out_log2 += ' '.join(maskc_out1)
                out_log2 += ' '.join(maskc_out2)
                shapem_log.write(out_log2)
                shapem_log.close()
            except:
                pass
            #self.ffsobmap_header()
            self.prepare_timer.stop()

    def ffsobmap_header(self):
        '''
        Parses the header for the filtered search object map file
        '''
        self.pdbmap = os.path.join(self.job_location,
                                   self.args.searchmap.value)
        self.ffpdbmap = os.path.join(self.job_location,
                                     self.args.FilteredSmap.value)
        sobj_map_data = read_mrc_header.MRCMapHeaderData(self.ffpdbmap)
        sobjff_map_data = read_mrc_header.MRCMapHeaderData(self.ffpdbmap)
        smapff_values = [str(sobj_map_data.mx_my_mz[0]) + ' ,' +
                         str(sobj_map_data.mx_my_mz[1]) + ' ,' +
                         str(sobj_map_data.mx_my_mz[2]),
                         str(sobjff_map_data.mx_my_mz[0]) + ' ,' +
                         str(sobjff_map_data.mx_my_mz[1]) + ' ,' +
                         str(sobjff_map_data.mx_my_mz[2])]
        nrow = 0
        for value in smapff_values:
            self.smapff_gridlayout.addWidget(QtGui.QLabel(value), nrow, 1)
            nrow += 2

    def run_detached_process(self):
        '''
        Launch detached job.
        '''
        self.status_widget.set_running()
        self.an_gridLayout.addWidget(self.job_process, 7, 0)
        run_task = shapem.RunTask(
            job_id=None,
            stdin_file_shapem=os.path.join(self.job_location,
                                           'stdin_shapem.txt'),
            stdin_file_shapema=os.path.join(self.job_location,
                                            'stdin_shapema.txt'),
            stdin_file_shapemb=os.path.join(self.job_location,
                                            'stdin_shapemb.txt'),
            args=self.args,
            parent=self)
        self.analyzer_dock.raise_()
        self.ouput_tb.set_filepath(self.job_location+'/stdout_shapem.txt')
        self.args.theta_step.value = self.args.angl_samp.value

        # Makes the angles file in SPIDER format
        angle_distn(theta_step=self.args.theta_step.value,
                    output_path=self.job_location)
        self.analyzer_tb.set_filepath(
            filepath=self.job_location+'/searchdocIP123.dkm',
            job_location=self.job_location,
            job_id=self.job_id)
        self.filePath = self.job_location

    def check_job_status(self):
        pass

    def closeEvent(self, event):
        print 'Closing window...'

    def handle_toolbar_load(self):
        '''
        Open job arguments and redraw widget.
        '''
        filename = QtGui.QFileDialog.getOpenFileName(None,
                                                     'Open a File',
                                                     QtCore.QDir.currentPath())
        self.args = shapem.shapem_parser().generate_arguments()

        # Check for json file
        if filename:
            self.args.import_args_from_json(filename)
            print 'Import job arguments'
            print self.args.output_args_as_text()
        else:
            print 'Please upload arguments file'

        # Delete the args and second dialog and widgets
        while self.setup_ui_vbox_layout.count():
            child = self.setup_ui_vbox_layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
        items = (self.an_gridLayout.itemAt(i) for i in range(
            self.an_gridLayout.count()))
        for w in items:
            w.widget().deleteLater()
        self.args_widget = window_utils.CCPEMArgsDialog()
        self.second_dialog = window_utils.CCPEMArgsDialog()
        prog_text = self.prog_label.text()
        self.prog_label = QtGui.QLabel(prog_text)
        self.prog_label.setWordWrap(True)
        self.prog_label.setFrameStyle(QtGui.QFrame.StyledPanel |
                                      QtGui.QFrame.Sunken)
        self.setup_ui_vbox_layout.addWidget(self.prog_label)
        self.adv_button = QtGui.QCheckBox('Show me advanced options')
        self.setup_ui_vbox_layout.addWidget(self.adv_button)
        self.setup_ui_vbox_layout.addWidget(self.args_widget)
        self.setup_ui_vbox_layout.addWidget(self.second_dialog)

        self.set_args()
        self.set_second_dialog()
        self.analyzer_args()

        reply = QtGui.QMessageBox.question(
            self,
            'Select options',
            ('Please check / uncheck options for filtering target map and/or '
             'search map'))


def main():
    '''
    Launch stand alone window.
    '''
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    window = ShapemMainWindow()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
