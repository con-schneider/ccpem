# CMakeLists.txt for FindEM

# Test cmake / make install command:
# cmake -H. -Bbuild
# make -C build && make install -C build
# -H = source directory, -B = build directory

cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
enable_language(Fortran)
project(ccpem)

# Set all variables needed through whole file
# Add static flag to all.  Exception on Mac systems as statically linked libs are not supported by OS X
# See:
#    https://developer.apple.com/library/mac/qa/qa1118/_index.html
# For testing set shared to true
# set(BUILD_SHARED "${BUILD_SHARED}")
set(BUILD_SHARED TRUE)

if(BUILD_SHARED)
    set(LIBRARY_TYPE SHARED)
    set(CMAKE_C_FLAGS "-w -DGFORTRAN")
else()
    set(LIBRARY_TYPE STATIC)
    set(CMAKE_C_FLAGS "-static -w -DGFORTRAN")
endif()

set(CMAKE_Fortran_FLAGS "-w -ffixed-line-length-132")

# Set vars to help with cross-compilation - to make all libraries static
# add_library to create shared libraries if on (unless library explicitly added as static)
if(NOT BUILD_SHARED)
  set(BUILD_SHARED_LIBS ON)
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES} .a)
endif()

# Build FindEM
message("Configuring FindEM programs")

set(find_em_progs FindEM_FLCF
                  FindEM_filter_coords
                  FindEM_filter_coords2
                  downsizeandfilter2
                  FindEM_processlcfmapI
                  FindEM_processlcfmapII)

foreach(_prog ${find_em_progs})
    message("  Configuring ${_prog}")
    add_executable(${_prog} ${_prog}.f90)
    target_link_libraries(${_prog} imlib2010 misclib ifftlib genlib)
    install(TARGETS ${_prog} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
endforeach()

## FindEM2
#include_directories(./include/fftw)
#include_directories(./include/mrc)
#include_directories(${BUILD_BINARY_DIR}/lib)

#add_library(weightedflcf_lib STATIC WeightedFLCF_lib.f90)
#install(TARGETS weightedflcf_lib DESTINATION lib)

#add_executable(FindEM2_V1.00 FindEM2_V1.00.f90)
#target_link_libraries(FindEM2_V1.00 imlib2010
#                                    genlib
#                                    librfftw
#                                    libfftw
#                                    weightedflcf_lib)
#install(TARGETS FindEM2_V1.00 DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

message("Finished Configuring FindEM")
