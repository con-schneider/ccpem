# CMakeLists.txt for ShapeEM

# Test cmake / make install command:
# cmake -H. -Bbuild
# make -C build && make install -C build
# -H = source directory, -B = build directory

cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(ccpem Fortran)

# Set all variables needed through whole file
# Add static flag to all.  Exception on Mac systems as statically linked libs are not supported by OS X
# See:
#    https://developer.apple.com/library/mac/qa/qa1118/_index.html
# For testing set shared to true
# set(BUILD_SHARED "${BUILD_SHARED}")
set(BUILD_SHARED TRUE)

if(BUILD_SHARED)
    set(LIBRARY_TYPE SHARED)
else()
    set(LIBRARY_TYPE STATIC)
endif()

if (CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
 # gfortran
 set (CMAKE_Fortran_FLAGS "-w  -ffixed-line-length-132")
elseif (CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
 set (CMAKE_Fortran_FLAGS "-extend-source 132")
else ()
 set (CMAKE_Fortran_FLAGS "-O0")
endif (GNU)

set(CMAKE_Fortran_MODULE_DIRECTORY ${BUILD_BINARY_DIR}/src/ccpem_progs/shape_em_2/modules)

# Set vars to help with cross-compilation - to make all libraries static
# add_library to create shared libraries if on (unless library explicitly added as static)
if(NOT BUILD_SHARED)
  set(BUILD_SHARED_LIBS ON)
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES} .a)
endif()

# Build ShapeEM2
message("Configuring ShapeEM programs")
add_library(dockmanlibv1 STATIC DockManLibsV1.0.f90)
#depend on MRC3DFourier as well but built in DockEM
install(TARGETS dockmanlibv1 DESTINATION lib)

set(shape_em_2_progs MapFourier-filterv2
		     ShapeMv2
                     ShapeM-XtAv2
                     ShapeM-XtBv2		    
                    )

foreach(_prog ${shape_em_2_progs})
    message("  Configuring ${_prog}")
    add_executable(${_prog} ${_prog}.f90)
    target_link_libraries(${_prog} imlib2010
                                   misclib
                                   ifftlib
                                   genlib
                                   mrc3dfourier
                                   dockmanlibv1
                                   )
    install(TARGETS ${_prog} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
endforeach()

if (APPLE)
SET(CMAKE_Fortran_ARCHIVE_CREATE   "<CMAKE_AR> Scr <TARGET> <LINK_FLAGS> <OBJECTS>")
SET(CMAKE_Fortran_ARCHIVE_FINISH   "<CMAKE_RANLIB> -c <TARGET>")
endif(APPLE)

message("Finished Configuring ShapeEM2")
