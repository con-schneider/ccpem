#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
CCP-EM main window.
'''

import sys
import os
from PyQt4 import QtGui
from PyQt4 import QtCore
import ccpem_core
from ccpem_gui.tasks import ccpem_tasks
from ccpem_gui import icons
from ccpem_gui.project_database import sqlite_project_database
from ccpem_gui.project_database import database_widget
from ccpem_core import settings
from ccpem_gui.project_database import project_widget
from ccpem_gui.project_database import project_manager
from ccpem_gui.utils import ccpem_widgets
from ccpem_gui.image_viewer import gallery_viewer
from ccpem_gui.image_viewer import mrc_edit_window
from ccpem_core import process_manager
from ccpem_gui.utils import window_utils
from ccpem_core import ccpem_utils

# Import modeller if available
try:
    import modeller
    modeller_available = True
except ImportError:
    modeller_available = False


class CCPEMProjectWindow(QtGui.QMainWindow):
    '''
    Window to show job view and project manager
    '''
    def __init__(self, parent=None):
        super(CCPEMProjectWindow, self).__init__(parent)
        # Reassign main window from parent for call backs
        self.main_window = parent
        self.set_docks()
        self.set_jobs_widget()
        self.set_projects_widget()

    def set_jobs_widget(self):
        '''
        Set jobs widget
        '''
        self.jobs_widget = None
        if self.main_window.database is not None:
            self.jobs_widget = MainWindowProjectView(
                main_window=self.main_window)
            self.jobs_dock.setWidget(self.jobs_widget)
            self.jobs_dock.show()
            self.jobs_dock.raise_()
        else:
            self.jobs_dock.hide()

    def set_projects_widget(self):
        '''
        Set projects widget
        '''
        if self.main_window.ccpem_projects is not None:
            self.projects_widget = MainWindowProjectWidget(
                ccpem_projects=self.main_window.ccpem_projects,
                main_window=self.main_window)
            self.projects_scroll = QtGui.QScrollArea()
            self.projects_scroll.setWidgetResizable(True) # Set to make the inner widget resize with scroll area
            self.projects_scroll.setWidget(self.projects_widget)
            self.projects_dock.setWidget(self.projects_scroll)

    def set_docks(self):
        '''
        Setup docks to hold projects and jobs widgets
        '''
        self.jobs_dock = QtGui.QDockWidget('Jobs',
                                           self,
                                           QtCore.Qt.Widget)
        self.projects_dock = QtGui.QDockWidget('Projects',
                                               self,
                                               QtCore.Qt.Widget)
        # Set docks
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.jobs_dock)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.projects_dock)
        self.tabifyDockWidget(self.jobs_dock, self.projects_dock)
        # Set dock options
        self.setDockOptions(
            QtGui.QMainWindow.ForceTabbedDocks |
            QtGui.QMainWindow.VerticalTabs |
            QtGui.QMainWindow.AnimatedDocks)


class CCPEMMainWindow(QtGui.QMainWindow):
    '''
    Main window class to hold job history and task launch buttons.
    '''
    def __init__(self, args=None, parent=None):
        super(CCPEMMainWindow, self).__init__(parent)
        self.args = args
        self.database = None
        self.ccpem_projects = None
        self.version = ccpem_utils.CCPEMVersion()

        # Get tasks
        self.tasks = ccpem_tasks.CCPEMTasks(verbose=False)

        # Set main layout
        widget = QtGui.QWidget()
        self.main_layout = QtGui.QHBoxLayout(widget)
        self.setCentralWidget(widget)

        # Set size and title
        self.resize(
            QtGui.QDesktopWidget().availableGeometry(self).size() * 0.5)
        self.setWindowTitle('CCPEM | {0}'.format(self.version.name))

        # Set task buttons
        self.set_task_buttons()

        # Set project view
        self.set_project_window()

        # Show task errors
        if self.tasks.errors:
            self.show_task_errors()

    def show_task_errors(self):
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Information)
        q_settings = QtCore.QSettings()
        if q_settings.value('dont_show_task_warnings', type=bool):
            return
        msg.setText('Some CCP-EM tasks not available')
        msg.setInformativeText('This is additional information')
        msg.setWindowTitle('CCP-EM task warning')
        errors = 'Task errors:'
        for key, value in self.tasks.errors.iteritems():
            print key
            print str(value)
            errors += '\n{0} : {1}'.format(key, value)
        msg.setDetailedText(errors)
        dont_show = QtGui.QCheckBox("Don't show this again")
        dont_show.blockSignals(True)
        msg.addButton(dont_show, QtGui.QMessageBox.ApplyRole)
        msg.setStandardButtons(
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        retval = msg.exec_()
        if retval == QtGui.QMessageBox.Ok:
            if dont_show.isChecked():
                q_settings.setValue('dont_show_task_warnings', True)

    def set_tasks_projects(self):
        '''
        This is called outside of init otherwise any warning messages are hidden
        behind splash screen.
        '''
        self.set_projects()
        tasks_projects_splitter = QtGui.QSplitter()
        tasks_projects_splitter.addWidget(self.task_scroll)
        tasks_projects_splitter.addWidget(self.project_box)
        self.main_layout.addWidget((tasks_projects_splitter))
        self.set_test_mode(False)

    def set_project_window(self):
        # Add group box for frame
        self.project_box = QtGui.QGroupBox()
        self.project_box_layout = QtGui.QHBoxLayout()
        self.project_box.setLayout(self.project_box_layout)
        # Set project window
        self.project_window = CCPEMProjectWindow(parent=self)
        self.project_box_layout.addWidget(self.project_window)

    def set_projects(self):
        if self.args.projects.value is '':
            settings_path = os.path.dirname(self.args.location.value)
            projects_path = os.path.join(settings_path, 'ccpem_projects.json')
            self.args.projects.value = projects_path
            self.args.output_args_as_json(self.args.location.value)
        self.ccpem_projects = project_manager.CCPEMProjectContainer(
            filename=self.args.projects.value)
        self.project_window.set_projects_widget()
        self.active_project_path = \
            self.ccpem_projects.get_active_project_path()
        if self.active_project_path is None:
            self.database_path = None
            self.database = None
            self.task_buttons.setEnabled(False)
        else:
            self.set_database()
            self.task_buttons.setEnabled(True)

    def set_active_project(self, active_project_path):
        # Set active project
        self.active_project_path = active_project_path
        self.set_database()
        self.project_window.set_jobs_widget()
        if self.active_project_path is not None:
            # Set working directory to project directory
            os.chdir(self.ccpem_projects.get_active_project().path)
            # Add project directory to dialog side bar
            dialog = QtGui.QFileDialog()
            dialog_urls = dialog.sidebarUrls()
            project_url = QtCore.QUrl.fromLocalFile(self.active_project_path)
            if project_url not in dialog_urls:
                dialog_urls.append(project_url)
                dialog.setSidebarUrls(dialog_urls)
            self.args.output_args_as_json(self.args.location.value)
            self.task_buttons.setEnabled(True)

    def set_database(self):
        if self.active_project_path is not None:
            self.database_path = os.path.join(
                self.ccpem_projects.get_active_project().path,
                '.ccpemDB.sqlite')
            self.database = sqlite_project_database.CCPEMDatabase(
                database_path=self.database_path)
        else:
            self.database_path = None
            self.database = None

    def set_test_mode(self, test_mode):
        self.test_mode = test_mode
        if self.test_mode:
            self.test_button.setChecked(True)
        else:
            self.test_button.setChecked(False)
            if self.ccpem_projects.get_active_project_path() is not None:
                os.chdir(self.ccpem_projects.get_active_project_path())

    def set_task_button_disabled(self, button):
        button.setDisabled(True)
        button.setToolTip('Error: task unavailable')

    def set_task_buttons(self):
        '''
        Set all task launch buttons.
        '''
        self.task_buttons = QtGui.QWidget()
        self.task_buttons_layout = QtGui.QVBoxLayout()
        self.task_buttons.setLayout(self.task_buttons_layout)

        # CCPEM logo
        ccpem_logo = QtGui.QPixmap(icons.icon_utils.get_ccpem_icon())
        ccpem_button = QtGui.QPushButton()
        ccpem_button.setToolTip(
            'CCP-EM\n'
            'Collaborative Computational Project for Electron cryo-Microscopy')
        ccpem_icon = QtGui.QIcon(ccpem_logo)
        ccpem_button.setIcon(ccpem_icon)
        ccpem_button.setIconSize(ccpem_logo.rect().size())
        ccpem_button.clicked.connect(self.handle_ccpem_logo)
        self.task_buttons_layout.addWidget(ccpem_button)

        # Separator
        self.task_buttons_layout.addWidget(ccpem_widgets.CCPEMMenuSeparator())

        # Tasks
        # Buccaneer
        self.button_run_buccaneer = QtGui.QPushButton(
            'Buccaneer',
            self)
        self.task_buttons_layout.addWidget(self.button_run_buccaneer)
        if self.tasks.buccaneer is not None:
            self.button_run_buccaneer.setToolTip(
                self.tasks.buccaneer.task.task_info.short_description)
            self.button_run_buccaneer.clicked.connect(self.handle_run_buccaneer)
        else:
            self.set_task_button_disabled(self.button_run_buccaneer)

        # Choyce
        self.button_run_choyce = QtGui.QPushButton(
            'Choyce',
            self)
        self.task_buttons_layout.addWidget(self.button_run_choyce)
        if self.tasks.choyce is not None:
            self.button_run_choyce.setToolTip(
                self.tasks.choyce.task.task_info.short_description)
            self.button_run_choyce.clicked.connect(self.handle_run_choyce)
        else:
            self.set_task_button_disabled(self.button_run_choyce)

        # DockEM
        self.button_run_dock_em = QtGui.QPushButton(
            'DockEM',
            self)
        self.task_buttons_layout.addWidget(self.button_run_dock_em)
        if self.tasks.dock_em is not None:
            self.button_run_dock_em.setToolTip(
                self.tasks.dock_em.task.task_info.short_description)
            self.button_run_dock_em.clicked.connect(
                self.handle_run_dock_em)
        else:
            self.button_run_dock_em.setDisabled(True)
            self.button_run_dock_em.setToolTip('Error not available')

        # Flex-EM
        self.button_run_flexem = QtGui.QPushButton(
            'Flex-EM',
            self)
        self.task_buttons_layout.addWidget(self.button_run_flexem)
        if self.tasks.flex_em is not None:
            self.button_run_flexem.setToolTip(
                self.tasks.flex_em.task.task_info.short_description)
            self.button_run_flexem.clicked.connect(self.handle_run_flexem)
        else:
            self.set_task_button_disabled(self.button_run_flexem)

        # Molrep
        self.button_run_molrep = QtGui.QPushButton(
            'Molrep',
            self)
        self.task_buttons_layout.addWidget(self.button_run_molrep)
        if self.tasks.molrep is not None:
            self.button_run_molrep.setToolTip(
                self.tasks.molrep.task.task_info.short_description)
            self.button_run_molrep.clicked.connect(self.handle_run_molrep)
        else:
            self.set_task_button_disabled(self.button_run_molrep)

        # MRC to MTZ
        self.button_run_refmac_sb = QtGui.QPushButton(
            'MRC to MTZ',
            self)
        self.task_buttons_layout.addWidget(self.button_run_refmac_sb)
        if self.tasks.mrc_to_mtz is not None:
            self.button_run_refmac_sb.setToolTip(
                self.tasks.mrc_to_mtz.task.task_info.short_description)
            self.button_run_refmac_sb.clicked.connect(self.handle_run_refmac_sb)
        else:
            self.set_task_button_disabled(self.button_run_refmac_sb)

        # MRC Allspace
        self.button_mrc_mrc_allspacea = QtGui.QPushButton(
            'MRC-Allspace',
            self)
        self.task_buttons_layout.addWidget(self.button_mrc_mrc_allspacea)
        if self.tasks.mrcallspacea is not None:
            self.button_mrc_mrc_allspacea.setToolTip(
                self.tasks.mrcallspacea.task.task_info.short_description)
            self.button_mrc_mrc_allspacea.clicked.connect(
                self.handle_run_mrc_allspacea)
            self.task_buttons_layout.addWidget(self.button_mrc_mrc_allspacea)
        else:
            self.set_task_button_disabled(self.button_mrc_mrc_allspacea)

        # MRC to tif
        self.button_mrc_mrc2tif = QtGui.QPushButton(
            'MRC-Tif',
            self)
        self.task_buttons_layout.addWidget(self.button_mrc_mrc2tif)
        if self.tasks.mrc2tif is not None:
            self.button_mrc_mrc2tif.setToolTip(
                self.tasks.mrc2tif.task.task_info.short_description)
            self.button_mrc_mrc2tif.clicked.connect(
                self.handle_run_mrc_mrc2tif)
        else:
            self.set_task_button_disabled(self.button_mrc_mrc2tif)

        # Nautilus
        self.button_run_nautilus = QtGui.QPushButton(
            'Nautilus',
            self)
        self.task_buttons_layout.addWidget(self.button_run_nautilus)
        if self.tasks.nautilus is not None:
            self.button_run_nautilus.setToolTip(
                self.tasks.nautilus.task.task_info.short_description)
            self.button_run_nautilus.clicked.connect(self.handle_run_nautilus)
        else:
            self.set_task_button_disabled(self.button_run_nautilus)

        # Ribfind
        self.button_run_ribfind = QtGui.QPushButton(
            'Ribfind',
            self)
        self.task_buttons_layout.addWidget(self.button_run_ribfind)
        if self.tasks.ribfind is not None:
            self.button_run_ribfind.setToolTip(
                self.tasks.ribfind.task.task_info.short_description)
            self.button_run_ribfind.clicked.connect(self.handle_run_ribfind)
        else:
            self.set_task_button_disabled(self.button_run_ribfind)

        #
        self.button_run_prosmart = QtGui.QPushButton(
            'ProSMART',
            self)
        self.task_buttons_layout.addWidget(self.button_run_prosmart)
        if self.tasks.prosmart is not None:
            self.button_run_prosmart.setToolTip(
                self.tasks.prosmart.task.task_info.short_description)
            self.button_run_prosmart.clicked.connect(self.handle_run_prosmart)
        else:
            self.set_task_button_disabled(self.button_run_prosmart)

        # Refmac
        self.button_run_refmac5 = QtGui.QPushButton(
            'Refmac5',
            self)
        self.task_buttons_layout.addWidget(self.button_run_refmac5)
        if self.tasks.refmac is not None:
            self.button_run_refmac5.setToolTip(
                self.tasks.refmac.task.task_info.short_description)
            self.button_run_refmac5.clicked.connect(self.handle_run_refmac5)
        else:
            self.set_task_button_disabled(self.button_run_refmac5)

        # Shake
        self.button_run_shake = QtGui.QPushButton(
            'Shake',
            self)
        self.task_buttons_layout.addWidget(self.button_run_shake)
        if self.tasks.shake is not None:
            self.button_run_shake.setToolTip(
                self.tasks.shake.task.task_info.short_description)
            self.button_run_shake.clicked.connect(
                self.handle_run_shake)
        else:
            self.set_task_button_disabled(self.button_run_shake)

        # TEMPy: Diffmap
        self.button_run_tempy_difference_map = QtGui.QPushButton(
            'TEMPy: DiffMap',
            self)
        self.task_buttons_layout.addWidget(self.button_run_tempy_difference_map)
        if self.tasks.tempy_diff_map is not None:
            self.button_run_tempy_difference_map.setToolTip(
                self.tasks.tempy_diff_map.task.task_info.short_description)
            self.button_run_tempy_difference_map.clicked.connect(
                self.handle_run_tempy_difference_map)
        else:
            self.set_task_button_disabled(self.button_run_tempy_difference_map)

        # TEMPy:SCCC
        self.button_run_tempy_sccc = QtGui.QPushButton(
            'TEMPy: SCCC',
            self)
        self.task_buttons_layout.addWidget(self.button_run_tempy_sccc)
        if self.tasks.tempy_sccc is not None:
            self.button_run_tempy_sccc.setToolTip(
                self.tasks.tempy_sccc.task.task_info.short_description)
            self.button_run_tempy_sccc.clicked.connect(
                self.handle_run_tempy_sccc)
        else:
            self.set_task_button_disabled(self.button_run_tempy_sccc)

        # TEMPy: SMOC
        self.button_run_tempy_smoc = QtGui.QPushButton(
            'TEMPy: SMOC',
            self)
        self.task_buttons_layout.addWidget(self.button_run_tempy_smoc)
        if self.tasks.tempy_smoc is not None:
            self.button_run_tempy_smoc.setToolTip(
                self.tasks.tempy_smoc.task.task_info.short_description)
            self.button_run_tempy_smoc.clicked.connect(
                self.handle_run_tempy_smoc)
        else:
            self.set_task_button_disabled(self.button_run_tempy_smoc)

        ### Utilities
        # Separator
        self.task_buttons_layout.addWidget(ccpem_widgets.CCPEMMenuSeparator())
        # Images - gallery
        self.button_run_images = QtGui.QPushButton(
            'Images',
            self)
        self.button_run_images.setToolTip(
            'Image gallery')
        self.button_run_images.clicked.connect(self.handle_run_images)
        self.task_buttons_layout.addWidget(self.button_run_images)
        # MRC Edit
        self.button_run_mrc_edit = QtGui.QPushButton(
            'MRCEdit',
            self)
        self.button_run_mrc_edit.setToolTip(
            'MRC header viewer / editor')
        self.button_run_mrc_edit.clicked.connect(self.handle_run_mrc_edit)
        self.task_buttons_layout.addWidget(self.button_run_mrc_edit)
        self.task_buttons_layout.addStretch()

        ### Information
        # Separator
        self.task_buttons_layout.addWidget(ccpem_widgets.CCPEMMenuSeparator())
        
        # Info button
        self.info_button = QtGui.QPushButton('Info')
        self.info_button.setToolTip('CCP-EM information')
        self.info_button.clicked.connect(self.handle_info_button)
        self.task_buttons_layout.addWidget(self.info_button)

        # Test mode button
        self.test_button = QtGui.QPushButton('Test mode')
        self.test_button.setCheckable(True)
        self.test_button.setChecked(True)
        self.test_button.setStyleSheet(
            'QPushButton:checked { background-color: lightgreen; }')
        self.test_button.setToolTip(
            'Test mode launches tasks with test parameters')
        self.test_button.clicked.connect(self.handle_test_button)
        self.task_buttons_layout.addWidget(self.test_button)
        self.task_scroll = QtGui.QScrollArea()
        self.task_scroll.setWidgetResizable(True) # Set to make the inner widget resize with scroll area
        self.task_scroll.setWidget(self.task_buttons)
        self.task_scroll.ensureWidgetVisible(ccpem_button)

    def handle_ccpem_logo(self):
        '''
        Launch ccpem website via system default browser.
        '''
        QtGui.QDesktopServices.openUrl(QtCore.QUrl('http://www.ccpem.ac.uk/'))

    def handle_info_button(self):
        '''
        Launch CCP-EM information
        '''
        # Bugs etc
        info_str = '\nFeatures, bugs and requests please contact'
        info_str += '\nccpem@stfc.ac.uk\n'
        # CCP-EM source
        ccpem_src = os.path.dirname(ccpem_core.__file__)
        ccpem_src.replace('/src/ccpem_core', '')
        info_str += '\nCCP-EM source location\n{0}\n'.format(ccpem_src)

        # Python interpreter
        info_str += '\nccpem-python bin location\n{0}\n'.format(os.path.dirname(
            sys.executable))

        # CCP-EM Git version
        info_str += '\nCCP-EM version\n{0}\n'.format(
            self.version.version)
        info_str += '\nCCP-EM Git revision\n{0}\n'.format(
            self.version.git_revision)
        info_str += '\nCCP-EM build time\n{0}\n'.format(
            self.version.build_time)

        # Modeller info
        if modeller_available:
            info_str += '\nModeller version\n{0}\n'.format(
                modeller.info.version)
        else:
            info_str += '\nModeller\nNot available\n'
        info_str += '\nSettings\n'
        info_str +=  self.args.output_args_as_text()

        # Task errors
        if self.tasks.errors:
            info_str += '\nTask errors'
            for task, error in self.tasks.errors.iteritems():
                info_str += '\n{0}: {1}'.format(task, error)

        # Info message box
        info_box = QtGui.QMessageBox(self)
        info_box.setText(info_str)
        info_box.addButton(QtGui.QPushButton('Edit'), QtGui.QMessageBox.YesRole)
        info_box.addButton(QtGui.QPushButton('Ok'), QtGui.QMessageBox.NoRole)
        info_box.setIconPixmap(QtGui.QPixmap(icons.icon_utils.get_ccpem_icon()))
        ret = info_box.exec_()
        if ret == 0:
            # Launch default editor to edit json settings
            path = self.args.location()
            url = QtCore.QUrl.fromLocalFile(QtCore.QString(path))
            QtGui.QDesktopServices.openUrl(url)

    def handle_test_button(self):
        test_mode = self.test_button.isChecked()
        self.set_test_mode(test_mode=test_mode)

    def handle_run_mrc_allspacea(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.mrcallspacea.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.mrcallspacea.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.mrcallspacea.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_mrc_mrc2tif(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.mrc2tif.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.mrc2tif.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.mrc2tif.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_flexem(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.flex_em.test_data,
                'demo_args.json')
        else:
            args_json = None
        task = self.tasks.flex_em.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.flex_em.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_choyce(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.choyce.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.choyce.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.choyce.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_dock_em(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.dock_em.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.dock_em.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.dock_em.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_buccaneer(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.buccaneer.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.buccaneer.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.buccaneer.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_nautilus(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.nautilus.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.nautilus.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.nautilus.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_prosmart(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.prosmart.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.prosmart.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.prosmart.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_refmac5(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.refmac.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.refmac.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.refmac.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_refmac_sb(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.mrc_to_mtz.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.mrc_to_mtz.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.mrc_to_mtz.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_molrep(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.molrep.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.molrep.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.molrep.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_ribfind(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.ribfind.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.ribfind.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.ribfind.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_tempy_difference_map(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.tempy_diff_map.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.tempy_diff_map.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.tempy_diff_map.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_tempy_smoc(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.tempy_smoc.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.tempy_smoc.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.tempy_smoc.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_tempy_sccc(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.tempy_sccc.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.tempy_sccc.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.tempy_sccc.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_shake(self):
        if self.test_mode:
            args_json = os.path.join(
                self.tasks.shake.test_data,
                'unittest_args.json')
        else:
            args_json = None
        task = self.tasks.shake.task(
            parent=self,
            args_json=args_json,
            database_path=self.database_path)
        window = self.tasks.shake.window(
            parent=self,
            task=task)
        window.show()

    def handle_run_images(self):
        window = gallery_viewer.CCPEMGalleryWindow(parent=self)
        window.show()

    def handle_run_mrc_edit(self):
        window = mrc_edit_window.CCPEMMrcEditWindow(parent=self)
        window.show()

class MainWindowProjectView(database_widget.CCPEMProjectTableView):
    def __init__(self, main_window):
        super(MainWindowProjectView, self).__init__(parent=main_window)
        self.main_window = main_window
        self.tasks = main_window.tasks

    def on_click_custom(self, index, path):
        program_index = index.sibling(index.row(),
                                      self.model.fieldIndex('program'))
        program = str(self.model.data(program_index).toString())
        task_class = self.tasks.get_task_class(program=program)
        window_class = self.tasks.get_window_class(program=program)
        task_file = os.path.join(path, process_manager.task_filename)
        if task_class is not None and os.path.exists(task_file):
            window_utils.relaunch_task_window(
                task_class=task_class,
                window_class=window_class,
                task_file=task_file,
                main_window=self.main_window)
        else:
            message = 'No task found'
            QtGui.QMessageBox.warning(
                self,
                'CCP-EM warning',
                message)

class MainWindowProjectWidget(project_widget.CCPEMProjectsWidget):
    def __init__(self, main_window, ccpem_projects):
        self.main_window = main_window
        super(MainWindowProjectWidget, self).__init__(
            ccpem_projects=ccpem_projects,
            parent=main_window)

    def set_active_project_display(self):
        active_project = self.projects.get_active_project()
        if active_project is None:
            self.active_project_text = 'No active project: please add a project'
        else:
            self.active_project_text = '''Active project:
    User : {0}
    Name : {1}
    Path : {2}'''.format(active_project.user,
                         active_project.name,
                         active_project.path)
            self.active_project_button.setToolTip('Click to explore')
        self.active_project_button.setText(self.active_project_text)
        self.active_project_button.setStyleSheet("Text-align:left")
        # Update parent GUI
        if self.main_window is not None:
            self.main_window.set_active_project(
                active_project_path=self.projects.get_active_project_path())


def main():
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    args = settings.get_ccpem_settings()
    # Set styles
    if args.style.value == 'default':
        if sys.platform == 'linux' or sys.platform == 'linux2':
            args.style.value = 'plastique'
    app.setStyle(args.style.value)
    app.setStyleSheet('''QToolTip {background-color: black;
                                   color: white;
                                   border: black solid 1px
                                   }''')

    # Launch main GUI
    window = CCPEMMainWindow(args=args)
    window.show()
    window.set_tasks_projects()
    #
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
