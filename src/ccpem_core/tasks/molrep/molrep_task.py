#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils

#-> Reset map to origin of 0,0,0
#  -> stcheck -f map_name -map -origin

# check handedness?

class MolRep(task_utils.CCPEMTask):
    '''
    CCPEM molrep task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Molrep',
        author='Vagin AA',
        version='11.3.02',
        description=(
            '<p>Molrep automated program for molecular replacement.  Places '
            'atomic model into EM map.  N.B. requires CCP4.</p>'
            '<p>Full documentation:</p>'
            '<p>http://www.ccp4.ac.uk/html/molrep.html</p>'),
        short_description=(
            'Automatic docking program.  Requires CCP4'),
        documentation_link='http://www.ccp4.ac.uk/html/molrep.html',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        command = settings.which(program='molrep')
        super(MolRep, self).__init__(command=command,
                                     task_info=task_info,
                                     database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     parent=parent)
        self.command_sfcheck = settings.which(program='sfcheck')

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mode',
            '--mode',
            help='Electron density search method',
            metavar='Mode',
            choices=['Rotation translation function',
                     'Spherically averaged phased translation function'
                     ],
            type=str,
            default='Spherically averaged phased translation function')
        #
        parser.add_argument(
            '-input_map',
            '--input_map',
            help='''Target input map (mrc format)#CCPEMTypeFileMap#''',
            metavar='Input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_pdb',
            '--input_pdb',
            help='''Input coordinate file (pdb format)#CCPEMTypeFilePDB#''',
            metavar='Input PDB',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-n_monomers',
            '--n_monomers',
            help='''Number of copies to find''',
            metavar='Copies',
            type=int,
            default=1)
        #
        parser.add_argument(
            '-run_sfcheck',
            '--run_sfcheck',
            help='Check input map and check agreement of output PDB with map',
            metavar='Run sfcheck',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options.  Select file or define text',
            type=str,
            metavar='Keywords',
            default='')
        #
        parser.add_argument(
            '-scale_xyz',
            '--scale_xyz',
            help='Scale map (xyz)',
            type=float,
            metavar='Scale map',
            default=1.0)
        #
        parser.add_argument(
            '-score_no',
            '--score_no',
            help='Turn scoring off, write out any solution even if it is scored low',
            type=bool,
            metavar='Scoring off',
            default=False)
        #
        parser.add_argument(
            '-ncsm',
            '--ncsm',
            help='Number of identical monomers in the search model (Input PDB). Specify for multimers.',
            type=int,
            metavar='# of subunits',
            default=1)

        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        # Run if to scale map
        if self.args.scale_xyz.value != 1.0:
            self.args.run_sfcheck.value = True

        # Map must have .map extension for molrep
        # Place map in job location
        if os.path.exists(path=self.args.input_map.value):
            if self.args.input_map.value[-4:] != '.map':
                rename = os.path.basename(self.args.input_map.value)
                if rename[-4:] == '.mrc':
                    rename = rename.replace('.mrc', '.map')
                else:
                    rename += '.map'
                rename_path = os.path.join(self.job_location,
                                           rename)
                shutil.copy(src=self.args.input_map.value, dst=rename_path)
                self.args.input_map.value = rename_path

        # Generate processes
        if self.args.run_sfcheck.value:
            self.job_sfcheck_input_map()
            self.job_molrep()
            self.job_sfcheck_map_vs_model()
            pl = [[self.sfcheck_input_process],
                  [self.molrep_process],
                  [self.sfcheck_map_vs_model_process]]
        else:
            self.job_molrep()
            pl = [[self.molrep_process]]

        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            db_inject=db_inject,
            location=self.job_location,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
        args_correct = True
        warnings = ''
        # Check input map
        warnings += self.set_arg_absolute_path(arg=self.args.input_map)
        # Check input PDB
        warnings += self.set_arg_absolute_path(arg=self.args.input_pdb)
        # Display warnings in parent GUI
        if warnings != '':
            args_correct = False
            self.print_command_line_warnings(warnings=warnings)
        return args_correct

    def job_sfcheck_input_map(self):
        '''
        Run sfcheck on input map.
        '''
        args = ['-f', self.args.input_map.value]
        if self.args.scale_xyz.value != 1.0:
            args += ['-scl', str(self.args.scale_xyz.value), '-map']
        self.sfcheck_input_process = process_manager.CCPEMProcess(
            name='SFCheck (input map)',
            command=self.command_sfcheck,
            args=args,
            location=self.job_location,
            stdin=None)

    def job_molrep(self):
        '''
        Run molrep.  '-i' argument required for stdin arguments'
        '''
        args = ['-m', self.args.input_pdb.value,
                '-f', self.args.input_map.value,
                '-i']
        stdin = self.write_molrep_stdin()
        self.molrep_process = process_manager.CCPEMProcess(
            name='Molrep',
            command=self.command,
            args=args,
            location=self.job_location,
            stdin=stdin)

    def job_sfcheck_map_vs_model(self):
        '''
        Run sfcheck to check agreement between map and output model.
        '''
        args = ['-f', self.args.input_map.value,
                '-m', 'molrep.pdb'
                ]
        self.sfcheck_map_vs_model_process = process_manager.CCPEMProcess(
            name='SFCheck (map vs model)',
            command=self.command_sfcheck,
            args=args,
            location=self.job_location,
            stdin=None)

    def write_molrep_stdin(self):
        '''
        Convert args to task arguments for molrep stdin.
        '''
        out = '_NMON {0}'.format(self.args.n_monomers.value)
        if self.args.mode.value == 'Spherically averaged phased translation function':
            out += '\n_PRF S'
        if self.args.score_no.value is not None:
                if self.args.score_no.value == True:
                    out += '\nSCORE N'
        if self.args.ncsm.value is not None:
                out += '\nNCSM %d' % self.args.ncsm.value
        if self.args.scale_xyz.value != 1.0:
            out += '\n_dscale ' + str(self.args.scale_xyz.value)
        out += '\nstick n' # Ensure model remains in map ASU
        if self.args.keywords.value is not None:
            out += '\n' + self.args.keywords.value
        out += '\n\n'
        return out

def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=MolRep)

if __name__ == '__main__':
    main()
