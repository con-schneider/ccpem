<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-5860" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>RCSB</processing_site>
        </current_status>
        <sites>
            <deposition>RCSB</deposition>
            <last_processing>RCSB</last_processing>
        </sites>
        <key_dates>
            <deposition>2014-01-13</deposition>
            <header_release>2014-02-12</header_release>
            <map_release>2014-02-19</map_release>
            <update>2014-02-19</update>
        </key_dates>
        <title>Structures of Cas9 endonucleases reveal RNA-mediated conformational activation</title>
        <authors_list>
            <author>Jinek M</author>
            <author>Jiang F</author>
            <author>Taylor DW</author>
            <author>Sternberg SH</author>
            <author>Kaya E</author>
            <author>Ma E</author>
            <author>Anders C</author>
            <author>Hauer M</author>
            <author>Zhou K</author>
            <author>Lin S</author>
            <author>Kaplan M</author>
            <author>Iavarone AT</author>
            <author>Charpentier E</author>
            <author>Nogales E</author>
            <author>Doudna JA</author>
        </authors_list>
        <keywords>CRISPR-Cas, crRNA, tracrRNA, Cas9, genome engineering, bacterial immunity</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Jinek M</author>
                    <author order="2">Jiang F</author>
                    <author order="3">Taylor DW</author>
                    <author order="4">Sternberg SH</author>
                    <author order="5">Kaya E</author>
                    <author order="6">Ma E</author>
                    <author order="7">Anders C</author>
                    <author order="8">Hauer M</author>
                    <author order="9">Zhou K</author>
                    <author order="10">Lin S</author>
                    <author order="11">Kaplan M</author>
                    <author order="12">Iavarone AT</author>
                    <author order="13">Charpentier E</author>
                    <author order="14">Nogales E</author>
                    <author order="15">Doudna JA</author>
                    <title>Structures of Cas9 Endonucleases Reveal RNA-Mediated Conformational Activation.</title>
                    <journal>SCIENCE</journal>
                    <volume>n/a</volume>
                    <first_page>n/a</first_page>
                    <last_page>n/a</last_page>
                    <year>2014</year>
                    <external_references type="pubmed">24505130</external_references>
                    <external_references type="doi">doi:10.1126/science.1247997</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
    </crossreferences>
    <sample>
        <name>Cas9 loaded with crRNA:tracrRNA and bound to target DNA duplex</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <oligomeric_state>one Cas9 binds to one crRNA:tracRNA heteroduplex and one target DNA duplex</oligomeric_state>
                <number_unique_components>5</number_unique_components>
                <molecular_weight>
                    <theoretical units="MDa">0.230</theoretical>
                </molecular_weight>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="dna">
                <name>55 bp DNA duplex target strand</name>
                <natural_source>
                    <organism ncbi="10710">Enterobacteria phage lambda</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.017</theoretical>
                </molecular_weight>
                <details>annealed with 55 bp DNA duplex non-target strand to form a double helix</details>
                <sequence>
                    <string>GCTCAATTTTGACAGCCCACATGGCATTCCACTTATCACTGGCATCCTTCCACTC</string>
                </sequence>
                <classification>DNA</classification>
                <structure>DOUBLE HELIX</structure>
                <synthetic_flag>true</synthetic_flag>
            </macromolecule>
            <macromolecule id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="Cas9">CRISPR-associated endonuclease Cas9</name>
                <natural_source>
                    <organism ncbi="1314">Streptococcus pyogenes</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.159</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>monomer</oligomeric_state>
                <recombinant_exp_flag>true</recombinant_exp_flag>
                <recombinant_expression>
                    <organism ncbi="562">Escherichia coli</organism>
                    <strain>Rosetta 2(DE3)</strain>
                </recombinant_expression>
                <sequence>
                    <external_references type="UNIPROTKB">Q99ZW2</external_references>
                    <external_references type="GO">GO:0051607</external_references>
                    <external_references type="GO">GO:0043571</external_references>
                    <external_references type="GO">GO:0008408</external_references>
                    <external_references type="GO">GO:0004520</external_references>
                    <external_references type="GO">GO:0003723</external_references>
                    <external_references type="GO">GO:0003677</external_references>
                    <external_references type="INTERPRO">IPR010145</external_references>
                    <external_references type="INTERPRO">IPR025978</external_references>
                    <external_references type="INTERPRO">IPR003615</external_references>
                </sequence>
            </macromolecule>
            <macromolecule id="3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rna">
                <name synonym="crRNA">CRISPR RNA</name>
                <natural_source>
                    <organism ncbi="1314">Streptococcus pyogenes</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.014</theoretical>
                </molecular_weight>
                <details>Forms complete guide RNA by hybridization with tracrRNA.</details>
                <sequence>
                    <string>GUGAUAAGUGGAAUGCCAUGGUUUUAGAGCUAUGCUGUUUUG</string>
                </sequence>
                <classification>OTHER</classification>
                <structure>OTHER</structure>
                <synthetic_flag>true</synthetic_flag>
            </macromolecule>
            <macromolecule id="4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rna">
                <name synonym="tracrRNA">trans-activating CRISPR RNA</name>
                <natural_source>
                    <organism ncbi="1314">Streptococcus pyogenes</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.024</theoretical>
                </molecular_weight>
                <details>Forms complete guide RNA by hybridization with crRNA. Made using T7 RNA polymerase.</details>
                <sequence>
                    <string>GGACAGCAUAGCAAGUUAAAAUAAGGCUAGUCCGUUAUCAACUUGAAAAAGUGGCACCGAGUCGGUGCUUUUU</string>
                </sequence>
                <classification>OTHER</classification>
                <structure>OTHER</structure>
                <synthetic_flag>false</synthetic_flag>
            </macromolecule>
            <macromolecule id="5" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="dna">
                <name>55 bp DNA duplex non-target strand</name>
                <natural_source>
                    <organism ncbi="10710">Enterobacteria phage lambda</organism>
                </natural_source>
                <molecular_weight>
                    <theoretical units="MDa">0.017</theoretical>
                </molecular_weight>
                <details>annealed with 55 bp DNA duplex target strand to form a double helix</details>
                <sequence>
                    <string>GAGTGGAAGGATGCCAGTGATAAGTGGAATGCCATGTGGGCTGTCAAAATTGAGC</string>
                </sequence>
                <classification>DNA</classification>
                <structure>DOUBLE HELIX</structure>
                <synthetic_flag>true</synthetic_flag>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">0.01</concentration>
                    <buffer>
                        <ph>7.5</ph>
                        <details>20 mM Tris-Cl pH 7.5, 100 mM KCl, 5 mM MgCl2, 1 mM DTT, 5% glycerol</details>
                    </buffer>
                    <staining>
                        <type>negative</type>
                        <details>After adsorption for 1 min, we stained the samples consecutively with six droplets of 2% (w/v) uranyl acetate solution, gently blotted off the residual stain, and air-dried the sample in a fume hood.</details>
                    </staining>
                    <grid>
                        <details>glow-discharged 400 mesh continuous carbon grids</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>NONE</cryogen_name>
                        <instrument>NONE</instrument>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI TECNAI 20</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>120</acceleration_voltage>
                    <nominal_cs>2.2</nominal_cs>
                    <nominal_defocus_min>-400</nominal_defocus_min>
                    <nominal_defocus_max>-1400</nominal_defocus_max>
                    <nominal_magnification>100000.</nominal_magnification>
                    <specimen_holder_model>SIDE ENTRY, EUCENTRIC</specimen_holder_model>
                    <temperature>
                        <temperature_average units="Kelvin">78</temperature_average>
                    </temperature>
                    <alignment_procedure>
                        <legacy>
                            <astigmatism>Objective astigmatism was corrected at 210,000 times magnification</astigmatism>
                            <electron_beam_tilt_params>0</electron_beam_tilt_params>
                        </legacy>
                    </alignment_procedure>
                    <details>Data acquired using Leginon.</details>
                    <date>2013-04-08</date>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="CCD">GATAN ULTRASCAN 4000 (4k x 4k)</film_or_detector_model>
                            <digitization_details/>
                            <number_real_images>213</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">20</average_electron_dose_per_image>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>Room temperature single tilt</specimen_holder>
                    <tilt_angle_min>0</tilt_angle_min>
                    <tilt_angle_max>0</tilt_angle_max>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <details>Particles were selected using template based picking in Appion.</details>
                <ctf_correction>
                    <details>whole micrograph</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>24435</number_images_used>
                    <applied_symmetry>
                        <point_group>C1</point_group>
                    </applied_symmetry>
                    <algorithm>Projection matching</algorithm>
                    <resolution>21</resolution>
                    <resolution_method>FSC 0.5, semi-independent</resolution_method>
                    <software_list>
                        <software>
                            <name>EMAN2, SPARX</name>
                        </software>
                    </software_list>
                    <details>Image pre-processing performed in Appion.</details>
                </final_reconstruction>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="11665" format="CCP4">
        <file>emd_5860.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>144</col>
            <row>144</row>
            <sec>144</sec>
        </dimensions>
        <origin>
            <col>-72</col>
            <row>-72</row>
            <sec>-72</sec>
        </origin>
        <spacing>
            <x>144</x>
            <y>144</y>
            <z>144</z>
        </spacing>
        <cell>
            <a>313.92</a>
            <b>313.92</b>
            <c>313.92</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>90.0</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-7.53044891</minimum>
            <maximum>18.707427979999999</maximum>
            <average>-0.00000653</average>
            <std>1.00000167</std>
        </statistics>
        <pixel_spacing>
            <x>2.18</x>
            <y>2.18</y>
            <z>2.18</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>5.31</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>Reconstruction of Cas9 loaded with crRNA:tracrRNA and bound to target DNA duplex</annotation_details>
        <details>::::EMDATABANK.org::::EMD-5860::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling/>
        </modelling_list>
        <figure_list>
            <figure>
                <file>emd_5860.png</file>
            </figure>
        </figure_list>
    </interpretation>
</emd>
