#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
ShapeEM task
'''

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from textwrap import dedent
from ccpem_core.gui_ext import job_manager
from ccpem_core.gui_ext.job_manager import job_wrapper
from ccpem_core.tasks import task_utils
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import ccpem_widgets
import shapem_window
import ccpem_core.gui_ext as window_utils
import subprocess

def shapem_parser():
    parser = ccpem_argparser.ccpemArgParser(
             fromfile_prefix_chars='@',
             usage=dedent('''
ShapeM is a set of software programs to match 3D density object shapes.
Specifically, it has been designed to locate known molecular shapes within
molecular densities of larger complexes determined by cryoEM. It is based 
on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007).
It uses the same FLCF algorithm to efficiently calculate correlation
coefficients over a defined 3D mask boundary around the search object,
as in DockEM.
ShapeEM has a similar procedure to DockEM but input is a density map rather 
than coordinates.The solutions are volumes extracted from the target map into 
matching volumes and settings.           
             '''),
             description='ShapEM options',
             epilog=dedent('''
To import parameters from argument text file use e.g. :\
python class_selections.py @my_args.txt
             '''))
    target_map = parser.add_argument_group()
    target_map.add_argument('-tmap',
        '--targetmap',
        help='''
Target map is required. Search object will be matched against target map using 
rigid body search.
        ''',
        type=str,
        default='MRC')
    #
    search_map = parser.add_argument_group()
    search_map.add_argument('-smap',
        '--searchmap',
        help='Search density required. It will be matched against the target map.',
        type=str,
        default='MRC')
    #
    target_map_res = parser.add_argument_group()
    target_map_res.add_argument('-res',
        '--targetmap_res',
        help='''
Resolution of the target map is required in Angstorms.
        ''',
        type=float,
        default=None)
    #
    resolution_range = parser.add_argument_group()
    resolution_range.add_argument('-rh',
        '--rhigh',
        help= '''
Highest Resolution cut off in (A). 
This is same as the target map resolution and used for map filtering.
        ''',
        type=float,
        default=None)
    resolution_range.add_argument('-rl',
        '--rlow',
        help='''
Lowest Resolution cut off (A). This is used for map filtering
        ''',
        type=float,
        default=100.0)
    #
    grid_sampling = parser.add_argument_group()
    grid_sampling.add_argument('-g',
        '--grid_samp',
        help='''
Grid sampling/Voxel size is obtained from the header of the map file uploaded.\
        ''',
        type=float,
        default=None)
    #
    target_map_filtered = parser.add_argument_group()
    target_map_filtered.add_argument('-map_f',
        '--FilteredMap',
        help='''
Target map filtered using Mapfilter2. It sets the map header to have the correct 
unit cell for DockEM2.All solutions will be produced relative to this map
in this unit cell and setting.
        ''',
        type=str,
        default='MRC')
    #
    search_map_filtered = parser.add_argument_group()
    search_map_filtered.add_argument('-filsmap',
        '--FilteredSmap',
        help='''
Filtered map for the search density
        ''',
        type=str,
        default='MRC')
    #
    mask_radius = parser.add_argument_group()
    mask_radius.add_argument('-mr',
        '--mask_radius',
        help='''
Radius of sphere to convolute (in Angstroms)
        ''',
        type=float,
        default=None)
    #
    search_map_mask_uploaded = parser.add_argument_group()
    search_map_mask_uploaded.add_argument('-mu',
        '--smap_mask_up',
        help='''
Mask to define the extent of the search density-user uploaded
        ''',
        type=str,
        default='Optional input')
    #
    search_map_mask = parser.add_argument_group()
    search_map_mask.add_argument('-m',
        '--smap_mask',
        help='''
Mask to define the extent of the search density generated using MaskConvolute.
        ''',
        type=str,
        default='MRC')
    #
    mask_threshold = parser.add_argument_group()
    mask_threshold.add_argument('-mth',
        '--mask_threshold',
        help='''
mask threshold value
        ''',
        type=float,
        default=None)
    #
    theta_step = parser.add_argument_group()
    theta_step.add_argument('-s',
        '--theta_step',
        help='''
The value of theta step is between 0-180 and is used to generate angular 
distribution on sphere. \
Angles files are in Euler angles convention, in spider document format.
        ''',
        type=float,
        default=5.0) 
    #
    ang_sampling = parser.add_argument_group()
    ang_sampling.add_argument('-a',
        '--angl_samp',
        help='''
Angular sampling is required in degrees. \
increment for the angular omega search. Refer to DockEM paper for how 
to choose this.
        ''',
        type=float,
        default=5.0) 
    #
    angl_filename = parser.add_argument_group()
    angl_filename.add_argument('-ag',
        '--angl_filename',
        help='''
Angular sampling is required in degrees. \
The same as used to create the angles file. Refer to DockEm paper for how 
to choose this.
        ''',
        type=str,
        default='angles_file.spi')
    #
    class ChoiceList(list):
        def __contains__(self, other):
            return super(ChoiceList,self).__contains__(other.upper())
    choices = ChoiceList(['C','B'])
    ori_rot = parser.add_argument_group()
    ori_rot.add_argument('-ori',
        '--ori_rot',
        help='''
Origin of rotation at Center-of-mass of search object, or the Box center.Type C or B
        ''',
        type=str,
        choices=choices,
        default='C')
    #
    num_hits = parser.add_argument_group()
    num_hits.add_argument('-hits',
        '--num_hits',
        help='''
Enter the number of hits/solutions required (between 1-10000).
        ''',
        type=int,
        default=100)
    #
    peak_radius = parser.add_argument_group()
    peak_radius.add_argument('-peak_rad',
        '--peak_radius',
        help='''
Enter the peak radius, used to exclude the nearby shoulder solutions that are essentially
the same solution, in Angstroms.
        ''',
        type=float,
        default=20.0)
    #
    soln_range = parser.add_argument_group()
    soln_range.add_argument('-num_soln',
        '--soln_range',
        help='''
Enter the solution range required. (e.g. 1,10 for the top ten solutions).
        ''',
        type=int,
        default=10)

    #
    advanced_option = parser.add_argument_group()
    advanced_option.add_argument('-adv',
        '--advanced_option',
        help='''
Launches GUI in advanced layout. if script used as python script -adv
        ''',
        action='store_true')
    return parser

class ProgramInfo(object):
    '''
    Store program information for database.
    '''
    def __init__(self, database_path=None):
        self.program_id = 'ShapeEM_2' 
        self.jobname = 'ShapeEM'
        self.version = '2'
        self.description = '''
Welcome to ShapeEM2.
ShapeM is a set of software programs to match 3D density object shapes.
Specifically, it has been designed to locate known molecular shapes within 
molecular densities of larger complexes determined by cryoEM. It is based 
on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007). 
It uses the same FLCF algorithm to efficiently calculate correlation 
coefficients over a defined 3D mask boundary around the search object, 
as in DockEM.
ShapeEM has a similar procedure to DockEM but input is a density map rather 
than coordinates.The solutions are volumes extracted from the target map into 
matching volumes and settings.
'''
        self.author = 'Alan M. Roseman'
        self.module = os.path.realpath(__file__)
        self.database_path = database_path

def launch_gui(parent,
               database_path,
               args=None,
               job_manager=None):

    # Add program info to database
    if database_path is not None:
            program_info = ProgramInfo()
            task_utils.add_program_info_to_database(database_path,
                                                    program_info)
    window = shapem_window.DockEMMainWindow(
        parent=parent,
        args=args,
        job_manager=job_manager)
    window.setGeometry(parent.geometry().x()+50,
                       parent.geometry().x()+50,
                       window.geometry().width(),
                       window.geometry().height())
    window.show()


class RunTask_Check(object):
    '''
    Run task for preliminary steps
    '''
    def __init__(self,
                 job_id,
                 job_location,
                 database_path,
                 stdin_file_mapfilter=None,
                 stdin_file_makedensity=None,
                 stdin_file_smapff=None,
                 stdin_file_maskcon=None,
                 args=None,
                 parent=None,
                 mapf=False,
                 pdbf=False,
                 maskc=False):
        self.command_mapfmap = settings.which('MapFourier-filterv2')
        self.command_smapff = settings.which(program='MapFourier-filterv2')
        self.command_maskc = settings.which(program='MaskConvoluteDKM2')
        assert self.command_mapfmap is not None
        self.stdin_file_mapfilter = stdin_file_mapfilter
        self.stdin_file_smapff = stdin_file_smapff
        self.stdin_file_maskcon = stdin_file_maskcon
        if self.stdin_file_mapfilter is None:
            self.stdin_file_mapfilter = job_location + '/stdin_mapfilter.txt'
            self.stdin_file_smapff = job_location + '/stdin_smapff.txt'
            self.stdin_file_maskcon = job_location + '/stdin_maskcon.txt'
        self.mapf = mapf
        self.pdbf = pdbf
        self.maskc = maskc
        self.job_id = job_id
        self.job_location = job_location
        self.database_path = database_path
        self.args = args
        self.parent = parent

        if self.args is None:
            self.args = shapem_parser().generate_arguments()
        self.map_in = self.args.targetmap.value

        # Check target map file exists
        print '\ntargetmap.in : ', self.map_in
        assert os.path.exists(self.map_in)

        # Save arguments
        self.args.rhigh.value = self.args.targetmap_res.value
        self.args.output_args_as_json(self.job_location + '/args.json')

        #Run task process
        self.run_check()

    def run_check(self):
        '''
        Convert args to mrc stdin format, run as detached job.
        '''
        self.stdout_mapfmap = \
            os.path.join(self.job_location, 'stdout_mapfmap.txt')
        self.stderr_mapfmap = \
            os.path.join(self.job_location, 'stderr_mapfmap.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_mapfmap.json')
        job_mapfmap = job_wrapper.SeriesJobInfo(
            command=self.command_mapfmap,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_mapfilter,
            stdout_file=self.stdout_mapfmap,
            stderr_file=self.stderr_mapfmap,
            json_file=self.json_file)
        #
        self.stdout_smapff = \
            os.path.join(self.job_location, 'stdout_smapff.txt')
        self.stderr_smapff = \
            os.path.join(self.job_location, 'stderr_smapff.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_smapff.json')
        job_smapff = job_wrapper.SeriesJobInfo(
            command=self.command_smapff,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_smapff,
            stdout_file=self.stdout_smapff,
            stderr_file=self.stderr_smapff,
            json_file=self.json_file)
        #
        self.stdout_maskc = \
            os.path.join(self.job_location, 'stdout_maskc.txt')
        self.stderr_maskc = \
            os.path.join(self.job_location, 'stderr_maskc.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_maskc.json')
        job_maskc = job_wrapper.SeriesJobInfo(
            command=self.command_maskc,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_maskcon,
            stdout_file=self.stdout_maskc,
            stderr_file=self.stderr_maskc,
            json_file=self.json_file)
        #
        self.write_stdin_check()
        self.job_info_list_check = []
        if self.mapf:
            if self.pdbf:
                if self.maskc:
                    self.job_info_list_check = [job_mapfmap,
                                                job_smapff,
                                                job_maskc]
                else:
                    self.job_info_list_check = [job_mapfmap,
                                                job_smapff]
                    os.remove(self.stdin_file_maskcon)
            else:
                if self.maskc:
                    self.job_info_list_check = [job_mapfmap,
                                                job_maskc]
                    os.remove(self.stdin_file_smapff)
                else:
                    self.job_info_list_check = [job_mapfmap]
                    os.remove(self.stdin_file_smapff)
                    os.remove(self.stdin_file_maskcon)
        else:
            if self.pdbf:
                if self.maskc:
                    self.job_info_list_check = [job_smapff,
                                                job_maskc]
                    os.remove(self.stdin_file_mapfilter)
                else:
                    self.job_info_list_check = [job_smapff]
                    os.remove(self.stdin_file_mapfilter)
                    os.remove(self.stdin_file_maskcon)
            else:
                if self.maskc:
                    self.job_info_list_check = [job_maskc]
                    os.remove(self.stdin_file_mapfilter)
                    os.remove(self.stdin_file_smapff)
                else:
                    self.job_info_list_check = []
                    os.remove(self.stdin_file_mapfilter)
                    os.remove(self.stdin_file_smapff)
                    os.remove(self.stdin_file_maskcon)

        # Setup inputs for all DockEM binaries
        # Launches MakeDensity, Mapfilter (for search pdb) and MaskConvolute
        # serially
        if not self.job_info_list_check:
            print ('You did not choose to prepare any preliminary file. '
                   'Please proceed to ShapeEM directly')
        else:
            job_manager.job_wrapper.launch_detached_process_series(
                job_info_list=self.job_info_list_check,
                job_id=self.job_id,
                job_location=self.job_location,
                database_path=self.database_path,
                relay_output=False,
                parent=self.parent)

    def write_stdin_check(self):
        '''
        Convert args to task arguments for stdin for mapfilter and
        maskconvolute
        '''
        # High resolution is by default set to target map resolution
        self.args.rhigh.value = self.args.targetmap_res.value
        #
        dockem_input1 = open(self.stdin_file_mapfilter, 'w')
        dockem_input2 = open(self.stdin_file_smapff, 'w')
        dockem_input3 = open(self.stdin_file_maskcon, 'w')

        # MapFilter stdin for target map
        out1 = self.args.targetmap.value + '\n'
        out1 += str(self.args.grid_samp.value) + '\n'
        out1 += str(self.args.rhigh.value) + ',' + str(self.args.rlow.value) + '\n'
        out1 += (self.job_location + '/' + self.args.FilteredMap.value) + '\n'
        dockem_input1.write(out1)
        dockem_input1.close()

        # MapFilter stdin for search object
        out2 = self.args.searchmap.value + '\n'
        out2 += str(self.args.grid_samp.value) + '\n'
        out2 += str(self.args.rhigh.value) + ',' + str(self.args.rlow.value) + '\n'
        out2 += (self.job_location + '/' + self.args.FilteredSmap.value) + '\n'
        dockem_input2.write(out2)
        dockem_input2.close()

        # MaskConvolute stdin for search object
        out3 = self.args.searchmap.value + '\n'
        out3 += str(self.args.grid_samp.value) + '\n'
        out3 += str(self.args.mask_radius.value) + '\n'
        out3 += (self.job_location + '/' + self.args.smap_mask.value) + '\n'
        dockem_input3.write(out3)
        dockem_input3.close()


class RunTask(object):
    '''
    Run task for ShapeM and ShapeM-XtAv2, ShapeM-XtBv2
    '''
    def __init__(self,
                 job_id,
                 stdin_file_shapem=None,
                 stdin_file_shapema=None,
                 stdin_file_shapemb=None,
                 args=None,
                 parent=None):
        self.command_shapem = settings.which(program='ShapeMv2')
        self.command_shapema = settings.which(program='ShapeM-XtAv2')
        self.command_shapemb = settings.which(program='ShapeM-XtBv2')
        self.stdin_file_shapem = stdin_file_shapem
        self.stdin_file_shapema = stdin_file_shapema
        self.stdin_file_shapemb = stdin_file_shapemb
        job_location = os.getcwd()
        self.job_location = job_location
        self.stdin_file_shapem = job_location + '/stdin_shapem.txt'
        self.stdin_file_shapema = job_location + '/stdin_shapema.txt'
        self.stdin_file_shapemb = job_location + '/stdin_shapemb.txt'
        self.job_id = job_id
        self.stdout_shapem = \
            os.path.join(self.job_location, 'stdout_shapem.txt')
        self.stderr_shapem = \
            os.path.join(self.job_location, 'stderr_shapem.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_shapem.json')
        job_shapem = job_wrapper.SeriesJobInfo(
            command=self.command_shapem,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_shapem,
            stdout_file=self.stdout_shapem,
            stderr_file=self.stderr_shapem,
            json_file=self.json_file)
        #
        self.stdout_shapema = \
            os.path.join(self.job_location, 'stdout_shapema.txt')
        self.stderr_shapema = \
            os.path.join(self.job_location, 'stderr_shapema.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_shapema.json')
        job_shapema = job_wrapper.SeriesJobInfo(
            command=self.command_shapema,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_shapema,
            stdout_file=self.stdout_shapema,
            stderr_file=self.stderr_shapema,
            json_file=self.json_file)
        #
        self.stdout_shapemb = \
            os.path.join(self.job_location, 'stdout_shapemb.txt')
        self.stderr_shapemb = \
            os.path.join(self.job_location, 'stderr_shapemb.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_shapemb.json')
        job_shapemb = job_wrapper.SeriesJobInfo(
            command=self.command_shapemb,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_shapemb,
            stdout_file=self.stdout_shapemb,
            stderr_file=self.stderr_shapemb,
            json_file=self.json_file)
        self.job_info_list = [job_shapem, job_shapema, job_shapemb]
        self.args = args
        self.parent = parent
        if self.args is None:
            self.args = shapem_parser().generate_arguments()
        # Save arguments
        self.args.output_args_as_json(os.path.join(job_location,
                                                   'args.json'))
        # Run task process
        self.run_detached()

    def run_detached(self):
        '''
        Convert args to mrc stdin format, run as detached job.
        '''
        self.write_stdin()
        # Launches ShapeEM and ShapeEMA and ShapeEMB serially
        job_manager.job_wrapper.launch_detached_process_series(
            job_info_list = self.job_info_list,
            job_id=self.job_id,
            job_location=self.job_location,
            database_path=None,
            relay_output=False,
            parent=self.parent)

    def write_stdin(self):
        '''
        Convert args to task arguments for stdin for rest of the programs
        '''
        dockem_input4 = open(self.stdin_file_shapem, 'w')
        dockem_input5 = open(self.stdin_file_shapema, 'w')
        dockem_input6 = open(self.stdin_file_shapemb, 'w')

        # ShapeEM stdin
        out4 = (self.job_location + '/' + self.args.FilteredMap.value) + '\n'
        out4 += str(self.args.grid_samp.value) + '\n'
        out4 += (self.job_location + '/' + self.args.FilteredSmap.value) + '\n'
        out4 += (self.job_location + '/' + self.args.smap_mask.value) + '\n'
        out4 += str(self.args.mask_threshold.value) + '\n'
        out4 += str(self.args.angl_filename.value) + '\n'
        out4 += str(self.args.angl_samp.value) + '\n'
        out4 += '123' + '\n'
        out4 += str(self.args.ori_rot.value)
        dockem_input4.write(out4)
        dockem_input4.close()

        # ShapeM-XtA stdin
        out5 = str(self.args.grid_samp.value) + '\n'
        out5 += str(self.args.num_hits.value) + '\n'
        out5 += '123' + '\n'
        out5 += str(self.args.peak_radius.value)
        dockem_input5.write(out5)
        dockem_input5.close()

        # ShapeM-XtB stdin
        out6 = (self.job_location + '/' + self.args.FilteredMap.value) + '\n'
        out6 += str(self.args.grid_samp.value) + '\n'
        out6 += (self.job_location + '/' + self.args.FilteredSmap.value) + '\n'
        out6 += '123' + '\n'
        out6 += '1,' + str(self.args.soln_range.value) + '\n'
        dockem_input6.write(out6)
        dockem_input6.close()


class RunTask_Analyzer(object):
    '''
    Run task for Analyzer (ShapeM-XtAv2, ShapeM-XtBv2)
    '''
    def __init__(self,
                 job_id,
                 stdin_file_shapema=None,
                 stdin_file_shapemb=None,
                 args=None,
                 parent=None):
        self.command_shapema = settings.which(program='ShapeM-XtAv2')
        self.command_shapemb = settings.which(program='ShapeM-XtBv2')
        self.stdin_file_shapema = stdin_file_shapema
        self.stdin_file_shapemb = stdin_file_shapemb
        job_location = os.getcwd()
        self.job_location = job_location
        self.stdin_file_shapema = job_location + '/stdin_shapema.txt'
        self.stdin_file_shapemb = job_location + '/stdin_shapemb.txt'
        self.job_id = job_id
        #
        self.stdout_shapema = \
            os.path.join(self.job_location, 'stdout_shapema.txt')
        self.stderr_shapema = \
            os.path.join(self.job_location, 'stderr_shapema.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_shapema.json')
        job_shapema = job_wrapper.SeriesJobInfo(
            command=self.command_shapema,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_shapema,
            stdout_file=self.stdout_shapema,
            stderr_file=self.stderr_shapema,
            json_file=self.json_file)
        #
        self.stdout_shapemb = \
            os.path.join(self.job_location, 'stdout_shapemb.txt')
        self.stderr_shapemb = \
            os.path.join(self.job_location, 'stderr_shapemb.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_shapemb.json')
        job_shapemb = job_wrapper.SeriesJobInfo(
            command=self.command_shapemb,
            qarg_list=[],
            set_environment='',
            stdin_file=self.stdin_file_shapemb,
            stdout_file=self.stdout_shapemb,
            stderr_file=self.stderr_shapemb,
            json_file=self.json_file)
        self.job_info_list_an = [job_shapema, job_shapemb]
        self.args = args
        self.parent = parent
        if self.args is None:
            self.args = shapem_parser().generate_arguments()
        # Save arguments
        self.args.output_args_as_json(job_location + '/args.json')
        # Run task process
        self.run_analyser()

    def run_analyser(self):
        '''
        Convert args to mrc stdin format, run as detached job.
        '''
        self.write_stdin_an()
        # Launches MakeDensity, Mapfilter (for search pdb) and MaskConvolute 
        # serially
        job_manager.job_wrapper.launch_detached_process_series(
            job_info_list=self.job_info_list_an,
            job_id=self.job_id,
            job_location=self.job_location,
            database_path=None,
            relay_output=False,
            parent=self.parent)

    def write_stdin_an(self):
        '''
        Convert args to task arguments for stdin for rest of the programs
        '''
        dockem_input5 = open(self.stdin_file_shapema, 'w')
        dockem_input6 = open(self.stdin_file_shapemb, 'w')

        # ShapeM-XtA stdin
        out5 = str(self.args.grid_samp.value) + '\n'
        out5 += str(self.args.num_hits.value) + '\n'
        out5 += '123' + '\n'
        out5 += str(self.args.peak_radius.value)
        dockem_input5.write(out5)
        dockem_input5.close()

        # ShapeM-XtB stdin
        out6 = (self.job_location + '/' + self.args.FilteredMap.value) + '\n'
        out6 += str(self.args.grid_samp.value) + '\n'
        out6 += (self.job_location + '/' + self.args.FilteredSmap.value) + '\n'
        out6 += '123' + '\n'
        out6 += '1,' + str(self.args.soln_range.value) + '\n'
        dockem_input6.write(out6)
        dockem_input6.close()
