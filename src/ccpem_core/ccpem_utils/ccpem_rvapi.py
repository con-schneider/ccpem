#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import pyrvapi

def set_rvapi_document(doc_id,
                       output_path,
                       window_title,
                       mode=pyrvapi.RVAPI_MODE_Html,
                       layout=pyrvapi.RVAPI_LAYOUT_Tabs):
    html_filename = 'ccpem.html'
    pyrvapi.rvapi_init_document(
        doc_id,
        output_path,
        window_title,
        mode,
        layout,
        get_jsrview_uri(),
        None,
        html_filename,
        'ccpem_rv.tsk',
        None)
    #
    html_path = os.path.join(output_path,
                             html_filename)
    return html_path


def get_jsrview_uri():
    '''
    Return path of jsrview URI
    '''
    ccp4 = os.environ['CCP4']
    jsrview_uri = os.path.join(ccp4, 'share', 'jsrview')
    assert os.path.exists(jsrview_uri)
    return jsrview_uri
