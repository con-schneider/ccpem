#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
MRC Allspacea task
'''

import sys
import os
from ccpem_core.ccpem_utils import ccpem_argparser
from textwrap import dedent
from ccpem_core.gui_ext import job_manager
from ccpem_core.gui_ext.job_manager import job_wrapper
from ccpem_core.tasks import task_utils
import mrc_allspacea_window
import ccpem_core.ccpem_utils.file_tools as file_tools

def mrc_allspacea_parser():
    parser = ccpem_argparser.ccpemArgParser(
             fromfile_prefix_chars='@',
             usage=dedent('''
Umbrella program to calculate internal phase residual in all 17 space groups \
from data on a single film.
Can do the same thing within one film as ORIGTILT does between different films.
             '''),
             description='MRC Allspacea options',
             epilog=dedent('''
To import parameters from argument text file use e.g. :\
python class_selections.py @my_args.txt
             '''))
    datafile = parser.add_argument_group()
    datafile.add_argument('-f',
        '--datafile',
        help='''
Spot file input, any number of spots with any index (preceded by title line) H, K,\
amplitude, phase.
        ''',
        type=str,
        default='None')
    symmetry = parser.add_argument_group()
    symmetry.add_argument('-sym',
        '--symmetry',
        help='Symmetry required',
        choices=['ALL',
                 'HEXA',
                 'SQUA',
                 'RECT',
                 'OBLI',
                 'TWO',
                 'THRE',
                 'SIX',
                 'FOUR',
                 'SPEC'],
        type=str,
        default='ALL')
    symmetry.add_argument('-ispg',
        '--ispg',
        help='List of space group numbers (optional)',
        required=False,
        type=str,
        default='None')
    #
    search_refine_tilt_ncyc = parser.add_argument_group()
    search_refine_tilt_ncyc.add_argument('-s',
        '--search',
        help='Search',
        type=bool,
        default=True)
    search_refine_tilt_ncyc.add_argument('-r',
        '--refine',
        help='Search',
        type=bool,
        default = True)
    search_refine_tilt_ncyc.add_argument('-t', 
        '--tilt',
        help='Tilt',
        type=bool,
        default=True)
    search_refine_tilt_ncyc.add_argument('-c', 
        '--ncycle',
        help='Number of cycles',
        type=int,
        default=4000)
    #
    orig = parser.add_argument_group()
    orig.add_argument('-oh',
        '--origh',
        help='Starting origin phase shift H',
        type=float,
        default=0.)
    orig.add_argument('-ok',
        '--origk',
        help='Starting origin phase shift K',
        type=float,
        default=0.)
    #
    tilt = parser.add_argument_group()
    tilt.add_argument('-th',
        '--tilth',
        help='Starting beam tilt H',
        type=float,
        default=0.)
    tilt = parser.add_argument_group()
    tilt.add_argument('-tk',
        '--tiltk',
        help='Starting beam tilt K',
        type=float,
        default=0.)
    #
    stepsize = parser.add_argument_group()
    stepsize.add_argument('-st',
        '--stepsize',
        help='Phase-shift step size',
        type=float,
        default=1.0)
    phasesize = parser.add_argument_group()
    phasesize.add_argument('-is',
        '--phasesize',
        help='Size of search area',
        type=int,
        default=61)
    #
    cell_dims = parser.add_argument_group()
    cell_dims.add_argument('-a',
        '--alpha',
        help='Alpha cell dimension',
        type=float,
        default=1.0)
    cell_dims.add_argument('-b',
        '--beta',
        help='Beta cell dimension',
        type=float,
        default=1.0)
    cell_dims.add_argument('-g',
        '--gamma',
        help='Gamma cell dimension',
        type=float,
        default=1.0)
    #
    resolution_range = parser.add_argument_group()
    resolution_range.add_argument('-ri',
        '--rin',
        help='Resolution in (A)',
        type=float,
        default=10.0)
    resolution_range.add_argument('-ro',
        '--rout',
        help='Resolution out (A)',
        type=float,
        default=100.0)
    #
    cs = parser.add_argument_group()
    cs.add_argument('-cs',
        '--cs',
        help='Spherical aberration (nm)',
        type=float,
        default=1.0)
    #
    kv = parser.add_argument_group()
    kv.add_argument('-kv',
        '--kv',
        help='Microscope operating voltage (KV)',
        type=float,
        default=100.0)
    #
    ilist = parser.add_argument_group()
    ilist.add_argument('-il',
        '--ilist',
        help='Ilist = True gives more detailed output',
        type=bool,
        default=True)
    rot180  = parser.add_argument_group()
    ilist.add_argument('-r180',
        '--rot180',
        help='Rot180 = 1 enables same convention as other programs to be used for P3 indexing convention',
        type=int,
        default=0)
    iqmax  = parser.add_argument_group()
    iqmax.add_argument('-iqm',
        '--iqmax',
        help='IQmax is used to restrict the comparisons to sport with IQ <= IQmax',
        type=int,
        default=10)
    return parser

class ProgramInfo(object):
    '''
    Store program information for database.
    '''
    def __init__(self, database_path=None):
        self.program_id = 'mrcallspacea_0.1'
        self.name = 'mrcallspacea'
        self.version = '0.1'
        self.description = '''
Umbrella program to calculate internal phase residual in all 17 space groups \
from data on a single film.
Can do the same thing within one film as ORIGTILT does between different films.
'''
        self.author = 'Crowther RA, Henderson R, Smith JM'
        self.module = os.path.realpath(__file__)
        self.database_path = database_path

def launch_gui(parent,
               database_path,
               args=None,
               job_manager=None):
    # Add program info to database
    if database_path is not None:
            program_info = ProgramInfo()
            task_utils.add_program_info_to_database(database_path,
                                                    program_info)
    window = mrc_allspacea_window.MrcAllspaceaWindow(
                       parent=parent,
                       args=args,
                       job_manager=job_manager,
                       window_title='MRC-IPS | allspacea')
    window.setGeometry(parent.geometry().x()+50,
                       parent.geometry().x()+50,
                       window.geometry().width(),
                       window.geometry().height())
    window.show()

class RunTask(object):
    '''
    Run task
    '''
    def __init__(self,
                 job_id,
                 job_location,
                 database_path,
                 stdin_file,
                 args=None,
                 parent=None):
        self.command = file_tools.which(program='mrc_allspacea')
        assert self.command is not None
        self.stdin_file = stdin_file
        self.job_id = job_id
        self.job_location = job_location
        self.database_path = database_path
        self.stdin_file = stdin_file
        self.args = args
        self.parent = parent
        if self.args is None:
            self.args = mrc_allspacea_parser().generate_arguments()
        self.datafile_in = self.args.datafile.value
        # Check data file exists
        print '\ndatafile.in : ', self.datafile_in
        assert os.path.exists(self.datafile_in)
        # Save arguments
        self.args.output_args_as_json(self.job_location + '/args.json')
        # Setup inputs
        self.write_stdin()
        # Run task process
        self.run_detached()

    def run_detached(self):
        '''
        Convert args to mrc stdin format, run as detached job.
        '''
        job_manager.job_wrapper.launch_detached_process(
            command=self.command,
            job_id=self.job_id,
            job_location=self.job_location,
            database_path=self.database_path,
            qarg_list=[],
            relay_output=False,
            stdin_file=self.stdin_file,
            set_environment=[['IN', self.datafile_in]],
            parent=self.parent)

    def write_stdin(self):
        '''
        Convert args to task arguments for stdin
        '''
        mrc_input = open(self.stdin_file, 'w')
        out = self.args.symmetry.value + '\n'
        out += (str(self.args.search.value) + ' '
              + str(self.args.refine.value) + ' '
              + str(self.args.tilt.value) + ' '
              + str(self.args.ncycle.value) + '\n')
        out += (str(self.args.origh.value) + ' '
              + str(self.args.origk.value) + ' '
              + str(self.args.tilth.value) + ' '
              + str(self.args.tiltk.value) + '\n')
        out += (str(self.args.stepsize.value) + ' '
              + str(self.args.phasesize.value) + '\n')
        out += (str(self.args.alpha.value) + ' '
              + str(self.args.beta.value) + ' '
              + str(self.args.gamma.value) + ' '
              + str(self.args.rin.value) + ' '
              + str(self.args.rout.value) + ' '
              + str(self.args.cs.value) + ' '
              + str(self.args.kv.value) + '\n')
        out += (str(self.args.ilist.value) + ' '
              + str(self.args.rot180.value) + ' '
              + str(self.args.iqmax.value) + '\n')
        out += 'eot'
        mrc_input.write(out)
        mrc_input.close()

