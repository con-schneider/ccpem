#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from __future__ import print_function

import io
import multiprocessing
import os
import shutil
import sys
import time
import tempfile
import unittest

import pyrvapi
import pyrvapi_ext
from pyrvapi_ext import parsers

from ccpem_core.process_manager.ccpem_process import CCPEMProcess
from ccpem_core.process_manager.process_utils import run_ccpem_process
from ccpem_core.settings import which


class CCPEMProcessTest(unittest.TestCase):
    '''
    Tests for CCPEMProcess.
    '''
    def setUp(self):
        '''
        Setup test output directory.
        '''
        self.test_output = tempfile.mkdtemp()
        
        command = which('ccpem-python')
        args = ['-m', 'ccpem_core.process_manager.tests.dummy_task', 'process']
        name = 'Dummy task for CCPEMProcess test'
        self.process = CCPEMProcess(name=name,
                                    command=command,
                                    args=args,
                                    location=self.test_output,
                                    stdin=None)

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)
    
    def test_json_round_trip(self):
        # Write process to json file, create a new process from it, and compare
        self.process.export_json()
        process_2 = CCPEMProcess(import_json=self.process.json)
        assert self.process.__dict__ == process_2.__dict__
    
    def test_json_round_trip_with_parser(self):
        '''The log parser should not be serialized to JSON.'''
        # Add a log parser to the process
        self.process.log_parser = parsers.generic_parser('fake_rvapi_grid')
        
        # Carry out the JSON round trip
        self.process.export_json()
        process_2 = CCPEMProcess(import_json=self.process.json)
        
        # Remove the parser from the original process, which should make it
        # identical to the round-trip result
        self.process.log_parser = None
        assert self.process.__dict__ == process_2.__dict__
    
    def test_process_run(self):
        '''
        Run a single CCPEMProcess and check its results.
        '''
        dummy_json_pipeline_path = 'dummy_json_pipeline_path'
        assert self.process.status == 'ready'
        assert self.process.pid is None
        
        # Run process (this will block until the process has finished)
        run_ccpem_process(self.process, dummy_json_pipeline_path)
        
        # Update the process from disk again, then check results
        assert self.process.status == 'finished'
        assert os.path.isfile(self.process.stdout)
        assert os.path.isfile(self.process.stderr)
        assert os.path.isfile(self.process.json)
        assert self.process.json_pipeline == dummy_json_pipeline_path
        
        with open(self.process.stdout) as f:
            stdout = f.read()
            assert "CCP-EM Process | " + self.process.name in stdout
            assert self.process.command[0] in stdout
            assert "Hello world" in stdout
            assert "CCP-EM process finished" in stdout
        
        with open(self.process.stderr) as f:
            stderr = f.read()
            assert len(stderr) == 0, "stderr is not empty:\n" + stderr
        
        log_name = os.path.join(self.test_output, "process_test.log")
        assert os.path.isfile(log_name)
        with open(log_name) as f:
            log = f.read()
            assert log == "TEST"
    
    def test_process_run_from_separate_process(self):
        '''
        Run a CCPEMProcess from a separate process (equivalent to what is done
        by the CCPEMPipeline) and check its progression and results.
        '''
        dummy_json_pipeline_path = 'dummy_json_pipeline_path'
        proc = multiprocessing.Process(target=run_ccpem_process,
                                       args=(self.process,
                                             dummy_json_pipeline_path))
        assert self.process.status == 'ready'
        assert self.process.pid is None
        proc.start()
        
        # Wait for the process to start up, then check status
        count = 0
        while self.process.pid is None:
            time.sleep(0.2)
            # Check the process has not already finished (which would probably
            # indicate an error) 
            assert proc.exitcode is None
            
            # Update the process from the json file on disk (since it is
            # running in a different process and the Python object state is not
            # shared)
            self.process.import_json(self.process.json)
            
            count += 1
            if count > 20:
                self.fail("Timed out waiting for process to start")
        
        assert self.process.status == 'running', "Status is " + self.process.status
        
        # Wait plenty of time for the process to finish, and check it finished
        # cleanly
        proc.join(10.0)
        assert not proc.is_alive()
        assert proc.exitcode == 0
        
        # Update the process from disk again, then check results
        self.process.import_json(self.process.json)
        assert self.process.status == 'finished'
        assert os.path.isfile(self.process.stdout)
        assert os.path.isfile(self.process.stderr)
        assert os.path.isfile(self.process.json)
        assert self.process.json_pipeline == dummy_json_pipeline_path
        
        with open(self.process.stdout) as f:
            stdout = f.read()
            assert "CCP-EM Process | " + self.process.name in stdout
            assert self.process.command[0] in stdout
            assert "Hello world" in stdout
            assert "CCP-EM process finished" in stdout
        
        with open(self.process.stderr) as f:
            stderr = f.read()
            assert len(stderr) == 0, "stderr is not empty:\n" + stderr
        
        log_name = os.path.join(self.test_output, "process_test.log")
        assert os.path.isfile(log_name)
        with open(log_name) as f:
            log = f.read()
            assert log == "TEST"
    
    def test_process_with_stdin_string(self):
        command = which('ccpem-python')
        args = ['-m', 'ccpem_core.process_manager.tests.dummy_stdin_task']
        name = 'CCPEMProcess test for stdin from string'
        stdin = "Dummy stdin string contents\n\nMore stuff\n...and more"
        self.process = CCPEMProcess(name=name,
                                    command=command,
                                    args=args,
                                    location=self.test_output,
                                    stdin=stdin)
        
        proc = multiprocessing.Process(target=run_ccpem_process,
                                       args=(self.process, None))
        proc.start()
        
        # Wait plenty of time for the process to finish, and check it finished
        # cleanly
        proc.join(10.0)
        assert not proc.is_alive()
        assert proc.exitcode == 0
        
        # Update the process from disk again, then check results
        self.process.import_json(self.process.json)
        assert self.process.status == 'finished', self.process.status
        assert os.path.isfile(self.process.stdout)
        assert os.path.isfile(self.process.stderr)
        assert os.path.isfile(self.process.json)
        assert self.process.json_pipeline == None
        
        with open(self.process.stdout) as f:
            stdout = f.read()
            assert "CCP-EM Process | " + self.process.name in stdout
            assert self.process.command[0] in stdout
            assert "CCP-EM process finished" in stdout
        
        with open(self.process.stderr) as f:
            stderr = f.read()
            assert len(stderr) == 0, "stderr is not empty:\n" + stderr
        
        log_name = os.path.join(self.test_output, "stdin_string_test.log")
        assert os.path.isfile(log_name)
        with open(log_name) as f:
            log = f.read()
            assert log == stdin
    
    def test_process_with_error(self):
        '''
        Test behaviour with a process that fails with an error.
        '''
        
        command = which('ccpem-python')
        args = ['-c', 'raise RuntimeError']
        name = 'Example task failure'
        self.process = CCPEMProcess(name=name,
                                    command=command,
                                    args=args,
                                    location=self.test_output)
        
        dummy_json_pipeline_path = 'dummy_json_pipeline_path'
        assert self.process.status == 'ready'
        assert self.process.pid is None
        
        # Redirect sys.stdout to capture output from print statements
        orig_stdout = sys.stdout
        temp_stdout = io.BytesIO()
        try:
            sys.stdout = temp_stdout
            
            # Run process (this will block until the process has finished)
            run_ccpem_process(self.process, dummy_json_pipeline_path)
        finally:
            sys.stdout = orig_stdout
        
        # Check a failure message was printed to this process's stdout
        expected_stdout = "Job {} failed!".format(self.process.pid)
        captured_stdout = temp_stdout.getvalue()
        assert expected_stdout in captured_stdout
        
        # Update the process from disk again, then check results
        assert self.process.status == 'failed'
        assert os.path.isfile(self.process.stdout)
        assert os.path.isfile(self.process.stderr)
        assert os.path.isfile(self.process.json)
        assert self.process.json_pipeline == dummy_json_pipeline_path
        
        with open(self.process.stdout) as f:
            stdout = f.read()
            assert "CCP-EM Process | " + self.process.name in stdout
            assert self.process.command[0] in stdout
            assert "CCP-EM process failed" in stdout
        
        with open(self.process.stderr) as f:
            stderr = f.read()
            assert "Traceback" in stderr
            assert "RuntimeError" in stderr
    
    def test_process_creation_with_bad_command_name(self):
        '''
        Test creation of a CCPEMProcess with a nonexistent command name.
        '''
        with self.assertRaises(AssertionError):
            CCPEMProcess(name='Bad command test',
                         command='nonexistent-command',
                         args=[],
                         location=self.test_output)
    
    def test_running_bad_command_name(self):
        '''
        Test running of a process with a nonexistent command name.
        '''
        # Need to create the process with a real command first
        self.process = CCPEMProcess(name='Bad command test',
                                    command='ccpem-python',
                                    args=[],
                                    location=self.test_output)
        
        # Now change the command to make the process invalid
        # (It might be better to test this with a command which is somehow
        # invalid on creation - if we find an example we could consider
        # changing this test to match.)
        self.process.command = 'nonexistent-command'
        
        # Redirect sys.stdout and sys.stderr to capture output
        orig_stdout = sys.stdout
        orig_stderr = sys.stderr
        temp_stdout = io.BytesIO()
        temp_stderr = io.BytesIO()
        try:
            sys.stdout = temp_stdout
            sys.stderr = temp_stderr
            
            # Run process (this will block until the process has finished)
            run_ccpem_process(self.process, None)
        finally:
            sys.stdout = orig_stdout
            sys.stderr = orig_stderr
        
        # Check failure messages and tracebacks were printed to this process's
        # stdout
        captured_stdout = temp_stdout.getvalue()
        assert "Warning! Error starting CCP-EM process:" in captured_stdout
        assert "nonexistent-command" in captured_stdout
        assert "No such file or directory" in captured_stdout
        assert "Traceback" in captured_stdout
        assert "OSError" in captured_stdout
        assert "Job None failed!" in captured_stdout
        
        # Check exception was printed to this process's stderr
        captured_stderr = temp_stderr.getvalue()
        assert "Traceback" in captured_stderr
        assert "OSError" in captured_stderr
        
        # Update the process from disk again, then check results
        assert self.process.status == 'failed'
        assert os.path.isfile(self.process.stdout)
        assert os.path.isfile(self.process.stderr)
        assert os.path.isfile(self.process.json)
        
        with open(self.process.stdout) as f:
            stdout = f.read()
            assert "CCP-EM Process | " + self.process.name in stdout
            assert self.process.command[0] in stdout
            assert "CCP-EM process failed" in stdout
        
        with open(self.process.stderr) as f:
            stderr = f.read()
            assert len(stderr) == 0, "stderr is not empty:\n" + stderr
    
#     def test_process_with_log_parser(self):
#         '''
#         Run a CCPEMProcess with a log parser and check the output.
#         
#         NOTE: the log parsing feature should be considered experimental and
#         not used yet for real jobs! The pyrvapi_ext API is also somewhat
#         unstable at the moment.
#         '''
#         name = 'Dummy task for CCPEMProcess test'
#         
#         command = which('ccpem-python')
#         args = ['-m', 'ccpem_core.process_manager.tests.dummy_log_parser_task']
#         
#         pyrvapi_ext.document.newdoc(name + ' Output',
#                                     reportdir=os.path.join(self.test_output,
#                                                            'report'),
#                                     layout=pyrvapi.RVAPI_LAYOUT_Plain)
#         pyrvapi_ext.flush()
#         parser = parsers.generic_parser('body')
#         
#         self.process = CCPEMProcess(name=name,
#                                     command=command,
#                                     args=args,
#                                     location=self.test_output,
#                                     stdin=None,
#                                     log_parser=parser)
#         
#         dummy_json_pipeline_path = 'dummy_json_pipeline_path'
#         assert self.process.status == 'ready'
#         assert self.process.pid is None
#         
#         # Run process (this will block until the process has finished)
#         run_ccpem_process(self.process, dummy_json_pipeline_path)
#         
#         # Update the process from disk again, then check results
#         assert self.process.status == 'finished'
#         assert os.path.isfile(self.process.stdout)
#         assert os.path.isfile(self.process.stderr)
#         assert os.path.isfile(self.process.json)
#         assert self.process.json_pipeline == dummy_json_pipeline_path
#         
#         with open(self.process.stdout) as out_file:
#             stdout = out_file.read()
#             assert "CCP-EM Process | " + self.process.name in stdout
#             assert self.process.command[0] in stdout
#             assert "CCP-EM process finished" in stdout
#         
#         with open(self.process.stderr) as f:
#             stderr = f.read()
#             assert len(stderr) == 0, "stderr is not empty:\n" + stderr
#         
#         report_dir = os.path.join(self.test_output, 'report')
#         assert os.path.isdir(report_dir)
#         assert os.path.isfile(os.path.join(report_dir, 'index.html'))

if __name__ == '__main__':
    unittest.main()
