!=******************************************************************************
!=* ShapeM - Programs              		          AUTHOR: A.ROSEMAN    *
!=*                                    		                               *
!=*									       *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
!=* written by Alan Roseman at the University of Manchester, 2013. It is based *
!=* on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007).     *
!=* It uses the same FLCF algorithm to efficiently calculate correlation       *
!=* coefficients over a defined 3D mask boundary around the search object,     *
!=* as in DockEM.							       *
!=*									       *
!=*    Copyright (C) 2013 The University of Manchester 		    	       *
!=*                                                                   	       *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 								               *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!
!
!Mapfourier-filterV1.0.f90

! f90 program filter maps
! 
! uses f90 array features
!
! filter is a sharp truncation
! note maps must be "cubic".

    program MapFourierfilter
	use  image_arrays
 	Use  mrc_image

	real sampling,lp,hp
	integer next_ft_size
	character*256 filename, outfile2
        DATA IFOR/0/,IBAK/1/,ZERO/0.0/
 
   
1     format(A45)      
      write(*,1) 'Map Fourier filter program'
      write(*,1) '--------------------------'
      

! 1.  read in template filename, and threshold of data to use.

      write(*,*) 'Enter the map filename >'
      read (5,'(a)') filename
      print*,filename    
      
      write(*,*) 'Enter the sampling of the map (A/pixel) > '
      read*,sampling
      print*,sampling
            
      write(*,*) 'Enter the high resolution and low resolution cutoffs (A) > '  
      read*,lp,hp
      print*,lp,hp
      if ((lp.lt.0) .OR. (hp.lt.0)) STOP 'Can''t have negative values for the filters.'
      if (lp.gt.hp) STOP 'Low pass filter higher than high pass filter!'
       
      write(*,*) 'Enter the output map filename >'
      read (5,'(a)') outfile2
      print*,outfile2
             
  
! 2. read in the map
  
      CALL IMOPEN(1,filename(1:lnblnk(filename)),'RO')
      CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      
	print*, NX,NY,NZ
	
!	if (odd(nx).eq..true.) STOP 'MAP DIMENSION MUST BE EVEN'
	
	nxp2=nx+2
	nxp1=nx+1
	nym1=ny-1
	nzm1=nz-1
	
	if ((nx.ne.ny).and.(nx.ne.nz)) STOP "The map region must be a cube."
	
	mx=nx
	my=ny
	mz=nz
	mx=next_ft_size(mx)
	my=next_ft_size(my)
	mz=next_ft_size(mz)
	mxp2=mx+2
	mxp1=mx+1
	mym1=my-1
	mzm1=mz-1
	
	print*,mx,my,mz
	
	
        allocate (map(0:mXP1,0:mYM1,0:mZM1))	
 
 
      	call read_mrcimage(1,nx,ny,nz)
	call IMOPEN(4,outfile2(1:lnblnk(outfile2)),'NEW')
	call ITRHDR(4,1)
	call IWRHDR(4,title,ntflag,dmin,dmax,dmean)    
      
        CALL IMCLOSE(1)   

! 3. open input and output image stacks 

! 	4.5 filter image

	 print*,mxp2,my,mz
	
          CALL BCKW_FT(map,mXP2,mX,mY,mZ)
          
          print*, 'ft'
          
          CALL FILTER(sampling,lp,hp)
          
          print*,'filt'
          
 	  CALL FORW_FT(map,mXP2,mX,mY,mZ)
      	
	
	  print*, 'rev ft'
  
  
9999    continue
      	call write_mrcimage(4,sampling)       
	
	

! 8.  close files and exit
	
      CALL IMCLOSE(4) 

      print*,'Program finished O.K.'
            
      STOP
997   STOP 'Error on file read.'
      
      
      CONTAINS


      subroutine read_mrcimage(stream,x,y,z)
 
		
        Use  image_arrays
        Use  mrc_image	
	INTEGER  stream, IX,IY,IZ
	integer x,y,z
 	     
	nxp2=nx+2
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1

	print*,'read_mrcimage'	

	
      print*,NX,NY,NZ,x,y,z     	      	
!     read in file 
 	map=0
      DO 350 IZ = 0,z-1
    	  DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE   

350   CONTINUE

      return
998   STOP 'Error on file read.'
      end subroutine 


      subroutine write_mrcimage(stream,sampling)	
      	
      Use  image_arrays
      Use  mrc_image
  	
      INTEGER stream,IX,IY,IZ
   
 
      nxp2=nx+2
      nxm1=nx-1
      nym1=ny-1
      nzm1=nz-1
	
      print*,'write_mrcimage'	      	

      print*,NX,NY,NZ  	
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZM1
      DO 450 IY = 0,NYM1
            DO 400 IX = 0,NXM1
            	B = map(IX,IY,IZ)
                ALINE(IX+1) = B
                
                DOUBLMEAN = DOUBLMEAN + B         
                IF (B .LT. DMIN) DMIN = B
                IF (B .GT. DMAX) DMAX = B

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)
      
      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90
	
      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      
      return
999   STOP 'Error on file write.'
      end subroutine 


  
!C***************************************************************************

        SUBROUTINE FILTER(sampling,lp,hp)
        Use  image_arrays


	real sampling,lp_cutoff,hp_cutoff,rad,r,hp,lp
	real cx,cy,cz,x,y,z
	integer ix,iy,iz
	integer nx,ny,nz,nxm1,nym1,nzm1pwd
	
	     
	nx=size(map,1)
	ny=size(map,2)
	nz=size(map,3)
	
	nx=nx-2
	
	nxm1=nx-1
	nym1=ny-1	
	nzm1=nz-1
	
	nxp1=nx+1
		
	cx=int(nx/2) 
	cy=int(ny/2) 
	cz=int(nz/2) 
	
	print*,nx,ny,nz,cx,cy,cz
	
	lp_cutoff = nx*sampling/lp
	hp_cutoff = nx*sampling/hp
	
	
 	DO 301 iz=0,nzm1
        DO 301 iy=0,nym1
        DO 303 ix=0,nx,2
        
        	x=int(ix/2.)
        	y=iy
        	z=iz
        	
        	if (y.gt.cy) y=y-ny
        	if (z.gt.cz) z=z-nz
        	
                r=sqrt((x*x) + (y*y)+  (z*z))
                if ((r.le.hp_cutoff).OR.(r.ge.lp_cutoff)) then 
                			map(ix,iy,iz)=(0.0)
                			map(ix+1,iy,iz)=(0.0)
                			
                endif
                
303     CONTINUE
301     CONTINUE

       



        RETURN
        END  SUBROUTINE FILTER

  
  	LOGICAL FUNCTION ODD(N)
  	INTEGER N
  	REAL F,I ,T 	
  	
  	F=FLOAT(N)
  	I=INT(F/2)
  	R=I/2
  	T=(I-R)
  	
  	IF (T.GT.0.00001) THEN
  					ODD=.TRUE.
  				ELSE
  					ODD=.FALSE.
  				ENDIF
  	END FUNCTION ODD 
  
  	end program MapFourierfilter



