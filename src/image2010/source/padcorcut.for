C*************************************************************************
C*** padcorcut - program to pad images to power of 2, fft then flip phases 
C*** or correct by multiplying by ctf, then cut image out for output
C*** CTF values generated from ctfgen program (RAC)
C*************************************************************************
C*** 	Data input
C*** 	(a) Input file name can be a stack  (Output file will be input file 
C***        with extension .cor)
C***    (*) dfmid1 dfmid2 angast - defocus estimate                      
C***    (*) dstep xmag cs kv ctfminmod wamp    - image parameters
C***                         default value of ctfminmod = 0.05
C***	    dstep - densitometer step size in microns
C***	    xmag - magnification
C***	    cs - spherical abberation
C***	    kv -kilovoltage
C***        ctfminmod - min modulus of CTF in denominator for ictf 2 or 3
C***                    similar to Wiener filter
C***        wamp - Amplitude contrast 
C***    (*) ictf ipad 
C***        ictf : 1 multiply by ctf
C***               2 divide by ctf up to 1st max, then multiply
C***               3 divide by ctf (need ctfminmod for this)
C***               4 phase flip
C***        ipad   0 do not pad
C***               1 pad image(s) to power of 2 before correction 
C***                 (needed if box size odd or prime factor > 19)
C***
C***	V1.0	16.07.2008
C***	V1.1	17.09.2008
C***	V1.2	12.10.2011
C*************************************************************************
	integer*4		idevin
	parameter		(idevin = 1)
	integer*4		idevout
	parameter		(idevout = 2)
	integer*4		idevtest
	parameter		(idevtest = 3)
	real*4			kvscale
	parameter		(kvscale = 1000.)
	integer*4		maxctf
	parameter		(maxctf = 50000)
	integer*4		maxline
	parameter		(maxline = 8194)
        real*4          	twopi
        parameter       	(twopi = 2.0 * 3.1415926)
	real*4			rdeg
        parameter		(rdeg = 360.0/twopi)
	integer*4		itest
        parameter		(itest = 0)

C***
	character*24		date
	character*80		title
	character*512		filin
	character*512		filout
C***
	integer*4		nxyz(3)
	integer*4		mxyz(3)
C***
	real*4			aline(maxline)
	real*4			density(maxline * maxline)
	real*4			ctfwt(maxctf,360)
	real*4			kv
	real*4			radmax(360)
	real*4			transform(maxline * maxline)
C***
	write(6,'(//''***** Padcorcut V1.2 12.10.2011 *****''/)')
	write(6,'(''Input file name ... ?'')')
	read(5,'(a)') filin
	write(6,'(a)') filin(1:lnblnk(filin))
	write(6,'('' dfmid1 dfmid2 angast ...'')')
	read(5,*) dfmid1, dfmid2, angast
	write(6,'(''dstep xmag cs kv ctfminmod wamp ...'')')
        read(5,*) dstep, xmag, cs, kv, ctfminmod, wamp
	if(ctfinmod .eq. 0.0) ctfinmod = 0.05
	ctfinmod = abs(ctfinmod)
        write(6,'(
     *         /''  dfmid1 dfmid2 angast     '',3F10.2,
     *         /''  Kilovolts                '',F10.2,'' kV'',
     *         /''  dstep,xmag               '',2F10.2,
     *         /''  Spherical aberr.         '',F10.2,'' mm''
     *         /''  ctfinmod, wamp           '',2f10.2)')
     *   dfmid1,dfmid2,angast,kv,dstep,xmag,cs,ctfminmod, wamp
C***
	write(6,'(''Multiply by CTF ................. 1''/
     *            ''Divide to 1st max then multiply.. 2''/
     *            ''Divide by CTF ................... 3''/
     *            ''Phase flip ...................... 4'')')
	write(6,'(
     *  ''ipad - do not pad/pad image(s) to power of 2 .. 0/1'')')
	read(5,*) ictf, ipad
C*** multiply by ctf
	if(ictf .eq. 1) then
	 write(6,'(''Padcorcut V1.0 multiplying by CTF : '',a)')
     *   filin(1:lnblnk(filin))
	 write(6,'(''Multiplying by CTF.'')')
	else if(ictf .eq. 2) then
	 write(6,'(
     *   ''Padcorcut V1.0 dividing/multiplying by CTF : '',a)')
     *   filin(1:lnblnk(filin))
	 write(6,'(''Dividing/Multiplying by CTF.'')')
C*** divide by ctf
	else if(ictf .eq. 3) then
	 write(6,'(''Padcorcut V1.0 dividing by CTF : '',a)')
     *   filin(1:lnblnk(filin))
	 write(6,'(''Dividing by CTF.'')')
	else
	 write(6,'(''Padcorcut V1.0 phase flipping : '',a)')
     *   filin(1:lnblnk(filin))
	 write(6,'(''Phase flipping.'')')
	end if
	filout = filin(1:lnblnk(filin))//'.cor'
C*** open input file
	call imopen(idevin,filin,'ro')
        call irdhdr(idevin,nxyz,mxyz,mode,dmin,dmax,dmean)
	nxin = nxyz(1)
	nyin = nxyz(2)
	nzin = nxyz(3)
C*** open output file mode 2
	call imopen(idevout,filout,'unknown')
        call itrhdr(idevout,idevin)
	mode2 = 2
	call ialmod(idevout,mode2)
        tmin = 1.e10
        tmax = -tmin
        tmean = 0.
	if(ipad .eq. 1) then
C*** calculate nearest power of 2 to pad to
	 numin = max(nxin,nyin)
	 nytrans = 2
 1000    nytrans = nytrans * 2
	 if(nytrans . le. numin) go to 1000
	 if(nytrans .gt. maxline) then
	  write(6,'(''Error - file too large for program.'')')
	  stop
	 end if
	else
	 nytrans = nyin
	end if
	write(6,'(''Padding to '',i5)') nytrans
	nxtrans = nytrans + 2
        nxhalf = nytrans / 2 + 1  
 	isize = (nxtrans/2 - 1) * 2
	nxo2 = nxtrans / 2
	ny21 = nytrans / 2 + 1
	angast = angast * twopi / 360.0
C*** spherical abberation(Ang), kilovoltage (volts), wavelength (angstroms)
        CS = CS * (10.0**7.0)                        
        KV = KV * 1000.0                          
        WL = 12.3/SQRT(KV + KV**2/(10.0**6.0))
        WRITE(6,'(''Wavelength angstroms'',f10.4)') wl
        STEPR = DSTEP * (10.0**4.0) / XMAG
C***  THETATR IS DIFFRACTION ANGLE OF POINT (0,1) IN TRANSFORM (IN RADIANS)
        THETATR = WL / (STEPR * isize)
        angtit = rdeg * angast
C*** calculate radius at 1st maximum taking astigmatism into account
	if(ictf .eq. 2) then
	 nctf = min(nytrans / 2, maxctf)
	 do irad=1,nctf
	  do iangspt=1,360
           angle = float(irad) * thetatr       
	   angspt = float(iangspt)
           C1 = TWOPI * ANGLE * ANGLE / (2.0 * WL)
           C2 = -C1 * CS * ANGLE * ANGLE / 2.0       
           ANGDIF = ANGSPT - ANGAST
           CCOS = COS(2.0 * ANGDIF)
           DF = 0.5 * (DFMID1 + DFMID2 + CCOS * (DFMID1 - DFMID2))
           CHI = C1 * DF + C2           
           CNTRST = -SIN(CHI) - WAMP * COS(CHI)
	   ctfwt(irad,iangspt) = cntrst
	  end do
	 end do
	 do iangspt=1,360
     	  do irad = 2,nctf-1
	   rad1 = abs(ctfwt(irad-1,iangspt))
	   rad2 = abs(ctfwt(irad,iangspt))
	   rad3 = abs(ctfwt(irad+1,iangspt))
	   if(rad2 .gt. rad1 .and. rad2 .gt. rad3) then
	    radmax(iangspt) = irad
	    go to 1100
	   end if
	  end do
 1100    continue
	 end do
	end if
C********************************************************
C*** loop over sections
C********************************************************
	do nz = 1,nzin
C*** read first line 
	 call imposn(idevin,nz-1,0)
	 call irdlin(idevin,density)
C*** calculate average perimeter density for padding
	 background = 0.
	 do ix=1,nxin
	  background = background + density(ix)
	 end do
C*** read vertical box sides
	 do iy=1,nyin - 2
	  call irdlin(idevin,density)
	  background = background + density(1) + density(nxin)
	 end do
C*** read last line
	 call irdlin(idevin,density)
	 do ix=1,nxin
	  background = background + density(ix)
	 end do
	 background = float(nint(background / 
     *   (2.0 * float(nxin + nyin - 1)))) 
	 write(6,'(''Background calculated from perimeter : '',f8.1)')
     *   background
C*** set the array to background but ignore the 2 extra columns
	 do iy=1,nytrans
	  ixy = (iy - 1) * nxtrans
	  do ix=1,nytrans
	   density(ixy+ix) = background
	  end do
	  density(ixy+nytrans+1) = 0
	  density(ixy+nytrans+2) = 0
	 end do
C*** read the image, position in the output array
	 call imposn(idevin,nz-1,0)
	 istartx = (nxtrans - nxin) / 2
	 istarty = (nytrans - nyin) / 2
	 do iy=1,nyin
	  ixy = nxtrans * (istarty + iy - 1) + istartx
	  call irdlin(idevin,density(ixy))
	  do ix=1,nxin
	   density(ixy+ix) = density(ixy+ix)
	  end do
	 end do
C**************************************************************
C*** write test map but do not include 2 extra columns
C************************************************************** 
	if(itest .eq. 1) then
	 mxyz(1) = nytrans
	 mxyz(2) = nytrans
	 mxyz(3) = 1
	 mode = 1
	 call imopen(idevtest,'padcorcut.mrc','unknown')
         call icrhdr(idevtest,mxyz,mxyz,mode,title,0)
         dmin = 1000000.
         dmax = -dmin
         dmean = 0.
	 do iy=1,nytrans
	  ixy = (iy - 1) * nxtrans
	  do ix=1,nytrans
	   den = density(ixy+ix)
	   dmin = min(den,dmin)
	   dmax = max(den,dmax)
	   dmean = dmean + den
	  end do
          call iwrlin(idevtest,density(ixy+1))
         end do
         dmean = dmean / float(nytrans * nytrans)
         call iwrhdr(idevtest,title,-1,dmin,dmax,dmean)
         call imclose(idevtest)
	end if
C*************************************************************
C***  call forward FFT for the padded map
C*************************************************************
 1200	 call todfft(density,nytrans,nytrans,0)
C*** write test fft map
	if(itest .eq. 1) then
	 call imopen(idevtest,'padcorcut.fft','unknown')
	 mxyz(1) = nxhalf
	 mxyz(2) = nytrans
	 mxyz(3) = 1
	 mode = 4
	 xorigin = 0.
	 yorigin = 0.
	 zorigin = 0.
         call icrhdr(idevtest,mxyz,mxyz,mode,title,-1)
	 call imposn(idevtest, 0, 0)
	 call ialorg(idevtest,xorigin,yorigin,zorigin)
	 do iy=nxhalf,nytrans
	  ixy = nxtrans * (iy - 1) + 1
	  call iwrlin(idevtest,density(ixy))
	 end do
	 do iy=1,nytrans/2
	  ixy = nxtrans * (iy - 1) + 1
	  call iwrlin(idevtest,density(ixy))
	 end do
         call iclcdn(density,nxhalf,nytrans,1,nxhalf,1,nytrans,
     *   dmin,dmax,dmean)       
         call iwrhdr(idevtest,title,-1,dmin,dmax,dmean)
	 call imclose(idevtest)
	end if
C*****************************************************************
C*** calculate CTF
C*****************************************************************
C*** apply the CTF
 1300	 do 1400 iy=1,nytrans
	  if(iy .lt. ny21) then
	   ydist = iy - 1
	  else
	   ydist = iy - nytrans - 1
	  end if
	  indy = (iy - 1) * nxtrans
	  do 1400 ix = 1,nxo2
	   xdist = ix - 1
	   index = (ix - 1) * 2 + indy + 1
           rad = xdist**2+ydist**2
	   if(rad .ne. 0) then
	    rad = sqrt(rad)
	    angle = rad * thetatr
	    angspt = atan2(ydist,xdist)
	    iangspt = angspt * 360. / twopi
	    if(iangspt .le. 0) iangspt = iangspt + 360
            C1 = TWOPI * ANGLE * ANGLE / (2.0 * WL)
            C2 = -C1 * CS * ANGLE * ANGLE / 2.0
            ANGDIF = ANGSPT - ANGAST
            CCOS = COS(2.0 * ANGDIF)
            DF = 0.5 * (DFMID1 + DFMID2 + CCOS * (DFMID1 - DFMID2))
            CHI = C1 * DF + C2
            CNTRST = -SIN(CHI) - WAMP * COS(CHI)
           ELSE
C*** set this value for the origin to avoid crash when testing radmax
	    iangspt = 1
            CNTRST = 0.0
           ENDIF
C*** multiply by ctf
           if(ictf .le. 2) then
C*** special case for divmult at rad = 0.0
	    if(ictf .eq. 1 .or. rad .eq. 0. 
     *                     .or. rad .gt. radmax(iangspt)) then
             density(INDEX)     = density(INDEX)     * CNTRST               
             density(INDEX + 1) = density(INDEX + 1) * CNTRST
C*** divide by ctf to 1st maximum
	    else
             IF(CNTRST .LT. 0.0) CNTRST = AMIN1(-CTFMINMOD,CNTRST)      
             IF(CNTRST .GE. 0.0) CNTRST = AMAX1(CTFMINMOD,CNTRST)
             density(INDEX)   = density(INDEX) / CNTRST
             density(INDEX+1) = density(INDEX+1) / CNTRST
	    end if 
C*** divide by ctf
           else if(ictf .eq. 3) then
            IF(CNTRST .LT. 0.0) CNTRST = AMIN1(-CTFMINMOD,CNTRST)      
            IF(CNTRST .GE. 0.0) CNTRST = AMAX1(CTFMINMOD,CNTRST)
            density(INDEX)   = density(INDEX) / CNTRST
            density(INDEX+1) = density(INDEX+1) / CNTRST
C*** phase flip
	   else
	    if(cntrst .lt. 0.0) cntrst = -1.0
	    if(cntrst .ge. 0.0) cntrst = 1.0
	    density(index) = density(index) * cntrst 
	    density(index+1) = density(index+1) * cntrst
           ENDIF
1400     CONTINUE
	if(itest.eq.1) then
C*** write test fft after ctf correction
	 call imopen(idevtest,'padcorcut.ctf.fft','unknown')
	 mxyz(1) = nxhalf
	 mxyz(2) = nytrans
	 mxyz(3) = 1
	 mode = 4
	 xorigin = 0.
	 yorigin = 0.
	 zorigin = 0.
         call icrhdr(idevtest,mxyz,mxyz,mode,title,-1)
	 call imposn(idevtest, 0, 0)
	 call ialorg(idevtest,xorigin,yorigin,zorigin)
	 do iy=nxhalf,nytrans
	  ixy = nxtrans * (iy - 1) + 1
	  call iwrlin(idevtest,density(ixy))
	 end do
	 do iy=1,nytrans/2
	  ixy = nxtrans * (iy - 1) + 1
	  call iwrlin(idevtest,density(ixy))
	 end do
         call iclcdn(density,nxhalf,nytrans,1,nxhalf,1,nytrans,
     *   dmin,dmax,dmean)       
         call iwrhdr(idevtest,title,-1,dmin,dmax,dmean)
	 call imclose(idevtest)
	 write(6,'('' back transform'')')
	end if
C******************************************************************
C*** back transform
C******************************************************************	
 1600    call todfft(density,nxtrans-2,nytrans,1)
C**************************************************************
C*** write test map but do not include 2 extra columns
C************************************************************** 
	if(itest .eq. 1) then
	 mxyz(1) = nytrans
	 mxyz(2) = nytrans
	 mxyz(3) = 1
	 mode = 1
	 call imopen(idevtest,'padcorcut.mrc.cor','unknown')
         call icrhdr(idevtest,mxyz,mxyz,mode,title,0)
         dmin = 1000000.
         dmax = -dmin
         dmean = 0.
	 do iy=1,nytrans
	  ixy = (iy - 1) * nxtrans
	  do ix=1,nytrans
	   den = density(ixy+ix)
	   dmin = min(den,dmin)
	   dmax = max(den,dmax)
	   dmean = dmean + den
	  end do
          call iwrlin(idevtest,density(ixy+1))
         end do
         dmean = dmean / float(nytrans * nytrans)
         call iwrhdr(idevtest,title,-1,dmin,dmax,dmean)
         call imclose(idevtest)
	end if
C**********************************************************
C*** extract map from padded image and write to disk
C**********************************************************
	 do iy=1,nyin
	  ixy = nxtrans * (istarty + iy - 1) + istartx
	  do ix=1,nxin
	   aline(ix) = density(ixy + ix - 1)
	  end do
	  call iwrlin(idevout,aline)
	 end do
	 call iclden
     *   (density,nxtrans,nytrans,1,nxtrans-2,1,nytrans,dmin,dmax,dmean)
	 if(dmin .lt. tmin) tmin = dmin
	 if(dmax .gt. tmax) tmax = dmax
	 tmean = tmean + dmean
	 write(6,'(''Final min,max,mean :'',3f12.5)') tmin,tmax,tmean
	end do
	tmean = tmean / float(nzin)
        call getdate(date,nsecs)
	mid1 = nint(dfmid1)
	mid2 = nint(dfmid2)
	if(ictf .eq. 1) then
         write(title,'(''CTF multiplied, defocus '',2i6,f6.2,1x,a)') 
     *   mid1, mid2, angast, date(5:24)
	else if(ictf .eq. 2) then
         write(title,'(
     *   ''CTF divided/multiplied, defocus '',2i6,f6.2,1x,a)') 
     *   mid1, mid2, angast, date(5:24)
	else if(ictf .eq. 3) then
         write(title,'(''CTF divided, defocus '',2i6,f6.2,1x,a)') 
     *   mid1, mid2, angast, date(5:24)
	else
         write(title,'(''Phases flipped, defocus '',2i6,f6.2,1x,a)') 
     *   mid1, mid2, angast, date(5:24)
	end if
	write(6,'(a)') title
        call iwrhdr(idevout,title,0,tmin,tmax,tmean)
        call imclose(idevout)
	write(6,'(/''***** Normal termination. *****''/)')
        end

