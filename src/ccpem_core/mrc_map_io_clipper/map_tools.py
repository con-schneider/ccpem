#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
# Class for map data object (using cmaplib via clipper)

import sys
import numpy as np
from PIL import Image, ImageOps
from ccpem_core import ccpem_utils
# XXX Temp. fix for OS X build
try:
    from ccpem_core.mrc_map_io_clipper.ext import mrc_map_io_clipper as mrc_map_io_clipper_ext
except:
    ImportError
import read_mrc_header

class ClipperXMap(object):
    '''
    XMap = Xray map w/ symmetry, infinite volume
    '''
    def __init__(self, filename):
        self.filename = filename
        self.clipper_xmap = mrc_map_io_clipper_ext.XMap(filename)

    def print_stats(self):
        '''
        Print map stats
        '''
        ccpem_utils.print_sub_sub_header(message = 'Map data statistics')
        print '  Filename     : {0}'.format(self.filename)
        print '  Map min      : {0:8.2f}'.format(self.clipper_xmap.GetMin() )
        print '  Map max      : {0:8.2f}'.format(self.clipper_xmap.GetMax() )
        print '  Map mean     : {0:8.2f}'.format(self.clipper_xmap.GetMean() )
        print '  Map stdev    : {0:8.2f}'.format(self.clipper_xmap.GetStdDev() )
        print '  SG number    : {0:8d}'.format(self.clipper_xmap.GetSpaceGroupNumber() )
        print '  SG symbol    : {:>8}'.format(self.clipper_xmap.GetSpaceGroupHMSymbol() )
        unit_cell =  self.clipper_xmap.GetUnitCell()
        print '  Unit cell    : {0[0]:8.2f} {0[1]:5.2f} {0[2]:5.2f} {0[3]:5.2f} {0[4]:5.2f} {0[5]:5.2f}'.format(unit_cell)
    
    def write_xmap(self, filename_out = None):
        '''
        Output map (mrc format)
        '''
        if filename_out == None:
            filename_out = self.filename + '_x1.map'
        self.clipper_xmap.WriteMap(filename_out)

class ClipperNXMap(object):
    '''
    Non xray map w/out symmetry, finite volume
    '''
    def __init__(self, filename, is_image_stack=False, is_volume_stack=False):
        self.filename = filename
        self.is_image_stack = is_image_stack
        self.is_volume_stack = is_volume_stack
        self.clipper_nxmap = mrc_map_io_clipper_ext.NXMap(filename)
        self.mrc_map_header_data = read_mrc_header.MRCMapHeaderData(filename = self.filename)
        # Estimate if volume or stack
        # N.B. SG = 0 for image stacks. N.B. not always observed
        if filename[-5:] == '.mrcs' or self.clipper_nxmap.GetSpaceGroupNumber() == 0:
            self.is_image_stack = True
        if not all(v == 1 for v in self.mrc_map_header_data.vx_vy_vz):
            self.is_volume_stack = True
        assert [self.is_volume_stack, self.is_image_stack].count(True) <= 1

    def print_stats(self):
        '''
        Print map stats
        '''
        ccpem_utils.print_sub_sub_header(message = 'Map statistics')
        print '  Filename     : {0}'.format(self.filename)
        print '  Map min      : {0:8.2f}'.format(self.clipper_nxmap.GetMin() )
        print '  Map max      : {0:8.2f}'.format(self.clipper_nxmap.GetMax() )
        print '  Map mean     : {0:8.2f}'.format(self.clipper_nxmap.GetMean() )
        print '  Map stdev    : {0:8.2f}'.format(self.clipper_nxmap.GetStdDev() )
        print '  SG number    : {0:8d}'.format(self.clipper_nxmap.GetSpaceGroupNumber() )
        print '  SG symbol    : {:>8}'.format(self.clipper_nxmap.GetSpaceGroupHMSymbol() )
        print '  Is stack     : {:>8}'.format(str(self.is_image_stack))
        unit_cell =  self.clipper_nxmap.GetUnitCell()
        print '  Unit cell    : {0[0]:8.2f} {0[1]:5.2f} {0[2]:5.2f} {0[3]:5.2f} {0[4]:5.2f} {0[5]:5.2f}'.format(unit_cell)
        if self.is_image_stack:
            print '  Image dims   : {0[0]:8d} {0[1]:5d}'.format(self.clipper_nxmap.GetGrid())
            print '  Num images   : {0[2]:8d}'.format(self.clipper_nxmap.GetGrid())
        elif self.is_volume_stack:
            print '  Num volumes  : {0[2]:8d}'.format(self.mrc_map_header_data.vx_vy_vz)
        else:
            print '  Grid dims    : {0[0]:8d} {0[1]:5d} {0[2]:5d}'.format(self.clipper_nxmap.GetGrid())

    def get_numpy_map_array(self):
        '''
        Return whole map as array
        '''
        return np.array(self.clipper_nxmap.GetMapArray())

    def get_numpy_map_sector(self, n_slice = 0):
        '''
        Return single sector of map e.g. image in image stack
        '''
        grid = self.clipper_nxmap.GetGrid()
        # Must declare array of known size first, note x = grid[1]
        inplace_array = np.empty((grid[1], grid[0]), 'f')
        # Fil array data inplace
        self.clipper_nxmap.GetMapSectorNumpy(inplace_array, n_slice)
        return inplace_array

    def get_numpy_volume_stack(self, stack_number):
        '''
        Return volume from volume stack
        '''
        assert stack_number > 0
        if self.is_volume_stack != True:
            print "  Warning map is not a volume stack"
        if self.mrc_map_header_data.vx_vy_vz[0] > 1 \
          or self.mrc_map_header_data.vx_vy_vz[1] > 1:
            print "  Warning only volumes stacked in z dimension are supported"
        if stack_number >= self.mrc_map_header_data.vx_vy_vz[2]:
            print "  Map does not contain requested volume stack"
        mz = self.mrc_map_header_data.mx_my_mz[2]
        min_slice = 1 + ( (stack_number-1) * mz)
        max_slice = (mz) + ((stack_number-1) * mz)
        map_array = np.array(self.clipper_nxmap.GetMapSector(min_slice))
        for n_z in xrange(min_slice, max_slice):
            map_array = np.dstack((map_array, self.clipper_nxmap.GetMapSector(n_z)))
        return map_array

    def get_pil_image_array_from_stack(self, auto_contrast=True,
                                             slice_list=None):
        pil_image_list = []
        if slice_list == None:
            num_stack_images = self.clipper_nxmap.GetGrid()[2]
            slice_list = list(xrange(num_stack_images))
        for n in slice_list:
            image_array = self.clipper_nxmap.GetMapSector(n)
            image_array = np.array(image_array)
            # mode = F by default (floating point)
            # convert to mode = 'L' as b/w micrographs and allows extended 
            #   image format options e.g. png, jpg
            im = Image.fromarray(image_array)
            im = Image.fromarray(image_array).convert('L')
            # e.g. for better viewing
            if auto_contrast:
                im_auto_contrast = ImageOps.autocontrast(im)
            pil_image_list.append(im_auto_contrast)
        return pil_image_list
    
    def write_nxmap(self, filename_out = None):
        '''
        XMap output (mrc format)
        '''
        if filename_out == None:
            filename_out = self.filename + '_nx1.map'
        self.clipper_nxmap.WriteMap(filename_out)

def write_map(filename, cell_a, cell_b, cell_c, map_data):
    mrc_map_io_clipper_ext.ExportNXMap(filename,
                                       cell_a,
                                       cell_b,
                                       cell_c,
                                       map_data)
