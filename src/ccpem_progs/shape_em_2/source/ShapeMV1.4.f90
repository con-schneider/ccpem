!=******************************************************************************
!=* ShapeEMv1.4.f90                  		          AUTHOR: A.ROSEMAN    *
!=*                                    		                               *
!=*									       *
!=* ShapeEM - Programs		       					       *
!=*									       *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
!=* written by Alan Roseman at the University of Manchester, 2013. It is based *
!=* on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007).     *
!=* It uses the same FLCF algorithm to efficiently calculate correlation       *
!=* coefficients over a defined 3D mask boundary around the search object,     *
!=* as in DockEM.							       *
!=*									       *
!=*    Copyright (C) 2013 The University of Manchester 		    	       *
!=*                                                                   	       *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 								               *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!
!
!ShapeMv1.1, amr 8/5/15 handle smaller sobj and mask
!v1.2 change cx,cy,cz to absolute centre in the std unit cell


!ShapeMv1.0

!ShapeMv1.0
!v1.3c shift cccmap, circular

!v1.4, padding to eliminate border artefacts

! to add in still: writemrcmap2(), writes part of map, without the borde

! 1. read map, search object, mask, angles
! 2. rotate sobj,mask, calc flcf, store max, loop over angles
! 3. output: cccmap, psimap, phimap, thetamap

!C input: spider angles file, 'angles.spi'
!C       Three digit run version key.
!C      Angular sampling for psi, degrees.


        Module image_arrays2
        real, dimension (:,:,:), allocatable :: map, rotsobj, rotmask,aa             
		real, dimension (:,:,:), allocatable :: lcf1,cnv2
		real meanmap,meanaa,meanmask,stdmap,stdaa,stdmask,meanrot,stdrot
        End Module image_arrays2



        program ShapeEM
  
  
        Use  image_arrays2
        Use  mrc_image
        real, dimension (:,:,:), allocatable :: cccmaxmap,psimap,phimap,thetamap,scalemap
        real, dimension (:,:,:), allocatable :: sobj,mask,lcfmask,pmask,commap,cmap
     
     	
	real angles(99999,3),psi,theta,phi,val,STD
	integer b,nb
	
	integer runcode,err
	integer nm,ni,cxyz(3)
	integer a,numangles,ji1,ji2
	integer counter,counter2,acount,bcount
	
	
	character*80 filename,sobjfilename,maskfilename,anglesfile,junk,pdbfile
        character*81 line
        character*24 space1
        character*6 head
        character*10 space2
        character*2 element
	character*3 num
	logical t,f
	real x,y,z,maskth
	integer lx,ly,lz,bnx,bny,bnz,pdx,pdy,pdz
	integer nm8,numatoms,pdnxp1,pdnxm1,pdnym1,pdnzm1,pdnxp2
	real*8 summap,sumcccs,sumnumccc,stdccc
	real cx,cy,cz
	real deg,rad
	integer ix,iy,iz,vec(3)
	real s1,s2,s3,iscale,omst,domega
	integer sloop,ssteps
	real comx,comy,comz,r
	integer i,j,k,comix,comiy,comiz
        character*2 box
	
	t=.true.
	f=.false.

        print*, 'ShapeM, V1.4d'
        print*, '============='


	print*,'Enter the EM map filename.'
	read*,filename
	
	print*,'Enter the sampling of the map (A/pixel)'
	read*, sampling
	
	print*,'Enter the search object map filename.'
	read*,sobjfilename
	
	print*,'Enter the mask map filename.'
	read*,maskfilename
	
	print*,'Enter a threshold for the mask map.'
	read*,maskth
	
	print*,'Enter the angles file (spider format).'
	read*,anglesfile
	
	print*,'Enter the increment for the angular omega search.'
	read*,domega
	
        write (6,*) 'Enter a run code, for output files.'
        read (5,30) runcode
30      format (I3)

! for parallel operation: enter the angle file range.
! GUI will allow easy, flexible way to submit range of jobs,
! and then combine the outmaps.

        pdbfile = 'com'//num(runcode)//'.pdb'       

33	print*,'Origin of rotation at Center-of-mass of search object, or the Box center.  Type> C or B'
	read*,box
	if (((box.ne.'C') .and. (box .ne.'B')) .and. ((box.ne.'c') .and. (box .ne.'b'))) goto 33	
	
        

! pdb file com used to record origin of rotations.
 

!	print*,'Enter the scale factor range, and step.'
!	read*,s1,s2,s3
!	print*,s1,s2,s3
        s1=1
        s2=1
        s3=1	


!C ***********************************************
!C open input from MRC format file
        
        CALL IMOPEN(1,filename,'RO')
        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

	nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1

        bnx=nx
        bny=ny
        bnz=nz

        nxp1=nx+1
        nxp2=nx+2


        CALL IMOPEN(2,sobjfilename,'RO')
        CALL IRDHDR(2,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	lx=nx
	ly=ny
	lz=nz
        
        pdx = bnx + lx
  	pdy = bny + ly
	pdz = bnz + lz

        pdnxp1 = pdx+1
        pdnxp2 = pdx+2
        pdnxm1 = pdx-1
  	pdnym1 = pdy-1
	pdnzm1 = pdz-1

!C allocate  image_files 
        allocate (map(0:pdnxp1,0:pdnym1,0:pdnzm1), sobj(0:pdnxp1,0:pdnym1,0:pdnzm1))
        allocate (mask(0:pdnxp1,0:pdnym1,0:pdnzm1),pmask(0:pdnxp1,0:pdnym1,0:pdnzm1))
        allocate (commap(0:pdnxp1,0:pdnym1,0:pdnzm1),cmap(0:pdnxm1,0:pdnym1,0:pdnzm1))
        
        allocate (rotsobj(0:pdnxp1,0:pdnym1,0:pdnzm1),rotmask(0:pdnxp1,0:pdnym1,0:pdnzm1))
        allocate (AA(0:pdnxp1,0:pdnym1,0:pdnzm1))
	   
        map=0
        CALL READMRCMAP2(1,filename,map,pdx,pdy,pdz,bnx,bny,bnz,T,F,T,err) !read, is open, dont close, add 2 bytes


        sobj=0

        CALL READMRCMAP2(2,sobjfilename,sobj,pdx,pdy,pdz,lx,ly,lz,F,T,T,err) !read, is open,  close, add 2 bytes 

        mask=0

        CALL IMOPEN(2,maskfilename,'RO')
        CALL IRDHDR(2,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        CALL READMRCMAP2(2,maskfilename,mask,pdx,pdy,pdz,lx,ly,lz,F,T,T,err) !read, is open,  close, add 2 bytes  
	

        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)


!make mask binary

! vs maskth
	where (mask.ge.maskth) 
			  pmask=1.0
	elsewhere
			pmask=0.0
	endwhere
	
	nm = int(sum(pmask(0:nxp1,0:nym1,0:nzm1)))			!number of non zero mask points

	sobj=sobj*pmask	  !this will speed up the rotations, uninteresting regions are now 0.
	
	print*, nm
	
! now get the com of sobj

        commap=sobj
        where (sobj.lt.0) commap=0  ! eliminate negative regions

! v1.3 on 27-5-15
	cx=int(lx/2)
	cy=int(ly/2)
	cz=int(lz/2)
	r=nx/2-1

! v1.3 on 27-5-15
!	zero corners
!	do I=0,nx-1
!	 do j=0,ny-1
!	   do k=0,nz-1
!	        if (radius(I,j,k,cx,cy,cz) .gt. r ) commap(i,j,k)=0.0
!	   enddo
!	 enddo
!	enddo

!       print*,"Mean of commap=",meanval(commap)
        print*,"Sum of commap=",sum(commap),minval(commap),maxval(commap)
    

	comx=0
	comy=0
	comz=0

!	forall (I=1:nx, j=1:ny,k=1:nz)
	do I=0,nx-1
	 do j=0,ny-1
	   do k=0,nz-1
 		comx = comx + commap(i,j,k) * (float(i))
 		comy = comy + commap(i,j,k) * (float(j))
 		comz = comz + commap(i,j,k) * (float(k))
!v1.2



	   enddo
	  enddo
	enddo
        comx=comx/sum(commap)
        comy=comy/sum(commap)
        comz=comz/sum(commap)





	if ((box.eq.'B') .or. (box .eq.'b')) then
		comx=cx
		comy=cy
		comz=cz
	endif

!save the com to pdbfile

	head = "ATOM  "
	element = "C "
	space1="   0001  CA  ALA C   1  "
	space2="          "

	open (1,file=trim(pdbfile),status='unknown', err=777)
	
	write(1,501) head,space1,comx*sampling,comy*sampling,comz*sampling,1.0,1.0,space2,element,"  "
   	
	501       FORMAT(A6,A24,3F8.3,2F6.2,A10,A2,A2)
      
 !      write(6,501) head,space1,X,Y,Z,occupancy,temperature,space2,element,charge
       		
       	write (1,501) "END  "
	close (1)
	



        PRINT*,'COM of sobj at ',comx,comy,comz,' pixels.'



!C ***************************************************


!c 2.   read angles
        open(2,file=anglesfile,status='old',err=902)
          read(2,*) junk

        a=0
!       do a=1,numangles
1999    	continue
        	a=a+1
                read(2,200,err=901,end=901) ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
200             format (i5,i2,3G12.5)
        	print*, ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
        goto 1999
!       enddo
901     continue
        close(2)
        numangles=a-1
        write(6,*) numangles,' angles read from ', anglesfile
        if (numangles.eq.0) STOP 'Angles file is null.'



!C ***********************************************
!C  set up and open  output image_files

        allocate (cccmaxmap(0:pdnxp1,0:pdnym1,0:pdnzm1),phimap(0:pdnxp1,0:pdnym1,0:pdnzm1))
        allocate (thetamap(0:pdnxp1,0:pdnym1,0:pdnzm1), psimap(0:pdnxp1,0:pdnym1,0:pdnzm1))
        allocate (cnv2(0:pdnxp1,0:pdnym1,0:pdnzm1),lcf1(0:pdnxp1,0:pdnym1,0:pdnzm1),scalemap(0:pdnxp1,0:pdnym1,0:pdnzm1))          

!C ***********************************************
	cccmaxmap = -1.2
	phimap=-999
	psimap=-999
	thetamap=-999
	summap=0
	sumcccs=0
	sumnumccc=0
	stdccc=0
	
! get number of +ve map points for norm of stds.
	where (map.gt.0.0)
		aa=1.0
	elsewhere 
		aa=0.0
	endwhere
	
	summap=sum(aa)	
	
!C 5. do the search
     
     
! set up ffts
! fft map, mapsq, 
! A,AA     
		print*, "001"
	!	nm8=nx*ny*nz
		nm8 =pdx*pdy*pdz
		print*,nm,nm8

		val=minval(map(0:nxm1,0:nym1,0:nzm1))
		print*,val
		map(0:nxm1,0:nym1,0:nzm1)=map(0:nxm1,0:nym1,0:nzm1)-val
!		map = map -val
		call msdset(map,pdx,pdy,pdz,nm8,val,err)
		print*,nm,nm8		
		call msd(map,pdx,pdy,pdz,nm8,meanmap,stdmap,err)
		PRINT*,'MSD t ',meanmap,stdmap
		
		print*,val
		AA=map*map
		call msd(AA,pdx,pdy,pdz,nm8,meanaa,stdaa,err)
		print*,nm8,meanaa,stdaa,err
		print*,NXP2,NX,NY,NZ
		print*,"hello"
		print*,pdNXP2,pdX,pdY,pdZ
                CALL BCKW_FT(map,pdNXP2,pdX,pdY,pdZ)
                print*,'ft map done'
		CALL BCKW_FT(AA,pdNXP2,pdX,pdY,pdZ)
		print*,'ft aa done'
	print*,'done fts'

	print*,nx,ny,nz
    	val=0
        counter = 0
        counter2 = 0
        acount=0
        
   !     comx=comx+cx
   !     comy=comy+cy       
   !     comz=comz+cz

!v1.3 make nearest integer   

	comx=int((comx/1)+.5)
	comy=int((comy/1)+.5)
	comz=int((comz/1)+.5)

	comix=int((comx/1)+.5)
	comiy=int((comy/1)+.5)
	comiz=int((comz/1)+.5)


	print*,comx,comy,comz

        do a = 1, numangles      
	        print*,'angle searching = ',a,' numangles = ',numangles,'nm=',nm
        	bcount = 0
       		omst=-((int(360./domega))/2)*domega
       		
		!do b=0,0
		nb=int(360/domega)
 !      		do b = omst,179.0,domega
 		do b=0,nb
        		bcount = bcount +1
       			 !psi = angles(a,1)
        		psi = b*domega+ omst
		        theta = angles(a,2)
       			 phi = angles(a,3)
       		
       		
!       			do iscale=s1,s2,s3
			ssteps=int((s2-s1)/s3)
			do sloop=0,ssteps
			iscale=sloop*s3+s1
       			print*,iscale,sloop,ssteps
			rotsobj=0
       			call rotobjSL(sobj,rotsobj,psi,theta,phi,pdx,pdy,pdz,comx,comy,comz,iscale,lx,ly,lz)
       		        rotmask=0
  		        call rotobjSL(pmask,rotmask,psi,theta,phi,pdx,pdy,pdz,comx,comy,comz,iscale,lx,ly,lz)
	
!make mask binary, get rid of feeble edge points
			where (rotmask.ge.0.5)  
					rotmask=1
				elsewhere
					rotmask=0
			endwhere
			
			
			nm = sum(rotmask(:,:,:))			!number of non zero mask points

       			rotsobj=rotsobj*rotmask			!get rid of ojbect points beyond the mask.
			call msd(rotmask,nx,ny,nz,nm8,meanmask,stdmask,err)
	
		ni=nx*ny*nz
	 	call msdset(rotsobj,pdx,pdy,pdz,nm,val,err)	!set the msd of sobj to 0,1
			
		rotsobj=rotsobj*rotmask				! zero background region
		
			
		call msd(rotsobj,pdx,pdy,pdz,nm,meanrot,stdrot,err)

 	  		    
    		    CALL BCKW_FT(rotsobj,pdNXP2,pdX,pdY,pdZ)
		    CALL BCKW_FT(rotmask,pdNXP2,pdX,pdY,pdZ)
	   
      		    call FLCF3D(pdx,pdy,pdz,nm) ! map, mapsq,     
				print*,'done flcf',b
				
				
				where (lcf1.gt.cccmaxmap)
					cccmaxmap=lcf1
					phimap=phi
					psimap=psi
  					thetamap=theta
  					scalemap=iscale
  				endwhere
  				sumcccs=sumcccs+sum(lcf1)
  				sumcccssq=sumcccssq+sum(lcf1*lcf1)
  				sumnumccc=sumnumccc+nm8
  		enddo
  		enddo
  	enddo
  	
                     
!C 6. write the searchmaps





	CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(7,1)
	CALL WRITEMRCMAP2(7,cccmaxmap,pdx,pdy,pdz,nx,ny,nz,sampling,err) !open already, close it, subtract 2 from nx

	CALL IMOPEN(8,'phimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(8,1)
        CALL WRITEMRCMAP2(8,phimap,pdx,pdy,pdz,nx,ny,nz,sampling,err)
	        
	CALL IMOPEN(9,'thetamap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(9,1)
	CALL WRITEMRCMAP2(9,thetamap,pdx,pdy,pdz,nx,ny,nz,sampling,err)

        CALL IMOPEN(10,'psimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(10,1)
	CALL WRITEMRCMAP2(10,psimap,pdx,pdy,pdz,nx,ny,nz,sampling,err)

	CALL IMOPEN(11,'scalemap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(11,1)
	CALL WRITEMRCMAP2(11,scalemap,pdx,pdy,pdz,nx,ny,nz,sampling,err)
	
	
	val=maxval(cccmaxmap(0:nxm1,0:nym1,0:nzm1))
	print*,'max ccc is :',val
	print*,'at ', maxloc(cccmaxmap)
	vec=maxloc(cccmaxmap)
	ix=vec(1)-1
	iy=vec(2)-1
	iz=vec(3)-1
	print*,ix,iy,iz
	print*,'phi=',phimap(ix,iy,iz)
	print*,'theta=',thetamap(ix,iy,iz)
	print*,'psi=',psimap(ix,iy,iz)
	print*,'ccc=',cccmaxmap(ix,iy,iz)
	print*,'scale=',scalemap(ix,iy,iz)
	
	print*,'meanCCC = ',sumcccs/sumnumccc
	stdccc=((sumnumccc*sumcccssq)+(sumcccs*sumcccs))/(sumnumccc*sumnumccc)
	print*,'stdevCCC = ',stdccc
	

! write a shifted cccmaxmap
     
	cmap(0:pdnxm1, 0:pdnym1, 0:pdnzm1)  = cccmaxmap(0:pdnxm1, 0:pdnym1, 0:pdnzm1 )	
        cmap= cshift(cmap,shift=-comix,dim=1)
        cmap= cshift(cmap,shift=-comiy,dim=2)
        cmap= cshift(cmap,shift=-comiz,dim=3)

	cccmaxmap(0:nxm1, 0:nym1, 0:nzm1)  = cmap(0:nxm1, 0:nym1, 0:nzm1 )	

	CALL IMOPEN(7,'cccmaxmap-shifted'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(7,1)
	CALL WRITEMRCMAP2(7,cccmaxmap,pdx,pdy,pdz,nx,ny,nz,sampling,err) !open already, close it, subtract 2 from nx

	val=maxval(cccmaxmap(0:nxm1,0:nym1,0:nzm1))
        print*,"corrected max ccc =",val
 	print*, "at ", maxloc(cccmaxmap(0:nxm1,0:nym1,0:nzm1))
! maxloc doesn't recognise subregion
	ix=vec(1)-1
	iy=vec(2)-1
	iz=vec(3)-1
        print*,ix,iy,iz


	goto 999

!C ***********************************************

902     continue  
! err reading angles
        print*, 'error reading angles file.'
        stop
903     continue
!Cerr reading map
        print*,'error reading EM-map.'
        stop
777     continue
        print*,'error writing pdbfile.'
        stop
           
999    continue    

       call imclose(1)
  
       print*,'Program finished O.K.'

       STOP
              
       
       CONTAINS


	SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
 
        Use mrc_image

         INTEGER  X,Y,Z,err
         INTEGER  stream, IX,IY,IZ,xm1
         real, DIMENSION (0:X+1,0:Y-1,0:Z-1) :: map  
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
            
          if(.not. open) then
          	       CALL IMOPEN(stream,filename,'RO')
       		       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	  endif
            
         
      
      
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
       !     print*,ix
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
	if(add2) then  
     		       map(X+0,iy,iz)=0
      		       map(X+1,iy,iz)=0
        endif
        
!           print*,ix,iy,iz
350   CONTINUE

	if (close) then
		CALL IMCLOSE(stream)
		print*,'closed ',stream
	endif

      return
998   STOP 'Error on file read.'
     	
	END SUBROUTINE READMRCMAP
	

	SUBROUTINE READMRCMAP2(stream,filename,map,X,Y,Z,lx,ly,lz,open,close,add2,err)
 
        Use mrc_image

         INTEGER  X,Y,Z,err,lx,ly,lz
         INTEGER  stream, IX,IY,IZ,xm1
         real, DIMENSION (0:X+1,0:Y-1,0:Z-1) :: map  
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
            
          if(.not. open) then
          	       CALL IMOPEN(stream,filename,'RO')
       		       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	  endif
            
         map=0
      
      
!     read in file 

      DO 1350 IZ = 0,lz-1
          DO 1350 IY = 0,ly-1
            CALL IRDLIN(stream,ALINE,*1998)
            DO 1300 IX = 0,lx-1       
       !     print*,ix
                map(ix,iy,iz) = ALINE(IX+1)
1300         CONTINUE  
	if(add2) then  
     		       map(lX+0,iy,iz)=0
      		       map(lX+1,iy,iz)=0
        endif
        
!           print*,ix,iy,iz





1350   CONTINUE

	if (close) then
		CALL IMCLOSE(stream)
		print*,'closed ',stream
	endif

      return
1998   STOP 'Error on file read.'
     	
	END SUBROUTINE READMRCMAP2
	
	
       
      SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         

      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZM1
      DO 450 IY = 0,NYM1
            
            DO 400 IX = 0,NXM1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP
	
	
	      
      SUBROUTINE WRITEMRCMAP2(stream,map,x,y,z,lx,ly,lz,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz,lx,ly,lz
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage2'         

      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 1450 IZ = 0,lZ-1
      DO 1450 IY = 0,lY-1
            
	!    if (IY.gt.ly) cycle
	 !   if (IZ.gt.lz) cycle
            DO 1400 IX = 0,lx-1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

1400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
1450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
1999   STOP 'Error on file write2.'
 
      END SUBROUTINE WRITEMRCMAP2
	
	
	
	SUBROUTINE rotobj(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz)
	      

!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
	real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
        real cx,cy,cz,psi,phi,theta,x,y,z
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1
	real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
        real val,b
        
        therotobj=0
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))
        matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)


!	print*,matd


	do iz=0,nzm1
		do iy=0,nym1
			do ix=0,nxm1
			
				x=float(ix)
				y=float(iy)
				z=float(iz)
				
				val=obj(ix,iy,iz)
				if (val.gt.0.1) then			!if density is 0 don't waste time.
					call matmult2(x,y,z,matd,cx,cy,cz)  	!transform coords by rotn
	
	call interpo2(therotobj,nx,ny,nz,x,y,z,val)		!interpo density at new posn to new map.)
	
				endif
  			enddo
        	enddo
        enddo
	therotobj(nx:nx+1,0:ny-1,0:nz-1)=0
        return

	END SUBROUTINE rotobj


	subroutine matmult(mat1,mat2,mat)
!C multiply 2 matrices together
        real mat(3,3),mat1(3,3),mat2(3,3)

        mat=matmul(mat1,mat2)
!	print*,'mm'
        return 
        end subroutine matmult

        subroutine matmult2(x,y,z,mat,cx,cy,cz)
!c mult coords by transformation mat


        real mat(3,3),cx,cy,cz, x,y,z
        real coords(3),newcoords(3)

        coords(1) = x-cx
        coords(2) = y-cy
        coords(3) = z-cz

        newcoords=matmul(mat,coords)

        x = newcoords(1) + cx
        y = newcoords(2) + cy
        z = newcoords(3) + cz

        return
        end subroutine matmult2

		
	
	SUBROUTINE FLCF3D(x,y,z,nm)
	
	
	! in, ft of :map , mapsq, sobj, mask
	! out FLCF/cccmap
	use image_arrays2
	integer x,y,z,nm

	real*8 nm8
	real sd,masksd,val
	integer err
	integer ni
	
	ni=x*y*z
	nm8=nm

	call CORRELATION3D(rotmask,map,lcf1,x,y,z)

	call CORRELATION3D(rotmask,aa,cnv2,x,y,z)

	cnv2=(nm8*cnv2)
	cnv2=(cnv2-(lcf1*lcf1))/(nm8*nm8)
	
	call CORRELATION3D(rotsobj,map,lcf1,x,y,z)

	val=minval(cnv2)
!	print*,'min of v=',val

	where (cnv2.gt.0.00001)
			cnv2=sqrt(cnv2)

		elsewhere
			cnv2=0.
!			print*,'V was lt 0 !!'
	endwhere
	
	where (cnv2.gt.0)
			lcf1=(lcf1/cnv2)/nm8
	elsewhere
			lcf1=-2
	endwhere
	

	END SUBROUTINE FLCF3D
	
	
	subroutine CONVOLUTION3D(a,b,c,nx2,ny2,nz2)
        integer nx2,ny2,nz2,i,j,k
        real a(0:nx2+1,0:ny2-1,0:nz2-1),b(0:nx2+1,0:ny2-1,0:nz2-1),c(0:nx2+1,0:ny2-1,0:nz2-1)
        real n
        integer nxp2
	integer dx

        nxp2=nx2+2

        c=0
        
        do k=0,nz2-1
  	      do j=0,ny2-1
            	    do i=0,nx2+1,2
                
                        c(i,j,k)=a(i,j,k)*b(i,j,k)-a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)+a(i+1,j,k)*b(i,j,k)
               
                enddo
            enddo
       enddo

        CALL FORW_FT(c,NXP2,NX2,NY2,NZ2)

        n=nx2*ny2*nz2		
        C=C*N

        end subroutine CONVOLUTION3D


         subroutine CORRELATION3D(a,b,c,nx,ny,nz)

        integer nx,ny,nz
        real a(0:nx+1,0:ny-1,0:nz-1),b(0:nx+1,0:ny-1,0:nz-1),c(0:nx+1,0:ny-1,0:nz-1)
        real r1,r2,hp,lp,sampling,n
        integer nxp2
        integer i,j,k
  
	integer dx
	
        nxp2=nx+2
!        r1=hp/sampling
!       r2=lp/sampling
	dx=int(nx/2.)+1


        c=0
        do k=0,nz-1
       	    do j=0,ny-1
                do i=0,nx+1,2
                        !r=radft(i,j,ny)
                        !if ((r.gt.r1).and.(r.lt.r2)) then
                        c(i,j,k)=a(i,j,k)*b(i,j,k)+a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)-a(i+1,j,k)*b(i,j,k)
                        !endif
                enddo
            enddo
        enddo
        
        CALL FORW_FT(c,NXP2,NX,NY,NZ)
 
        n=nx*ny*nz					
        C=C*n

        end subroutine CORRELATION3D



      subroutine msdset(map,nx,ny,nz,nm,val,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
      real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th
      integer nm
      real nm8
     

   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
      nm8=nm
      
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))

      mean=lsum/nm8

	map8=map

       lsum = sum(map8(0:nxm1,0:nym1,0:nzm1))
        mean=lsum/nm8


	map8=map8*map8

       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
    
 	 lsum=lsum*lsum
 !	 sum_sqs=nm8*sum_sqs
 !	 sq=((sum_sqs-lsum))
 !	 sq=sq/(nm8*nm8)
  
          sq = sum_sqs - (nm8*mean*mean)

	  print*, sq,sum_sqs,mean,nm8,lsum

	  sq=sq/nm8
  
        print*,'sq=',sq
       if (sq.lt.0.0) stop 'sd lt zero in msd set...'

       th=0.000000000001
      if (sq.gt.th) then
                sd = sqrt(sq)
                map=(map-mean)/sd
                val= -mean/sd
                err=0
        elseif (sq.le.0.0) then
                err=1
                print*,'le0'
     		stop
        elseif (sq.le.th) then 
                map=(map-mean)
        	print*,'at threshold'
                val= -mean/sd
                err=0
		stop
        endif

       return
       end subroutine msdset

	SUBROUTINE MSD(map,nx,ny,nz,nm,val,STD,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
 	real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th,STD
      integer nm
      real nm8
     
   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
       nm8=nm
      SD =-9999999
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
  
	map8=map

       lsum = sum(map8(0:nxm1,0:nym1,0:nzm1))
       mean=lsum/nm8


	map8=map8*map8
        sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
 
 	 lsum=lsum*lsum
 	 sum_sqs=nm8*sum_sqs
 	 sq=((sum_sqs-lsum))
 	 sq=sq/(nm8*nm8)
    	
	

       if (sq.lt.0) stop 'sd lt zero in msd.'

       th=0.00001
      if (sq.gt.th) then
                sd = sqrt(sq)
                val= -mean/sd
                err=0
        elseif (sq.le.0) then
                err=1
                print*,'le0'
                stop
        elseif (sq.le.th) then 
                !map=(map-mean)
        	print*,'at threshold'
                err=0
		stop
        endif

   	VAL=MEAN
   	STD=SD
       return
       end subroutine MSD
	
	
	
	SUBROUTINE rotobjSL(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz,iscale,lx,ly,lz)
	
	
	    
!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
	real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
        real cx,cy,cz,psi,phi,theta,x,y,z,scx,scy,scz
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1,lx,ly,lz
	real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
        real val,b,iscale
        
        therotobj=0
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))
        matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)



!	print*,matd


	do iz=0,lz-1
		do iy=0,ly-1
			do ix=0,lx-1
			
				x=float(ix)*iscale
				y=float(iy)*iscale
				z=float(iz)*iscale
				scx=cx*iscale
				scy=cy*iscale
				scz=cz*iscale
				
				val=obj(ix,iy,iz)
				if (val.gt.0.1) then			!if density is 0 don't waste time.
					call matmult2(x,y,z,matd,scx,scy,scz)  	!transform coords by rotn
	
	call interpo2(therotobj,nx,ny,nz,x,y,z,val)		!interpo density at new posn to new map.)
	
				endif
  			enddo
        	enddo
        enddo
	therotobj(nx:nx+1,0:ny-1,0:nz-1)=0
        return

	END SUBROUTINE rotobjSL

    real function radius(x,y,z,cx,cy,cz)
    integer x,y,z
    real cx,cy,cz
    radius = sqrt (  (x-cx)**2 + (y-cy)**2 + (z-cz)**2  )
    end function radius

	end program ShapeEM
