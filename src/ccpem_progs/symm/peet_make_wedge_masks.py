#!/usr/bin/python
#
# Takes a .prm file in a directory in which PEET has run and uses
# this information to output missing wedge masks, appropriately
# oriented for each particle.

import os
import sys
import re
import subprocess 
import argparse


class HeaderClass:
    def __init__(self):
        self.pixSize = PointClass(0, 0, 0)
        self.dimensions = PointClass(0, 0, 0)

    def read_header(self, filename):
        pix_spacing_re = re.compile("\s+Pixel spacing \(Angstroms\)\.+\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)")
        dimensions_re = re.compile("\s+Number of columns, rows, sections \.\.\.\.\.\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)")

        commands = [os.environ["IMOD_DIR"]+'/bin/header', filename]
        process = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        try:
            process = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except:
            sys.stderr.write("ERROR: executing 'header': is it installed?\n\n" + " ".join(commands) + "\n\n")
            sys.exit(1)
        stdout, stderr = process.communicate()
        lines = stdout.rsplit("\n")
        for line in lines:
            m = pix_spacing_re.search(line)
            if m:
                self.pixSize = PointClass(float(m.group(1)), float(m.group(2)), float(m.group(3)))
            m = dimensions_re.search(line)
            if m:
                self.dimensions = PointClass(int(m.group(1)), int(m.group(2)), int(m.group(3)))


class ParticleClass:
    def __init__(self):
        self.originalVolume = ""


def parse_peet_line_elements(composite_elements):
    strip_space_re = re.compile("\s*(.*)\s*")
    strip_quotes_re = re.compile("'(.*)'")
    output = []
    elements = composite_elements.split(";")
    for element in elements:
        n = strip_space_re.match(element)
        element = n.group(1) 
        m = strip_quotes_re.match(element)
        if m:
            element = m.group(1)
        output.append(element)
    return output


def parse_tilt_ranges(tilt_ranges):
    tilt_ranges_re = re.compile('\[([0-9\.\-]*),\s*([0-9\.\-]*)\]')
    m = tilt_ranges_re.search(tilt_ranges)
    if m:
        return m.group(1), m.group(2)


# extracts the relevant information after a run from the .prm file of that run
def parse_prm(prm_filename):
    particles = []
    fn_volume_re = re.compile("fnVolume = \{([^}]+)\}")
    fn_output_re = re.compile("fnOutput = '([^\']*)'")
    tilt_range_re = re.compile("tiltRange = \{([^}]+)\}")
    search_radius_re = re.compile("searchRadius = \{([^}]+)\}")

    prm = open(prm_filename, 'r')

    line = prm.read()
    line = line.replace('\n', '')
    line = line.replace('],', '];')
    line = line.replace('\',', '\';')

    m = fn_output_re.search(line)
    fn_output = m.group(1)

    m = fn_volume_re.search(line)
    fn_volumes = parse_peet_line_elements(m.group(1))

    m = tilt_range_re.search(line)
    tilt_ranges = parse_peet_line_elements(m.group(1))

    if len(tilt_ranges) != len(fn_volumes):
        sys.stderr.write("ERROR: len(tiltRanges) != len(fnVolumes)!\n")
        sys.exit(1)
    for count in range(len(tilt_ranges)):
        particle = ParticleClass()
        particle.originalVolume = fn_volumes[count]
        min_tilt, max_tilt = parse_tilt_ranges(tilt_ranges[count])
        particle.min_tilt = min_tilt
        particle.max_tilt = max_tilt
        header = HeaderClass()
        header.read_header(particle.originalVolume)
        particle.box_size = header.dimensions.x
 
        particles.append(particle)
    
    m = search_radius_re.search(line)
    search_radius = parse_peet_line_elements(m.group(1))

    num_particles = len(particles)
    iterations = len(search_radius)
    for count in range(num_particles):
        particles[count].motl_file = fn_output+"_MOTL_Tom"+str(count+1)+"_Iter"+str(iterations)+".csv"

    return fn_output, particles, iterations


class PointClass:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('prm_file', help=".prm file from completed PEET run.", type=str)

    args = parser.parse_args()

    if not os.path.isfile(args.prm_file):
        sys.stderr.write("ERROR: " + args.prm_file + " does not exist!\n")
        sys.exit(1)

    fn_output, particles, iterations = parse_prm(args.prm_file)

    count = 1
    for particle in particles:
        path = os.path.dirname(os.path.realpath(__file__))
        os.system(path+"/make_wedge_mask.py -- " + particle.min_tilt + " " + particle.max_tilt + " " +
                  str(int(particle.box_size)) + "  unoriented_wedgeMask_Tom"+str(count)+".mrc")
        os.system(path + "/peet_apply_motl.py unoriented_wedgeMask_Tom" + str(count) + ".mrc " + particle.motl_file +
                  " 0 wedgeMask_Tom" + str(count) + ".mrc -e ZXZ")
        # os.system("rm .temp.mrc")
        count += 1

if "IMOD_DIR" not in os.environ:
    sys.stderr.write("ERROR: Need 'IMOD_DIR' defined as an environment variable!\n")
    sys.exit(1)


if __name__ == "__main__":
    main()
