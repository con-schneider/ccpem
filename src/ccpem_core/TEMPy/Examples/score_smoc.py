'''
USE THIS SCRIPT TO GENERATE LOCAL CROSS CORRELATION SCORES
E.g. : Initial and output models from FLEX_EM refinement can be scored against the target map.
OUTPUTS local scores and PDB with B-factor records replaced with SMOC scores (CAN BE COLORED IN CHIMERA)
NOTE: that for models with both protein and nucleic acids, the range of local cross correlation values differs for protein and nucleic acid contents and their scores need to be analysed separately.
*NOTE: Residues whose smoc score calculation fails gets a score of 0.0
NOTE: For multiple model inputs, make sure that they have same chain IDs for comparison
'''

import sys
import os
import json
import pickle
from TEMPy.MapParser import MapParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
from TEMPy.ScoringFunctions import ScoringFunctions
import numpy as np

def main():
    #
    ccpem_mode = False

    if len(sys.argv) == 1:
        #PARAMETERS TO CHANGE
        #fitted models
        list_to_check = ['pdb5fuu.ent']
        #map file
        map_file = "emd_3308.map"
        #directory with the models and map
        DATADIR = os.path.join(os.getcwd(),"Test_Files")
        #map resolution
        res_map = 4.2
    else:
        # For CCP-EM pass inputs as json file
        try:
            with open(sys.argv[1]) as json_file:
                args = json.load(json_file)
                map_file = args['map_path']
                res_map =  args['map_resolution']
                list_to_check = args['input_pdbs']
                if not isinstance(list_to_check, list):
                    list_to_check = [list_to_check]
                ccpem_mode = True
                DATADIR = None
                os.chdir(args['job_location'])
        except IOError:
            map_file=os.path.basename(os.path.abspath(sys.argv[1]))
            list_to_check = [os.path.basename(os.path.abspath(sys.argv[2]))]
            res_map = float(sys.argv[3])
            DATADIR = os.path.split(os.path.abspath(sys.argv[1]))[0]

    #score window
    win=9
    #sigma coefficient for generating gaussian density
    sim_sigma_coeff = 0.187

    #change these in case of multiple fitted models with single chain
    #in such cases smoc plots are generated
    #labels for the models
    list_labels = ['initial','final']#['initial','final']
    list_styles = [':','-.','--','-','-']#'--'
    #index of model to check z-scores
    z_score_check = 2

    #----------------------------
    start_pdb = list_to_check[0]
    iter_num = len(list_to_check)
    intermed_file = ""
    slow = 0.20
    shigh = 0.20 # fraction of structure fitted reasonably well initially
    #rigid body file
    rigidbody_file = None#
    blurrer = StructureBlurrer()
    sc = ScoringFunctions()
    #read map file
    if ccpem_mode:
        emmap = MapParser.readMRC(args['map_path'])
    else:
        emmap = MapParser.readMRC(os.path.join(DATADIR,map_file))

    #-----------------------------
    #set plotting parameters
    flagplot = 1
    try:
        import matplotlib
    except ImportError:
        flatplot = 0

    if flagplot == 1:
        print 'Setting maptpltlib parameters'
        try:
            ##matplotlib.use('Agg')
            try:
                import matplotlib.pyplot as plt
            except ImportError:
                flagread = 0 
            
            legendlist = ['iter_0']
            ###colorlist = ['darkcyan','brown','gray','darkgreen','darkkhaki','coral','indigo','maroon']#,'darkkhaki'
            colormap = plt.cm.Spectral#Set1,Spectral#YlOrRd,Spectral,BuGn,Set1,Accent,spring
            plt.gca().set_color_cycle(
                [colormap(i) for i in np.linspace(0, 1, iter_num+1)])
            axes = plt.gca()
            plt.gca().set_ylim([0.0,1.0])
            params = {'legend.fontsize': 18}#,'legend.markerscale': 5}
            plt.rcParams['figure.figsize'] = [12, 5]
            plt.rcParams.update(params)
            plt.rcParams.update({'font.size': 18})
            plt.xlabel('Residue Num', fontsize=15)
            plt.ylabel('SMOC',fontsize=15)
            #plt.rcParams['figure.figsize'] = 10, 5
            #plt.figure(figsize=(12,5))
        except:
            flagplot = 0

    #------------------------------
    #get scores for start pdb
    rfilepath = rigidbody_file
    dict_chains_scores = {}
    if rigidbody_file is not None: rfilepath = os.path.join(DATADIR,rigidbody_file)
    #--------------------------------
    list_zscores = []
    rerun_ct = 0
    flag_rerun = 0
    it = 0
    dict_reslist = {}

    #flex_em run iter
    while iter_num > 0:
        #if it > 0:
        #  prevratio = avghigh/avglow
        #if it > 1: score_inc_prev = score_inc[:]
        if DATADIR is not None:
            os.chdir(DATADIR)
        out_iter_pdb = list_to_check[it]
        print 'PDB : ', out_iter_pdb
        structure_instance = None

        if DATADIR is not None:
            if os.path.isfile(os.path.join(DATADIR,out_iter_pdb)):
                #read pdb
                structure_instance=PDBParser.read_PDB_file(
                    'pdbfile',
                    os.path.join(DATADIR,out_iter_pdb),
                    hetatm=False,
                    water=False)
        else:
            structure_instance = PDBParser.read_PDB_file(
                'pdbfile',
                out_iter_pdb,
                hetatm=False,
                water=False)

        # Get SMOC scores
        if structure_instance is not None:
            dict_ch_scores, dict_chain_res = sc.SMOC(
                emmap,
                res_map,
                structure_instance,
                win,
                rfilepath,
                sim_sigma_coeff)

        for ch in dict_ch_scores:
          flagch = 1
          dict_res_scores = dict_ch_scores[ch]
          #get res number list (for ref)
          if it == 0:
            dict_reslist[ch] = dict_chain_res[ch][:]
          try: 
            if len(dict_reslist[ch]) == 0: 
              print 'Chain missing:', out_iter_pdb, ch
              flagch = 0
              continue
          except KeyError: 
            print 'Chain not common:', ch, out_iter_pdb
            flagch = 0
            continue
          try: reslist = dict_reslist[ch]
          except KeyError:
            print 'Chain not common:', ch, out_iter_pdb
            flagch = 0
            continue
          if not ch in dict_chains_scores: dict_chains_scores[ch] = {}
          scorelist = []
          for res in reslist:
            try: scorelist.append(dict_res_scores[res])
            except KeyError: 
              if reslist.index(res) <= 0: scorelist.append(dict_res_scores[reslist[reslist.index(res)+1]])
              else: 
                try:  scorelist.append(dict_res_scores[reslist[reslist.index(res)-1]])
                except IndexError: scorelist.append(0.0)
            #save scores for each chain
            curscore = "{0:.2f}".format(round(scorelist[-1],2))
            try: 
              dict_chains_scores[ch][res][it] = str(curscore)
            except KeyError: 
              dict_chains_scores[ch][res] = [str(0.0)]*len(list_to_check)
              dict_chains_scores[ch][res][it] = str(curscore)
              
          #calc ratio between current and prev scores
          if it > 0:
            score_cur = scorelist[:]
            score_inc = [(1+x)/(1+y) for x, y in zip(score_cur, score_prev)][:]
            score_diff = [(x-y) for x, y in zip(score_cur, score_prev)][:]
          #calculate z-scores
          npscorelist = np.array(scorelist)
          try: list_zscores.append((npscorelist-np.mean(npscorelist))/np.std(npscorelist))
          except: list_zscores.append((npscorelist-np.mean(npscorelist)))
          #calculate low and high score bounds
          list_sccc = scorelist[:]
          score_prev = scorelist[:]
          list_sccc.sort()
        
          #save avg of highest and lowest 20%  
          avglow = list_sccc[int(len(list_sccc)*slow)]
          if avglow == 0.0: avglow = 0.00001
          avghigh = list_sccc[int(len(list_sccc)*(1-shigh))]
          if it == 0: avghigh1 = list_sccc[int(len(list_sccc)*(1-shigh))]
          curratio = avghigh/avglow
          '''
          #modify rigid bodies if
          if abs(avghigh/avglow) > 1.0:
            goodset = []
            badset = []
            intrmset = []
            for l in range(len(scorelist)):
              # get indices of good and bad sets based on score
              if scorelist[l] > avghigh1: goodset.append(l)
              elif scorelist[l] < avglow: badset.append(l)
              else: intrmset.append(l)
          else:
            goodset = range(len(scorelist))
          '''
          print list_to_check[it],ch, 'avg-top20%, avg-low20%, avg-high/avg-low', avghigh, avglow, avghigh/avglow
          print list_to_check[it],ch, 'avg', sum(scorelist)/len(scorelist)
        
          if len(list_to_check) > 0 and len(dict_reslist) == 1 and flagplot == 1:
            plt.plot(reslist,scorelist,linewidth=3.0,label=list_labels[it],linestyle=list_styles[it])

        #include smoc scores as b-factor records
        for x in structure_instance.atomList:
          cur_chain = x.chain
          cur_res = x.get_res_no()
          if not dict_reslist.has_key(cur_chain): continue
          if dict_chains_scores.has_key(cur_chain):
            try: x.temp_fac = dict_chains_scores[cur_chain][cur_res][it]
            except KeyError, IndexError: 
              print 'Residue missing: ',cur_res, ch, out_iter_pdb 
              x.temp_fac = 0.0
          else:
            x.temp_fac = 0.0
          
        #write out pdb file with scores as bfactor records
        pdbid = out_iter_pdb.split('.')[0]
        structure_instance.write_to_PDB(os.path.join(DATADIR,pdbid+"_sc.pdb"))
        it = it+1
        iter_num = iter_num-1

        #legendlist.append('iter_'+`it`)
    #-------------------------------------
    ##plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
    #set legend parameters and plot
    if len(list_to_check) > 0 and len(dict_reslist) == 1 and flagplot == 1:
      leg = plt.legend(loc='lower right')
      for legobj in leg.legendHandles:
        legobj.set_linewidth(2.0)
      plt.show()
    
    #-------------------------------------
    '''
    plt.xlabel('Residue Num', fontsize=15)
    plt.ylabel('Z-score',fontsize=15)
    itns = [z_score_check]
    for itn in itns:
      plt.plot(reslist,list_zscores[itn-1],linewidth=3.0,label=list_labels[itn],linestyle=list_styles[itn],color=colorlist[itn])
    leg = plt.legend(loc='lower right')
    for legobj in leg.legendHandles:
      legobj.set_linewidth(2.0)
    plt.show()
    '''
    for ch in dict_chains_scores:
        ch1 = ch
        if ch == ' ': ch1 = ''
        if ccpem_mode:
            scoreout = os.path.join(args['job_location'],
                                    'smoc_'+ch1)
        else:
            scoreout = os.path.join(DATADIR,"smoc_"+ch1)
        sco = open(scoreout,'w')
        sco.write("Res_num\t"+'\t'.join(list_to_check)+"\n")
        for res in dict_reslist[ch]:
            sco.write(str(res)+"\t"+'\t'.join(dict_chains_scores[ch][res])+"\n")
        sco.close()

if __name__ == '__main__':
    main()
