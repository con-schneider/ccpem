#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---For Option 1---

# (i)  will read in an alignment and a structure file
# (ii) will create a specified number of homology models

from modeller import *
from modeller.scripts import complete_pdb
from modeller.automodel import *

import sys

#--------------------------------------------------------------------------------------
#---------------read in data via keyboard----------------------------------------------
#--------------------------------------------------------------------------------------

# read in number of models to create from keyboard:
if len(sys.argv) != 2:  
    print "usage: filename.py [number of models]"
    sys.exit()

# alignment file is named alignment.ali

no_models       = int(sys.argv[1])              # number of models to create


#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---start homology modelling process
env = environ()

#---create number of specified models via automodel()---------------------
a = automodel(env, alnfile='alignment.ali',                 #---Alignment file should be named "alignment.ali" by the preprocess script "isALIGNMENT.py"
              knowns='template', sequence='target',         #---the structure file should be named "template.pdb" the target sequence file "target.fa" by the webserver
              assess_methods=(assess.normalized_dope))      #---score the created models according to their normalized_dope score
a.starting_model = 1
a.ending_model = no_models                                  #---number of models to create
a.make()


#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---------------(i)   get list with successfully built models--------------------------
#---------------(ii)  rank the models via Normalized_Dope score and others-------------
#---------------(iii) store top scored model in file Top_model-------------------------

# Get a list of all successfully built models from a.outputs
ok_models = filter(lambda x: x['failure'] is None, a.outputs)

# Sort the models by Normalized_Dope score
key = 'Normalized DOPE score'                                                                
ok_models.sort(lambda a,b: cmp(a[key], b[key]))                                     

# Get the best model (lowest Normalized_Dope score) and store it in 'Top_model' file
m = ok_models[0]
##print "Top model: %s (DOPE score %.3f)" % (m['name'], m[key])                       
f = open("Top_model.txt","w")
f.write(m['name'])
f.close()

# store ranked list of models in file "Models_summary"
# and store names of produced models in File "Model_names"
g = open("Models_summary.txt","w")
g.write("Filename Normalized_DOPE\n")
i = 0
while i < len(ok_models):
    mn = ok_models[i]
    g.write("%s %f\n" % (mn['name'], mn[key]))
    i += 1

g.close()

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
