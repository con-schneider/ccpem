#!/usr/bin/env python
# coding: utf-8

import argparse
import glob
import os
import subprocess
import random
import re
import sys
import math
import numpy as np

from make_autocorr_samples import main
from npy2mrc import save_npy_as_mrc_file

'''
Extracts a number of random subvolumes from a tomogram, randomly rotates them, then runs the sampling and 
autocorrelation functions on the subvolumes, and determines the average noise profile and the standard deviations of the
noise profile.
'''


class MRCHeaderClass:

    def __init__(self):
        self.rotation_angle = 0
        self.columns = 0
        self.rows = 0
        self.sections = 0
        self.pixelSize_nm = 0.

    def read(self, filename):
        rotation_angle_re = re.compile("Tilt axis rotation angle =\s+([0-9\-\.]+)")
        dimensions_re = re.compile("Number of columns, rows, sections .....\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)")
        pixel_size_re = re.compile(" Pixel spacing \(Angstroms\)\.+\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)")

        output = subprocess.Popen(["header", filename], stdout=subprocess.PIPE).communicate()[0].split("\n")
        for line in output:
            m = rotation_angle_re.search(line)
            if m:
                self.rotation_angle = float(m.group(1))

            m = dimensions_re.search(line)
            if m:
                self.columns = int(m.group(1))
                self.rows = int(m.group(2))
                self.sections = int(m.group(3))

            m = pixel_size_re.search(line)
            if m:
                pixel_size_x = m.group(1)
                pixel_size_y = m.group(2)
                pixel_size_z = m.group(3)
                if pixel_size_x != pixel_size_y:
                    sys.stderr.write("ERROR: Pixel sizes different for different axes for "+filename+"! Aborting!\n")
                    sys.exit(1)
                self.pixelSize_nm = float(pixel_size_x) / 10.0


class ArrayWelford:

    def __init__(self, dtype=np.float64):
        self.mean = 0  # running mean
        self.s = 0  # variance times n-1
        self.k = 0  # counter
        self.dtype = dtype  # float32 can cause inaccuracies, therefore float64 is recommended

    def update(self, array):  # presumes that all arrays are same size

        self.k += 1
        farray = np.asfarray(array, dtype=self.dtype)

        if self.k == 1:
            self.mean = farray

        prev_mean = np.copy(self.mean)
        self.mean = prev_mean + np.divide((farray - self.mean), self.k)

        self.s = self.s + np.multiply((farray - self.mean), (farray - prev_mean))

    def variance(self):
        return np.divide(self.s, (self.k - 1))

    def stddev(self):
        var = self.variance()
        return np.sqrt(var)


def generate_profiles(ifile, xdimension, ydimension=None, zdimension=None, number=250, avgfile=None,
                      stdfile=None, seed_random=False, method="spectral", non_autocorrelate=False):

    # Program to characterise the noise profile of the tomograms. The noise profiles can be used to normalise
    # the output of the getSymmetries.py program.

    if seed_random:  # for unittesting: ensure that the same values are picked every time
        random.seed(seed_random)

    if not ydimension:
        ydimension = xdimension

    if not zdimension:
        zdimension = xdimension

    # initialise welford algorithm
    welford = ArrayWelford()

    for i in range(number):
        o1 = "box"+str(i) + ".mrc"
        o2 = "normbox"+str(i) + ".mrc"

        # Extract volumes form random locations in tomogram
        print "Extracting box %d..." % (i + 1)
        extract_box_function(ifile, o1, xdimension, ydimension, zdimension)

        # Normalise volumes
        command = 'newstack', '-mea', '0,1', str(o1), str(o2)
        p = subprocess.Popen(command, stdout=subprocess.PIPE)
        p.communicate()

        # Detect symmetries
        print "Generating angular power spectrum..."
        freq_box = main(o2, for_get_noise=True, method=method, verbose=False, no_autocorrelate=non_autocorrelate)

        for j in range(freq_box.shape[1]):
            welford.update(freq_box[:, j, :])

        print "Calculating box average and standard deviation...\n"

        # save_npy_as_mrc_file("prenorm_aps_box_" + str(i) + ".mrc", freq_box)

    out_file_shape = freq_box.shape

    mean_slice = welford.mean
    stdev_slice = welford.stddev()

    tiled_avg_array = np.tile(mean_slice, (out_file_shape[1], 1, 1))
    tiled_avg_array = tiled_avg_array.swapaxes(0, 1)

    tiled_std_array = np.tile(stdev_slice, (out_file_shape[1], 1, 1))
    tiled_std_array = tiled_std_array.swapaxes(0, 1)

    save_npy_as_mrc_file(avgfile.replace(".mrc", "") + ".mrc", tiled_avg_array)
    save_npy_as_mrc_file(stdfile.replace(".mrc", "") + ".mrc", tiled_std_array)

    # Clean up directory
    for f in glob.glob("box*.mrc"):
        os.remove(f)
    for g in glob.glob("normbox*.mrc"):
        os.remove(g)


def extract_box_function(inputfile, output, xdimension, ydimension, zdimension):

    tomogram = MRCHeaderClass()
    tomogram.read(inputfile)

    if xdimension >= ydimension and xdimension >= zdimension:
        dim = int(math.sqrt((xdimension ** 2) * 3))

    elif ydimension >= xdimension and ydimension >= zdimension:
        dim = int(math.sqrt((ydimension ** 2) * 3))

    else:
        dim = int(math.sqrt((zdimension ** 2) * 3))

    xvalue = random.randint(dim/2 - (dim-xdimension)/2, tomogram.columns - (dim/2 - (dim-xdimension)/2))
    yvalue = random.randint(dim/2 - (dim-ydimension)/2, tomogram.rows - (dim/2 - (dim-ydimension)/2))
    zvalue = random.randint(dim/2 - (dim-zdimension)/2, tomogram.sections - (dim/2 - (dim-zdimension)/2))

    dimensions = xdimension, ydimension, zdimension  # x y z dimension of final box
    dimensions = "{}".format(",".join(map(repr, dimensions)))

    ax = random.randint(0, 360)
    ay = random.randint(0, 360)
    az = random.randint(0, 360)
    a = ax, ay, az
    a = "{}".format(",".join(map(repr, a)))

    center = 'clip', 'resize', '-ox', str(dim), '-oy', str(dim), '-oz', str(dim), '-cx', str(xvalue),\
             '-cy', str(yvalue), '-cz', str(zvalue), str(inputfile), 'convert.rec'
    rotate = 'rotatevol', '-a', str(a), '-si', str(dimensions), str('convert.rec'), str(output)

    c = subprocess.Popen(center, stdout=subprocess.PIPE)  # cuts out a larger box at a random position
    c.communicate()

    r = subprocess.Popen(rotate, stdout=subprocess.PIPE)  # rotates the larger box randomly
    r.communicate()

    rm = subprocess.Popen(['rm', 'convert.rec'])  # clean up the directory
    rm.communicate()

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=str,
                        help="Name of input tomogram from which to extract the volumes to compute the noise profiles. "
                             "Needs to be larger than the output noise profile dimensions.")
    parser.add_argument("xdimension", type=int,
                        help="X dimension of the volumes to compute the noise profile. "
                             "Should be the same as the subtomograms to normalise.")
    parser.add_argument("-y", "--ydimension", type=int,
                        help="Y dimension of the volumes to compute the noise profile. "
                             "Should be the same as the subtomograms to normalise. Default: same as X dimension.")
    parser.add_argument("-z", "--zdimension", type=int,
                        help="Z dimension of the volumes to compute the noise profile. "
                             "Should be the same as the subtomograms to normalise. Default: same as X dimension.")
    parser.add_argument("-n", "--number", type=int, default=10,
                        help="Number of volumes to compute the noise profile from. Default: 10.")
    parser.add_argument("-a", "--avgfile", type=str, default="avgNoiseProfile.mrc",
                        help="Name of output file containing the averaged noise profile. Default: avgNoiseProfile.mrc")
    parser.add_argument("-s", "--stdfile", type=str, default="stdNoiseProfile.mrc",
                        help="Name of output file containing the standard deviation of the noise profile. "
                             "Default: stdNoiseProfile.mrc")
    parser.add_argument("-m", "--method", type=str, default="spectral",
                        help="Method used for spectral decomposition of the autocorrelated particles. fft or spectral. "
                             "Default: spectral.")
    parser.add_argument("-na", "--non-autocorrelate", action="store_true",
                        help="Provide if the -na flag is also provided in the make_autocorr_samples.py script. Turns "
                             "off the autocorrelation in the analysis.")

    args = parser.parse_args()
    args.ydimension = args.ydimension if args.ydimension else args.xdimension
    args.zdimension = args.zdimension if args.zdimension else args.xdimension

    generate_profiles(**vars(args))
