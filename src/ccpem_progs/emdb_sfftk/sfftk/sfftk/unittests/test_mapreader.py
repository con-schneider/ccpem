# test_mapreader.py
# -*- coding: utf-8 -*-
"""
Unit tests for :py:mod:``mapreader`` module
"""

__author__ = 'Paul K. Korir, PhD'
__email__ = 'pkorir@ebi.ac.uk, paul.korir@gmail.com'
__date__ = '2016-09-02'


import sys, os, glob
from readers.mapreader import Map, get_data
import unittest


class TestMapreader_basic(unittest.TestCase):
    """
    - test mask/non-mask
    
    exceptions:
    - test incomplete read (ValueError)
    """
    @classmethod
    def setUpClass(cls):
        print >> sys.stderr, "mapreader tests..."
    
    @classmethod
    def tearDownClass(cls):
        print >> sys.stderr, ""
        
    def setUp(self): 
        self.my_map = Map('sff/test_data/test_data.map')
        
    def test_read(self):
        """Test read map file"""
        # headers
        self.assertTrue(self.my_map._voxel_count > 0)
        self.assertEqual(self.my_map._voxel_count, len(self.my_map._voxels)) # correct amount of data read
    
    def test_write(self):
        """Test write map file"""
        written_maps = glob.glob('sff/test_data/test_write_map.map')
        
        self.assertEqual(len(written_maps), 0)
        
        with open('sff/test_data/test_write_map.map', 'w') as f:
            self.my_map.write(f)
        
        written_maps = glob.glob('sff/test_data/test_write_map.map')
        
        self.assertEqual(len(written_maps), 1)
        
        map(os.remove, written_maps)
    
    def test_invert(self):
        """Test invert map intensities"""
        self.assertFalse(self.my_map._inverted)
        self.my_map.invert()
        self.assertTrue(self.my_map._inverted)
    
    def test_fix_mask(self):
        """Test fix mask for fixable mask"""
        fixable_mask = Map('sff/test_data/test_fixable_mask.map')
        self.assertFalse(fixable_mask.is_mask)
        fixable_mask.fix_mask()
        self.assertTrue(fixable_mask.is_mask)
    
    def test_unfixable_mask(self):
        """Test exception for unfixable mask"""
        unfixable_mask = Map('sff/test_data/test_unfixable_mask.map')
        self.assertFalse(unfixable_mask.is_mask)
        with self.assertRaises(ValueError):
            unfixable_mask.fix_mask()
        self.assertFalse(unfixable_mask.is_mask)
    
    def test_get_data(self):
        """Test ``get_data()``"""
        my_map = get_data('sff/test_data/test_data.map')
        self.assertTrue(my_map._voxel_count > 0)
        self.assertEqual(my_map._voxel_count, len(my_map._voxels)) # correct amount of data read
    
    def test_get_data_inverted(self):
        """Test ``get_data(inverted=True)``"""
        my_inverted_map = get_data('sff/test_data/test_data.map', inverted=True)
        self.assertTrue(my_inverted_map._voxel_count > 0)
        self.assertEqual(my_inverted_map._voxel_count, len(my_inverted_map._voxels)) # correct amount of data read
        self.assertTrue(my_inverted_map._inverted)
    
    def test_bad_data_fail(self):
        """Test that a corrupted file (extra data at end) raises Exception"""
        with self.assertRaises(ValueError):
            Map('sff/test_data/test_bad_data1.map')