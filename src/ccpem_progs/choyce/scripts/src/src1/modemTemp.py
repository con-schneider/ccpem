
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---For Option 2---

# (i)   This script will read in the denisty map (and its details), details for the grid
#       search and the structure of best fitted model from step 1 (model.py) ("Top_model.txt")

from modeller import *
from modeller.scripts import complete_pdb
from modeller.automodel import *

import sys

#--------------------------------------------------------------------------------------
#---read file-names and details via keyboard-------------------------------

if len(sys.argv) != 11:
    print "usage: filename.py [map-format] [res] [ori x] [ori y] [ori z] [render_func] [box_size] [voxel] [start_type] [no steps]"
    sys.exit()


map_file    = 'map'                     # EM map name
map_format  = sys.argv[1]               # EM map format ('MRC' or 'XPLOR')
res         = float(sys.argv[2])        # resolution
x           = float(sys.argv[3])        # map origin (x-coordinate)
y           = float(sys.argv[4])        # map origin (y-coordinate)
z           = float(sys.argv[5])        # map origin (z-coordinate)
render_func = sys.argv[6]               # density type (options 'GAUSS' or 'SPHERE')
box_size    = int(sys.argv[7])          # grid size (has to be a box)
apix        = float(sys.argv[8])        # voxel size
start	    = sys.argv[9]               # start type
noSteps     = int(sys.argv[10])         # number of steps to perform in density grid search

#--------------------------------------------------------------------------------------
# Provided inputs

##start='CENTER'          #'CENTER' 'ENTIRE' 'SPECIFIC' ** we should use either center or specific
trans='EXHAUSTIVE'      #'EXAUSTIVE' :scanning MC (Monte Carlo)
                        #'NONE' rotation only #'RANDOM' MC ** we should use EXHAUSTIVE
temp=0.                 # for MC: 5000 for gauss
                        # can play with this parameter for MC acceptance <500 ** use T=0  
steps=noSteps           # optimization steps: 4-5 for 'EXAUSTIVE', 100-250 for 'RANDOM'
filter='NONE'           #'LAPLACIAN' (only works with GAUSS) | 'NONE'the default ** we should use NONE
fits_num=1              # number of wanted solutions (ranked) ** keep it 1

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

#---get the best ranked model from step 1
f = open("Top_model.txt","r")           # protein structure to be fit read from file top_model
struc = f.readline()                
f.close()

#--------------------------------------------------------------------------------------
#---density fit the best model from step 1 (model.py)

log.verbose()
env = environ()


aln = alignment(env)
aln = alignment(env, file='alignment.ali', align_codes=('template', 'target'))

#---reference model is read in and stored in alignment environment
mdl = model(env)
code = 'template'
mdl.read(file='template.pdb')

#---structure(s) to superpose is read in
mdl2 = model(env)
struc_to_sup = struc
code = 'target'
mdl2.read(file=struc_to_sup)

#---now superpose the two structures
mdl = model(env, file='template.pdb')
atmsel = selection(mdl).only_atom_types('CA')
mdl2 = model(env, file=struc_to_sup)
r = atmsel.superpose(mdl2, aln,fit=True)

#---write a superposed coordinate file
struc_supo = struc_to_sup.rstrip('.pdb')+"Superposed.pdb"
mdl2.write(file='targ_1_1.pdb')


### write the code of the fitted model into a textfile
##g = open("FittedCode.txt","w")
##g.write("target")
##g.close
