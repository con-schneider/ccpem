#!/bin/bash 

#printf "Hello!\n"

#printf "${11}/test.file\n"

suffixes=("A" "B" "CA" "CB")

for var in ${!suffixes[*]}
do
  #echo ${suffixes[${var}]}
  #echo $var
  #echo ${suffixes[$var]}
  ftc=${11}"/coords"${suffixes[${var}]}".xyz"  
  echo $ftc

  if [ -a $ftc ]; then
    rm $ftc
    if [ "$?" == "0" ]; then
      echo "$ftc deleted successfully"
    else 
      echo "$ftc couldn't be deleted"
    fi
    else echo "$ftc didn't exist"
  fi

done


#if [ -a ${11}/coordsA.xyz ]
#  then
#    rm 
#    echo "exists!"
#  else
#    echo "no!"
#fi

#\rm coordsA.xyz
#\rm coordsB.xyz
#\rm coordsCA.xyz
#\rm coordsCB.xyz

FindEM_filter_coords <<eot
${11}
${1}
${2}
${3}
${4}
${5}
${6}
${7}
${8}
${9}
${10}
eot

# called by FindEM_GUI
#
# CCC threshold for A
# CCC threshold for B
# diameter of A
# diameter of B
# peak width
# sampling
# code A
# code B
# maxnumA
# maxnumB
#
