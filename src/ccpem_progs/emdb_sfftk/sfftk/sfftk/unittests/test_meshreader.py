#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

"""
test_meshreader.py

Unit tests for meshreader module

Run as:
python -m unittest test_meshreader


TODO:
- 

Version history:
0.0.1, 2016-02-22, TestMeshreader_basic tests
0.0.2, 2016-02-23, Finally have data checks working without log4j spew

"""

__author__  = 'Paul K. Korir, PhD'
__email__   = 'pkorir@ebi.ac.uk'
__date__    = '2016-01-27'

import sys
import unittest
from readers import meshreader
import tests
    
class TestMeshreader_basic(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.meshmodel = meshreader.get_data(tests.TEST_DATA_PATH + 'test_data/test_data.am', read_data=False)
        
    def test_first_line_amiramesh(self):
        """test that it's declared as an AmiraMesh file"""
        self.assertEqual(self.meshmodel.filetype, 'AmiraMesh')    
    
    def test_first_line_binary_little_endian(self):
        """test that it is formatted as BINARY-LITTLE-ENDIAN"""
        self.assertEqual(self.meshmodel.format, 'BINARY-LITTLE-ENDIAN')
    
    def test_first_line_version(self):
        """test that it is version 2.1"""
        self.assertEqual(self.meshmodel.version, '2.1')    
    
    def test_fails_on_nonbinary_file(self):
        """test format ASCII not supported"""
        with self.assertRaises(TypeError):
            meshreader.get_data(tests.TEST_DATA_PATH + 'test_data/test_bad_data1.am', read_data=False)
    
    def test_lattice_present(self):
        """test Lattice definition exists and is an instance of meshreader.Definition"""
        self.assertTrue(('Lattice' in self.meshmodel.Definitions) and isinstance(self.meshmodel.Definitions['Lattice'], meshreader.Definition))
    
    def test_materials_present(self):
        """test Materials exist and is an instance of meshreader.Material"""
        self.assertIsNotNone(self.meshmodel.Materials)


class TestMeshreader_data(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """setup the testClass"""
        cls.meshmodel = meshreader.get_data(tests.TEST_DATA_PATH + 'test_data/test_data.am', read_data=True)
  
    def test_lattice_dimensions_correct(self):
        """test that the dimensions of the data correspond with lattice specifications"""
        # we use the zero-th dataset
        # assume all are the same
        y, x, c = self.meshmodel.Mesh[0].shape
        z = len(self.meshmodel.Mesh)
        X, Y, Z = self.meshmodel.Definitions['Lattice'].value
        self.assertEqual(x, X)
        self.assertEqual(y, Y)
        self.assertEqual(z, Z)
         
        
#         self.am = meshreader.get_data(open('test_data/test_data.am', 'rb'))
    
#     def test_no_of_patches(self):
#         """
#         number of patches
#         """
#         self.assertEqual(self.am.Patches_count, len(self.am.Patches))
#     
#     def test_no_of_vertices(self):
#         """
#         number of vertices
#         """
#         self.assertEqual(self.am.Vertices_count, len(self.am.Vertices))
#     
#     def test_no_of_branching_points(self):
#         """
#         number of branching points
#         """
#         self.assertEqual(self.am.NBranchingPoints_count, len(self.am.NBranchingPoints))
#         
#     def test_no_of_vertices_on_curves(self):
#         """
#         number of vertices on curves
#         """
#         self.assertEqual(self.am.NVerticesOnCurves_count, len(self.am.NVerticesOnCurves))
#     
#     def test_no_of_boundary_curves(self):
#         """
#         number of boundary curves
#         """
#         self.assertEqual(self.am.BoundaryCurves_count, len(self.am.BoundaryCurves))
#     
#     def test_no_of_triangles_in_patches(self):
#         """
#         number of triangles in patches
#         """
#         for p,Patch in self.am.Patches.iteritems():
#             self.assertEqual(Patch.triangle_count, len(Patch.Triangles))
#         
#     def test_meshmodel_is_not_populated(self):
#         """
#         check the is_populated flag as False for empty model
#         """
#         mesh = meshreader.Model('0.1', 'BINARY')
#         self.assertFalse(mesh.is_populated)
#     
#     def test_meshmodel_is_populated(self):
#         """
#         check the is_populated flag as True for populated model
#         """
#         self.assertTrue(self.am.is_populated)
        

class TestMeshreader_commandlineoptions(unittest.TestCase):
    def test_missing_arguments(self):
        """Test that missing arguments raise TypeError"""
        self.assertRaises(TypeError, meshreader.parse_args, [])
    
    def test_missing_mesh_file(self):
        """.am file absent -> TypeError"""
        self.assertRaises(TypeError, meshreader.parse_args, ['-o', 'output.txt'])
    
    def test_pos_arg_not_mesh_file(self):
        """
        pos_arg != .am -> TypeError
        """
        self.assertRaises(TypeError, meshreader.parse_args, ['file.xxxx'])
        
    def test_verbose(self):
        """-v only raises TypeError"""
        self.assertRaises(TypeError, meshreader.parse_args, ['-v'])
    
    @unittest.expectedFailure
    def test_no_fail_proper_filename(self):
        """doesn't raise TypeError with proper filename"""
        self.assertRaises(TypeError, meshreader.parse_args, ['file.am'])
    
    @unittest.expectedFailure
    def test_no_fail_proper_filename(self):
        """\
        doesn't raise TypeError with properly defined arguments\
        """
        self.assertRaises(TypeError, meshreader.parse_args, ['-o', 'output.txt', 'file.am'])
        self.assertRaises(TypeError, meshreader.parse_args, ['-v', 'file.am'])
        self.assertRaises(TypeError, meshreader.parse_args, ['-v', '-o', 'output.txt', 'file.am'])
        self.assertRaises(TypeError, meshreader.parse_args, ['-o', 'output.txt', '-v', 'file.am'])


if __name__ == "__main__":
    import javabridge, bioformats
    javabridge.start_vm(class_path=bioformats.JARS, max_heap_size='10G', run_headless=True)
    bioformats.init_logger()
    try:
        unittest.main()
    finally:
        javabridge.kill_vm()