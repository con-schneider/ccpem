#!/usr/bin/env python
# coding: utf-8

import numpy as np
import math
import argparse
from helper_functions.nptomo import NumpyTom, MRCfile
from npy2mrc import save_npy_as_mrc_file
from analysis_methods import spectral_analyze_data, fft_data
import logging
import subprocess
import scipy.stats
from helper_functions.average_helper import RealSpaceAverage
import sys
from autocorr import autocorrelation

# this script takes subtomograms, autocorrelated along sampling radii according to getSymmetries2_constantin.py
# and averages them (ignoring nan values from masking), then applying the analysis method of choice
# the autocorrelated subtomograms need to be provided in a single folder containing only those files


# creates and returns an array of all mrc/npy file values in the specified path
# DEPRECEATED: for large numbers of tomograms, RAM full
def import_tomos(path, text_file=None, verbose=False):

    # if text file provided, read out filenames
    if text_file:
        file_list = []
        with open(text_file, "r") as in_names:
            for line in in_names.readlines():
                file_list.append(line.replace("\n", ""))

    else:
        # read out the files in the specified directory
        file_list = subprocess.check_output(['ls %s' % path], shell=True)
        file_list = file_list.split('\n')[:-1]

    # read out the shape of the files from the first list item
    if file_list[0][-4:].lower() == ".npy":
        open_tom = NumpyTom
    elif file_list[0][-4:].lower() == ".mrc":
        open_tom = MRCfile
    else:
        print 'Error: Input file format not supported'
        logging.error("Input file format not supported.")
        return 0
    with open_tom(file_list[0]) as in_tomo:
        file_0_data = in_tomo.get_all_tom_density_values()
        file_shape = file_0_data.shape

    array_shape = tuple([len(file_list)]) + file_shape
    tomo_array = np.zeros(array_shape)

    for i in range(len(file_list)):
        if file_list[i][-4:].lower() == ".npy":
            open_tom = NumpyTom
        elif file_list[i][-4:].lower() == ".mrc":
            open_tom = MRCfile
        else:
            print 'Error: Input file format not supported'
            logging.error("Input file format not supported.")
            return 0

        if verbose:
            # print "\r" + str(i+1).zfill(3) + "/" + str(len(file_list)),
            sys.stdout.write("\r" + str(i+1).zfill(3) + "/" + str(len(file_list)))
            sys.stdout.flush()

        with open_tom(file_list[i]) as in_tomo:
            in_tomo_data = in_tomo.get_all_tom_density_values()

        tomo_array[i, ] = in_tomo_data

    sys.stdout.write("\n")

    return tomo_array


# to handle larger numbers of arrays without eating all of the RAM and being able to handle large arrays in the future:
# averages the current array and the "running average", factoring the NaN occurences into the weight matrix
# replicates np.nanmean for sample numbers with low RAM usage
def import_avg_many_tomos(path, text_file=None, verbose=False):

    # if text file provided, read out filenames
    if text_file:
        file_list = []
        with open(text_file, "r") as in_names:
            for line in in_names.readlines():
                file_list.append(line.replace("\n", ""))

    else:
        # read out the files in the specified directory
        try:
            file_list = subprocess.check_output(['ls %s' % path], shell=True)
        except:
            print "ERROR: Provided regular expression did not return any files."
            sys.exit(1)

        file_list = file_list.split('\n')[:-1]

    # read out the shape of the files from the first list item
    if file_list[0][-4:].lower() == ".npy":
        open_tom = NumpyTom
    elif file_list[0][-4:].lower() == ".mrc" or file_list[0][-4:].lower() == ".rec":
        open_tom = MRCfile
    else:
        print 'Error: Input file format not supported'
        logging.error("Input file format not supported.")
        sys.exit(1)
    with open_tom(file_list[0]) as in_tomo:
        file_0_data = in_tomo.get_all_tom_density_values()
        file_shape = file_0_data.shape
        file_dt = file_0_data.dtype

    # this reduces the size of the weight matrix by 1/2
    if len(file_list) < 65536:
        weight_dt = np.int16
    else:
        weight_dt = np.int32  # if we ever get to 4,294,967,296 subtomograms, this software will be obsolete anyways

    weight_matrix = np.zeros(tuple([2]) + file_shape, dtype=weight_dt)
    weight_matrix[1, ] = np.ones(file_shape, dtype=weight_dt)
    averaging_array = np.zeros(tuple([2]) + file_shape)  # , dtype=file_dt)

    for i in range(len(file_list)):
        if file_list[i][-4:].lower() == ".npy":
            open_tom = NumpyTom
        elif file_list[i][-4:].lower() == ".mrc" or file_list[0][-4:].lower() == ".rec":
            open_tom = MRCfile
        else:
            print 'Error: Input file format not supported'
            logging.error("Input file format not supported.")
            sys.exit(1)

        # prints a running counter (ie does not need a new line in every loop)
        sys.stdout.write("\r" + str(i + 1).zfill(3) + "/" + str(len(file_list)))
        sys.stdout.flush()

        with open_tom(file_list[i]) as in_tomo:
            in_tomo_data = in_tomo.get_all_tom_density_values()

        averaging_array, weight_matrix = rolling_average(in_tomo_data, averaging_array, weight_matrix)

    print "\n"

    return averaging_array[0, ], len(file_list)


# averages the newly input array on the previous average, observing weights and NaN counts
# as far as I can tell, this presents the lowest RAM load
def rolling_average(in_array, avg_array, weight_matrix):

    in_array_nonan = np.where(np.isnan(in_array), 0, in_array)  # ma.average cannot handle nan, even when masked
    in_array_masked = np.ma.masked_where(np.isnan(in_array), in_array_nonan)

    if in_array_masked.shape != avg_array[0, ].shape:
        print "ERROR: averaging_array has different shape from input array."
        sys.exit(1)

    if is_empty(avg_array):  # first round
        avg_array[0, ] = in_array_masked
        weight_matrix[0, ] += np.where(in_array_masked.mask, 0, 1)

        return avg_array, weight_matrix

    else:
        avg_array[1, ] = in_array_masked
        weight_matrix[1, ] = np.where(in_array_masked.mask, 0, 1)
        avg_array[0, ] = np.ma.average(avg_array, axis=0, weights=weight_matrix)
        weight_matrix[0, ] += np.where(in_array_masked.mask, 0, 1)

        return avg_array, weight_matrix


# checks if any of the sub-arrays along axis 0 are not all 0s
# return True if all sub-arrays are unequal to np.zeros of the same shape
# also returs the index of the first all 0 sub-array
def is_full(array):

    for i in range(array.shape[0]):
        if (array[i, ] == np.zeros(array[i, ].shape)).all():
            return False, i

    return True, 0


# checks if any of the sub-arrays along axis 0 are not all 0s
def is_empty(array):

    for i in range(array.shape[0]):
        if (array[i, ] != np.zeros(array[i, ].shape)).any():
            return False

    return True


# converts z scores into p values for significance testing
def z_to_p(score_array):
    p_value_array = scipy.stats.norm.sf(np.absolute(score_array))
    p_smaller_01 = np.where(p_value_array <= 0.1, 1000, 0)
    p_smaller_005 = np.where(p_value_array <= 0.05, 1000, 0)
    p_smaller_001 = np.where(p_value_array <= 0.01, 1000, 0)
    return p_value_array, p_smaller_01, p_smaller_005, p_smaller_001


# removes all frequency signal not at an r, y position that belongs to a point in the particle
# input array needs to be have same r, y coords as sampling matrix before autocorr
def rm_nonparticle_signal(input_array, avg_file, threshold=0.3):

    avg = RealSpaceAverage(avg_file, threshold)

    ry_pairs = avg.ry_pairs

    r_values = tuple([ry[0] for ry in ry_pairs])  # conversion to tuples necessary for correct indexing
    y_values = tuple([ry[1] for ry in ry_pairs])

    input_mask = np.zeros(input_array.shape)
    input_mask[:, y_values, r_values] = 1

    input_array = np.where(input_mask == 0, np.zeros(input_array.shape), input_array)

    return input_array


# sets all output values past the Nyquist frequency to 0
def rm_past_nyquist(input_array):

    for r in range(input_array.shape[2]):
        nyq = math.pi * (r + 1)
        input_array[int(nyq + 1):, :, r] = 0

    return input_array


def main(autocorr_path, method="spectral", ofilepath="symmetryExtraction.mrc", save_format=".mrc", text_file=None,
         averaged_particle=None, threshold=0.3, verbose=False, avg_noise_profile=None,
         std_noise_profile=None, no_nyquist=False, p_values=False):


    print "\nOpening tomo files:"

    # importing the autocorrelated sampling arrays
    mean_tomo_array, num_of_files = import_avg_many_tomos(autocorr_path, text_file=text_file, verbose=verbose)

    # analysis step
    if verbose:
        print "Analysis step:"
    else:
        print "Analysing..."

    mask = np.ones(mean_tomo_array.shape)

    if np.isnan(mean_tomo_array).any():
        mask = np.where(np.isnan(mean_tomo_array), np.zeros(mean_tomo_array.shape), np.ones(mean_tomo_array.shape))

    if method.lower() == "fft":
        if verbose:
            print "Using FFT...\n"
        logging.info("Using FFT.")
        analysis_result = fft_data(mean_tomo_array)

    elif method.lower() == "spectral":
        logging.info("Using Lomb-Scargle analysis.")
        if verbose:
            print "Using Lomb-Scargle analysis...\n"
        analysis_result = spectral_analyze_data(mean_tomo_array, precenter=True, normalize=True, missing_wedge=mask)

    else:
        logging.info("Using Lomb-Scargle analysis.")
        if verbose:
            print "Using Lomb-Scargle analysis...\n"
        analysis_result = spectral_analyze_data(mean_tomo_array, precenter=False, normalize=False, missing_wedge=mask)

    if avg_noise_profile:
        with MRCfile(avg_noise_profile) as t:
            av_noise = t.get_all_tom_density_values()
            analysis_result = np.subtract(analysis_result, av_noise)

        if not std_noise_profile:
            print "WARNING: An average noise profile, but no standard deviation profile was provided. The output " \
                  "array does not contain z-scores."

    if std_noise_profile:
        np.seterr(all="ignore")  # zero division warnings caused by 0s past Nyquist frequency
        with MRCfile(std_noise_profile) as t:
            std_noise = t.get_all_tom_density_values()
            # std_error = np.divide(std_noise, np.sqrt(num_of_files))
            # analysis_result = np.divide(analysis_result, std_error)
            analysis_result = np.divide(analysis_result, std_noise)
        if not avg_noise_profile:
            print "WARNING: A standard deviation profile, but no average noise profile was provided. The output " \
                  "array does not contain z-scores."

    # remove non-relevant data
    if averaged_particle:  # remove data that does not contribute to the particle
        analysis_result = rm_nonparticle_signal(analysis_result, averaged_particle, threshold)

    # remove data past the Nyquist frequency
    if no_nyquist:
        analysis_result = rm_past_nyquist(analysis_result)

    # remove remaining nan values
    if np.isnan(analysis_result).any():
        print "\nNaN (not a number) values are present in the output, these are replaced with 0."
        analysis_result = np.where(np.isnan(analysis_result), 0, analysis_result)

    if save_format.lower() == "npy" or save_format.lower() == ".npy":
        np.save(ofilepath.replace(".npy", "").replace(".mrc", "") + ".npy", analysis_result)
    elif save_format.lower() == "mrc" or save_format.lower() == ".mrc":
        save_npy_as_mrc_file(ofilepath.replace(".npy", "").replace(".mrc", "") + ".mrc", analysis_result)
    else:
        save_npy_as_mrc_file(ofilepath.replace(".npy", "").replace(".mrc", "") + ".mrc", analysis_result)
        print "Save format not recognised, saving as .mrc."

    if p_values and avg_noise_profile and std_noise_profile:

        p_values_array, p_values_01, p_values_005, p_values_001 = z_to_p(analysis_result)

        if save_format.lower() == "npy" or save_format.lower() == ".npy":
            ofilepath = ofilepath.replace(".npy", "").replace(".mrc", "") + ".npy"
        elif save_format.lower() == "mrc" or save_format.lower() == ".mrc":
            ofilepath = ofilepath.replace(".npy", "").replace(".mrc", "") + ".mrc"
        else:
            ofilepath = ofilepath.replace(".npy", "").replace(".mrc", "") + ".mrc"

        p_values_path = ofilepath[:-4] + "_p_values" + ofilepath[-4:]
        p_values_001_path = ofilepath[:-4] + "_p_smaller_001" + ofilepath[-4:]
        p_values_005_path = ofilepath[:-4] + "_p_smaller_005" + ofilepath[-4:]
        p_values_01_path = ofilepath[:-4] + "_p_smaller_01" + ofilepath[-4:]
        save_npy_as_mrc_file(p_values_path, p_values_array)
        save_npy_as_mrc_file(p_values_005_path, p_values_005)
        save_npy_as_mrc_file(p_values_001_path, p_values_001)
        save_npy_as_mrc_file(p_values_01_path, p_values_01)

    elif p_values:
        print "WARNING: p-values were not calculated since output array was not converted to z-scores."

    return analysis_result


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("autocorr_path",
                        type=str,
                        help="Regular expression indicating the autocorrelated files to be averaged and analysed.")
    parser.add_argument("-m", "--method",
                        type=str,
                        help="Choose either Fast Fourier Transform or Lomb-Scargle spectral decomposition as 'FFT' or "
                             "'spectral'. Default: spectral.",
                        default="spectral")
    parser.add_argument("-o", "--ofilepath",
                        type=str,
                        help="Path to which the analysis result is saved. Default: symmetryExtraction.mrc",
                        default="symmetryExtraction.mrc")
    parser.add_argument("-s", "--save_format",
                        type=str,
                        help="Format in which to save the analysis result, 'mrc' (default) or 'npy'.",
                        default="mrc")
    parser.add_argument("-f", "--text_file",
                        help="If flag is provided, <autocorr_path> is assumed to be a textfile containing the path to "
                             "an input file on each line.",
                        action="store_true")
    parser.add_argument("-ap", "--averaged-particle",
                        help="Averaged .mrc file of the provided subtomograms, e.g. from PEET run. Used to determine "
                             "which pairs of radii and z-coordinate are relevant.")
    parser.add_argument("-t", "--threshold",
                        help="Threshold value to use as cutoff for the particle specified with -p. All sampling "
                             "circles with average value of lower than the threshold in the normalised (0 to 1) average "
                             "particle will be set to 0 in the output angular power spectrum file. Default: 0.3",
                        default=0.3,
                        type=float)
    parser.add_argument('-v', '--verbose',
                        action="store_true",
                        help='If provided, print additional information during the run.',
                        default=True)
    parser.add_argument('-a', '--avg-noise-profile',
                        default=None,
                        help='Averaged noise profile. Used to normalise the output file to z-scores.')
    parser.add_argument('-n', '--std-noise-profile',
                        default=None,
                        help='Standard deviation of noise profile. Used to normalise the output file to z-scores.')
    parser.add_argument('-nn', '--no-nyquist', action="store_true",
                        help="If provided, set values past the nyquist frequency to 0. Not necessary when method = "
                             "spectral.")
    parser.add_argument("-pv", "--p-values", default=False, action="store_true",
                        help="If flag and both -a and -n are provided, apart from the z_score output array three files "
                             "are created, one containing all p-values, one with the p-values =< 0.1 set to 1000 (to "
                             "better visualise) and one with the p-values =< 0.01 and the p-values =< 0.1 set to 1000.")

    args = parser.parse_args()

    main(**vars(args))
