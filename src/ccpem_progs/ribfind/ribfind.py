"""
 * ribfind.py Copyright 2012
 * Arun Prasad Pandurangan and Maya Topf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
"""

##
# =====
# EDITS
# =====
#
# 14/03/2014: Program modified to be run from an external script in OO way (class ribfind with an __init__ def)
#  - all references to sys.argv[1] replaced with self.pdbfile
#  - all references to sys.argv[2] replaced with self.dsspfile
#  - all references to sys.argv[3] replaced with self.contactDistance
# 24/03/2014: weighted number of clusters returned as an array as well as printed, so that the gui/wrapper can just use the array rather than have to parse the output file
# 14/04/2014: some checking of whether FlexEM is run in interactive or background mode so that signals only emit when they're meant to! 
##

import os, sys, time, shutil
from Bio.PDB import *
#import scipy
from numpy import *
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.dom import minidom

class protein(object):
	"class protein gathers information about helix, sheet and loop in the protein for clustering"
	def __init__(self,file1,file2):
		#file1_base = os.path.splitext(file1)[0]
		#file2_base = os.path.splitext(file2)[0]
		"Read the PDB helix and sheet records and identify loops \
		Store them in their appropriate lists"
		self.n_helix = 0
		self.n_sheet = 0
		self.n_loop = 0
		self.helix_list = []
		self.sheet_dict = {}
		self.loop_list = []
		sheets = {}
		helices = []
		
		#create the structure object from the PBD file
		protein.pdbfile = file
		parser = PDBParser()
		protein.structure = parser.get_structure('pdb',file1)
		
		#create a Polypeptide builder to get the sequence information from the structure object
		ppb=PPBuilder() 
 		for pp in ppb.build_peptides(protein.structure): 
 			protein.seq_list = list(pp.get_sequence())

		#form the dssp file name for the corresponding protein filename
		dsspfile = file2
		parse_dssp(dsspfile,sheets,helices)
		#load the helix information into helix_list
		for i in helices:
			self.helix_list.append(i)
			self.n_helix += 1
		
		#load the sheet information into sheet_dict	
		nsheet = 0
		for keys in sheets:
			self.sheet_dict[nsheet] = sheets[keys]
			nsheet += 1
		self.n_sheet = nsheet

	#Check whether a given residue id is in helix list
	def in_helix(self,chain_id,resid):
		flag = 0
		for i in range(self.n_helix):
			h_chain_id = self.helix_list[i][0]
			start_res = self.helix_list[i][1]
			end_res = self.helix_list[i][2]
			if (chain_id == h_chain_id) and (resid >= start_res and resid <=end_res):
				flag = 1
		return flag
		
	#Check whether a give residue id is in sheet list
	def in_sheet(self,chain_id,resid):
		flag = 0
		for i in range(self.n_sheet):
				strand_list = self.sheet_dict[i]
				#print strand_list
				for j in range(len(strand_list)):
					s_chain_id = strand_list[j][0]
					start_res = strand_list[j][1]
					end_res = strand_list[j][2]
					if (chain_id == s_chain_id) and (resid >= start_res and resid <=end_res):
						flag = 1
		return flag
	
	def find_loop(self):
		#Construct loop residue list(loop_list)
		all_loop = []
		for model in protein.structure.get_list():
			for chain in model.get_list():
				for residue in chain.get_list():
					resinfo = residue.get_full_id()
					resinfo = list(resinfo)
					chain_id = str(resinfo[2])
					resid = int(resinfo[3][1])
					if not(self.in_helix(chain_id,resid)) and not(self.in_sheet(chain_id,resid)) and residue.get_resname() != "HOH":
						tlist = [chain_id,resid]
						all_loop.append(tlist)
						#print tlist
	
		#creat a list of loops from all_loop list
		if(len(all_loop)>0):
			tlist = all_loop[0]
			start = tlist[1]
			end = tlist[1]
			for i in range(len(all_loop)):
				#print i
				if(i == len(all_loop)-1):
					self.n_loop += 1
					self.loop_list.append([chainid,start,end])
				else:
					tlist = all_loop[i+1]
					resid = tlist[1]
					chainid = tlist[0]
					if(resid-1 == end):
						end = resid
					else:
						self.n_loop += 1
						tlist = all_loop[i]
						chainid = tlist[0]
						self.loop_list.append([chainid,start,end])
						start = resid
						end = resid

class helix(protein):
	"Class helix stores information about the individual helical segments"
	def __init__(self,chain_id,startres,endres):
		"Constructor: Initializes helix data structure and creates the C-beta dictionary with its own residue numbering"
		self.chain_id = chain_id
		self.nres = endres-startres+1
		self.startres = startres
		self.endres = endres
		self.is_in_clust = 10
		self.helix_cb = {}
 		# read from the structure object of the protein class and create C-beta dictionary and also include the GLY C-alpha
 		n = 1
 		for i in range(startres,endres+1):
			#print i
			natm = 0
			sum = Vector([0, 0, 0])
			for model in protein.structure.get_list():
				for chain in model.get_list():
					for residue in chain.get_list():
						rname = residue.get_resname()
						for atom in residue.get_list():
							#check for the C-beta and GLY C-alpha atoms
							atmid = atom.get_id()
							if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
								atminfo = atom.get_full_id()
								atminfo = list(atminfo)
								tlist = residue.get_id()
								if str(atminfo[2]) == chain_id and tlist[1] == i:
									v = atom.get_vector()
									sum = sum + v
									natm = natm + 1
			v = sum/natm
			self.helix_cb[n] = v
			n += 1
	def get_nres(self):
		"Return number of helical residues"
		return self.nres
	def get_cb_coord(self,resid):
		"Return C-beta coordinate vector"
		cb_vector = helix_cb[resid]
		return cb_vector
	def get_startres(self):
		"Return the starting residue of the helix"
		return self.startres
	def get_endres(self):
		"Return the ending residue of the helix"
		return self.endres
	def get_cb(self):
		cb_vec_list = []
		for key, value in self.helix_cb.iteritems():
			if(self.nres > 3):
				cb_vec_list.append(value)
		return cb_vec_list
	def print_cb_dict(self):
		"Prints the helix cbeta dictionary"
		print 'Helix information:'
		for key, value in self.helix_cb.iteritems():
			print key, value
	def get_type(self):
		return 1
	def get_start_end(self):
		tlist = []
		tlist.append(self.startres)
		tlist.append(self.endres)
		return tlist

class strand(object):
	"Class strand stores information about the individual stand segments"
	def __init__(self,chain_id,startres,endres):
		"Constructor: Initializes strand data structure and creates the C-beta dictionary with its own residue numbering"
		self.chain_id = chain_id
		self.nres = endres-startres+1
		self.startres = startres
		self.endres = endres
		self.strand_cb = {}
		# read from the structure object and create C-beta dictionary and also include the GLY C-alpha
 		n = 1
 		for i in range(startres,endres+1):
 			sum = Vector([0, 0, 0])
 			natm = 0
			#print i
			for model in protein.structure.get_list():
				for chain in model.get_list():
					for residue in chain.get_list():
						rname = residue.get_resname()
						for atom in residue.get_list():
							#check for the C-beta and GLY C-alpha atoms
							atmid = atom.get_id()
							if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
								atminfo = atom.get_full_id()
								atminfo = list(atminfo)
								tlist = residue.get_id()
								if str(atminfo[2]) == chain_id and tlist[1] == i:
									v = atom.get_vector()
									sum = sum + v
									natm = natm + 1
			v = sum/natm
			self.strand_cb[n] = v
			n += 1
	def get_nres(self):
		"Return number of strand residues"
		return self.nres
	def get_cb_coord(self,resid):
		"Return cb coordinate vector"
		cb_vector = strand_cb[resid]
		return cb_vector
	def get_startres(self):
		"Return the starting residue of the strand"
		return self.startres
	def get_endres(self):
		"Return the ending residue of the strand"
		return self.endres
	def print_cb_dict(self):
		"Prints the strand cbeta dictionary"
		for key, value in self.strand_cb.iteritems():
			print key, value
	def get_start_end(self):
		tlist = []
		tlist.append(self.startres)
		tlist.append(self.endres)
		return tlist

class sheet(protein):
	"Creates a sheet object from strand objects using the strand_list" 
	def __init__(self,strand_list):
		self.nstrand = len(strand_list)
		self.is_in_clust = 20
		self.strand_list = strand_list
		self.strand = []
		for i in range(self.nstrand):
			#create individual strand objects and assemblem them into a sheet
			chain_id = str(strand_list[i][0])
			start_resid = int(strand_list[i][1])
			end_resid = int(strand_list[i][2])
			self.strand.append(strand(chain_id,start_resid,end_resid))
	def get_cb(self):
		cb_vec_list = []
		for i in range(self.nstrand):
			for key, value in self.strand[i].strand_cb.iteritems():
				if(self.strand[i].nres > 3):
					cb_vec_list.append(value)
		return cb_vec_list
	def print_cb_dict(self):
		print "Strant information:"
		for i in range(self.nstrand):
			print "Strand ",i,"= ",self.strand_list[i]
			#self.strand[i].print_cb_dict()
	def get_type(self):
		return 2
	def get_start_end(self):
		start_end = []
		for i in range(self.nstrand):
			start_end.extend(self.strand[i].get_start_end())
		return start_end

class Loop(protein):
	"Class loop stores information about the individual loop segments"
	def __init__(self,chain_id,startres,endres):
		"Constructor: Initializes loop data structure and creates the C-beta dictionary with its own residue numbering"
		self.chain_id = chain_id
		self.nres = endres-startres+1
		self.startres = startres
		self.endres = endres
		self.is_in_clust = 0
		self.loop_cb = {}
 		# read from the structure object of the protein class and create C-beta dictionary and also include the GLY C-alpha
 		n = 1
 		for i in range(startres,endres+1):
 			sum = Vector([0, 0, 0])
 			natm = 0 		
			for model in protein.structure.get_list():
				for chain in model.get_list():
					for residue in chain.get_list():
						rname = residue.get_resname()
						for atom in residue.get_list():
							#check for the C-beta and GLY C-alpha atoms
							atmid = atom.get_id()
							if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
								atminfo = atom.get_full_id()
								atminfo = list(atminfo)
								tlist = residue.get_id()
								if str(atminfo[2]) == chain_id and tlist[1] == i:
									v = atom.get_vector()
									sum = sum + v
									natm = natm + 1
			v = sum/natm
			self.loop_cb[n] = v
			n += 1									
	def get_nres(self):
		"Return number of helical residues"
		return self.nres
	def get_cb_coord(self,resid):
		"Return C-beta coordinate vector"
		cb_vector = loop_cb[resid]
		return cb_vector
	def get_startres(self):
		"Return the starting residue of the helix"
		return self.startres
	def get_endres(self):
		"Return the ending residue of the helix"
		return self.endres
	def print_cb_dict(self):
		"Prints the loop cbeta dictionary"
		for key, value in self.loop_cb.iteritems():
			print key, value
	def get_type(self):
		return 3
	def get_start_end(self):
		tlist = []
		tlist.append(self.startres)
		tlist.append(self.endres)
		return tlist

#Function to return a random key from a given dictionary
def get_rand_key(hs_dict):
	keys = hs_dict.keys()
	random.shuffle(keys)
	return keys[1]

#distance between two vectors
def dist(v1,v2):
	s1 = (v1[0]-v2[0])**2
	s2 = (v1[1]-v2[1])**2
	s3 = (v1[2]-v2[2])**2
	return (s1+s2+s3) ** 0.5

#Function returns 1 if sse1 and sse2 are close; otherwise 0
def is_close(sse1,sse2,control,cutoff,per_similar):
	close = 0
	per1 = 0.0
	per2 = 0.0
	small = 0
	large = 0
	l1 = sse1.get_cb()
	l2 = sse2.get_cb()
	if(len(l1)==0 or len(l2)==0):
		return 0
	#if two SSEs are helcies and one is relatively small than the other, then treat it as not close 
	# this condition will avoid merging of two different cluster due to a small SSE in between them!
	rangelen1 = len(sse1.get_start_end())
	rangelen2 = len(sse2.get_start_end())
	relativelen = 0.0
	#make sure that its a helix pair!
	if(rangelen1 == 2 and rangelen2 == 2):
		len1 = sse1.get_endres()-sse1.get_startres()+1
		len2 = sse2.get_endres()-sse2.get_startres()+1
		if(len1 <= len2):
			small = float(len1)
			large = float(len2)
		else:
			small = float(len2)
			large = float(len1)
		relativelen = (small/large)*100.0
		if (relativelen <= 40.0):
			return 0
	c1 = 0.0
	for i in l1:
		for j in l2:
			if(dist(i,j)<=cutoff):
				c1 = c1 + 1.0
				break
	#%sse1 cb close to sse2 cb
	c2 = 0.0
	for i in l2:
		for j in l1:
			if(dist(i,j)<=cutoff):
				c2 = c2 + 1.0
				break
	per1 = c1/len(l1)*100
	per2 = c2/len(l2)*100
	if(per1 >= per_similar or per2 >= per_similar):
		close = 1
	return close

def isin(item,den_cluster):
	yes = False
	for i_list in den_cluster:
		for j_list in i_list:
			if(item == j_list):
				yes = True
	return yes			

#Function to parse dssp file and return sheet and helix information
def parse_dssp(file,sheets,helices):
	infile = open(file,'rU')
	#extract sheets information from dssp output file
	n = 0
	new = 0
	start = 0
	end = 0
	cid = ''
	#sheets = {}
	#helices = []
	tlist = []
	lstlbl = '99'
	check_dssp_loop = 0
	for line in infile:
		temp = line.split()
		if(temp[0] == "" or temp[0] =="."):
			continue
		if(temp[0]=="#" and check_dssp_loop == 0):
			check_dssp_loop = 1
			continue
		if(temp[1]!="!*" and temp[1]!="!" and (check_dssp_loop==1 and temp[3]!="X") and temp[0]!="#" and check_dssp_loop==1):
			resnum = int(line[6:10])
			chain = str(line[11:12])
			stype = str(line[16:17])
			betalbl = str(line[33:34])
			if(stype=='E' and betalbl != ' '):
				if(new==0):
					new = 1
					start = resnum
					cid = chain
					lstlbl = betalbl
			else:
				if(stype!='E' and new==1):
					new = 0
					end = resnum - 1 
					if(lstlbl in sheets):
						tlist = sheets[lstlbl]
						tlist.append([cid,start,end])
						new_entry = {lstlbl:tlist}
						sheets.update(new_entry)
					else:
						tlist = []
						tlist.append([cid,start,end])
						sheets[lstlbl] = tlist
			if(stype=='E' and lstlbl!= betalbl and betalbl != ' ' and new == 1):
				end = resnum - 1 
				if(lstlbl in sheets):
					tlist = sheets[lstlbl]
					tlist.append([cid,start,end])
					new_entry = {lstlbl:tlist}
					sheets.update(new_entry)
				else:
					tlist = []
					tlist.append([cid,start,end])
					sheets[lstlbl] = tlist	
				start = resnum
				cid = chain
				lstlbl = betalbl

	infile.close()

	#extract helix information from dssp output file
	new = 0
	n = 0
	infile = open(file,'rU')
	check_dssp_loop = 0
	for line in infile:
		n =  n + 1
		temp = line.split()
                if(temp[0] == "" or temp[0] =="."):
                        continue
                if(temp[0]=="#" and check_dssp_loop == 0):
                        check_dssp_loop = 1
                        continue
		
		if(temp[1]!="!*" and temp[1]!="!" and (check_dssp_loop==1 and temp[3]!="X") and temp[0]!="#" and check_dssp_loop==1):
			resnum = int(line[6:10])
			chain = str(line[11:12])
			stype = str(line[16:17])
			betalbl = str(line[33:34])
			if(stype=='H' and new==0):
				new = 1
				start = resnum
				cid = chain
				lstlbl = betalbl
			else:
				if(stype!='H' and new==1):
					new = 0
					end = resnum -1 
					helices.append([cid,start,end])
	infile.close()

#Return the formatted xml string from the element tree
def formatXML(element):
	unformatted = ElementTree.tostring(element, 'utf-8')
	formatted = minidom.parseString(unformatted)
	return formatted.toprettyxml(indent="   ")

class ribfind:
	def __init__(self,pdbfile,dsspfile,contactDistance,sync=None):
		self.pdbfile = pdbfile
		self.dsspfile = dsspfile
		self.contactDistance = contactDistance
		self.sync = sync
		
		#Main program
		#create protein object and gather info about helix,sheet and loop from the pdb file
		start = time.time()

		obj = protein(self.pdbfile,self.dsspfile) #obj = protein(sys.argv[1],sys.argv[2])
		#print "done protien initialization"
		file1_base = os.path.splitext(os.path.basename(self.pdbfile))[0]

		cutoff = float(self.contactDistance)
		obj.find_loop()
		#print "done loops initilization"
		hx = []
		sh = []
		lp = []
		initial_reps = {}

		#Declare a dictionary containing pointers to helix and sheet objects
		main_hs_dict = {}
		hs_dict = {}


		#collect all helix objects
		print "===========RIBFIND: a program to find rigid bodies in protein structures============"
		print "Reference: Pandurangan, A.P., Topf, M., 2012. J. Struct. Biol. 177, 520-531."
		print "===================================================================================="
		print "Input PDB file : ",self.pdbfile #sys.argv[1]

		print "Input DSSP file : ",self.dsspfile #sys.argv[2]
		print "Contact distance : ",self.contactDistance #sys.argv[3]
		print "Summary of secondary structural elements"
		print "----------------------------------------"
		print "Helix initilization: "
		print "Number of helices : ",str(obj.n_helix)
		for i in range(obj.n_helix):
			chain_id = str(obj.helix_list[i][0])
			start_resid = int(obj.helix_list[i][1])
			end_resid = int(obj.helix_list[i][2])
			print "Helix ",i,1,":"," [",chain_id,",",start_resid,",",end_resid,"]"
			hx.append(helix(chain_id,start_resid,end_resid))
		#print "done Helix initilization"
		#collect all sheet objects
		print "----------------------------------------"
		print "Sheet initilization: "
		print "Number of sheets  ",str(obj.n_sheet)
		for i in range(obj.n_sheet):
			#print "sheet : ",i+1
			sh.append(sheet(obj.sheet_dict[i]))

		for i in range(obj.n_sheet):
		        print "Sheet : ",i+1
		        sh[i].print_cb_dict()

		#print "done Sheet initilization"
		#collect all loop objects
		print "----------------------------------------"
		print "Loop initilization: "
		for i in range(obj.n_loop):
			chain_id = str(obj.loop_list[i][0])
			start_resid = int(obj.loop_list[i][1])
			end_resid = int(obj.loop_list[i][2])
			print "Loop "+str(i+1),":"," [",chain_id,",",start_resid,",",end_resid,"]"
			lp.append(Loop(chain_id,start_resid,end_resid))
		#print "done loop initilization"

		noclustfile = file1_base+'.noclust'
		outfile1 = open(noclustfile,'w')	
		clustfile = file1_base+'.clustlog'
		clusterfile = open(clustfile,'w')

		print "---------------------------------------------------------------------------------------------------"
		print "Summary of RIBFIND clusters for the range of cutoffs"
		print "Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster"
		clusterfile.write("Summary of RIBFIND clusters for the range of cutoffs" + '\n')
		clusterfile.write("Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster" + '\n')

		if self.sync is not None: self.sync.emit(1)
		#edit: 24/03/2014 array added
		self.w_clust2_array = []

		#Edit: 30/07/2014; Added feature to write out RIBIND result data as a xml fotmatted file 		
		ribfindResult = Element('RibfindResults')
		preferredSolution = SubElement(ribfindResult, 'preferredSolution')
		preferredSolution.text = 'X'
		
		ribfind = SubElement(ribfindResult, 'ribfind')

		for per_similar in range(1,101):

			ribfindRun = SubElement(ribfind, 'ribfindRun', id=str(per_similar))

			#write the *denclust file for each cutoff value (1 to 100)
			clustfile = file1_base+".denclust_"+str(per_similar)
			outfile = open(clustfile,'w')
			per_similar = per_similar*1.0
			den_cutoff = 2
			#Initialize a dictionary containing pointers to helix and sheet objects
			indx = 1
			for i in range(0,obj.n_helix):
				tlist = ['H',i]
				hs_dict[indx] = hx[i]
				main_hs_dict[indx] = hx[i]
				indx = indx + 1
			for i in range(0,obj.n_sheet):
				tlist = ['S',i]
				hs_dict[indx] = sh[i]
				main_hs_dict[indx] = sh[i]
				indx = indx + 1
			
			#start of clustering loop
			den_cluster = []
			# calculate the neighbor density for each secondary structural elements
			indx = 1
			for i in hs_dict.keys():
				tlist = []
				tlist.append(i)
				sse1 = hs_dict[i]
				nd = 0
				for j in hs_dict.keys():
					sse2 = hs_dict[j]
					if(i!=j):
						if(is_close(sse1,sse2,1,cutoff,per_similar)):
							tlist.append(j)
							nd = nd + 1
				if(nd >= den_cutoff):
					initial_reps[i] = tlist
				else:
					initial_reps[i] = []
			
			# for each such initial_reps iteratively collect all closer elements and make a cluster
			for i in initial_reps.keys():
				#tclust_list contains list of sse's close to initial_reps[i], including i
				tclust_list = initial_reps[i]
				#Iteratively find all the sse in the main pool close to the cluster members (tclust_list)
				itr_list = []
				for j in tclust_list:
					itr_list.append(j)
					
				if(len(itr_list) > 0 and (not(isin(itr_list[0],den_cluster)))):
					while(1):
						tclose = []
						for k in itr_list:
							for l in hs_dict.keys():
								if(not(l in tclust_list)):
									if(is_close(hs_dict[k],hs_dict[l],1,cutoff,per_similar)):
										tclose.append(l)
										tclust_list.append(l)
						itr_list = tclose
						
						if(len(itr_list) <= 0 and len(tclust_list) > 1):
							den_cluster.append(tclust_list)
							#remove all the records corresponding to tclust_list from the hs_dict
							for n in tclust_list:
								del hs_dict[n]
								#clear tclust_list
							tclust_list = []
							break
			
			nummem = 0
			for i in den_cluster:
				nummem = nummem + len(i)
			w_clust = 0.0
			total_sse = obj.n_helix+obj.n_sheet
			w_clust = float(len(den_cluster))+(float(nummem)/float(total_sse))
			self.w_clust2_array.append(round(w_clust,2))
			print "%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2)))
			clusterfile.write("%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2))))
			# Write header
			outfile.write('#RIBFIND_clusters: '+str(cutoff)+' '+str(per_similar)+' '+str(len(den_cluster))+' '+str(nummem)+' '+str(total_sse)+' '+str(round(w_clust,2))+'\n')
			outfile.write('#CUTOFF_value: ' + str(per_similar) +'\n')
			#write the clustering information to the denclust file
			clust_count = 0
			for i in den_cluster:
				clust_count = clust_count +1
				cluster = SubElement(ribfindRun, 'cluster', id=str(clust_count))
				tlist = i
				for j in tlist:
					sse = main_hs_dict[j]
					temp = sse.get_start_end()
					stype = sse.get_type()
					n = 0
					tstr = ''
					sse = SubElement(cluster, 'sse', id=str(j))					
					for k in temp:
						outfile.write(str(k)+' ')

				#Now append the loops that are connected within the cluster members
				lcnt = 0				
				for l in range(obj.n_loop):
					temp1 = lp[l].get_start_end()
					stype = lp[l].get_type()
					loop_start = temp1[0]
					loop_end = temp1[1]
					loop_start_in_cluster = 0
					loop_end_in_cluster = 0
					for m in tlist:
						sse = main_hs_dict[m]
						temp2 = sse.get_start_end()
						n = 1
						while(n<=len(temp2)/2):
							sse_start = temp2[n*2-2]
							sse_end = temp2[n*2-1]
							n = n + 1
							if(loop_start == sse_end+1):
								loop_start_in_cluster = 1
								#print "found loop start in cluster ",i
							if(loop_end == sse_start-1):
								loop_end_in_cluster = 1
								#print "found loop end in cluster ",i
					if(loop_start_in_cluster==1 and loop_end_in_cluster==1):
						loop = SubElement(cluster, 'loop', id=str(l+1))
						outfile.write(str(loop_start)+' '+str(loop_end)+' ')
						#color the loops falling into the respective clusters
						tstr = str(loop_start)+'-'+str(loop_end)
						#outfile1.write('runCommand("color '+colname+' #0:'+tstr+'")\n')
				outfile.write('\n')
			
			cmem = []
			all = []
			noclust = []
			
			#collect the list of all SSE indexes
			for i in range(1,obj.n_helix+obj.n_sheet+1):
				all.append(i)
			#collect the list of all cluster's SSE 
			for j in den_cluster:
				tlist = j
				for k in tlist:
					cmem.append(k)
			#deduce the list of indexes which are not in the cluster
			for i in all:
				found = 0
				for j in cmem:
					#print i,j
					if(i == j):
						found = 1
				if(found == 0):
					noclust.append(i)

			#write the SSE with their clusters information (input file to flex-em)
			outfile.write("#individual_rigid_bodies "+str(len(noclust))+"\n")
			if len(noclust) > 0 :
				nonClusteredSSEs = SubElement(ribfindRun, 'nonClusteredSSEs')
			for j in noclust:
				sse = main_hs_dict[j]
				temp = sse.get_start_end()
				stype = sse.get_type()
				sse = SubElement(nonClusteredSSEs, 'sse', id=str(j))				
				for k in temp:
					outfile.write(str(k)+' ')
				outfile.write('\n')
			outfile.close()

		#write the individual SSE without their clusters (input file to flex-em)
		IndividualSSEs = SubElement(ribfind, 'IndividualSSEs')
		outfile1.write("#individual_rigid_bodies "+str(len(all))+"\n")
		for j in all:
			sse = main_hs_dict[j]
			temp = sse.get_start_end()
			stype = sse.get_type()
			if stype == 1:
				label = 'Helix'
			if stype == 2:
				label = 'Sheet'
			sse = SubElement(IndividualSSEs, 'sse', id=str(j), stype=label)
			for k in range(len(temp)/2):
				residueRange = SubElement(sse, 'residueRange')
				begin = SubElement(residueRange, 'begin')
				begin.text = str(temp[k*2])
				end = SubElement(residueRange, 'end')
				end.text = str(temp[k*2+1])
			for k in temp:
				outfile1.write(str(k)+' ')
			outfile1.write('\n')
		outfile1.close()

		#write the individual loop information
		IndividualLoops = SubElement(ribfind, 'IndividualLoops')
		for l in range(obj.n_loop):
			temp1 = lp[l].get_start_end()
			stype = lp[l].get_type()
			loop_start = temp1[0]
			loop_end = temp1[1]
			loop = SubElement(IndividualLoops, 'loop', id=str(l+1))				
			residueRange = SubElement(loop, 'residueRange')
			begin = SubElement(residueRange, 'begin')
			begin.text = str(loop_start)
			end = SubElement(residueRange, 'end')
			end.text = str(loop_end)

		end = time.time()

		print 'Total cpu time : %f minutes' %((end-start)/60.0)
		self.max_cluster_index = self.w_clust2_array.index(max(self.w_clust2_array))+1
		preferredSolution.text = str(self.max_cluster_index)
		xmlstring = formatXML(ribfindResult)
		xmloutfile = open('ribfind.xml','w')
		xmloutfile.write(xmlstring)
		xmloutfile.close()
		#end of main program
