#!/bin/sh
#image.login
#
# test to see if we are using LMB architecture-aware scripts

export IMAGE_COM="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
IMAGE_PUBLIC=IMAGE_COM../
export IMAGE_PUBLIC
IMAGE_LASER=../postscript
export IMAGE_LASER
#
IMAGELIB=$IMAGE_PUBLIC/lib
export IMAGELIB
IMAGETEST=$IMAGE_PUBLIC/test
export IMAGETEST
IMAGEBIN=$IMAGE_PUBLIC/bin
export IMAGEBIN
IMAGECOM=$IMAGE_PUBLIC/com
export IMAGECOM
#
export PATH=${PATH}:$IMAGE_PUBLIC/bin:${PATH}
export PATH=${PATH}:$IMAGE_PUBLIC/local:${PATH}

