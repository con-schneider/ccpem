import re
import os
from ccpem_core import ccpem_utils
from struct import unpack_from

class MRCMapHeaderData(object):
  def __init__(self,
               filename,
               verbose=False,
               imod_SerialEM=True,
               imod_Agard=False):
    self.filename = filename
    self.is_map_file = True

    # Check filename is not None and exists
    if self.filename is None:
        self.is_map_file = False
        return
    elif not os.path.exists(filename):
        self.is_map_file = False
        return
    # Check file extension for the uploaded file, if not mrc raise error
    elif self.filename[-4:] not in ['.map', '.mrc', 'mrcs', 'ccp4']:
        print 'Warning: file extension not recognised'
        self.is_map_file = False
        return

    self.verbose = verbose
    self.imod_SerialEM = imod_SerialEM
    self.imod_Agard = imod_Agard
    self.resolution = None
    self.nx_ny_nz = ()
    self.mode = None
    self.nxstrt_nystrt_nzstrt = ()
    self.mx_my_mz = ()
    self.a_b_c = ()
    self.al_be_ga = ()
    self.dmin_dmax_dmean = ()

    if verbose:
        ccpem_utils.print_sub_sub_header(message='Map header meta data')
    header = file(filename).read(1344)
    self.nx_ny_nz = unpack_from('iii', header, 0)
    self.map_header_string = unpack_from('cccc', header, 52*4)
    expected_map_header_string = ('M', 'A', 'P', ' ')
    if (self.verbose):
      print '  NX,NY,NZ = ', self.nx_ny_nz
      print '  MAP = ', self.map_header_string
    self.mode = unpack_from('i', header, 3*4)[0]
   
    #to check header for the uploaded file and read header if not mrc format raise error
    if (self.map_header_string != expected_map_header_string):
        print '  WARNING map header string does not match expected'
        print '    Expected      : ', expected_map_header_string
        print '    Header string : ', self.map_header_string
        print 'Warning: file extension not recognised'
        self.is_map_file = False
        return
    
    if (self.verbose):
      print '  MODE = ', self.mode
      if (self.mode == 0):
        print 'File contains signed 8-bit bytes (range -128 to 127)'
      elif (self.mode == 1):
        print 'File contains signed 2-byte integers'
      elif (self.mode == 2):
        print 'File contains 4-byte reals'
      elif (self.mode == 3):
        print 'File contains complex 16-bit integers'
      elif (self.mode == 4):
        print 'File contains complex 32-bit reals'
      elif (self.mode == 5):
        print 'File is mode 5: we thought this was never used!'
      elif (self.mode == 6):
        print 'File is mode 6 (non-standard): contains unsigned 2-byte integers'
      elif (self.mode == 7):
        print 'File is mode 7 (non-standard): contains signed 4-byte integer'   
    
    if self.is_map_file: 
        self.nxstrt_nystrt_nzstrt = unpack_from('iii', header, 4*4)
        self.mx_my_mz = unpack_from('iii', header, 7*4)
        # v = number of volumes in dimension
        self.vx_vy_vz = tuple(map(lambda x,y: x/y, self.nx_ny_nz, self.mx_my_mz) )
        self.a_b_c = unpack_from('fff', header, 10*4)
        self.map_volume = self.a_b_c[0] * self.a_b_c[1] * self.a_b_c[2] # Ang^3
        self.voxx_voxy_voxz = tuple(map(lambda x,y: x/y, self.a_b_c, self.nx_ny_nz) )
        self.al_be_ga = unpack_from('fff', header, 13*4)
        self.mapc_mapr_maps = unpack_from('iii', header, 16*4)
        self.dmin_dmax_dmean = unpack_from('fff', header, 19*4)
    
        if (self.verbose):
          print '  NXSTART, NYSTART, NZSTART = ', self.nxstrt_nystrt_nzstrt
          print '  MX, MY, MZ = ', self.mx_my_mz
          print '  VX, VY, VZ = ', self.vx_vy_vz
          print '  A, B, C = ', self.a_b_c
          print '  AL, BE, GA = ', self.al_be_ga
          print '  MAPC, MAPR, MAPS = ', self.mapc_mapr_maps
          print '  DMIN, DMAX, DMEAN = ', self.dmin_dmax_dmean
        
        self.ispg = unpack_from('i', header, 22*4)[0]
        self.nsymbt = unpack_from('i', header, 23*4)[0]
        if (self.verbose):
          print '   ISPG = ', self.ispg
          print '   NSYMBT = ', self.nsymbt
        if (self.nsymbt > 0):
          print '  There is an extended header of %d bytes' % self.nsymbt
          print '  This is expected to consist of %d symmetry record(s)' % (self.nsymbt / 80)
    
        if (self.imod_SerialEM or self.imod_Agard):
          self.nint_nreal = unpack_from('hh', header, 32*4)
          self.imod_stamp = unpack_from('i', header, 38*4)
          self.imod_flags = unpack_from('i', header, 39*4)
          self.idtype_lens = unpack_from('hh', header, 40*4)
          self.nd1_nd2 = unpack_from('hh', header, 41*4)
          self.vd1_vd2 = unpack_from('hh', header, 42*4)
          self.tiltangles = unpack_from('ffffff', header, 43*4)
          if self.verbose:
            print 'IMOD specific header items:'
            (self.nint,nreal) = unpack_from('hh', header, 32*4)
            print '  nint, nreal = ', self.nint_nreal
            print '  imodStamp = ', self.imod_stamp
            print '  imodFlags = ', self.imod_flags
            print '  idtype, lens = ', self.idtype_lens
            print '  nd1,nd2 = ', self.nd1_nd2
            print '  vd1,vd2 = ', self.vd1_vd2
            print '  tiltangles = ', self.tiltangles
            print '  End of IMOD section'
        else:
          if (self.verbose):
            print '   (extra) = ', unpack_from('25i', header, 24*4)
    
        self.origin = unpack_from('fff',header,49*4)
        self.machst = '%x' % unpack_from('i',header,53*4)
        self.header_rms = unpack_from('f', header, 54*4)
        if self.verbose:
          print '  ORIGIN = ', self.origin
          #print '  MAP = ', self.map_header_string
          print '  MACHST = ', self.machst, '[should be 4144 or 4444]'
          print '  RMS = ', self.header_rms
    
        if ((not re.search('4144|4444', self.machst)) and self.verbose):
          print '  WARNING machst does not match expected'
          print '    Expected      : 4144 or 4444'
          print '    Header machst : ', self.machst
    
        self.nlabel = unpack_from('i',header,55*4)[0]
        if self.verbose:
          print '   NLABL = ', self.nlabel
          for i in range(self.nlabel):
             print '   label %d = ' % i,unpack_from('80s', header, 56*4 + i*80)
        
        if (self.nsymbt > 0 and self.nint_nreal[0] > 0):
          if (self.imod_SerialEM):
            for i in range(self.nsymbt / self.nint_nreal[0]):
              offset = 256*4 + i * self.nint_nreal[0]
              print unpack_from('h', header, offset)
          else:
            print '  The following symmetry records are present:'
            for i in range(self.nsymbt / 80):
              offset = 256*4 + i*80
              print unpack_from('80s', header, offset)[0]
        #to calculate extent of the box to search for in dockem
        self.min_box_size = min(int(self.mx_my_mz[0]), int(self.mx_my_mz[1]), int(self.mx_my_mz[2]))
        #self.box_sampling = (self.min_box_size * round(self.voxx_voxy_voxz[2],2))/2
    else:
        self.origin = (0,0,0) 
        self.vx_vy_vz = (0,0,0)
        self.voxx_voxy_voxz = (0,0,0)
        self.min_box_size = 0