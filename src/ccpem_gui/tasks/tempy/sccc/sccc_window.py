#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


### TODO
# Add rvapi results window:
#     Show rigid body regions
#     Show rigid body vs sccc
# Sort out displaying scores in mol. graphics.  e.g. bfactor plot etc.

'''
Task window for TEMPy SCCC Score.
'''
import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.tempy.sccc import sccc_task


class SCCCMapWindow(window_utils.CCPEMTaskWindow):
    '''
    SCCC window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(SCCCMapWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''

        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Map resolution
        self.map_resolution = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution)

        # Input pdb
        input_pdb_path = window_utils.FileArgInput(
            parent=self,
            arg_name='pdb_path',
            required=True,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_pdb_path)

        # Input rigid body
        input_rigid_body = window_utils.FileArgInput(
            parent=self,
            arg_name='rigid_body_path',
            required=True,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_rigid_body)

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.raise_()

    def set_on_job_finish_custom(self):
        # Add pdb to launcher
        self.launcher.add_file(
            arg_name='pdb_path',
            file_type='map',
            selected=True)
        # Add rigid body to launcher
        self.launcher.add_file(
            arg_name='rigid_body_path',
            selected=True)
        # Add map to launcher
        self.launcher.add_file(
            arg_name='map_path',
            file_type='map',
            description=self.args.map_path.help,
            selected=True)
        # Add raw data
        self.rawdata_path = os.path.join(self.task.job_location,
                                         'sccc_score.txt')
        if os.path.exists(self.rawdata_path):
            self.launcher.add_file(
                description='Raw sccc scores (CSV text)',
                path=self.rawdata_path)
        # Add chimera attributes file
        
        
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()

def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=sccc_task.SCCC,
        window=SCCCMapWindow)

if __name__ == '__main__':
    main()
