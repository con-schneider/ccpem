//***********************************************************************************************

//# Image display program : "GenesisViewer"
//# intial Qt program to display an image and overlay a transparent correlation map.
//# by Ashley Seepujak and Alan Roseman, 2013.
//# Funding by Genesis (Charity Number XXXXXXX) is gratefully acknowleged.


//***********************************************************************************************




#ifndef QT_NO_PRINTER

#include <QPrintDialog>
#endif

#include "imageviewer.h"

#include <string>
#include <QApplication>
#include <QWidget>
#include <QtWidgets>
#include <QtWidgets>
#include <QTimer>
#include <QVBoxLayout>
#include <QFont>
#include <QPainter>
#include <QLineF>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QHeaderView>

//************************************************************************************************
ImageViewer::ImageViewer()
{
    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    setCentralWidget(scrollArea);

    createActions();
    createMenus();
    createModel();
   // loadDataFromFile();
   // paintEvent();

    setWindowTitle(tr("Genesis Viewer v. 1.0.0"));
    resize(800, 700);

    //label = new QLabel("hI", this);
   // label->setGeometry(500, 40, 250, 50); // x-coord,y-coord, width, height
    //label->setStyleSheet("QLabel { background-color : red; color : blue; }");

    std::cout <<"\n*** Exit Genesis Viewer 1.0.0 ***\n\n";

    // QString testString = "C:/Users/Archimedes/Pictures/Mammogram1/n</p><p> sdftgr";
 //   label = new QLabel(testString, this);
  //   label->setFont(QFont("Purisa", 10));

  //   QVBoxLayout *vbox = new QVBoxLayout();
  //   vbox->addWidget(label);
  //   setLayout(vbox);

   // QwtPlotMarker *mY = new QwtPlotMarker();
     //   mY->setLabel(QString::fromLatin1("y = 0"));
      //  mY->setLabelAlignment(Qt::AlignRight|Qt::AlignTop);
    //    mY->setLineStyle(QwtPlotMarker::HLine);
    //    mY->setYValue(0.0);
   //     mY->attach(this);

//QwtPlotMarker *mY = new QwtPlotMarker();
  //  model = new QStandardItemModel( 0,0,this );
   // model->setHorizontalHeaderLabels( QStringList() << "Name" << "#" );


    //QWidget *window=new QWidget;
       //window->setWindowTitle(“Enter the marks”);
   // QSlider *slider=new QSlider(Qt::Horizontal);
   // slider->setRange(0,100);
   // QHBoxLayout *layout=new QHBoxLayout;
   //   layout->addWidget(slider);

   //   window->setLayout(layout);
   //   window->showNormal();
    imageCounter=1;
    opacityValue=1;
    CMLT=50;
    RMLT=60;


    controlPanel();



}
//********************************************************************************************



void ImageViewer::controlPanel()

{
QWidget *window = new QWidget;
window->setWindowTitle("Genesis Viewer Control Panel");

QSpinBox *spinBox = new QSpinBox;
QSlider *slider = new QSlider(Qt::Horizontal);
QDial *dial = new QDial;

QLabel *minimumLabel = new QLabel("Transparency [0 - 100%]");

spinBox->setRange(0, 100);
spinBox->setSingleStep(20);
spinBox->setSingleStep(20);
//spinBox->setPageStep(20);

slider->setRange(0, 100);
slider->setSingleStep(20);
slider->setPageStep(20);
slider->setTickPosition(QSlider::TicksBothSides);
slider->setTickInterval(20);

dial->setRange(0, 100);
dial->setSingleStep(20);
dial->setSingleStep(20);
dial->setPageStep(20);


QObject::connect(spinBox, SIGNAL(valueChanged(int)),slider, SLOT(setValue(int)));
QObject::connect(slider, SIGNAL(valueChanged(int)),slider, SLOT(valueHasChanged()));
QObject::connect(slider, SIGNAL(valueChanged(int)),spinBox, SLOT(setValue(int)));
QObject::connect(dial, SIGNAL(valueChanged(int)),spinBox, SLOT(setValue(int)));
QObject::connect(slider, SIGNAL(valueChanged(int)),dial, SLOT(setValue(int)));

QHBoxLayout *layout = new QHBoxLayout;
layout->addWidget(spinBox);
layout->addWidget(slider);
layout->addWidget(dial);
layout->addWidget(minimumLabel, 0, 0);
window->setLayout(layout);
   window->show();



}


//********************************************************************************************







void ImageViewer::open()

{
    QMessageBox::information(this, tr("Genesis Viewer"),tr("Please select raw image to open"));
    fileName = QFileDialog::getOpenFileName(this,tr("Open File"), QDir::currentPath());
    QMessageBox::information(this, tr("Genesis Viewer"),tr("Please select correlation map to open"));
    fileName2= QFileDialog::getOpenFileName(this,tr("Open File"), QDir::currentPath());
    QImage BottomMammogram(fileName);
    QImage TopMammogram(fileName2);
   // QMessageBox::information(this, tr("Genesis Viewer"),
                         //    tr("Cannot load %1.").arg(fileName));

    //fileName = QFileDialog::getOpenFileName(this,
   //                                 tr("Open File"), QDir::currentPath());

    QLabel *label = new QLabel("   "+fileName, this);
    label->setGeometry(550, 40, 250, 50); // x-coord,y-coord, width, height
    label->setStyleSheet("QLabel { background-color : red; color : blue; }");
    label->show();

    QLabel *label1 = new QLabel("   Tumour co-ordinates (x,y):", this);
    label1->setGeometry(550, 100, 250, 50); // x-coord,y-coord, width, height
    label1->setStyleSheet("QLabel { background-color : red; color : blue; }");
    label1->show();

    QLabel *label2 = new QLabel("   Tumour radius [a.u.]:", this);
    label2->setGeometry(550, 160, 250, 50); // x-coord,y-coord, width, height
    label2->setStyleSheet("QLabel { background-color : red; color : blue; }");
    label2->show();

    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Genesis Viewer"),
                                     tr("Cannot load %1.").arg(fileName));
            return;
        }

      changeOpacity(2,0);


     //  imageLabel->setPixmap(QPixmap::fromImage(image));// shows first image

// ----------------------------------------

       // QImage BottomMammogram(fileName);
       // BottomMammogram= BottomMammogram.convertToFormat(QImage::Format_ARGB32);
      //  image.fill(qRgba(0,0,0,255));

      //  QImage TopMammogram("C:/Users/Archimedes/Pictures/Mammogram1");
    //    TopMammogram= TopMammogram.convertToFormat(QImage::Format_ARGB32);
     //   image.fill(qRgba(0,0,0,255));


        //applyGreenAndTransparent(1.0);


      //  //  FLOATING WINDOW
     //   // Draw into floating window
     //   l_img->setPixmap(*img);//shows composite img which is now Ash image AND ring in floating window
     //   // l_img->setPixmap(QPixmap::fromImage(image));  //shows solely image in floating window
    //    l_img->show();

//QPixmap result(100,100);
//result.fill(Qt::transparent);
//{
 //   QPainter painter(result);

//}
//overlay = new QPixmap(100, 100);
//painter2 = new QPainter;
 //   painter2.drawPixmap(100,100,overlay);

//QLineF line(10.0, 80.0, 90.0, 20.0);
//QPainter painter(this);
//painter.drawLine(line);

      //  label5->setGeometry(10, 10, 250, 50); // x-coord,y-coord, width, height
       // label5->setStyleSheet("QLabel { background-color : red; color : blue; }");


      //  QColor hourColor(127, 0, 127);
      //  QPainter painter(this);
      //  painter.setRenderHint(QPainter::Antialiasing);
      //  painter.setBrush(hourColor);
     //   painter.setPen(hourColor);
      //  painter.drawLine(88, 10, 96, 30);
     //   painter.drawArc(50,30,150,150,0,16*360);

        scaleFactor = 1.0;

        printAct->setEnabled(true);
        fitToWindowAct->setEnabled(true);
        updateActions();

        if (!fitToWindowAct->isChecked())
            imageLabel->adjustSize();
    }
}
//************************************************************************************************
void ImageViewer::drawMarker(int imageCounter)

{
 painter1->drawArc(50,30,30,30,0,16*360);//(xcentre,ycentre,xdiameter,ydiameter)
}
//************************************************************************************************
void ImageViewer::print()

{
    Q_ASSERT(imageLabel->pixmap());
#ifndef QT_NO_PRINTER

    QPrintDialog dialog(&printer, this);

    if (dialog.exec()) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = imageLabel->pixmap()->size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(imageLabel->pixmap()->rect());
        painter.drawPixmap(0, 0, *imageLabel->pixmap());
    }
#endif
}
//************************************************************************************************
void ImageViewer::zoomIn()

{
    scaleImage(1.25);
}
//************************************************************************************************
void ImageViewer::zoomOut()

{
    scaleImage(0.8);
}
//************************************************************************************************
void ImageViewer::makeOpaque()

{
  changeOpacity(1,1);
}
//************************************************************************************************
void ImageViewer::makeTransluscent()

{
     changeOpacity(0,1);
}
//************************************************************************************************
void ImageViewer::previousImage()

{
   imageCounter-=1;
   QString imageSuffix= QString::number(imageCounter);

    QString previousImageFileName = "C:/Users/Archimedes/Pictures/Input/Mammogram0";
   // previousImageFileName.remove(QRegExp("1"));
    previousImageFileName.chop(1);
    previousImageFileName+=imageSuffix;

   // QString testString = "C:/Users/Archimedes/Pictures/Mammogram1/n</p><p> sdftgr";
  //  label = new QLabel(testString, this);
  //  label->setFont(QFont("Purisa", 10));
  //   QVBoxLayout *vbox = new QVBoxLayout();
  //  vbox->addWidget(label);
  //  setLayout(vbox);

    QLabel *label = new QLabel("   "+previousImageFileName, this);
    label->setGeometry(550, 40, 250, 50); // x-coord,y-coord, width, height
    label->setStyleSheet("QLabel { background-color : red; color : blue; }");
    label->show();

    QImage image(previousImageFileName);

    imageLabel->setPixmap(QPixmap::fromImage(image));//shows previous image

    scaleFactor = 1;

    printAct->setEnabled(true);
    fitToWindowAct->setEnabled(true);
    updateActions();

    if (!fitToWindowAct->isChecked())
        imageLabel->adjustSize();
}
//************************************************************************************************
void ImageViewer::nextImage()

{

    open();

   // imageCounter+=1;
  //  QString imageSuffix= QString::number(imageCounter);


   // QString nextImageFileName = "C:/Users/Archimedes/Pictures/Input/Mammogram1";
   // nextImageFileName.remove(QRegExp(""));
   // nextImageFileName.chop(1);
   // nextImageFileName+=imageSuffix;

  //  fileName.chop(1);

  // fileName+=imageSuffix;
   // fileName2.chop(1);

//  fileName2+=imageSuffix;



   // QLabel *label = new QLabel("   "+nextImageFileName, this);
  // label->setGeometry(550, 40, 250, 50); // x-coord,y-coord, width, height
  //  label->setStyleSheet("QLabel { background-color : red; color : blue; }");
  //  label->show();

   // QImage image(nextImageFileName);

   // imageLabel->setPixmap(QPixmap::fromImage(image));//shows next image

  //  scaleFactor = 1;

  //  printAct->setEnabled(true);
   // fitToWindowAct->setEnabled(true);
 //   updateActions();

  //  if (!fitToWindowAct->isChecked())
  //      imageLabel->adjustSize();

    // THIS WORKS
   // fileName = "C:/Users/Archimedes/Pictures/Input/Mammogram2";
   // fileName2 = "C:/Users/Archimedes/Pictures/Output/Mammogram2";
   // changeOpacity(2);

}
//************************************************************************************************
void ImageViewer::normalSize()

{
    imageLabel->adjustSize();
    scaleFactor = 1.0;
}
//************************************************************************************************
void ImageViewer::fitToWindow()

{
    bool fitToWindow = fitToWindowAct->isChecked();
    scrollArea->setWidgetResizable(fitToWindow);

    if (!fitToWindow) {
        normalSize();
    }
    updateActions();
}
//************************************************************************************************
void ImageViewer::info()

{
    QMessageBox::about(this, tr("About Genesis Viewer"),
                       tr("<p><b>Genesis Viewer v. 1.0.0 </b> "
               "</p><p>Authors: A. Seepujak (University of "
               "Manchester, current address: University of St Andrews) and A.M. Roseman "
               "(University of Manchester).</p><p>"
               "Funding by Genesis (Charity Number XXXXXXX) is gratefully acknowleged."));
}
//************************************************************************************************
void ImageViewer::createActions()

{
    openAct = new QAction(tr("&Open Files..."), this);
    openAct->setShortcut(tr("Ctrl+O"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    printAct = new QAction(tr("&Print..."), this);
    printAct->setShortcut(tr("Ctrl+P"));
    printAct->setEnabled(false);
    connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    zoomInAct = new QAction(tr("Zoom &In (25%)"), this);
    zoomInAct->setShortcut(tr("Ctrl++"));
    zoomInAct->setEnabled(false);
    connect(zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()));

    zoomOutAct = new QAction(tr("Zoom &Out (25%)"), this);
    zoomOutAct->setShortcut(tr("Ctrl+-"));
    zoomOutAct->setEnabled(false);
    connect(zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()));

    makeOpaqueAct = new QAction(tr("&Make Opaque"), this);
    makeOpaqueAct->setShortcut(tr("Ctrl+D"));
   // makeOpaqueAct->setEnabled(false);
    connect(makeOpaqueAct, SIGNAL(triggered()), this, SLOT(makeOpaque()));

    makeTransluscentAct = new QAction(tr("&Make Transluscent"), this);
    makeTransluscentAct->setShortcut(tr("Ctrl+L"));
   // makeOpaqueAct->setEnabled(false);
    connect(makeTransluscentAct, SIGNAL(triggered()), this, SLOT(makeTransluscent()));

    normalSizeAct = new QAction(tr("&Normal Size"), this);
    normalSizeAct->setShortcut(tr("Ctrl+S"));
    normalSizeAct->setEnabled(false);
    connect(normalSizeAct, SIGNAL(triggered()), this, SLOT(normalSize()));

    fitToWindowAct = new QAction(tr("&Fit to Window"), this);
    fitToWindowAct->setEnabled(false);
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setShortcut(tr("Ctrl+F"));
    connect(fitToWindowAct, SIGNAL(triggered()), this, SLOT(fitToWindow()));

    nextImageAct = new QAction(tr("&Show next image"), this);
    nextImageAct->setShortcut(tr("Ctrl+."));
    connect(nextImageAct, SIGNAL(triggered()), this, SLOT(nextImage()));

    previousImageAct = new QAction(tr("&Show previous image"), this);
    previousImageAct->setShortcut(tr("Ctrl+,"));
    connect(previousImageAct, SIGNAL(triggered()), this, SLOT(previousImage()));

    infoAct = new QAction(tr("&About"), this);
    connect(infoAct, SIGNAL(triggered()), this, SLOT(info()));

    infoQtAct = new QAction(tr("About &Qt"), this);
    connect(infoQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));




}
//************************************************************************************************
void ImageViewer::createMenus()

{
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(openAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    viewMenu = new QMenu(tr("&View"), this);
    viewMenu->addAction(zoomInAct);
    viewMenu->addAction(zoomOutAct);
    viewMenu->addAction(normalSizeAct);
    viewMenu->addSeparator();
    viewMenu->addAction(fitToWindowAct);
    viewMenu->addAction(makeOpaqueAct);
    viewMenu->addAction(makeTransluscentAct);

    mammogramMenu = new QMenu(tr("&Mammogram"), this);
    mammogramMenu->addAction(nextImageAct);
    mammogramMenu->addAction(previousImageAct);

    helpMenu = new QMenu(tr("&Help"), this);

    aboutMenu = new QMenu(tr("&About"), this);
    aboutMenu->addAction(infoAct);
    aboutMenu->addAction(infoQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(viewMenu);
    menuBar()->addMenu(mammogramMenu);
    menuBar()->addMenu(helpMenu);
    menuBar()->addMenu(aboutMenu);
}
//************************************************************************************************
void ImageViewer::updateActions()

{
    zoomInAct->setEnabled(!fitToWindowAct->isChecked());
    zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
    normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
}
//************************************************************************************************
void ImageViewer::scaleImage(double factor)

{
    Q_ASSERT(imageLabel->pixmap());
    scaleFactor *= factor;
    imageLabel->resize(scaleFactor * imageLabel->pixmap()->size());

    adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(scrollArea->verticalScrollBar(), factor);

    zoomInAct->setEnabled(scaleFactor < 3.0);
    zoomOutAct->setEnabled(scaleFactor > 0.333);
}


void ImageViewer::valueHasChanged()
{
QMessageBox::information(this, "SimpleSlider", "Value has Changed");
}




//************************************************************************************************
void ImageViewer::changeOpacity(int otSwitch, int transparencySwitch)

{
    QImage BottomMammogram(fileName);
     BottomMammogram=BottomMammogram.convertToFormat(QImage::Format_ARGB32);
    QImage TopMammogram(fileName2);
    TopMammogram=TopMammogram.convertToFormat(QImage::Format_ARGB32);


  //  QImage TopMammogram("C:/Users/Archimedes/Pictures/Output/Mammogram2");
 //  TopMammogram=TopMammogram.convertToFormat(QImage::Format_ARGB32);
 // QImage BottomMammogram("C:/Users/Archimedes/Pictures/Input/Mammogram2");


   // QImage BottomMammogram(fileName);
   // QImage TopMammogram(fileName2);




   int height = TopMammogram.height();
   int width = TopMammogram.width();
   int z,y,j;


if(  transparencySwitch==0 ) {
             for( y = 0; y < height; ++y ) {
             QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine( y ) );
                   for( j = 0; j < width; ++j ) {
                       // MAKE TOP LAYER TRANSPARENT IF BELOW CMLT THRESHOLD, ELSE GREENSCALE IF ABOVE
                       if( qRed(line[j]) <CMLT )  {
                                                   line[j] = qRgba(0, 0, 0, 0);
                       }
                       else                       {
                                                   line[j] = qRgb(0,qGreen(line[j]),0);
                       }
                   }
             }
}



if(  transparencySwitch==1 ) {



    // MAKE TOP LAYER GREENSCALE IF ABOVE CMLT THRESHOLD

        for( y = 0; y < height; ++y ) {
        QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine ( y ) );
           for( j = 0; j < width; ++j ) {
               if(  qRed(line[j]) >CMLT ) {
                                           line[j] = qRgb(0,qGreen(line[j]),0);
               }
           }
        }

}






// MAKE TOP LAYER GREENSCALE IF ABOVE CMLT THRESHOLD AND ABOVE RMLT THRESHOLD

//    for( y = 0; y < height; ++y ) {
 //   QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine ( y ) );
//       for( j = 0; j < width; ++j ) {
 //          if(  qRed(line[j]) >CMLT ) {
 //                                      line[j] = qRgb(0,qGreen(line[j]),0);
 //          }
 //      }
 //   }










// MAKE TOP LAYER TRANSPARENT IF BELOW CMLT THRESHOLD
// for (z = 0; z < CMLT; ++z) {
//         for( y = 0; y < height; ++y ) {
//         QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine( y ) );
//               for( j = 0; j < width; ++j ) {
//                   if( qRed(line[j]) == z )  {
//                                               line[j] = qRgba(0, 0, 0, 0);
//                   }
//               }
//         }
//  }
//}

// MAKE TOP LAYER GREENSCALE IF ABOVE CMLT THRESHOLD AND ABOVE RMLT THRESHOLD
//for (z = CMLT; z < 255; ++z) {
//for( y = 0; y < height; ++y ) {
//QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine ( y ) );
//QRgb *line1 = reinterpret_cast< QRgb * >( BottomMammogram.scanLine ( y ) );
//   for( j = 0; j < width; ++j ) {
//       if( qRed(line1[j]) > RMLT && qRed(line[j]) == z) {
//                                                           line[j] = qRgb(0,z,0);
//       }
//   }
//}
//}
















painter1 = new QPainter;
img= new QPixmap (BottomMammogram.size());
l_img = new QLabel;

painter1->begin(img);

//Puts BottomMammogram into the base layer
painter1->fillRect(img->rect(), Qt::transparent);
painter1->drawImage(0, 0, BottomMammogram); // puts BottomMammogram image into img

if (otSwitch==0){
opacityValue*=0.5;
}

if (otSwitch==1){
opacityValue*=1.5;
}

if (otSwitch==2){
opacityValue=1;
}

painter1->setOpacity(opacityValue);
painter1->drawImage(0, 0, TopMammogram); // puts TopMammogram image into img

// Draw ring

// QPen transRed(QColor(0xFF, 0,10, 0x80));
QPen pen1(Qt::red, 6);
painter1->setPen(pen1);
// painter1->drawArc(50,30,30,30,0,16*360);//(xcentre,ycentre,xdiameter,ydiameter)
//drawMarker(imageCounter);
loadDataFromFile();

painter1->end();

//draws the overlay containing the ring onto base layer
// imageLabel->setPixmap(QPixmap::fromImage(image));// shows image in main window
imageLabel->setPixmap(*img); //draws overlay ring onto img
imageLabel->show();




}
//************************************************************************************************
void ImageViewer::adjustScrollBar(QScrollBar *scrollBar, double factor)

{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}
//************************************************************************************************
void ImageViewer::loadDataFromFile()

{
    for( int i=0; i<10; ++i)
             freqMap[i]=0;
  //  treeView->setModel( model );
    model = new QStandardItemModel( 0,0,this );

    QFile infile( "C:/Qt/Qt5.0.2/Tools/QtCreator/bin/Main/data1.txt" );

    if( infile.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
        QTextStream textStream( &infile );
     QList<QStandardItem *> items;


         for( int r=0; r<10; ++r){

        }
         QString tmpStr;
         QString line;


         while( !textStream.atEnd() ) {

                  items.clear();






       line=textStream.readLine();


items << new QStandardItem( line.section( " ", 0, 0 ) );
int xcentre = line.section( " ", 0,0 ).toInt();

items << new QStandardItem( line.section( " ", 1, 1 ) );
int ycentre = line.section( " ", 1,1 ).toInt();

items << new QStandardItem( line.section( " ", 2, 2 ) );
int xdiameter = line.section( " ", 2,2 ).toInt();

items << new QStandardItem( line.section( " ", 3, 3 ) );
int ydiameter = line.section( " ", 3,3 ).toInt();
    painter1->drawArc(xcentre,ycentre,xdiameter,ydiameter,0,16*360);//(xcentre,ycentre,xdiameter,ydiameter)


  //   for( int i = 0; i < 3; ++i ){
  //        items << new QStandardItem( tmpStr.section( " ", i+2, i+2 ) );
         //  increment key in freqMap when # encountered
   //     freqMap[ tmpStr.section( " ", i+2, i+2 ).toInt() ]++;
}

  //   for( int i=0; i<10; ++i ) {
  //      textEdit->append( tr( "%1 = %2" ).arg( i ).arg( freqMap[i] ) );
  //      textEdit->textCursor().setPosition( QTextCursor::Start );
  //   }




    //    model->appendRow( items );


         infile.close();
        }




}
      //  QString tmpStr1;
  //      int count = 0;

        // populate freqMap ::  key/value  0=0, 1=0, ..., 9=0
   //*     for( int i=0; i<10; ++i)
   //*         freqMap[i]=0;

   //*     while( !textStream.atEnd() ) {

    //*        items.clear();

            // incoming text in form of
            // col 1 = title
            // col 2 = # in array
            // col 3-# = numbers

          //  tmpStr = textStream.readLine();

            // col 1 - title
        //    items << new QStandardItem( tmpStr.section( " ", 0, 0 ) );

            // col 2 - count ref
       //     items << new QStandardItem( tmpStr.section( " ", 4, 4 ) );
           // count = tmpStr.section( " ", 1,1 ).toInt();

            // col 3 - count ref
           // items << new QStandardItem( tmpStr.section( " ", 5, 5 ) );
            //count = tmpStr.section( " ", 1,1 ).toInt();

            // col 3 - count ref
          //  items << new QStandardItem( tmpStr.section( " ", 6, 6 ) );
          //  count = tmpStr.section( " ", 1,1 ).toInt();

            // cols 5 ->
           // for( int i = 0; i < count; ++i ){
           //      items << new QStandardItem( tmpStr.section( " ", i+2, i+2 ) );
                 // increment key in freqMap when # encountered
            //     freqMap[ tmpStr.section( " ", i+2, i+2 ).toInt() ]++;



           // }
          //  model->appendRow( items );




       //     QStandardItem *firstRow = new QStandardItem(QString("ColumnValue"));
          //     for( int i = 0; i < count; ++i ){
            //QLabel *label3 = new QLabel(tmpStr.section( " ", 2, 2 ), this);
       //    QLabel *label3 = new QLabel(tmpStr.section( "mdb001", 0,0 ), this);
         //   QLabel *label3 = new QLabel(model, this);


        //   QLabel *label3 = new QLabel(model, this);
       //   label3->setGeometry(550, 220, 250, 50); // x-coord,y-coord, width, height
      //     label3->setStyleSheet("QLabel { background-color : red; color : blue; }");
       //     label3->show();
       //*        }


    //    }
//************************************************************************************************
void ImageViewer::createModel()

{
    model = new QStandardItemModel( 0,0,this );
    model->setHorizontalHeaderLabels( QStringList() << "Name" << "#" );
}
//************************************************************************************************
void ImageViewer::paintEvent(QPaintEvent *)
{
  // ash.show();
    QPainter painter(this);
    painter.drawLine(88, 10, 96, 300);

    painter.drawArc(50,30,150,150,0,16*360);
          painter.drawArc(200,80,100,100,0,16*360);
          painter.drawArc(300,130,50,50,0,16*360);
}
//************************************************************************************************
void ImageViewer::createTable(int imageCounter)

{

}
//************************************************************************************************
//************************************************************************************************
//void ImageViewer::applyGreenAndTransparent(double factor)

//{
//    int height, width, lowThreshold;
//    lowThreshold=200;

//    QImage TopMammogram("C:/Users/Archimedes/Pictures/Output/Mammogram1");
//    TopMammogram=TopMammogram.convertToFormat(QImage::Format_ARGB32);
//    QImage BottomMammogram("C:/Users/Archimedes/Pictures/Input/Mammogram1");
//    //BottomMammogram=BottomMammogram.convertToFormat(QImage::Format_ARGB32);

//   width = TopMammogram.width();
 //   height = TopMammogram.height();

 //   for (int z = 0; z < 255; ++z) {
 //       for (int x = 0; x < width; ++x) {
 //           for (int y = 0; y < height; ++y) {

 //           color = TopMammogram.pixel(x,y);
 //                //if (qRed(color) == z && qGreen(color) == z && qBlue(color) == z){
 //                 if (qRed(color) == z){
 //                   if (z >= lowThreshold) {
 //                       TopMammogram.setPixel(x, y, qRgba(0, z, 0, 255));
 //                       } else {
 //                       TopMammogram.setPixel(x, y, qRgba(0, 0, 0, 0));
//                        }
//
//                    }
//
 //               }
 //           }
//        }

// MAKE TOP LAYER GREENSCALE IF ABOVE THRESHOLD
 //   for (int z = lowThreshold; z < 255; ++z) {
 //       QRgb green = qRgb(0,z,0), black = qRgb(z,z,z);
 //       for( int y = 0; y < height; ++y ) {
 //          QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine( y ) );
 //          for( int j = 0; j < TopMammogram.width(); ++j ) {
 //             if( line[j] == black ) {
//                 line[j] = green;
 //             }

//           }
 //       }
//    }

// MAKE TOP LAYER TRANSPARENT IF BELOW THRESHOLD
//for (int z = 0; z < lowThreshold; ++z) {
//    QRgb black = qRgb(z,z,z);
//    for( int y = 0; y < height; ++y ) {
//       QRgb *line = reinterpret_cast< QRgb * >( TopMammogram.scanLine( y ) );
//       for( int j = 0; j < width; ++j ) {
//          if( line[j] == black ) {
//             line[j] = qRgba(0, 0, 0, 0);
//          }

//       }
//    }
//}

 //   painter1 = new QPainter;
 //   img= new QPixmap (BottomMammogram.size());
//    l_img = new QLabel;

 //   painter1->begin(img);

    //Puts BottomMammogram into the base layer
 //   painter1->fillRect(img->rect(), Qt::transparent);
//    painter1->drawImage(0, 0, BottomMammogram); // puts BottomMammogram image into img
//    opacityValue*=factor;
//    painter1->setOpacity(opacityValue);
//    painter1->drawImage(0, 0, TopMammogram); // puts TopMammogram image into img

   // Draw ring

   // QPen transRed(QColor(0xFF, 0,10, 0x80));
//    QPen pen1(Qt::red, 6);
//    painter1->setPen(pen1);
    // painter1->drawArc(50,30,30,30,0,16*360);//(xcentre,ycentre,xdiameter,ydiameter)
    //drawMarker(imageCounter);
//    loadDataFromFile();

//    painter1->end();

    //draws the overlay containing the ring onto base layer
    // imageLabel->setPixmap(QPixmap::fromImage(image));// shows image in main window
//    imageLabel->setPixmap(*img); //draws overlay ring onto img
//    imageLabel->show();
//
//}

//*****************************************************************************************************





































