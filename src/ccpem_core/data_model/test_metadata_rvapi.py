#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Unit tests for metadata_utils
'''

import sys
import os
import shutil
import unittest
import matplotlib.pyplot as plt
from ccpem_core.data_model import metadata_utils
from ccpem_core.ccpem_utils import ccpem_rvapi
import pyrvapi
import tempfile

class Test(unittest.TestCase):
    '''
    Unit test
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    @unittest.skipIf(sys.platform == 'darwin',
                    "does not run on mac")
    def test_matplotlib(self, show=False):
        '''
        Test matplotlib usage with pandas
        '''
        print '\n\n', sys._getframe().f_code.co_name
        metadata = metadata_utils.MetaDataContainer()
        metadata.import_star_file(
            filename=self.test_data + '/rln_postprocess_run1.star',
            block_names=None)
        guinier_data = metadata['guinier']
        guinier_data.set_index('ResolutionSquared', inplace=True)
        guinier_data.plot()
        # show as pop-up
        plt.style.use('ggplot')
        if show:
            plt.show()

    @unittest.skipIf(sys.platform == 'darwin',
                    "does not run on mac")
    def test_rvapi(self, show=False):
        '''
        Test metadata / RVAPI bindings
        '''
        print '\n\n', sys._getframe().f_code.co_name
        print 'TEST Metadata / RVAPI'

        # Setup share jsrview
        html_path = ccpem_rvapi.set_rvapi_document(
            'md_unit_test',
            self.test_output,
            'MD Unit Test')
#         rv_doc = ccpem_rvapi.RVAPIDocument('md_unit_test',
#                                            self.test_output,
#                                            'MD Unit Test')
        metadata = metadata_utils.MetaDataContainer()
        metadata.import_star_file(
            filename=self.test_data + '/rln_postprocess_run1.star',
            block_names=None)
        guinier_data = metadata['guinier']
        guinier_data.set_index('ResolutionSquared', inplace=True)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    unittest.main()
