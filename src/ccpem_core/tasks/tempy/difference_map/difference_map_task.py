#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.TEMPy.Examples import difference_map


class DifferenceMap(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy difference map wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPyDifferenceMap',
        author='M. Topf, A. Joseph',
        version='1.1',
        description=(
            'Difference map calculation using TEMPy library'),
        short_description=(
            'Difference map calculation'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=None)

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        command = ['ccpem-python', os.path.realpath(difference_map.__file__)]
        #
        super(DifferenceMap, self).__init__(command=command,
                                     task_info=task_info,
                                     database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path_1 = parser.add_argument_group()
        map_path_1.add_argument(
            '-map_path_1',
            '--map_path_1',
            help='Input map 1 (mrc format)',
            metavar='Input map 1',
            type=str,
            default=None)
        #
        map_resolution_1 = parser.add_argument_group()
        map_resolution_1.add_argument(
            '-map_resolution_1',
            '--map_resolution_1',
            help='''Resolution of map 1 (Angstrom)''',
            metavar='Resolution map 1',
            type=float,
            default=None)
        #
        map_contour_1 = parser.add_argument_group()
        map_contour_1.add_argument(
            '-map_contour_1',
            '--map_contour_1',
            help='''Contour level of map 1 (optional)''',
            metavar='Contour map 1',
            type=float,
            default=None)
        #
        map_path_2 = parser.add_argument_group()
        map_path_2.add_argument(
            '-map_path_2',
            '--map_path_2',
            help='Input map 2 (mrc format)',
            metavar='Input map 2',
            type=str,
            default=None)
        #
        map_resolution_2 = parser.add_argument_group()
        map_resolution_2.add_argument(
            '-map_resolution_2',
            '--map_resolution_2',
            help='''Resolution of map 2 (Angstrom)''',
            metavar='Resolution map 2',
            type=float,
            default=None)
        #
        map_contour_2 = parser.add_argument_group()
        map_contour_2.add_argument(
            '-map_contour_2',
            '--map_contour_2',
            help='''Contour level of map 2 (optional)''',
            metavar='Contour map 2',
            type=float,
            default=None)
        #
        pdb_path = parser.add_argument_group()
        pdb_path.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Synthetic map will be created and difference maps '
                  'calculated using this'),
            metavar='Input PDB',
            type=str,
            default=None)
        #
        map_or_pdb_selection = parser.add_argument_group()
        map_or_pdb_selection.add_argument(
            '-map_or_pdb_selection',
            '--map_or_pdb_selection',
            help=('Select map or PDB'),
            metavar='Input selection',
            choices=['Map', 'PDB'],
            type=str,
            default='Map')
        #
        shell_width = parser.add_argument_group()
        shell_width.add_argument(
            '-shell_width',
            '--shell_width',
            help='''Shell width for map scaling (1/Angstrom) (optional)''',
            metavar='Shell width',
            type=float,
            default=0.05)
        #
        dust_filter = parser.add_argument_group()
        dust_filter.add_argument(
            '-dust_filter',
            '--dust_filter',
            help='''Remove dust noise''',
            metavar='Dust filter',
            type=bool,
            default=True)
        #
        map_alignment = parser.add_argument_group()
        map_alignment.add_argument(
            '-map_alignment',
            '--map_alignment',
            help='''Map alignment (~15mins) (optional)''',
            metavar='Align maps',
            type=bool,
            default=False)
        #
        soft_mask = parser.add_argument_group()
        soft_mask.add_argument(
            '-soft_mask',
            '--soft_mask',
            help='''Apply soft mask to input maps (optional)''',
            metavar='Apply soft mask',
            type=bool,
            default=False)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        # Generate process
        self.tempy_difference_map = TEMPyDifferenceMap(
            job_location=self.job_location,
            command=self.command,
            map_path_1=self.args.map_path_1.value,
            map_path_2=self.args.map_path_2.value,
            map_resolution_1=self.args.map_resolution_1.value,
            map_resolution_2=self.args.map_resolution_2.value,
            map_contour_1=self.args.map_contour_1.value,
            map_contour_2=self.args.map_contour_2.value,
            pdb_path=self.args.pdb_path.value,
            map_or_pdb=self.args.map_or_pdb_selection.value,
            shell_width=self.args.shell_width.value,
            dust_filter=self.args.dust_filter.value,
            map_alignment=self.args.map_alignment.value,
            soft_mask=self.args.soft_mask.value)

        pl = [[self.tempy_difference_map.process]]
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class TEMPyDifferenceMap(object):
    '''
    Wrapper for TEMPy difference map process.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path_1,
                 map_resolution_1,
                 map_contour_1=None,
                 map_path_2=None,
                 map_resolution_2=None,
                 map_contour_2=None,
                 pdb_path=None,
                 map_or_pdb='Map',
                 shell_width=None,
                 dust_filter=True,
                 map_alignment=False,
                 soft_mask=False,
                 name=None):
        assert [pdb_path, map_path_2].count(None) != 2
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path_1 = ccpem_utils.get_path_abs(map_path_1)
        # Set args
        self.args = ['-m1', self.map_path_1,
                     '-r1', map_resolution_1]
        # Difference map 1 vs map 2 or PDB
        if map_or_pdb == 'PDB':
            self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
            self.args += ['-p', self.pdb_path]
        else:
            self.map_path_2 = ccpem_utils.get_path_abs(map_path_2)
            self.args += ['-m2', self.map_path_2]
            self.args += ['-r2', map_resolution_2]
        #
        if map_contour_1 == 0:
            map_contour_1 = None
        if map_contour_2 == 0:
            map_contour_2 = None
        #
        if map_contour_1 is not None:
            self.args += ['-t1', map_contour_1]
        if map_contour_2 is not None:
            self.args += ['-t2', map_contour_2]
        #
        if not dust_filter:
            self.args += ['-nodust']
        if shell_width is not None:
            self.args += ['-sw', shell_width]
        if soft_mask:
            self.args += ['-smask']

#         XXX TODO: Agnel to write code for alignment, may take some time!
#         if map_alignment:
#             self.args += ['-XXX', '???']

        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

def main():
    '''
    Run task
    '''
    task_utils.command_line_task_launch(
        task=DifferenceMap)

if __name__ == '__main__':
    main()
