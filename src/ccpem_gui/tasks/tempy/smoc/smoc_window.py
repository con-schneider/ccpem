#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for TEMPy SMOC Score.
'''
import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.tempy.smoc import smoc_task


class PBBDatasets(object):
    def __init__(self,
                 pdb_path=None):
        self.pdb_path = None
        self.set_pdb_path(pdb_path=pdb_path)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = os.path.abspath(pdb_path)

    def validate(self):
        if [self.pdb_path].count(None) == 0:
            return True
        else:
            return False


class PDBInput(QtGui.QWidget):
    def __init__(self,
                 label='PBD',
                 pdb_path=None,
                 chains=None,
                 parent=None):
        super(PDBInput, self).__init__()
        self.parent = parent
        self.dataset = PBBDatasets()
        self.pdb_stats = None
        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        self.setToolTip('Add PDB')

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        self.layout.addLayout(pdb_box)

        self.pdb_label = QtGui.QLabel(label)
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        pdb_select = QtGui.QPushButton('Select')
        pdb_select.setFixedWidth(button_width)
        pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finised_pdb)
        pdb_box.addWidget(self.pdb_value)

        width_ar = 30
        # Add
        add_button = QtGui.QPushButton('+')
        add_button.setFixedWidth(width_ar)
        add_button.setToolTip('Add dataset')
        add_button.clicked.connect(self.add)
        pdb_box.addWidget(add_button)

        # Remove
        remove_button = QtGui.QPushButton('-')
        remove_button.setFixedWidth(width_ar)
        remove_button.setToolTip('Remove dataset')
        remove_button.clicked.connect(self.remove)
        pdb_box.addWidget(remove_button)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        if path is not None:
            self.set_pdb_file(path=path)

    def edit_finised_pdb(self):
        path = self.pdb_value.text()
        self.set_pdb_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)
            else:
                path = None

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        '''
        Only remove if more than 2 are present
        '''
        if hasattr(self.parent, 'number_pdb_inputs'):
            if self.parent.number_pdb_inputs() >= 2:
                self.deleteLater()

    def add(self):
        if hasattr(self.parent, 'add_dataset'):
            self.parent.add_dataset()

class MultiPDBInput(QtGui.QWidget):
    def __init__(self,
                 group_label=None,
                 pdbs_arg=None,
                 chains_arg=None,
                 required=True,
                 parent=None):
        super(MultiPDBInput, self).__init__()
        self.parent = parent
        self.pdbs_arg = pdbs_arg
        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Set tooltip
        tooltip_text = self.pdbs_arg.help
        if required:
            tooltip_text += ' | Required'
        self.setToolTip(tooltip_text)
        
        # Label
        if group_label is not None:
            ds_label = QtGui.QLabel(group_label)
            self.layout.addWidget(ds_label)

        # Set args
        pdbs = self.pdbs_arg()

        # Add datasets
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n])
        else:
            self.add_dataset(
                pdb_path=pdbs)

    def handle_add_button(self):
        '''
        Handle dataset button
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    chains=None):
        dataset = PDBInput(pdb_path=pdb_path,
                           parent=self)
        self.layout.addWidget(dataset)

    def number_pdb_inputs(self):
        return len(self.findChildren(PDBInput))

    def validate_datasets(self):
        children = self.findChildren(PDBInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires PDB)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self):
        children = self.findChildren(PDBInput)
        pdbs = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
            self.pdbs_arg.value = pdbs
        else:
            child = children[0]
            self.pdbs_arg.value = child.dataset.pdb_path

class SMOCMapWindow(window_utils.CCPEMTaskWindow):
    '''
    SMOC window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(SMOCMapWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''

        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Map resolution
        self.map_resolution = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            maximum=7.5,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution)

        # Input PDBs
        self.input_pdbs = MultiPDBInput(
            group_label='Input PDB(s)',
            pdbs_arg=self.args.input_pdbs,
            chains_arg=self.args.input_pdb_chains,
            parent=self)
        self.args_widget.args_layout.addWidget(self.input_pdbs)

        # Advanced options
        advanced_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Advanced options',
            button_tooltip='Specify additional parameters')
        self.args_widget.args_layout.addLayout(advanced_options_frame)


        # -> Weight auto
        self.input_auto_length = window_utils.CheckArgInput(
            parent=self,
            arg_name='auto_fragment_length',
            args=self.args)
        advanced_options_frame.add_extension_widget(self.input_auto_length)
        self.input_auto_length.value_line.stateChanged.connect(
            self.set_fragment_length_auto)

        # Fragment length
        self.input_fragament_length = window_utils.NumberArgInput(
            parent=self,
            arg_name='fragment_length',
            required=True,
            args=self.args)
        advanced_options_frame.add_extension_widget(self.input_fragament_length)
        self.set_fragment_length_auto()

    def set_fragment_length_auto(self):
        if self.args.auto_fragment_length():
            self.input_fragament_length.hide()
        else:
            self.input_fragament_length.show()

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.raise_()

    def handle_toolbar_run(self):
        '''
        Over ride from baseclass to allow validation of multiple datasets
        '''
        if self.check_input():
            # Valdited multiple datasets
            if not self.input_pdbs.validate_datasets():
                return
            else :
                self.input_pdbs.set_args()
        self.run_detached_process()

    def set_on_job_finish_custom(self):
        # Add pdbs to launcher
        pdbs = self.task.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        for pdb in pdbs:
            self.launcher.add_file(
                path=pdb,
                file_type='pdb',
                description='Input pdb',
                selected=True)
        # Add map to launcher
        self.launcher.add_file(
            arg_name='map_path',
            file_type='map',
            description=self.args.map_path.help,
            selected=True)
        # Add raw data
        self.rawdata_path = os.path.join(self.task.job_location,
                                         'smoc_score.txt')
        if os.path.exists(self.rawdata_path):
            self.launcher.add_file(
                description='Raw smoc scores (CSV text)',
                path=self.rawdata_path)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()

def main():
    '''
    Launch stand alone window.
    '''
    window_utils.standalone_window_launch(
        task=smoc_task.SMOC,
        window=SMOCMapWindow)

if __name__ == '__main__':
    main()
