#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
For updating plots and displaying it in the output tabs
'''
import os
import warnings
from PyQt4 import QtGui
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas)
from matplotlib.backends.backend_qt4agg import (
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure


class DockEMPlot(QtGui.QWidget):
    '''
    Auto update checks file size, any increase automatically updates displayed
    text.
    '''
    plt.style.use('ggplot')
    # Supress pixel distance warning
    warnings.filterwarnings('ignore', category=UserWarning, module='matplotlib')

    def __init__(self,
                 parent,
                 peak_search_filepath=None):
        super(DockEMPlot, self).__init__(parent=parent)
        # Set canvas and toolbar
        dpi=100
        self.figure = Figure(dpi=dpi)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(self)
        self.canvas.updateGeometry()
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Set layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)

        # Set filepath and data
        if peak_search_filepath is not None:
            self.set_peak_search_filepath(
                peak_search_filepath=peak_search_filepath)

    def set_peak_search_filepath(self, peak_search_filepath):
        self.peak_search_filepath = peak_search_filepath
        if os.path.exists(self.peak_search_filepath):
            self.make_plot()

    def make_plot(self):
        '''
        Draws a ccc plot for the number of solutions and gives an option to 
        save
        '''
        # Get data
        result_file = open(self.peak_search_filepath, 'r')
        sol_list = []
        corr_list = []
        sigma_list = []
        for line in result_file:
            try:
                sol_num = int(line.split()[0])
            except ValueError:
                sol_num = 0
            try:
                corr_value = float(line.split()[-1])
            except ValueError:
                corr_value = 0
            try:
                sigma_value = float(line.split()[-2])
            except ValueError:
                sigma_value = 0
            sol_list.append(sol_num)
            corr_list.append(corr_value)
            sigma_list.append(sigma_value)

        result_file.close()
        # create an axis
        ax1 = self.figure.add_subplot(111)
        ax1.hold(False)
        ax1.plot(sol_list, corr_list, marker='.', color='b')
        ax2 = ax1.twinx()
        ax1.grid(b=False)
        ax1.set_ylabel('CCC value')
        ax1.set_xlabel('Solution number')
        ax2.set_ylabel(r'$\bar{x} \pm \sigma$')  
        # Set absolute scale limits
        ax1.set_ylim([min(corr_list), max(corr_list)])
        ax2.set_ylim([min(sigma_list), max(sigma_list)])
        #
        self.figure.tight_layout(pad=4.0)
        self.canvas.draw()
