#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import json
from ccpem_core.tasks.choyce import choyce_task
from ccpem_gui.utils import window_utils
try:
    import modeller
    modeller_available = True
except ImportError:
    modeller_available = False


class ChoyceWindow(window_utils.CCPEMTaskWindow):
    '''
    Choyce window.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(ChoyceWindow, self).__init__(task=task,
                                           parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input reference pdb
        pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='reference_model_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(pdb_input)

        # Input target sequence
        fasta_input = window_utils.FileArgInput(
            parent=self,
            arg_name='sequence_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(fasta_input)

        # Add files to launcher
        self.launcher.add_file(
            arg_name='seqeunce_path',
            file_type='standard',
            description='Target sequence',
            selected=True)
        self.launcher.add_file(
            arg_name='reference_model_path',
            file_type='pdb',
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        # Output model
        args = os.path.join(self.task.job_location,
                            'args.json')
        output_path = json.load(open(args,'r'))['output_model_path']
        if os.path.exists(output_path):
            self.launcher.add_file(
                path=output_path,
                file_type='pdb',
                description='Modelled PDB file',
                selected=True)


def main():
    '''
    Launch stand alone window.
    '''
    if modeller_available:
        window_utils.standalone_window_launch(
            task=choyce_task.Choyce,
            window=ChoyceWindow)
    else:
        print ('Modeller required and not available'
               '\nFor installation details please see: '
               '\n    https://salilab.org/modeller/')

if __name__ == '__main__':
    main()
