#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
from textwrap import dedent
import tempfile
from ccpem_core.ccpem_utils.ccpem_argparser import ccpemArgParser

class Test(unittest.TestCase):
    '''
    Unit test for argparser
    '''

    def setUp(self):
        self.test_output = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_output')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    # Setup parser for current job type
    def job_parser(self):
        tst_p = ccpemArgParser(
            fromfile_prefix_chars='@',
            usage=dedent('''
            Test description of program
            '''),
            description='Test program',
            epilog=dedent('''
            To import parameters from argument text file use e.g. :\
            python foo.py @my_args.txt'''))
        tst_p.add_argument(
            '-f',
            '--input_file',
            help='''
            Input data file
            ''',
            type=str,
            default='None')
        tst_p.add_argument(
            '-fs',
            '--input_files',
            help='''
            Input data file
            ''',
            type=str,
            default='None',
            nargs='*')
        tst_p.add_argument(
            '-sym',
            '--symmetry',
            help='Symmetry required',
            choices=['ALL',
                     'HEXA',
                     'SQUA',
                     'RECT',
                     'OBLI',
                     'TWO',
                     'THRE',
                     'SIX',
                     'FOUR',
                     'SPEC'],
            type=str,
            default='ALL')
        search_refine_tilt_ncyc = tst_p.add_argument_group()
        search_refine_tilt_ncyc.add_argument(
            '-s',
            '--search',
            help='Search',
            type=bool,
            default=True)
        search_refine_tilt_ncyc.add_argument(
            '-c',
            '--ncycle',
            help='Number of cycles',
            type=int,
            default=4000)
        search_refine_tilt_ncyc.add_argument(
            '-a',
            '--array',
            help='int array',
            type=int,
            nargs='*',
            default=4)
        search_refine_tilt_ncyc.add_argument(
            '-strings',
            '--strings',
            help='list of strings',
            type=str,
            nargs='*',
            default=None)
        return tst_p

    def test_setup_args(self):
        '''
        Test functions
        '''
        tst_parser = self.job_parser()
        # Import params for job from file
        args_filename = self.test_output
        args_filename += '/args_eg.txt'
        args_file = open(args_filename, 'w')
        args_file.write('''
--ncycle  500
--input_file  tst.data
--input_files tst.data1 tst.data2 tst.data3
--search  True
--symmetry  SPEC
--array 1 9 8 4
--triple foo.pdb foo.map foo.res
--strings test.pdb test2.pdb
''')
        args_file.close()
        # Ensure stored value assoicated with object instance
        job_args = tst_parser.generate_arguments(filename=args_filename)
        job_args_two = tst_parser.generate_arguments()
        assert job_args.get_value(key='ncycle') == 500
        assert job_args_two.get_value(key='ncycle') == 4000
        assert job_args.input_file() == 'tst.data'
        assert job_args.input_files() == ['tst.data1', 'tst.data2', 'tst.data3']

        # Set params
        # Two ways of getting object value
        assert job_args.ncycle.value == 500
        assert job_args.ncycle() == 500
        assert job_args.search.value is True
        # Set multiple values
        job_args.set_values(ncycle=1, search=False)
        assert job_args.ncycle.value == 1
        assert job_args.search.value is False
        # Ensure key exists and value type matches
        assert job_args.search.value is False
        job_args.set_values(search=True)
        assert job_args.search.value is True
        # Enforce choices where present
        assert job_args.symmetry.value == 'SPEC'
        job_args.set_values(symmetry='TWO')
        assert job_args.symmetry.value == 'TWO'
        # Write json output
        json_filename = os.path.join(self.test_output,
                                     'args_eg.json')
        job_args.output_args_as_json(filename=json_filename)

        # Reload job parser from defaults
        del job_args
        tst_parser = self.job_parser()
        job_args = tst_parser.generate_arguments()
        assert job_args.get_value(key='ncycle') == 4000
        job_args.import_args_from_json(filename=json_filename)
        assert job_args.get_value(key='ncycle') == 1

        # Check list of params imported correctly
        files = job_args.input_files()
        assert isinstance(files, list)
        assert isinstance(files[0], str)
        assert files == ['tst.data1', 'tst.data2', 'tst.data3']
        array = job_args.array()
        assert isinstance(array[0], int)
        assert array == [1,9,8,4]

        # Merge args from another parser
        new_parser = ccpemArgParser(
            fromfile_prefix_chars='@',
            usage=dedent('''
            Test description of program
            '''),
            description='Test program',
            epilog=dedent('''
            To import parameters from argument text file use e.g. :\
            python foo.py @my_args.txt
            '''))
        test_var = new_parser.add_argument_group()
        test_var.add_argument(
            '-tv',
            '--testvar',
            help='''A test variable''',
            type=str,
            default='TEST')
        new_args = new_parser.generate_arguments()
        job_args.add_args(args=new_args)
        assert job_args.testvar.value == 'TEST'
        merge_args_filename = os.path.join(self.test_output,
                                           'merge_args.json')
        new_args.output_args_as_json(merge_args_filename)


    def test_unicode_args(self):
        '''
        Test list of list of strings returns str not unicode or other
        '''
        path = os.path.join(self.test_output,
                            'ex_args.json')
        f = open(path, 'w')
        f.write('{"strings": [ ["test1", "test2"], ["zzz"] ]}')
        f.close()
        args = self.job_parser().generate_arguments()
        args.import_args_from_json(path)
        s = args.strings()
        assert isinstance(s[0][0], str)

if __name__ == "__main__":
    unittest.main()
