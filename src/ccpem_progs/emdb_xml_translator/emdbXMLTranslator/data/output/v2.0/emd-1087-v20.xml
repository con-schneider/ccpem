<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1087" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2004-06-09</deposition>
            <header_release>2004-06-10</header_release>
            <map_release>2004-06-10</map_release>
            <update>2013-10-02</update>
        </key_dates>
        <title>Three-dimensional structure of the bacterial multidrug transporter EmrE shows it is an asymmetric homodimer.</title>
        <authors_list>
            <author>Ubarretxena-Belandia I</author>
            <author>Baldwin JM</author>
            <author>Schuldiner S</author>
            <author>Tate CG</author>
        </authors_list>
        <keywords></keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Ubarretxena-Belandia I</author>
                    <author order="2">Baldwin JM</author>
                    <author order="3">Schuldiner S</author>
                    <author order="4">Tate CG</author>
                    <title>Three-dimensional structure of the bacterial multidrug transporter EmrE shows it is an asymmetric homodimer.</title>
                    <journal>EMBO J.</journal>
                    <volume>22</volume>
                    <first_page>6175</first_page>
                    <last_page>6181</last_page>
                    <year>2003</year>
                    <external_references type="pubmed">14633977</external_references>
                    <external_references type="doi">doi:10.1093/emboj/cdg611</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
        <pdb_list>
            <pdb_reference id="1">
                <pdb_id>2i68</pdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </pdb_reference>
        </pdb_list>
    </crossreferences>
    <sample>
        <name>EmrE from E. coli containing the bound substrate tetraphenylphosphonium</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>EmrE in the 2D crystals is in a functional  state with the
      substrate (TPP+) bound in a binding pocket formed from 6 out of
      the 8 helices in the asymmetric dimer.</details>
                <oligomeric_state>Asymmetric homodimer</oligomeric_state>
                <number_unique_components>1</number_unique_components>
                <molecular_weight>
                    <theoretical units="MDa">0.0152</theoretical>
                </molecular_weight>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="MvrC, EB">EmrE</name>
                <natural_source>
                    <organism ncbi="562">Escherichia coli</organism>
                    <strain>K12</strain>
                    <cellular_location>Inner membrane</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.0152</experimental>
                    <theoretical units="MDa">0.0152</theoretical>
                </molecular_weight>
                <details>4-helix integral membrane protein; The engineered EmrE is fully functional as assessed
 by in vivo and in vitro transport assays</details>
                <number_of_copies>2</number_of_copies>
                <oligomeric_state>dimer</oligomeric_state>
                <recombinant_exp_flag>true</recombinant_exp_flag>
                <recombinant_expression>
                    <organism ncbi="562">Escherichia coli</organism>
                    <plasmid>pT7-7</plasmid>
                </recombinant_expression>
                <sequence>
                    <external_references type="UNIPROTKB">P23895</external_references>
                    <external_references type="GO">GO:0016021</external_references>
                    <external_references type="INTERPRO">IPR000390</external_references>
                </sequence>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>electronCrystallography</method>
            <aggregation_state>twoDArray</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="crystallography_preparation_type">
                    <concentration units="mg/mL">1</concentration>
                    <buffer>
                        <ph>7.5</ph>
                        <details>20 mM Sodium phosphate pH7.5, 100 mM NaCl, 2mM
          MgCl2,       1mM       EDTA, 1 mM Na azide, 10uM tetraphenylphosphonium</details>
                    </buffer>
                    <staining>
                        <type>negative</type>
                        <details>Crystals on carbon support were washed with 1%
        glucose pH       8 (aq ammonia) containing 25ug/ml bacitracin,
        2 times 20ul, and       blotted to dryness</details>
                    </staining>
                    <grid>
                        <details>300 mesh molybdenum grid</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>NITROGEN</cryogen_name>
                        <chamber_humidity>45</chamber_humidity>
                        <chamber_temperature>77</chamber_temperature>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: manual. Performed at room temperature</details>
                        <method>Blot to dryness for 10 seconds before freezing</method>
                    </vitrification>
                    <crystal_formation>
                        <details>2D crystals were grown in suspension by dialysis</details>
                    </crystal_formation>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="crystallography_microscopy_type">
                    <microscope>FEI TECNAI F30</microscope>
                    <illumination_mode>SPOT SCAN</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>300</acceleration_voltage>
                    <nominal_cs>2.3</nominal_cs>
                    <nominal_defocus_min>200</nominal_defocus_min>
                    <nominal_defocus_max>1600</nominal_defocus_max>
                    <nominal_magnification>60000.</nominal_magnification>
                    <calibrated_magnification>57400.</calibrated_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_min units="Kelvin">77</temperature_min>
                        <temperature_max units="Kelvin">77</temperature_max>
                        <temperature_average units="Kelvin">77</temperature_average>
                    </temperature>
                    <alignment_procedure>
                        <legacy>
                            <astigmatism>objective lens astigmatism was corrected at
        200,000       times magnification</astigmatism>
                        </legacy>
                    </alignment_procedure>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="film">KODAK SO-163 FILM</film_or_detector_model>
                            <digitization_details>
                                <scanner>ZEISS SCAI</scanner>
                                <sampling_interval units="um">7</sampling_interval>
                            </digitization_details>
                            <number_real_images>47</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">15</average_electron_dose_per_image>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>Side entry liquid nitrogen-cooled cryo specimen holder</specimen_holder>
                    <tilt_angle_min>0</tilt_angle_min>
                    <tilt_angle_max>46</tilt_angle_max>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="crystallography_processing_type">
                <final_reconstruction>
                    <algorithm>Merge images of tilted crystals</algorithm>
                    <resolution>7.5</resolution>
                    <resolution_method>Point-spread function</resolution_method>
                    <software_list>
                        <software>
                            <name>MRC</name>
                        </software>
                    </software_list>
                    <details>Resolution in the membrane plane is 7.5       angstroms
        and       16       angstroms perpendicular to the membrane plane</details>
                </final_reconstruction>
                <crystal_parameters>
                    <unit_cell>
                        <a units="A">72.1</a>
                        <b units="A">86.8</b>
                        <gamma units="degrees">107.3</gamma>
                    </unit_cell>
                    <plane_group>P 2</plane_group>
                </crystal_parameters>
                <ctf_correction>
                    <details>CTFFIND2 on each image</details>
                </ctf_correction>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="593" format="CCP4">
        <file>emd_1087.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>81</col>
            <row>89</row>
            <sec>21</sec>
        </dimensions>
        <origin>
            <col>-2</col>
            <row>-63</row>
            <sec>-10</sec>
        </origin>
        <spacing>
            <x>150</x>
            <y>150</y>
            <z>80</z>
        </spacing>
        <cell>
            <a>72.10005</a>
            <b>86.80005</b>
            <c>200.0</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>107.3</gamma>
        </cell>
        <axis_order>
            <fast>Y</fast>
            <medium>X</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-156.908599850000002</minimum>
            <maximum>250.000015259999998</maximum>
            <average>-2.69374633</average>
            <std>47.366405489999998</std>
        </statistics>
        <pixel_spacing>
            <x>0.48066702</x>
            <y>0.578667</y>
            <z>2.5</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>59.7</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>Density map of the asymmetric dimer of EmrE,a
 multidrug transporter from Escherichia coli</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1087::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling/>
        </modelling_list>
    </interpretation>
</emd>
