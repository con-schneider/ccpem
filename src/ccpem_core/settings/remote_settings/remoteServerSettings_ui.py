# -*- coding: utf-8 -*-

#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_remoteServerSettingsWidget(object):
    def setupUi(self, remoteServerSettingsWidget):
        remoteServerSettingsWidget.setObjectName(_fromUtf8("remoteServerSettingsWidget"))
        remoteServerSettingsWidget.resize(400, 286)
        self.buttonBox = QtGui.QDialogButtonBox(remoteServerSettingsWidget)
        self.buttonBox.setGeometry(QtCore.QRect(10, 253, 381, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.tabWidget = QtGui.QTabWidget(remoteServerSettingsWidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 381, 237))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.basicTab = QtGui.QWidget()
        self.basicTab.setObjectName(_fromUtf8("basicTab"))
        self.layoutWidget = QtGui.QWidget(self.basicTab)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 361, 191))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.serverNameLabel = QtGui.QLabel(self.layoutWidget)
        self.serverNameLabel.setObjectName(_fromUtf8("serverNameLabel"))
        self.gridLayout.addWidget(self.serverNameLabel, 0, 0, 1, 1)
        self.serverNameEdit = QtGui.QLineEdit(self.layoutWidget)
        self.serverNameEdit.setObjectName(_fromUtf8("serverNameEdit"))
        self.gridLayout.addWidget(self.serverNameEdit, 0, 1, 1, 1)
        self.portLabel = QtGui.QLabel(self.layoutWidget)
        self.portLabel.setObjectName(_fromUtf8("portLabel"))
        self.gridLayout.addWidget(self.portLabel, 1, 0, 1, 1)
        self.portSpinBox = QtGui.QSpinBox(self.layoutWidget)
        self.portSpinBox.setFrame(True)
        self.portSpinBox.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.portSpinBox.setMaximum(99999)
        self.portSpinBox.setProperty("value", 22)
        self.portSpinBox.setObjectName(_fromUtf8("portSpinBox"))
        self.gridLayout.addWidget(self.portSpinBox, 1, 1, 1, 1)
        self.usernameEditLabel = QtGui.QLabel(self.layoutWidget)
        self.usernameEditLabel.setObjectName(_fromUtf8("usernameEditLabel"))
        self.gridLayout.addWidget(self.usernameEditLabel, 2, 0, 1, 1)
        self.usernameEdit = QtGui.QLineEdit(self.layoutWidget)
        self.usernameEdit.setObjectName(_fromUtf8("usernameEdit"))
        self.gridLayout.addWidget(self.usernameEdit, 2, 1, 1, 1)
        self.passwordLabel = QtGui.QLabel(self.layoutWidget)
        self.passwordLabel.setObjectName(_fromUtf8("passwordLabel"))
        self.gridLayout.addWidget(self.passwordLabel, 3, 0, 1, 1)
        self.passwordEdit = QtGui.QLineEdit(self.layoutWidget)
        self.passwordEdit.setEchoMode(QtGui.QLineEdit.Password)
        self.passwordEdit.setObjectName(_fromUtf8("passwordEdit"))
        self.gridLayout.addWidget(self.passwordEdit, 3, 1, 1, 1)
        self.testConnectionButton = QtGui.QPushButton(self.layoutWidget)
        self.testConnectionButton.setObjectName(_fromUtf8("testConnectionButton"))
        self.gridLayout.addWidget(self.testConnectionButton, 4, 0, 1, 1)
        self.tabWidget.addTab(self.basicTab, _fromUtf8(""))
        self.advancedTab = QtGui.QWidget()
        self.advancedTab.setObjectName(_fromUtf8("advancedTab"))
        self.label_5 = QtGui.QLabel(self.advancedTab)
        self.label_5.setGeometry(QtCore.QRect(10, 10, 101, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.schedulerComboBox = QtGui.QComboBox(self.advancedTab)
        self.schedulerComboBox.setGeometry(QtCore.QRect(110, 40, 69, 22))
        self.schedulerComboBox.setObjectName(_fromUtf8("schedulerComboBox"))
        self.schedulerComboBox.addItem(_fromUtf8(""))
        self.schedulerComboBox.addItem(_fromUtf8(""))
        self.schedulerLabel = QtGui.QLabel(self.advancedTab)
        self.schedulerLabel.setGeometry(QtCore.QRect(10, 40, 101, 16))
        self.schedulerLabel.setObjectName(_fromUtf8("schedulerLabel"))
        self.tabWidget.addTab(self.advancedTab, _fromUtf8(""))
        self.saveDetailsCheckBox = QtGui.QCheckBox(remoteServerSettingsWidget)
        self.saveDetailsCheckBox.setGeometry(QtCore.QRect(20, 260, 211, 17))
        self.saveDetailsCheckBox.setObjectName(_fromUtf8("saveDetailsCheckBox"))
        self.retranslateUi(remoteServerSettingsWidget)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(remoteServerSettingsWidget)

    def retranslateUi(self, remoteServerSettingsWidget):
        remoteServerSettingsWidget.setWindowTitle(_translate("remoteServerSettingsWidget", "Server settings", None))
        self.serverNameLabel.setText(_translate("remoteServerSettingsWidget", "Server name", None))
        self.portLabel.setText(_translate("remoteServerSettingsWidget", "Port", None))
        self.usernameEditLabel.setText(_translate("remoteServerSettingsWidget", "Username", None))
        self.passwordLabel.setText(_translate("remoteServerSettingsWidget", "Password", None))
        self.testConnectionButton.setText(_translate("remoteServerSettingsWidget", "Test connection", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.basicTab), _translate("remoteServerSettingsWidget", "Basic", None))
        self.label_5.setText(_translate("remoteServerSettingsWidget", "Ability to set proxy?", None))
        self.schedulerComboBox.setItemText(0, _translate("remoteServerSettingsWidget", "PBS", None))
        self.schedulerComboBox.setItemText(1, _translate("remoteServerSettingsWidget", "LSF", None))
        self.schedulerLabel.setText(_translate("remoteServerSettingsWidget", "Scheduler", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.advancedTab), _translate("remoteServerSettingsWidget", "Advanced", None))
        self.saveDetailsCheckBox.setText(_translate("remoteServerSettingsWidget", "Save details", None))

class remoteServerSettingsWidget(QtGui.QDialog, Ui_remoteServerSettingsWidget):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtGui.QDialog.__init__(self, parent, f)
        self.setupUi(self)
