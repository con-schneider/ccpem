C**************************      PICKPROFK    **********************************
C
C  Program for e.d. spot integration, using least-squares profile-fitting.
C	Many features of this program are similar to those found in the old
C	programs PICKOFF, PICKYCOR and PICKAUTO.  The main change is the
C	introduction of profile-fitting, which might help the accuracy of
C	measurement of weaker spots.
C
C	 remember to change version number if you update.
C       VX1.00	19.4.92		RH	original version
C       VX1.01	22.5.92		RH	substantial general debugging
C       VX1.02	19.3.96		RH	tidy up small raster performance
C	VX1.03	22.3.96		RH	debug lack of NSTEP some headers
C	VX2.00	23.3.96		RH	renamed as PICKPROFA - add unbend option
C                                       for use with CCD + tapered fibre optics
C	VX2.01	14.4.96		RH	substantial debugging e.g. subr CENTRE
C	VX2.02	25.6.96		RH	increase spots to 5000 and debug LOSCAL
C	VX2.03	28.6.96		RH	substantial debugging of profile fitting
C	VX2.04	 4.7.96		RH	profile flattened prior to normalisation
C	VX2.05	 6.5.97		RH	change PEN(2) to PEN(1)
C	VX2.06  8.11.97		RH	add rough code for tilted e.d. LPTYPE=3
C	VX2.07 20.11.97		RH	extensive debugging of old and new code
C                                       remove all JLOOK.le.0
C	VX3.00 24.11.97		RH	input B3, NCYC added; rename PICKPROFB
C	VX3.01 25.11.97		RH	option for global background averaging
C	VX3.02  1.12.97		RH	radius of gyration, tilted deconvolution
C 					profile LPTYPE=3 has shrink calculated
C					by deconvoluting radii of gyration
C	VX3.03 24.12.97		RH	apply TILTDIR after lattice refinement
C                                       debug plotting of lattice vectors
C	VX3.04 29.12.97		RH	debug GLOBAL, check spot is present
C	VX4.00 30.12.97		RH	subroutine_reindex added, PICKPROFC
C	VX4.01  17.7.98		RH	check input OD range is < NLOOKUP=1500
C	VX4.02  29.8.98		RH	improved description of ATANGL,ATAXIS
C	VX5.00  22.8.00		RH	convert to plot2000 -> PICKPROFK
C         "     13.6.01         TSH     P2K_FONT needed string terminator
C
C
C  Data cards are :
C
C  1.  A,B,G,ANGDIS,SHRINK,TILTDIR,KV,LPRINT
C	Real space cell dimensions and gamma angle,
C	  angle on film perpendicular to which a shrinkage by factor SHRINK is
C	  applied before calculating tiltaxis and tiltangle (using EMTILT).
C	  TILTDIR - direction of tilt on microscope.
C		    -1 = clockwise on goniometer
C		    +1 = counterclockwise
C	  LPRINT -  T  = comprehensive printout.
C
C  2.  LUNBEND,LGLOBL,NGLOBL
C        LUNBEND - T read in distortion correction table TABLEIN and use it to
C                     to correct predicted spot positions.
C                - F use the input file directly.
C        LGLOBL  - T OR F, apply wider global background subtraction
C        NGLOBL  - number of nearby spots to include global background correct.
C
C  3.  LPROFIT,LPTYPE,LPRANGE,INTERLEAVE,IFLATTEN,PROFMIN,TSIGMA
C        LPROFIT - T perform profile fitting
C                - F simple old-fashioned raster
C        LPTYPE - 0 use single profile for whole film.
C                 - 1 learn total of 5 profiles (4 outside quarters and centre)
C                      stored in APROF
C                 - 2 calculate profiles on a spot-by-spot basis, with LPRANGE
C                 - 3 learn 3 profiles for tilted e.d. with convolution to
C                      different parts of film, stored in TPROF - could be
C                      increased to 5 profiles if necessary later.
C        LPRANGE - Spots within LPRANGE pixels are used to perform local
C                    profile averaging, with option LPTYPE = 2.
C        INTERLEAVE - (T/F)  Apply odd/even scan line offset to eliminate
C                      effects of densitometer zig-zag offset.
C        IFLATTEN - (T/F)  Flatten profile when density falls below PROFMIN.
C                          this is compulsory for LPTYPE=3
C        PROFMIN  - Density threshold for profile flattening - normally 0.03
C        TSIGMA   - Throw out outliers from background that deviate by more
C                    than TSIGMA*stndev from the fitted value, and refit.
C
C
C  4.  J,K
C	Look-up table for optical density: LOOKUP(J)=K
C	  This is linearly interpolated for J=1,1500.  The first card must have
C	  J=1 and the last J=1500.
C
C  5.  Description of source of above look-up table - TITLE.
C
C
C  6.  NPLATE  plate number, used on output file
C
C  7.  TITLE  Title of diffraction pattern, for use on output file.
C
C  8.  LREINDEX, ATANGL, ATAXIS
C	Attempt to reindex the lattice parameters so that the titlangle and
C	  tiltaxis are close to the expected values given by ATANGL and ATAXIS
C	  ATANGL always positive, ATAXIS measured relative to the XY-axis
C
C  9.  X0,Y0,TLTAXA,TLTANG,B3
C	Coordinates of centre, position of tiltaxis (angle from tiltaxis to
C	  A-axis in direction A to B positive), size of tiltangle. If non-zero,
C	  X0,Y0 as well as TLTAXA and TLTANG and B3 override values calculated
C	  internally or taken from header of digitised pattern. Note that
C	  TLTAXA here is angle between tiltaxis and astar ON FILM, not in 3D
C	  as in ORIGTILT.  B3 is the pincushion distortion value (0.00000000240)
C
C  10. DX1,DY1,DX2,DY2
C	Position of (1,0) and (0,1) relative to centre.  If non-zero, these
C	  values override the values on the input file header (from AUTOINDEX).
C
C  11. ROUT,RIN,PRPMAX
C	Outer and inner radii in Angstroms, converted to pixels in the program
C	  using cell dimensions on card 1.  PRPMAX is the maximum perpendicular
C	  resolution from the tiltaxis.
C
C  12. NXM,NYM,    NXMT,NYMT
C	sizes for peak integration, peak raster sizes  used for initial
C         lattice parameter refinement, near and far from tiltaxis.
C
C  13. FRACT,ABSOL,XAMINE,NCYC
C	Reflections with Friedel differences greater than FRACT and ABSOL are
C	  rejected.  Weak reflections are rejected from the centre of gravity
C	  and lattice parameter calculations by a criterion using XAMINE applied
C         to the peak rasters.  NCYC is maximum number of cycles of refinement
C         of lattice parameters.   NCYC=0 indicates no refinement, where program
C         goes straight to integration - allows strict comparisons to be made.
C
C
C
C INPUT AND OUTPUT**************************************************************
C
C 'IN'     (1) : INPUT densitometered film array, any size.
C      FOR002  : OUTPUT of profile-fitted intensity values.
C 'RADIAL' (3) : INPUT radial density curve for background correction.
C 'YCORR'  (4) : INPUT Y-axis densitometer drift curve for background correction.
C      FOR005  : INPUT control cards.
C      FOR006  : OUTPUT datastream.
C 'PLOTOUT'    : PLOTTER OUTPUT of spot positions and c. of gravity deviations.
C 'INPARAM'(9) : INPUT header of image file with autoindexed lattice parameters
C 'DENOUT'(10) : OUTPUT of film density with radial background, ycorr, zigzag
C                      offset and profile fitted peaks subtracted - should be
C                      completely flat.
C 'TABLEIN'(11): INPUT of position distortion correction table from ccunbende.for
C
C*******************************************************************************
C  Notes on meaning of some of the variables:
C	GSBACK,GPBACK	- global backgrounds derived from simple fit and profile
C	SINT,BACK	- simple raster integrated peak and background density
C	ICORR		- background-corrected raster integrated intensity
C	IPROF		- profile-fitted integrated intensity
C       XCOORD,YCOORD	- distortion corrected coordinates of spots.
C       X0,Y0		- coordinates of pattern centre.
C       JPROF		- exact interpolated raster of density around each spot.
C       LPROF		- logical array which flags over- or underloaded pixels.
C	TPROF		- three profiles for convolution in tilted patterns
C	APROF		- profile to be used for measurement in PROFMEAS
C	JSTORE		- raw densities transferred from input array.
C
C*******************************************************************************
C
C    REMEMBER SOME OF THESE DIMENSIONS MUST ALSO BE CHANGED IN SUBROUTINES.
C
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      PARAMETER (NDIMXY=50000)
      PARAMETER (NLOOKUP=1500)
      PARAMETER (MAXCYCL=12)
      PARAMETER (MAXSIZ=2048)
      PARAMETER (MAXINDEX=70)
      REAL*4 ARRAY(MAXSIZ*MAXSIZ),LAMBDA,LOOKUP(NLOOKUP),VARTAB(NLOOKUP)
      REAL*4 APROF(5,IRAST,IRAST),TPROF(5,IRAST,IRAST)
      REAL*4 XCORR(NDIMXY),YCORR(NDIMXY)
      REAL*4 XCSPOT(NMAX),YCSPOT(NMAX)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST),JPROF(NMAX,IRAST,IRAST)
      LOGICAL LPROF(NMAX,IRAST,IRAST),LUNBEND,LGLOBL,LREINDEX
      LOGICAL NOC_OF_G,LPRINT,LPROFIT,INTERLEAVE,IFLATTEN
      CHARACTER DAT*24


C
      DIMENSION XCOORD(NMAX),YCOORD(NMAX),XCOREF(NMAX),YCOREF(NMAX)
      DIMENSION XREFINE(NMAX),YREFINE(NMAX)
      DIMENSION SINT(NMAX),BACK(NMAX),ICORR(NMAX),IPROP(NMAX)
      DIMENSION IPROF(NMAX),VAR(NMAX),VARP(NMAX)
      DIMENSION GSBACK(NMAX),GPBACK(NMAX)
      DIMENSION NBDBCK(NMAX),NGDBCK(NMAX),JH(NMAX),JK(NMAX)
      DIMENSION IOVER(NMAX),IOVERP(NMAX),ILOSC(NMAX),ILOSP(NMAX)
      DIMENSION SPVSTORE(NMAX),SP2VSTORE(NMAX)
      DIMENSION TITLE(18),PDPTIT(20),TITLEOUT(20),TITLEIN(20)
      DIMENSION YCURVE(MAXSIZ),ODBACK(MAXSIZ),STNDEV(MAXSIZ)
      DIMENSION NXYZ(3),MXYZ(3),IEXT(7)
      DIMENSION N9XYZ(3),M9XYZ(3),IEXT9(12)
CTSH++
	LOGICAL IPRINT
	CHARACTER*80 TMPTITLEOUT
	EQUIVALENCE (TMPTITLEOUT,TITLEOUT)
CTSH--
      DATA LOOKUP/NLOOKUP*0.0/
C
      PI=3.141592654
      HALFRAST=IRAST/2
      IOVERL=750 ! could have lower value with profile fitting
      IUNDER=0
      WRITE(6,10)
10    FORMAT(/' PICKPROFK - VX5.00 (22.8.00) - automatic ',
     .	'electron diffraction least squares profile-fitting.'/)
C
C  READ IN CELL DIMENSIONS FOR USE BY AUTOMATIC TILT CALCULATION(EMTILT).
      READ(5,*)A,B,G,ANGDIS,SHRINK,TILTDIR,KV,LPRINT
      XKV=KV*1000.0
      LAMBDA=12.3/SQRT(XKV+XKV**2/(10.0**6.0))
      GMSTAR=180.0-G
      ASTAR=1.0/(A*SIN(GMSTAR*PI/180.0))
      BSTAR=1.0/(B*SIN(GMSTAR*PI/180.0))
      WRITE(6,20)A,B,G,ANGDIS,SHRINK,TILTDIR,KV,LAMBDA
20    FORMAT(' Cell dimensions A,B,G',3F10.1/' Angle on film for ',
     1'correction of E.M. distortion and correction factor',2F10.5/
     2' Direction of tilt on microscope (-1=clockwise) ',F6.1/
     3' Microscope voltage, wavelength.................',I5,F8.3)
      ANGDIS=ANGDIS*PI/180.0
C
C  READ IN PROFILE FITTING AND OTHER CONTROL VALUES.
      READ(5,*) LUNBEND,LGLOBL,NGLOBL
      READ(5,*) LPROFIT,LPTYPE,LPRANGE,
     .	 INTERLEAVE,IFLATTEN,PROFMIN,TSIGMA
      	IF(LPTYPE.EQ.3) IFLATTEN=.TRUE.  ! compulsory
      	IF(IFLATTEN) THEN
      	 IF(PROFMIN.LT.0.005.OR.PROFMIN.GT.0.5) THEN
      	  PROFMIN=AMIN1(0.5,PROFMIN) ! check for reasonable values
      	  PROFMIN=AMAX1(0.005,PROFMIN)
      	  WRITE(6,*) ' PROFMIN set to reasonable value',PROFMIN
      	 ENDIF
      	ENDIF
      WRITE(6,22) LUNBEND,LGLOBL,NGLOBL,LPROFIT,LPTYPE,LPRANGE,
     .	 INTERLEAVE,IFLATTEN,PROFMIN,TSIGMA,LPRINT
22    FORMAT(/' Use distortion correction table - (T/F)',L5/
     .	' Carry out global background correction ',L5,
     .	'  with',I3,' of nearby spot backgrounds '/
     .	' Profile fitting will be performed (T/F)',L5/
     .	'  with profile LPTYPE -----------',I5/
     .	'               LPRANGE ----------',I5/
     .	'               INTERLEAVE -------',L5/
     .	'               IFLATTEN ---------',L5/
     .	'               PROFMIN ----------',F7.3/
     .	'               TSIGMA  ----------',F5.1/
     .  '               LPRINT  ----------',L5)
C
C read in distortion correction table for use with fibre-optic CCD
C
      IF (LUNBEND) THEN
      	CALL CCPDPN(11,'TABLEIN','READONLY','F',0,0)
      	READ(11,16) TITLEIN
      	WRITE(6,17) TITLEIN
16		FORMAT(20A4)
17		FORMAT(' CCD distortion table used :',20A4)
      	READ(11,18) ISTEP,MXDIM,MYDIM,NTABX,NTABY,NTABZ
18		FORMAT(25X,6I6)
C      	WRITE(6,18) ISTEP,MXDIM,MYDIM,NTABX,NTABY
      	MCALC=MXDIM*MYDIM
      	IF(MCALC.GT.NDIMXY) STOP ' NDIMXY PROG DIM TOO SMALL'
      	DO 23 I=1,MCALC
19		FORMAT(2F8.2)
    		READ(11,19) XCORR(I),YCORR(I)
23	CONTINUE
      	CLOSE(11)
      ENDIF
C
C
      CALL IMOPEN(1,'IN','RO')
C
      CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      IF(DMAX.GT.FLOAT(NLOOKUP)) THEN
      	STOP ' input data extends beyond lookup table dimension(1500)'
      ENDIF
      NX=NXYZ(1)
      NY=NXYZ(2)
      	IF(LUNBEND) THEN
      	 IF(NX.NE.NTABX.OR.NY.NE.NTABY) THEN
		WRITE(6,24) NTABX,NX,NTABY,NY
24		FORMAT(' Wrong distortion table, NX and NY must agree',2(I12,I6))
      	 STOP
      	 ENDIF
      	ENDIF
C         Get extra parameters from actual image header
      CALL IRTEXT(1,IEXT,1,7) ! just lattice parameters
      CLOSE(9)
      WRITE(6,25)(IEXT(J),J=1,7)
25    FORMAT(' extra parameters from header of IN on unit 1'/7I8)
      NSTEP=IEXT(1)/25.0
      IF(NSTEP.EQ.0) NSTEP=4  ! default for no header value
      STEP10=NSTEP*10
      X0 =IEXT(2)/STEP10 + NX/2.0
      Y0 =IEXT(3)/STEP10 + NY/2.0
      DX1=IEXT(4)/STEP10
      DY1=IEXT(5)/STEP10
      DX2=IEXT(6)/STEP10
      DY2=IEXT(7)/STEP10
C
      CALL IMOPEN(9,'INPARAM','RO')
C
      CALL IRDHDR(9,N9XYZ,M9XYZ,MODE9,D9MIN,D9MAX,D9MEAN)
      N9X=N9XYZ(1)
      N9Y=N9XYZ(2)
C         get extra parameters from header of autoindex image
      CALL IRTEXT(9,IEXT9,1,12) ! lattice parameters and raster parameters.
      WRITE(6,26) (IEXT9(J),J=1,12)
26    FORMAT(' extra params from INPARAM on unit 9',/12I8/)
C         use autoindex parameters only if none in real image
      IF(DX1+DY1.LT.0.0001.OR.DX2+DY2.LT.0.0001) THEN
      	NSTEP=IEXT9(1)/25.0
        IF(NSTEP.EQ.0) NSTEP=4  ! default for no header value
      	STEP10=NSTEP*10
      	DX1=IEXT9(4)/STEP10*(NX/N9X)
      	DY1=IEXT9(5)/STEP10*(NY/N9Y)
      	DX2=IEXT9(6)/STEP10*(NX/N9X)
      	DY2=IEXT9(7)/STEP10*(NY/N9Y)
      ENDIF
C
C  Read in the whole image area
C
	CALL IRDSEC(1,ARRAY,*9999)
      	CLOSE(1)
	IF(INTERLEAVE) CALL INTERLACE(NX,NY,ARRAY,DMEAN,IEDGE)
C
27    FORMAT(/' NX=',I5,'   NY=',I5)
      WRITE(6,27) NX,NY
      WRITE(6,28) NSTEP,DX1,DY1,DX2,DY2
28    FORMAT(' Information from image header(s)'/' NSTEP=',I5/
     .' Vector (1,0)',2F10.2,';  vector (0,1)',2F10.2/)
C
C
C  First read in and interpolate look-up table for OD correction.
30	READ(5,*)J,K ! for other values
      	LOOKUP(J) = K
      	IF(J.EQ.1) GO TO 30
      	DO 31 L=J-1,1,-1
      	 IF(LOOKUP(L).NE.0.) GO TO 32
31    	CONTINUE
32    	IF(L.EQ.(J-1)) GO TO 30
      	N=L+1
      	DO 33 M=N,J
33	  LOOKUP(M)=(LOOKUP(L)*(J-M)*2+J-L+LOOKUP(J)*(M-L)*2)/((J-L)*2)
      IF(J.NE.NLOOKUP) GO TO 30
      READ(5,39)TITLE !  OD look-up table descriptor
39    FORMAT(18A4)
C
C  READ IN MEAN RADIAL BACKGROUND DENSITY
      CALL CCPDPN(3,'RADIAL','READONLY','F',0,0)
      READ(3,43) NCOMP,TITLE
43    FORMAT(I5,18A4)
      IMAX=1+SQRT(((NX+1.0)/2.0)**2+((NY+1.0)/2.0)**2)
      	VARMIN=1000000.0
      	VARMAX=1.0
      DO 45 J=1,IMAX
      	READ(3,46) ODBACK(J),STNDEV(J)
46    	FORMAT(2F11.5)
      	VARMAX=AMAX1(STNDEV(J)**2,VARMAX)
45    	VARMIN=AMIN1(STNDEV(J)**2,VARMIN)
      CLOSE(3)
      WRITE(6,50) NCOMP,TITLE
50    FORMAT(/' RADIAL DENSITY'/I10,18A4)
      DO 55 J=1,IMAX,100
      I=J-1
55    WRITE(6,56) I,ODBACK(J),STNDEV(J)
56    FORMAT(I10,2F11.5)
C  Convert radial standard deviations into OD lookup table variances.
      	DO 58 JV=1,NLOOKUP
      	VARTAB(JV)=VARMAX
      	DO 58 JR=1,IMAX ! assumes decreasing radial background density
      	 IF(ODBACK(JR).GT.JV) VARTAB(JV) = STNDEV(JR)**2
58    	CONTINUE
C
C  Write out look-up table and calculated variances
C
      WRITE(6,40)TITLE
40    FORMAT(/'  LOOK-UP TABLE FOR ',18A4)
      WRITE(6,34)
34    FORMAT('    ODMEAS    ODCORR    STNDEV - OD LOOK-UP TABLE')
      DO 36 J=1,1401,100
36    WRITE(6,37) J,LOOKUP(J),SQRT(VARTAB(J))
37    FORMAT(I10,2F10.2)
C
C
C  READ IN MEAN Y-VARIATION CURVE FOR CORRECTION OF ALL DENSITY MEASUREMENTS.
      CALL CCPDPN(4,'YCORR','READONLY','F',0,0)
      READ(4,60)NYCOMP
      WRITE(6,61)NYCOMP
60    FORMAT(I10,F10.5)
61    FORMAT(' Y-VARIATION CURVE READ IN, IDENTIFIER',I10,'  BRIEFLY')
      DO 62 J=1,NY
      	READ(4,60)I,YCURVE(J)
      	IF(I.EQ.J) GO TO 62
      	STOP ' Y-CURVE DATA IS WRONG?????????'
62    CONTINUE
      CLOSE(4)
      DO 64 J=1,NY,200
64    WRITE(6,60)J,YCURVE(J)
C
C
      WRITE(6,65)
65    FORMAT(/////)
      READ(5,*) NPLATE
      READ(5,39)TITLE
      WRITE(6,70) NPLATE,TITLE
70    FORMAT(' TITLE FOR OUTPUT',5X,I5,18A4)
      IF(NCOMP.NE.NPLATE.OR.NYCOMP.NE.NPLATE) THEN
      	WRITE(6,71)
71	FORMAT('  RADIAL DENSITY OR Y-CURVE FOR WRONG FILM ON DISC')
      ENDIF
C
      READ(5,*) LREINDEX, ATANGL, ATAXIS
      WRITE(6,73) LREINDEX, ATANGL, ATAXIS
73    FORMAT(' As read in    LREINDEX ---------',L5/
     .       '               ATANGL -----------',F5.1/
     .       '               ATAXIS  ----------',F5.1)
C
      READ(5,*) CX0,CY0,TLTAXA,TLTANG,B3
      IF (CX0.NE.0.0.OR.CY0.NE.0.0) THEN
C     	  over-ride vectors on input diffraction pattern
      	X0=CX0
      	Y0=CY0
      	WRITE(6,75) CX0,CY0
75	FORMAT(' Centre (X0,Y0) taken from card input',2F10.2)
      ENDIF
      IF(B3.EQ.0) B3=0.00000000240
      	WRITE(6,72) B3
72	FORMAT(' STARTING VALUE OF RADIAL DISTORTION PARAMETER B3',F18.11)
      IF((TLTAXA.NE.0.0).OR.(TLTANG.NE.0.0)) WRITE(6,76)
76	FORMAT(6(' ***'/),' *** WARNING, TLTAXA READ IN MUST BE ON FILM',
     .	6(' ***'/))
C
80    READ(5,*)CDX1,CDY1,CDX2,CDY2
      IF (CDX1.NE.0.0.OR.CDY1.NE.0.0) THEN
C	override vectors on input file
      	DX1=CDX1
      	DY1=CDY1
      	DX2=CDX2
      	DY2=CDY2
      	WRITE(6,81)
81	FORMAT(' LATTICE VECTORS CORRECTED')
      ENDIF
      WRITE(6,82)DX1,DY1,DX2,DY2
82    FORMAT(' INITIAL POSITION OF (1,0)  ',60('-'),' ',2F10.3/
     .       ' INITIAL POSITION OF (0,1)  ',60('-'),' ',2F10.3)
C
      IF(ABS(DX1)+ABS(DY1).LE.0.0000001) THEN
      	 WRITE(6,84)
84  	 FORMAT(///' Lattice parameters not given either on input',
     .	 ' parameters or in header !!'//' !!!!!!!!!!!!!!!!!!!!!'///)
	 STOP
      ENDIF
C
      PI=3.141592654
      TLTAXA=TLTAXA*PI/180.0
      ANGA=ATAN2(DY1,DX1)
      ANGB=ATAN2(DY2,DX2)
      DANG=ANGB-ANGA ! keep this definition of DANG here, different later
      IF(DANG.GT.PI) DANG=DANG-PI*2.0
      IF(DANG.LT.-PI) DANG=DANG+PI*2.0
      IF((TLTAXA.EQ.0.0).AND.(TLTANG.EQ.0.0)) THEN ! skip if tilt read in.
      	IF(LREINDEX) THEN ! here for attempt to get correct indexing
      	 CALL REINDEX(ATANGL,ATAXIS,DX1,DY1,DX2,DY2,
     .	   TL,TAXA,TANG,ASTAR,BSTAR,GMSTAR)
      	 ANGA=ATAN2(DY1,DX1)
      	 ANGB=ATAN2(DY2,DX2)
      	 DANG=ANGB-ANGA
      	 IF(DANG.GT.PI) DANG=DANG-PI*2.0
      	 IF(DANG.LT.-PI) DANG=DANG+PI*2.0
      	ELSE
      	 ASTILT=SQRT(DX1**2+DY1**2)
      	 BSTILT=SQRT(DX2**2+DY2**2)
      	 GSTILT=DANG*180.0/PI
      	 IF(GSTILT.LT.0.0) GSTILT=-GSTILT
		CALL EMTILT(TL,TAXA,TANG,ASTAR,BSTAR,GMSTAR,ASTILT,BSTILT,GSTILT)
      	ENDIF
C		TANG SHOULD ALWAYS BE RETURNED +VE FROM EMTILT
C		TILTDIR DEFINED AT MICROSCOPE, -1 = CLOCKWISE ROTATION
C		THESE CONVENTIONS APPLICABLE FOR 600 TO 1000 MM CAMERA LENGTH ON
C		EM400 MICROSCOPE, WITH OBJECTIVE LENS CURRENT UNDERFOCUSSED
      	TANG=TILTDIR*TANG
      	WRITE(6,85)TAXA,TANG,TL
85	FORMAT(' TAXA AND TANG CALCULATED FROM LATTICE PARAMS AND',
     .	' CELL DIMENSIONS IN PROGRAM     ',2F10.2,'   FILM ANGLE ',F10.2)
      	TLTAXA=TL*PI/180.0
      	TLTANG=TANG
      ENDIF
      IF(DANG.LT.0.) THEN
      	TLTDIR=ANGA+TLTAXA
      ELSE
      	TLTDIR=ANGA-TLTAXA
      ENDIF
      IF(TLTDIR.GT.PI) TLTDIR=TLTDIR-PI
      IF(TLTDIR.LT.-PI) TLTDIR=TLTDIR+PI
      IF(TLTDIR.LT.0.) TLTDIR=TLTDIR+PI
C
C  THIS ENSURES TLTDIR IS BETWEEN 0 AND PI.
C  TLTNRM IS THEREFORE BETWEEN PI/2 AND 3*PI/2.
C  THUS, FOR CURRENT (JUNE 82) CONVENTION FOR FILM SCANNING AND ELECTRON
C  DIFFRACTION  TLTANG IS ALWAYS NEGATIVE.
C
      	 TLTNRM=TLTDIR+PI/2.0
      	 TLTAXA=TLTAXA*180.0/PI
      WRITE(6,90)TLTDIR*180.0/PI
      WRITE(6,91) X0,Y0,TLTAXA,TLTANG
      	 TLTANG=TLTANG*PI/180.0
      	 TLTAXA=TLTAXA*PI/180.0
90    FORMAT(' ABSOLUTE DIRECTION OF TILTAXIS ON FILM (RELATIVE TO',
     .' X SCAN AXIS) WAS',19('-'),F9.1,'  degrees')
91    FORMAT(' STARTING COORDS X0,Y0  ',64('-'),2F10.2/
     .' ANGLE BETWEEN ASTAR AND TILTAXIS ON FILM AND TLTANG',36('-'),
     . 2F10.2)
C
C
      READ(5,*) ROUT,RIN,PRPMAX        ! now read radii in, in Angstroms
      WRITE(6,92) ROUT,RIN,PRPMAX
92    FORMAT(' RADII IN ANGSTROMS AS READ IN',20('-'),3F10.2)
C	convert to radius in pixels, assuming ASTAR corresponds to (1,0)
		AH     = SQRT(DX1**2+DY1**2)
		AHCORR = (COS(TLTAXA))**2+(SIN(TLTAXA)*COS(TLTANG))**2
		AHCORR = AH*SQRT(AHCORR)
		ROUT   = AHCORR/ASTAR*(1.0/ROUT)
		RIN    = AHCORR/ASTAR*(1.0/RIN)
		PRPMAX = AHCORR/ASTAR*(1.0/PRPMAX)
		RHALF  = SQRT(0.2*ROUT**2 + 0.8*RIN**2)
C	now in pixels
C
C   This may be redundant with profile fitting !!
      READ(5,*) NXM,NYM, NXMT,NYMT
      IF(NXM.EQ.0) THEN
C          over-ride raster sizes on input files
		NXM=IEXT9(8)
		NYM=IEXT9(9)
		NXMT=0
		NYMT=0
      ENDIF
      IF((NXM.GT.IRAST).OR.(NYM.GT.IRAST)) THEN
      	STOP ' MEASURING RASTER TOO BIG FOR STORE'
      ENDIF
C
      WRITE(6,94) ROUT,RIN,PRPMAX
94	FORMAT(' DATA INCLUDED OUT TO RADIUS',60('-'),F10.2,'   FROM',
     .	F10.2/' WITH MAXIMUM PERPENDICULAR DISTANCE FROM TILT AXIS',
     .	37('-'),F10.2)
      WRITE(6,95) NXM,NYM
95	FORMAT(' MEASURING RASTER SIZE IS           ',I3,' X',I3)
      IF(NXMT.EQ.0) THEN
      	NXMT=NXM
      	NYMT=NYM
      ENDIF
      WRITE(6,96) NXMT,NYMT
96   	FORMAT(' AND FAR AWAY FROM TILT AXIS IS     ',I3,' X',I3)
      	IXMAX=AMAX1(ABS(DX1),ABS(DX2),ABS(DX1-DX2))
      	IYMAX=AMAX1(ABS(DY1),ABS(DY2),ABS(DY1-DY2))
      	NXB = 2.0*IXMAX-NXM
      	NYB = 2.0*IYMAX-NYM
      	NXB = MIN0(NXB,IRAST)
      	NYB = MIN0(NYB,IRAST)
C
C  This formula creates maximum raster size for profile which avoids the two
C   most distant adjacent spots in X and Y directions.
C  A final check on the possibility that the third spot is too near is needed.
C
      DO 99 IDECR=1,10
      IF(((2.0*ABS(DX1)-FLOAT(NXM)).GT.FLOAT(NXB).OR.
     .	  (2.0*ABS(DY1)-FLOAT(NYM)).GT.FLOAT(NYB)).AND.
     .	 ((2.0*ABS(DX2)-FLOAT(NXM)).GT.FLOAT(NXB).OR.
     .	  (2.0*ABS(DY2)-FLOAT(NYM)).GT.FLOAT(NYB)).AND.
     .	 ((2.0*ABS(DX1-DX2)-FLOAT(NXM)).GT.FLOAT(NXB).OR.
     .	  (2.0*ABS(DY1-DY2)-FLOAT(NYM)).GT.FLOAT(NYB)))
     .	GO TO 98
      	NXB=NXB-2
      	NYB=NYB-2
      	WRITE(6,100) NXB,NYB
100	FORMAT(' Profile raster refinement NXB,NYB ',2I4)
      	IF(NXB.LE.NXM.OR.NYB.LE.NYM) STOP ' Profile raster ridiculous'
99    CONTINUE
C
98      WRITE(6,97) NXB,NYB
97   	FORMAT(' TOTAL RASTER INCLUDING BACKGROUND  ',I3,' X',I3)
C
C
      READ(5,*) FRACT,ABSOL,XAMINE,NCYC
      WRITE(6,93) FRACT,ABSOL,XAMINE,NCYC
93    FORMAT(/' Friedel Pairs will be rejected when difference',
     .       ' > ','                                FRACT =',F10.3/
     .	     '  and >',74(' '),' ABSOL =',F8.1/
     .       ' Centres of Gravity for spots will be calculated',
     .       ' when 10% area > XAMINE*SD where XAMINE =',F9.2/
     .	     ' Maximum number of cycles of parameter refinement',
     .	     39X,'=',I6)
      IF(NCYC.GT.MAXCYCL) THEN ! be reasonable
      	NCYCLES=MAXCYCL
      ELSE IF(NCYC.EQ.0) THEN ! no lattice param refinement
      	NCYCLES=1
      ELSE
      	NCYCLES=NCYC
      ENDIF
C
C
C     Refine lattice parameters each time, and generate coordinates of all
C	spots out to required resolution.
C
      DO 300 NCYCLE=1,NCYCLES
C
      IF(LPRINT) WRITE(6,202)
202   FORMAT(//' NEW CYCLE'//)

      AH=SQRT(DX1**2+DY1**2)
      AHCORR=(COS(TLTAXA))**2+(SIN(TLTAXA)*COS(TLTANG))**2
      AHCORR=AH*SQRT(AHCORR)
      EWALD=(LAMBDA*0.5*TAN(TLTANG)*ASTAR/AHCORR)
      XEWALD=EWALD*COS(TLTNRM)
      YEWALD=EWALD*SIN(TLTNRM)
      CORMAX=EWALD*ROUT*ROUT
      WRITE(6,201) CORMAX,ROUT
201   FORMAT(' MAXIMUM EWALD SPHERE POSITION CORRECTION=',F7.2,
     .'   AT RADIUS',F9.2)
C
C      CALL GETBACKGRPOSN(NTYPE,DX1,DY1,DX2,DY2,
C     .		XBA,YBA,XBB,YBB,XBC,YBC)
C
      NDATA=0
      DO 210 IH=-MAXINDEX,+MAXINDEX
      DO 210 IK=-MAXINDEX,+MAXINDEX
      	RSQ=(IH*DX1+IK*DX2)**2+(IH*DY1+IK*DY2)**2
      	XCSTRT=X0+IH*DX1+IK*DX2+RSQ*XEWALD
      	YCSTRT=Y0+IH*DY1+IK*DY2+RSQ*YEWALD
      	XC=XCSTRT
      	YC=YCSTRT
      	DO 215 NAP=1,3 ! successive approximations to pincushion correction.
      	 RADSQ=(XC-X0)**2+(YC-Y0)**2
      	 XB3CR=(XC-X0)*B3*RADSQ
      	 YB3CR=(YC-Y0)*B3*RADSQ
      	 XC=XCSTRT+XB3CR
      	 YC=YCSTRT+YB3CR
215   	CONTINUE
C  Exclude if beyond inner or outer resolution radii.
      	DXC=XC-X0
      	DYC=YC-Y0
      	RAD=SQRT(DXC*DXC+DYC*DYC)
      	IF((RAD.GT.ROUT-HALFRAST).OR.(RAD.LT.RIN+HALFRAST)) GO TO 210
C		CALCULATION OF DISTANCE FROM TILTAXIS AND APPROPRIATE
C		SIZE OF SPOT RASTER SIZE AND POSITION.
      	DPERP=DYC*COS(TLTDIR)-DXC*SIN(TLTDIR)
      	IF (DPERP.LT.0.) DPERP=-DPERP
      	IF (DPERP.GT.PRPMAX-HALFRAST)GO TO 210
C
C  Test if too near any edge before looking up distortion correction
      	IF(XC+HALFRAST+IEDGE.GT.FLOAT(NX)) GO TO 210    ! ALLOWED AREA
      	IF(XC-HALFRAST-IEDGE.LT.1.0)       GO TO 210 ! FOR PROFILE FITTING
      	IF(YC+HALFRAST+IEDGE.GT.FLOAT(NY)) GO TO 210
      	IF(YC-HALFRAST-IEDGE.LT.1.0)       GO TO 210
C  Add on the distortion correction if required, before testing for near edge
      	 IF(LUNBEND) THEN
      	  INDY=YC/ISTEP
      	  IND=1+XC/ISTEP + INDY*MXDIM
      	  XDISTORT=XCORR(IND)
      	  YDISTORT=YCORR(IND)
      	  XC=XC+XDISTORT
      	  YC=YC+YDISTORT
      	 ENDIF
C  Exclude if too near any edge of the whole picture
      	IF(XC+HALFRAST+IEDGE.GT.FLOAT(NX)) GO TO 210 ! ALLOWED AREA
      	IF(XC-HALFRAST-IEDGE.LT.1.0)       GO TO 210 ! FOR PROFILE FITTING
      	IF(YC+HALFRAST+IEDGE.GT.FLOAT(NY)) GO TO 210
      	IF(YC-HALFRAST-IEDGE.LT.1.0)       GO TO 210
C
      	PROPOR=DPERP/PRPMAX
      	NXSH=0.5*NXB
      	NYSH=0.5*NYB
      	IF(DXC.LT.0)NXSH=-NXSH
      	IF(DYC.LT.0)NYSH=-NYSH
      	REXTR=(DXC*NXSH+DYC*NYSH)/RAD ! CHECK WHOLE RASTER IS ALSO WITHIN AREA
      	IF((RAD+REXTR).GT.ROUT)GO TO 210

      	NDATA=NDATA+1
      	IF(NDATA.GT.NMAX) THEN
      	 STOP '  *****ERROR - TOO MANY SPOTS FOR STORE'
      	ENDIF
      	IPROP(NDATA)=PROPOR*1000.
      	JH(NDATA)=IH
      	JK(NDATA)=IK
      	XCOORD(NDATA)=XC
      	YCOORD(NDATA)=YC
      	 IF(LUNBEND) XCSPOT(NDATA) = XDISTORT
      	 IF(LUNBEND) YCSPOT(NDATA) = YDISTORT
      	SINT(NDATA)=0.0
	VAR(NDATA)=0.0
	VARP(NDATA)=0.0
      	BACK(NDATA)=0.0
      	NGDBCK(NDATA)=0
      	NBDBCK(NDATA)=0
      	IOVER(NDATA)=0
      	IOVERP(NDATA)=0
      	XCOREF(NDATA)=0.0
     	YCOREF(NDATA)=0.0
      	XREFINE(NDATA)=0.0
      	YREFINE(NDATA)=0.0
      	ICORR(NDATA)=0.0
      	IPROF(NDATA)=0.0
      	ILOSC(NDATA)=0.0
      	ILOSP(NDATA)=0.0
210   CONTINUE
C
      WRITE(6,216) NDATA
216     FORMAT(//' NUMBER OF SPOTS WITHIN LIMITS = ',I10)
C
C
      DO 230 J=1,NDATA
      	PROP=IPROP(J)/1000.
      	NXMRA=PROP*(NXMT-NXM)+NXM
      	NYMRA=PROP*(NYMT-NYM)+NYM
      	 XMHALF=(NXB-1.0)/2.0
      	 YMHALF=(NYB-1.0)/2.0
      	NXST=XCOORD(J)-XMHALF+0.5
      	NYST=YCOORD(J)-YMHALF+0.5
      	 NXFI=NXST+NXB-1
      	 NYFI=NYST+NYB-1
      	IF(NXST.GE.1.AND.NXFI.LE.NX.AND.
     .	   NYST.GE.1.AND.NYFI.LE.NY) THEN
      CALL GETRAST(JSTORE,J,ARRAY,NX,NXST,NYST,NXB,NYB)
		ELSE
		STOP ' Raster outside image area '
	ENDIF
      CALL INTEGRATE(JSTORE,J,IOVERL,IUNDER,
     .	 NXB,NYB,NXMRA,NYMRA,NXST,NYST,X0,Y0,
     .	 LOOKUP,ODBACK,VARTAB,STNDEV,ODBTOT,YCURVE,
     .	 SINT,VAR,BACK,IOVER,NBDBCK,NGDBCK,IFAIL)
	IF(IFAIL.EQ.1) THEN
		WRITE(6,219) JH(J),JK(J),XCOORD(J),YCOORD(J)
219		FORMAT(' H,K,XYCOORDS were',2I5,2F12.2)
		STOP ' Failed in Subroutine_INTEGRATE'
	ENDIF
C
      	IPRINT=.FALSE.
C
      CALL CENTRE(JSTORE,J,XCEN,YCEN,NOC_OF_G,IPRINT,
     .	NXB,NYB,NXMRA,NYMRA,NXST,NYST,X0,Y0,
     .	JH(J),JK(J),XAMINE,ODBACK,ODBTOTC,
     .	ROUT,RIN,YCURVE,LOOKUP,IOVERL,IUNDER,IOVERC)
C
      	IF(ODBTOT.NE.ODBTOTC) WRITE(6,220) J,JH(J),JK(J),ODBTOT,ODBTOTC
220   	FORMAT(' ODBTOT not same for spot #,H,K',3I5,
     .	 '   from INTEGRATE & CENTRE',2F10.1)
      	AMEAS  = NXMRA*NYMRA
      	ABACK  = NXB*NYB - NXMRA*NYMRA
      	SFACTOR= AMEAS/ABACK
C
      TCORR = SINT(J) - ODBTOT - BACK(J)*SFACTOR
      ICORR(J)=NINT(TCORR)
C      write(6,221) NXMRA,NYMRA,ICORR(J),SINT(J),ODBTOT,BACK(J)*SFACTOR
221   format(' XxY,ICORR,SINT,ODBTOT,BACK*SFACTOR',2I3,I6,3F10.2)
C
      	IF(IOVERC.EQ.1)IOVER(J)=1
      	IF(IOVERC.EQ.2)IOVER(J)=2
      	IF(IOVERC.EQ.3)IOVER(J)=3
      	IF(NOC_OF_G)GO TO 230
      	XCOREF(J)=XCEN+NXST-1.0
      	YCOREF(J)=YCEN+NYST-1.0
C  Now subtract the distortion correction if it was added earlier
C   before using the refined coordinates to calculate the best lattice
      	 IF(LUNBEND) THEN
      	  XREFINE(J)=XCOREF(J)-XCSPOT(J)
      	  YREFINE(J)=YCOREF(J)-YCSPOT(J)
      	 ELSE
      	  XREFINE(J)=XCOREF(J)
      	  YREFINE(J)=YCOREF(J)
      	 ENDIF
230    CONTINUE
C
C     DATA HAS NOW BEEN MEASURED
C     REFINE LATTICE PARAMS, REMEASURE, THEN PRINT OUT IF REFINEMENT WAS O.K.
C     UNLESS NCYC=0
C
      	 IF(NCYC.EQ.0) THEN
      	  WRITE(6,298)
298		 FORMAT(' NCYC=0, no lattice parameter refinement, ',
     .	  'all calculations carried out using input parameters'/
     .	  ' ',119('*'))
      	  X0NEW=X0
      	  Y0NEW=Y0
      	  DX1NEW=DX1
      	  DY1NEW=DY1
      	  DX2NEW=DX2
      	  DY2NEW=DY2
      	  GO TO 400
      	 ENDIF
C
      CALL SEARCH(X0NEW,Y0NEW,DX1NEW,DY1NEW,DX2NEW,DY2NEW,JH,JK,
     .	XREFINE,YREFINE,SINT,NDATA,B3,XEWALD,YEWALD)
C
      WRITE(6,232)
232   FORMAT(' REFINED VALUES FOR INPUT TO FURTHER CYCLES ARE THOSE ',
     .	'IN LINE ABOVE'/)
      WRITE(6,234) X0NEW,Y0NEW
      WRITE(6,236) DX1NEW,DY1NEW,DX2NEW,DY2NEW
234   FORMAT('  REFINED VALUES OF X0 AND Y0  ',2F10.2)
236   FORMAT('  REFINED VECTOR FOR  (1,0)  ',2F10.3/'  REFINED VECTOR
     .	FOR (0,1)  ',2F10.3)
      IF(ABS(X0NEW-X0).LT.0.2   .AND.  ! converging parameters
     .	ABS(Y0NEW-Y0).LT.0.2    .AND.  !
     .	ABS(DX1NEW-DX1).LT.0.05 .AND.  !
     .	ABS(DY1NEW-DY1).LT.0.05 .AND.  !
     .	ABS(DX2NEW-DX2).LT.0.05 .AND.  !
     .	ABS(DY2NEW-DY2).LT.0.05 .AND.  !
     .	NCYCLE.GT.2) THEN   ! At least 2 cycles
      	 WRITE(6,238) NCYCLE
238   	 FORMAT(' END OF LATTICE PARAM REFINEMENT  '/
     .	 ' NUMBERS CONVERGE AFTER',I5,' CYCLES'/75('*'))
      	 GO TO 400
      ENDIF
      WRITE(6,296) NCYCLE
296   FORMAT(' END OF CYCLE NUMBER  ',I5,'   ***************************
     .***********************************************************')
      	X0=X0NEW
      	Y0=Y0NEW
      	DX1=DX1NEW
      	DY1=DY1NEW
      	DX2=DX2NEW
      	DY2=DY2NEW
300   CONTINUE
C
C
      WRITE(6,302) NCYCLES
302   FORMAT(' END OF FIXED NUMBER OF CYCLES',I5)
400   CONTINUE
      	 AH=DX1NEW**2+DY1NEW**2
      	 AK=DX2NEW**2+DY2NEW**2
      	 AI=(DX2NEW-DX1NEW)**2+(DY2NEW-DY1NEW)**2
      	 AH=SQRT(AH)
      	 AK=SQRT(AK)
      	 AI=SQRT(AI)
C
C           The following angles depend on scanning convention which at June
C           is origin in top right corner near film number.
      	 ANGH=ATAN2(DY1NEW,DX1NEW)
      	 ANGK=ATAN2(DY2NEW,DX2NEW)
      	 ANGI=ATAN2(-DY1NEW+DY2NEW,DX2NEW-DX1NEW)
      	 ANGH=ANGH*180.0/PI
      	 ANGK=ANGK*180.0/PI
      	 ANGI=ANGI*180.0/PI
      	 WRITE(6,241)AH,AK,AI
      	 WRITE(6,242)ANGH,ANGK,ANGI
241   	 FORMAT(60X,'LENGTHS OF H,K,-H+K  ARE',3F10.3)
242   	 FORMAT(60X,'ANGLES TO PLATE EDGE ARE',3(F9.2,1X))
C
C           Calculation of exact TLTAXA,TLTANG from lattice parameters & SHRINK.
      	 COSDIS=COS(ANGDIS)
      	 SINDIS=SIN(ANGDIS)
      	 DX1COR=COSDIS*(DX1NEW*COSDIS+DY1NEW*SINDIS)-SHRINK*SINDIS*
     .	 (-DX1NEW*SINDIS+DY1NEW*COSDIS)
      	 DY1COR=SINDIS*(DX1NEW*COSDIS+DY1NEW*SINDIS)+SHRINK*COSDIS*
     .	 (-DX1NEW*SINDIS+DY1NEW*COSDIS)
      	 DX2COR=COSDIS*(DX2NEW*COSDIS+DY2NEW*SINDIS)-SHRINK*SINDIS*
     .	 (-DX2NEW*SINDIS+DY2NEW*COSDIS)
      	 DY2COR=SINDIS*(DX2NEW*COSDIS+DY2NEW*SINDIS)+SHRINK*COSDIS*
     .	 (-DX2NEW*SINDIS+DY2NEW*COSDIS)
      	 WRITE(6,245)DX1COR,DY1COR,DX2COR,DY2COR
245   	 FORMAT(' DISTORTION CORRECTED REFINED VECTORS, (1,0)',
     .	  2F10.3/39X,'(0,1)',2F10.3)
      	 PI=3.141592654
      	 ASTILT=SQRT(DX1COR**2+DY1COR**2)
      	 BSTILT=SQRT(DX2COR**2+DY2COR**2)
      	 ANGA=ATAN2(DY1COR,DX1COR)
      	 ANGB=ATAN2(DY2COR,DX2COR)
      	 DANG=ANGA-ANGB
      	 IF(DANG.GT.PI)DANG=DANG-2.0*PI
      	 IF(DANG.LT.-PI)DANG=DANG+2.0*PI
      	 GSTILT=DANG*180.0/PI
      	 IF(GSTILT.LT.0.0) GSTILT=-GSTILT
      CALL EMTILT(TL,TAXA,TANG,ASTAR,BSTAR,GMSTAR,ASTILT,BSTILT,GSTILT)
      	TANG=TILTDIR*TANG
      	WRITE(6,85)TAXA,TANG,TL
      	TILTAXIS = ANGA*180.0/PI + TL*SIGN(1.0,DANG) ! angle wrt xy-axes
      IF(TILTAXIS.GT.180.0)  TILTAXIS=TILTAXIS-180.0
      IF(TILTAXIS.LT.-180.0) TILTAXIS=TILTAXIS+180.0
      IF(TILTAXIS.LT.0.)  TILTAXIS=TILTAXIS+180.0
      	WRITE(6,86)TILTAXIS
86	FORMAT(' Direction of tiltaxis relative to XY on film =',F8.2)
C
C
C Now write and plot individual spot data.
      CALL PLOTOUT(NX,NY,X0,Y0,DX1,DY1,DX2,DY2,NPLATE,NDATA,INTMAX,
     .	 NXM,NYM,NXB,NYB,NXMT,NYMT,NBDBCK,NGDBCK,
     .	 JH,JK,IPROP,ICORR,IOVER,LPRINT,ROUT,B3,
     .	 SINT,BACK,XCOREF,YCOREF,XCOORD,YCOORD)
C
C
      IF(LGLOBL) CALL GLOBAL(1,NGLOBL,ICORR,BACK,IPROP,IOVER,JH,JK,
     .	NXM,NYM,NXMT,NYMT,NXB,NYB,DX1,DY1,DX2,DY2,NDATA,
     .	SPVSTORE,SP2VSTORE)
C
C
C Profile fitting section.
      NAVPROF=0
      GOODFIT=0.0
      GOODFITP=0.0
      GOODFITB=0.0
      IF(LPROFIT) THEN
      	 CALL SUBTRACT(NX,NY,ARRAY,X0,Y0,LOOKUP,ODBACK,YCURVE)
      	 CALL PROFGET(NDATA,JSTORE,NXB,NYB,XCOORD,YCOORD,X0,Y0,
     .	  IOVERL,IUNDER,LOOKUP,ODBACK,YCURVE,JPROF,LPROF)
      	   IF(LPTYPE.EQ.0.OR.LPTYPE.EQ.1)
     .	 CALL PROFAVER(LPTYPE,LPRANGE,0,NDATA,JPROF,LPROF,
     .	  APROF,IOVER,ICORR,VAR,XCOORD,YCOORD,X0,Y0,
     .	  JH,JK,TILTAXIS,RHALF,NXB,NYB,LPRINT,
     .	  IFLATTEN,PROFMIN,NAVPROF)
      	   IF(LPTYPE.EQ.2) THEN
      	 DO 280 J=1,NDATA
      	  CALL PROFAVER(LPTYPE,LPRANGE,J,NDATA,JPROF,LPROF,
     .	   APROF,IOVER,ICORR,VAR,XCOORD,YCOORD,X0,Y0,
     .	   JH,JK,TILTAXIS,RHALF,NXB,NYB,LPRINT,
     .	   IFLATTEN,PROFMIN,NAVPROF)
      	  CALL PROFMEAS(LPTYPE,J,NXB,NYB,NXM,NYM,
     .	   JSTORE,JPROF,LPROF,APROF,IPROF,IOVERP,
     .	   VARP,VARTAB,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF,
     .	   ARRAY,NX,NY,LPRINT,IFLATTEN,TSIGMA,GOODFIT,
     .	   GOODFITP,GOODFITB,JH,JK,TPROF,ROUT,PRPMAX,GPBACK,
     .	   SPVSTORE,SP2VSTORE)
280   	 CONTINUE
      	 NAVPROF=NAVPROF/NDATA
      	   ENDIF
      	   IF(LPTYPE.EQ.3) CALL PROFDECONV(TPROF,NDATA,NXB,NYB,
     .	   XCOORD,YCOORD,X0,Y0,ICORR,VAR,IOVER,IFLATTEN,PROFMIN,
     .	   JPROF,TILTAXIS,RIN,ROUT,RHALF,PRPMAX,JH,JK,NAVPROF)
      	   IF(LPTYPE.NE.2)
     .	 CALL PROFMEAS(LPTYPE,NDATA,NXB,NYB,NXM,NYM,
     .	  JSTORE,JPROF,LPROF,APROF,IPROF,IOVERP,
     .	  VARP,VARTAB,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF,
     .	  ARRAY,NX,NY,LPRINT,IFLATTEN,TSIGMA,GOODFIT,
     .	  GOODFITP,GOODFITB,JH,JK,TPROF,ROUT,PRPMAX,GPBACK,
     .	  SPVSTORE,SP2VSTORE)
C
      	WRITE(6,284) NAVPROF,GOODFIT/NDATA,GOODFITP/NDATA,GOODFITB/NDATA
284   	FORMAT(/' Average number of rasters added to make each profile',
     .	     I5/' Mean value of goodness-of-fit overall       ',F5.2/
     .	 ' Mean value of goodness-of-fit for peaks     ',F5.2/
     .	 ' Mean value of goodness-of-fit for background',F5.2,
     .	 '     where a perfect fit of residual/sigma is 1.00'/
     .	    50X,'     and less than 1.00 is better than perfect')
      	IF(LGLOBL) CALL GLOBAL(2,NGLOBL,IPROF,GPBACK,IPROP,IOVERP,JH,JK,
     .	 NXM,NYM,NXMT,NYMT,NXB,NYB,DX1,DY1,DX2,DY2,NDATA,
     .	 SPVSTORE,SP2VSTORE)
      ENDIF
C
C
C
      WRITE(6,285)
285   FORMAT(/'      RESOLUTION DEPENDENT STATISTICS - before LOSCAL')
      IF(LPROFIT) THEN   ! first write out raw stats.
      	CALL STATS(NDATA,JH,JK,ICORR,IPROF,IOVERP,IPROP,VAR,VARP,
     .	 X0,Y0,XCOORD,YCOORD,ASTAR,AHCORR,ROUT,LPROFIT)
      	CALL LOSCAL(NDATA,JH,JK,IPROF,IOVERP,XCOORD,YCOORD,ILOSP)
      ELSE    ! no profile
      	CALL STATS(NDATA,JH,JK,ICORR,ICORR,IOVER,IPROP,VAR,VARP,
     .	 X0,Y0,XCOORD,YCOORD,ASTAR,AHCORR,ROUT,LPROFIT)
      ENDIF
      CALL LOSCAL(NDATA,JH,JK,ICORR,IOVER,XCOORD,YCOORD,ILOSC)
      WRITE(6,286)
286   FORMAT(/'      RESOLUTION DEPENDENT STATISTICS - after LOSCAL')
      IF(LPROFIT) THEN   ! then locally scaled stats
      	CALL STATS(NDATA,JH,JK,ILOSC,ILOSP,IOVERP,IPROP,VAR,VARP,
     .	 X0,Y0,XCOORD,YCOORD,ASTAR,AHCORR,ROUT,LPROFIT)
      ELSE    ! no profile
      	CALL STATS(NDATA,JH,JK,ILOSC,ILOSC,IOVER,IPROP,VAR,VARP,
     .	 X0,Y0,XCOORD,YCOORD,ASTAR,AHCORR,ROUT,LPROFIT)
      ENDIF
C
C
C
C Now average Friedel related spots and write out final averaged data
      IF(LPROFIT) THEN
      	CALL FRIEDEL_OUT(NPLATE,NDATA,FRACT,ABSOL,INTMAX,
     .	 NXM,NYM,NXMT,NYMT,LPRINT,TITLE,
     .	 JH,JK,IPROF,IPROP,IOVERP,SINT)  ! IPROF,IOVERP
      ELSE
      	CALL FRIEDEL_OUT(NPLATE,NDATA,FRACT,ABSOL,INTMAX,
     .	 NXM,NYM,NXMT,NYMT,LPRINT,TITLE,
     .	 JH,JK,ICORR,IPROP,IOVER,SINT)  ! ICORR,IOVER
      ENDIF
C
C
C
C Write out density after profile, background and ycor subtraction, as well as
C interlacing offset, for defects in zig-zag scans.
C
      IF(LPROFIT) THEN
      	CALL IMOPEN(10,'DENOUT','NEW')
      	CALL ITRHDR(10,1)
      	CALL IALEXT(10,IEXT9,1,12)
      	CALL FDATE(DAT)
CTSH      	ENCODE(80,9997,TITLEOUT) DAT(5:24)
CTSH++
      	WRITE(TMPTITLEOUT,9997) DAT(5:24)
CTSH--
9997  	FORMAT(' PICKPROF : all modelled density subtracted',7X,A20,10X)
      	WRITE(6,9997) DAT(5:24)
      	CALL IWRHDR(10,TITLEOUT,1,ZERO,ZERO,ZERO)
      	CALL IWRSEC(10,ARRAY)
      	CALL ICLDEN(ARRAY,NX,NY,1,NX,1,NY,DMIN,DMAX,DMEAN)
      	CALL IWRHDR(10,TITLEOUT,-1,DMIN,DMAX,DMEAN)
      	CALL IMCLOSE(10)
      ENDIF
C
      STOP
9999  WRITE(6,9998)
9998  FORMAT(' Error in IRDSEC reading whole image area')
      STOP
      END
C
C##############################################################################
C
      SUBROUTINE CENTRE(JSTORE,IS,XCEN,YCEN,NOC_OF_G,IPRINT,
     .	NXB,NYB,NXMRA,NYMRA,NXST,NYST,X0,Y0,
     .	JH,JK,XAMINE,ODBACK,ODBTOTC,
     .	ROUT,RIN,YCURVE,LOOKUP,IOVERL,IUNDER,IOVERC)
C
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST), JDEBUG(IRAST,IRAST)
      DIMENSION ODBACK(1),YCURVE(1)
      LOGICAL NOC_OF_G,IPRINT
      REAL*4 LOOKUP(1)
C
C     THIS ROUTINE CALCULATES THE CENTRE OF GRAVITY OF EACH SPOT
C     IT TERMINATES ABNORMALLY (NOC_OF_G=.TRUE.) IF THE SPOT IS TOO WEAK
C     OR TOO STRONG, AND PRINTS OUT SOME DATA IF IPRINT=.TRUE.
C     NEW VERSION 19.5.80 INCORPORATES RADIAL DENSITY SUBTRACTION.
C
C     CRITERION FOR DETECTION OF A SPOT IS THAT THERE SHOULD BE
C     MORE THAN 10 PERCENT OF THE MEASURED RASTER AREA HAVING A
C     DENSITY GREATER THAN XMIN + XAMINE*SQRT(MEAN PERIMETER OD).
C     XMIN IS LOWEST AVERAGE DENSITY ALONG TWO ADJACENT EDGES OF RASTER.
C
C     IN ALL CASES, CALCULATE TOTAL RADIAL AND Y-CURVE BACKGROUND
C     CORRECTION AND RETURN TO MAIN AS ODBTOTC
C     SET OVERLOAD INDICATORS IF NECESSARY
C
      IF(XAMINE.EQ.0.0)XAMINE=1.5
      NOC_OF_G=.FALSE.
      NXB_M=(NXB-NXMRA)/2
      NYB_M=(NYB-NYMRA)/2
      IMAX=0
      IMIN=1000000
      TOT=0.
      PER1=0.
      PER2=0.
      PER3=0.
      PER4=0.
      FACTOR=100./(NXMRA*NYMRA)
      LIMIT=(NXMRA*NYMRA)/10
      X1=NXST-X0+NXB_M ! beginning and ending coordinates of raster
      X2=X1+NXMRA-1 ! relative to centre
      Y1=NYST-Y0+NYB_M !
      Y2=Y1+NYMRA-1 !
      X1SQ=X1**2
      X2SQ=X2**2
      Y1SQ=Y1**2
      Y2SQ=Y2**2
C
C  Calculate IMIN here
C
      DO 5 IX=1+NXB_M,NXMRA+NXB_M
      	 TOT=TOT+JSTORE(IS,IX,1+NYB_M)+JSTORE(IS,IX,NYMRA+NYB_M)
      	 XCOORD=IX-1+NXST-X0
      	 XCOSQ=XCOORD**2
      	 IRAD=1.5+SQRT(XCOSQ+Y1SQ)
C     		IF(IRAD.GT.(ROUT+1))GO TO 950
C      		IF(IRAD.LE.RIN)GO TO 950
      	 PER1=PER1+JSTORE(IS,IX,1+NYB_M)-ODBACK(IRAD)
      	 IRAD=1.5+SQRT(XCOSQ+Y2SQ)
C      		IF(IRAD.GT.(ROUT+1))GO TO 950
C      		IF(IRAD.LE.RIN)GO TO 950
5     PER2=PER2+JSTORE(IS,IX,NYMRA+NYB_M)-ODBACK(IRAD)
      PER1=PER1/NXMRA
      PER2=PER2/NXMRA ! radial back-corrected perimeters at top and bottom
      DO 10 IY=1+NYB_M,NYMRA+NYB_M
      	 TOT=TOT+JSTORE(IS,1+NXB_M,IY)+JSTORE(IS,NXMRA+NXB_M,IY)
      	 YCOORD=IY-1+NYST-Y0
      	 YCOSQ=YCOORD**2
      	 IRAD=1.5+SQRT(YCOSQ+X1SQ)
C      		IF(IRAD.GT.(ROUT+1))GO TO 950
C      		IF(IRAD.LE.RIN)GO TO 950
      	 PER3=PER3+JSTORE(IS,1+NXB_M,IY)-ODBACK(IRAD)
      	 IRAD=1.5+SQRT(YCOSQ+X2SQ)
C     		IF(IRAD.GT.(ROUT+1))GO TO 950
C      		IF(IRAD.LE.RIN)GO TO 950
10    PER4=PER4+JSTORE(IS,NXMRA+NXB_M,IY)-ODBACK(IRAD)
      PER3=PER3/NYMRA
      PER4=PER4/NYMRA  ! perimeters left and right, radial back correct
      TOT=TOT/(2*(NXMRA+NYMRA)) ! non-background corrected mean perimeter OD
      AV1=(PER1+PER3)/2.
      AV2=(PER2+PER3)/2.
      AV3=(PER2+PER4)/2.
      AV4=(PER4+PER1)/2.
      XMIN=AMIN1(AV1,AV2,AV3,AV4)
C      IMIN=XMIN
C     NOW CALCULATE CENTRE OF GRAVITY
      ITEST=XMIN+XAMINE*SQRT(TOT)
      NABOVE=0
      DO 15 IX=1+NXB_M,NXMRA+NXB_M
      	XCOORD=NXST+IX-1-X0
      	XCOSQ=XCOORD**2
      	DO 15 IY=1+NYB_M,NYMRA+NYB_M
      	 YCOORD=NYST+IY-1-Y0
      	 IRAD=1.5+SQRT(XCOSQ+YCOORD**2)
      	 IF(JSTORE(IS,IX,IY).GT.IMAX) IMAX=JSTORE(IS,IX,IY)
      	 IF(JSTORE(IS,IX,IY).LT.IMIN) IMIN=JSTORE(IS,IX,IY)
      	 IF((JSTORE(IS,IX,IY)-ODBACK(IRAD)).GT.ITEST) NABOVE=NABOVE+1
15    CONTINUE
      IF(NABOVE.LE.LIMIT .OR.   ! if no spot detected
     .	IMAX.GT.IOVERL.OR.IMIN.LE.IUNDER) THEN ! or overload or underload, then return.
      	 NOC_OF_G=.TRUE.
      	 PERCNT=(100.*NABOVE)/(NXMRA*NYMRA)
      	 IF(IPRINT)
     .	  WRITE(6,51) PERCNT,XAMINE,JH,JK,NXMRA,NYMRA,
     .	       IOVERL,IUNDER,IMAX,IMIN
51    	  FORMAT(F8.1,' PERCENT OF RASTER ABOVE XMIN+',F6.2,
     .	  '*SQRT(PERIM)  CENTRE NOT CALC FOR REFLECTION',
     .	  2I4,' RASTER SIZE ',2I4/
     .	  '         IMAX >',I4,' or IMIN <',I3,'   IMAX,IMIN=',2I5)
      ENDIF
C
      ISUM=0
      IXSUM=0
      IYSUM=0
      ODBTOTC=0.0
      IOVERC=0
      DO 30 IX=1+NXB_M,NXMRA+NXB_M ! calculate c. of g. for these spots
      	XCOORD=NXST+IX-1-X0
      	XCOSQ=XCOORD**2
      	DO 30 IY=1+NYB_M,NYMRA+NYB_M
      	 IYCURV=IY-1+NYST
      	 YCOORD=NYST+IY-1-Y0
      	 RAD=1.+SQRT(XCOSQ+YCOORD**2)
      	 IF(RAD.GT.(ROUT+1.))IOVERC=3
      	 IF(RAD.LE.RIN)IOVERC=3
      	 IRAD=RAD
      	 DRAD=RAD-IRAD
      	 ODB=ODBACK(IRAD)*(1-DRAD)+ODBACK(IRAD+1)*DRAD+YCURVE(IYCURV)
C		  interpolate radial background, lookup ycurve
      	 JLOOK=ODB
      	 DJLOOK=ODB-JLOOK
      	 ODBTOTC=ODBTOTC+LOOKUP(JLOOK)*(1-DJLOOK)+LOOKUP(JLOOK+1)*DJLOOK
C		  interpolate lookup table for backgrounds
      	 IF(JLOOK.GT.IOVERL)IOVERC=1
      	 IF((JLOOK+1).LE.IUNDER)IOVERC=2
      	 IRAD=1.5+SQRT(XCOSQ+YCOORD**2)
      	 ID=JSTORE(IS,IX,IY)-ODBACK(IRAD)-XMIN
      	 JDEBUG(IX,IY)= JSTORE(IS,IX,IY)-ODBACK(IRAD)-XMIN
      	 ISUM=ISUM+ID
      	 IXSUM=IXSUM+ID*IX
30    IYSUM=IYSUM+ID*IY
      XSUM=IXSUM
      YSUM=IYSUM
      SUM=ISUM
      IF(SUM.NE.0.0)GO TO 930
      XCEN=(1+FLOAT(NXB))/2.
      YCEN=(1+FLOAT(NYB))/2.
      GO TO 931
930   XCEN=XSUM/SUM
      YCEN=YSUM/SUM
C				***************************
C debug, test to see whether calculated centre is outside measuring raster.
      IF(.NOT.NOC_OF_G) THEN
      	IF(XCEN.LT.FLOAT(1+NXB_M).OR.XCEN.GT.FLOAT(NXMRA+NXB_M).
     .	 OR.YCEN.LT.FLOAT(1+NYB_M).OR.YCEN.GT.FLOAT(NYMRA+NYB_M)) THEN
      	 PERCNT=NABOVE*FACTOR
      	 WRITE(6,932) JH,JK,XCEN,YCEN,XMIN,PERCNT
932			FORMAT(//' Possible spot found too far outside raster area'/
     .	   '   JH,JK,XCEN,YCEN,XMIN,PERCNT',2I5,4F10.2/
     .	   '  this Centre_of_Gravity discarded')
      	 DO 933 IY=1+NYB_M,NYMRA+NYB_M
      	  WRITE(6,934) (JSTORE(IS,IX,IY),IX=1+NXB_M,NXMRA+NXB_M),
     .	   (JDEBUG(IX,IY),IX=1+NXB_M,NXMRA+NXB_M)
934			FORMAT(20I5)
933		CONTINUE
      	 NOC_OF_G=.TRUE.
      	ENDIF
      ENDIF
C				****************************
931   IF(.NOT.IPRINT) RETURN
C
C  Here for interesting cosmetic printout
C
      WRITE(6,11) JH,JK,XMIN,NXMRA,NYMRA
11    FORMAT(//' FOR (',I4,',',I4,')  REFLECTION'/30X,'MINIMUM DENSITY
     .	FROM TWO SIDES OF PERIMETER=',F6.1,' RASTER SIZE=',2I5)
      PERCNT=NABOVE*FACTOR
      WRITE(6,911)PERCNT,XAMINE*SQRT(TOT)
911   FORMAT(F8.1,' PERCENT OF RASTER ABOVE XMIN+',F6.2,
     .	'*SQRT(PERIM)'/)
      IXSTP=1+NXMRA/30
      IYSTP=1+NYMRA/30
      DO 14 IY=1+NYB_M,NYMRA+NYB_M,IYSTP
14    WRITE(6,12)(JSTORE(IS,IX,IY),IX=1+NXB_M,NXMRA+NXB_M,IXSTP)
12    FORMAT(30I4)
      WRITE(6,31) XCEN,YCEN
31    FORMAT(70X,' REFINED POSN OF BACKGROUND CORR CENTRE IS',2F6.2)
      WRITE(6,912)ODBTOTC
912   FORMAT(60X,' SUMMATION OF RADIAL BACKGROUND CONTRIBUTION IS',
     .	F10.0//)
C
C950   NOC_OF_G=.TRUE.
C      WRITE(6,951)JH,JK
C951   FORMAT(' RASTER FOR REFLECTION',2I5,' GOES OUTSIDE MAX RAD')
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE REFIND(X0,Y0,DX1,DY1,DX2,DY2,JH,JK,XREF,YREF,SINT,
     .	NDATA,B3,XEWALD,YEWALD)
      DIMENSION XREF(1),YREF(1),SINT(1),JH(1),JK(1)
C
C     REFINE LATTICE PARAMETERS.
C
      DO 30 NAP=1,3
      S=0.
      SH=0.
      SK=0.
      SX=0.
      SY=0.
      SHH=0.
      SKK=0.
      SHK=0.
      SXH=0.
      SXK=0.
      SYH=0.
      SYK=0.
C
      DO 20 J=1,NDATA
      IF(XREF(J).EQ.0.0) GO TO 20
      RSQ=(JH(J)*DX1+JK(J)*DX2)**2 +
     1  (JH(J)*DY1+JK(J)*DY2)**2
      RADSQ=(XREF(J)-X0)**2 + (YREF(J)-Y0)**2
      XCORR=(XREF(J)-X0)*B3*RADSQ+XEWALD*RSQ
      YCORR=(YREF(J)-Y0)*B3*RADSQ+YEWALD*RSQ
      XOBS=XREF(J)-XCORR
      YOBS=YREF(J)-YCORR
      S=S+1.0
      SH=SH+JH(J)
      SK=SK+JK(J)
      SX=SX+XOBS
      SY=SY+YOBS
      SHH=SHH+JH(J)**2
      SKK=SKK+JK(J)**2
      SHK=SHK+JH(J)*JK(J)
      SXH=SXH+XOBS*JH(J)
      SXK=SXK+XOBS*JK(J)
      SYH=SYH+YOBS*JH(J)
      SYK=SYK+YOBS*JK(J)
20    CONTINUE
      IF (S.EQ.0)WRITE(6,920)
920   FORMAT(' NO REFLECTIONS TO USE IN REFIND')
      IF(S.EQ.0)STOP
      BOTTOM=(SH*SK-S*SHK)**2-(SK*SK-S*SKK)*(SH*SH-S*SHH)
      DX1=((SX*SK-S*SXK)*(SH*SK-S*SHK)-(SX*SH-S*SXH)*(SK*SK-S*SKK))/
     1 BOTTOM
      DX2=((SX*SH-S*SXH)*(SH*SK-S*SHK)-(SX*SK-S*SXK)*(SH*SH-S*SHH))/
     1 BOTTOM
      X0=(SX-SK*DX2-SH*DX1)/S
      DY1=((SY*SK-S*SYK)*(SH*SK-S*SHK)-(SY*SH-S*SYH)*(SK*SK-S*SKK))/
     1 BOTTOM
      DY2=((SY*SH-S*SYH)*(SH*SK-S*SHK)-(SY*SK-S*SYK)*(SH*SH-S*SHH))/
     1 BOTTOM
      Y0=(SY-SK*DY2-SH*DY1)/S
C
30    CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE SEARCH(X0,Y0,DX1,DY1,DX2,DY2,JH,JK,XREF,YREF,SINT,
     .	NDATA,B3,XEWALD,YEWALD)
C
C     THIS ROUTINE WILL DO A SIMPLE SEARCH PROCEDURE ON B3 WITH
C     L.S. REFINEMENT OF X0,Y0,DX1,DY1,DX2,DY2 AND MINIMISATION OF L.
C     ALTERED SO THAT XD AND YD STAY FIXED
C     SEARCH B3 FROM 0 TO 2*B3INIT.
C     CHECK THAT THERE ARE ENOUGH REFLECTIONS TO USE.
C
      DIMENSION XREF(1),YREF(1),SINT(1),JH(1),JK(1)
      REAL LBEST,L,LSQRT
      INTEGER REDO
      LBEST=10000.
      B3INIT=0.00000001500
      B3STEP=0.00000000750
      NINT=0
      NCOORD=0
C      WRITE(6,911)XEWALD,YEWALD
C911   FORMAT(' XEWALD AND YEWALD',2F17.11)
      DO 10 J=1,NDATA
      IF(SINT(J).EQ.0.0) GO TO 5
      NINT=NINT+1
5     CONTINUE
      IF(XREF(J).EQ.0.0) GO TO 10
      NCOORD=NCOORD+1
10    CONTINUE
      WRITE(6,11) NINT,NCOORD
11    FORMAT(I6,' REFLECTIONS WITH MEASURED INTENSITY'/I6,
     1'  REFLECTIONS WITH ACCURATE CENTRES OF GRAVITY')
      IF(NCOORD.LE.10) GO TO 400
      WRITE(6,302)
      NCYCLE=0
19    NREDO=0
20    REDO=0
      DO 100 IB3=1,5
      B3=B3INIT+(IB3-3)*B3STEP
      CALL REFIND(X0,Y0,DX1,DY1,DX2,DY2,JH,JK,XREF,YREF,SINT,
     .	NDATA,B3,XEWALD,YEWALD)
C     WRITE(6,920)
C920   FORMAT(' AFTER REFIND')
C     WRITE(6,301)X0,Y0,DX1,DY1,DX2,DY2,XD,YD,B3
      L=0.
      N=0
      DO 50 I=1,NDATA
      IF(XREF(I).EQ.0.0) GO TO 50
      RADSQ=(XREF(I)-X0)**2+(YREF(I)-Y0)**2
      RSQ=(JH(I)*DX1+JK(I)*DX2)**2 + (JH(I)*DY1+JK(I)*DY2)**2
      XCALC=X0+JH(I)*DX1+JK(I)*DX2+B3*(XREF(I)-X0)*RADSQ+XEWALD*RSQ
      YCALC=Y0+JH(I)*DY1+JK(I)*DY2+B3*(YREF(I)-Y0)*RADSQ+YEWALD*RSQ
      A=(XREF(I)-XCALC)**2+(YREF(I)-YCALC)**2
      L=L+SQRT(A)
      N=N+1
50    CONTINUE
      L=L/N
      IF(L.GT.LBEST) GO TO 100
      LBEST=L
      NTEST=IB3
      B3BEST=B3
100   CONTINUE
      IF((NTEST.EQ.1).OR.(NTEST.EQ.5)) REDO=1
      CALL REFIND(X0,Y0,DX1,DY1,DX2,DY2,JH,JK,XREF,YREF,SINT,
     1NDATA,B3BEST,XEWALD,YEWALD)
      WRITE(6,301)X0,Y0,DX1,DY1,DX2,DY2,B3BEST,LBEST,
     1NCYCLE,NTEST
      B3INIT=B3BEST
      IF(REDO.NE.1) GO TO 51
      NREDO=NREDO+1
      IF(NREDO.GT.2)GO TO 51
      GO TO 20
51    NCYCLE=NCYCLE+1
      IF(NCYCLE.GT.5) GO TO 200
      B3STEP=B3STEP/4.
      GO TO 19
200   B3=B3BEST
      RETURN
400   WRITE(6,401)
401   FORMAT('  LESS THAN 10 REFLECTIONS FOUND-PARAMS MUST BE WRONG')
      STOP
301   FORMAT(2F10.2,4F10.3,F17.11,F8.4,I10,I3)
302   FORMAT('      X0        Y0       DX1       DY1       DX2',
     1'       DY2            B3        LAVER ',
     2'   NCYCLE IB3')
      END
C
C##############################################################################
C
      SUBROUTINE EMTILT(TL,TLTAXA,TLTANG,A0,B0,GAMMA0,A1,B1,GAMMA1)
C  CALCULATE TILT ANGLES FROM TILTED AND UNTILTED RECIPROCAL
C  CELL DIMENSIONS.
C
C  CONVENTION FOR MEASURING TILT AXIS TO ASTAR IS THAT THE ANGLE IS
C  FROM TILTAXIS TO ASTAR IN THE DIRECTION GIVEN BY ASTAR TO BSTAR
C  BEING POSITIVE.
C
	IMPLICIT REAL*8 (A-H,O-Z)
	REAL*4 TL,TLTAXA,TLTANG,A0,B0,GAMMA0,A1,B1,GAMMA1
	A=A0
	B=B0
	GAMMA=GAMMA0
	AT=A1
	BT=B1
	GAMMAT=GAMMA1
C
	COSG = DCOS(GAMMA*3.14159/180.0)
	COSGT= DCOS(GAMMAT*3.14159/180.0)
	SING = DSIN(GAMMA*3.14159/180.0)
C
	C1 = (A*A)/(AT*AT)
	C2 = (B*B)/(BT*BT)
	C3 = (A*B)/(AT*BT*COSGT)
	C4 = (A*B*COSG)/(AT*BT*COSGT)
C
	AX = C2*(C1/C3)*(C1/C3) - C1
	BX = C2 + 2.0*C2*(C1/C3)*((C1-C4)/C3) - C1
	CX = C2*((C1-C4)/C3)*((C1-C4)/C3)
	DISC = BX*BX - 4.0*AX*CX
	IF (DISC .LT. 0.0) GO TO 200
	PSQ1 = (-BX + DSQRT(DISC))/(2.0*AX)
	PSQ2 = (-BX - DSQRT(DISC))/(2.0*AX)
	IF (PSQ1 .LT. 0.0 .AND. PSQ2 .LT. 0.0) GO TO 210
	IF (PSQ1 .GT. 0.0) PP = DSQRT(PSQ1)
	IF (PSQ2 .GT. 0.0) PP = DSQRT(PSQ2)
	QQ = (C1/C3)*PP + ((C1-C4)/C3)*(1.0/PP)
	XK = DSQRT(C1*(PP*PP + 1.0))
C	WRITE(6,110) PP,QQ,XK
C110	FORMAT (' P,Q, AND SCALE FACTOR ',3F15.5)
	PHI = DATAN2(SING,QQ/PP-COSG)
	SINPHI = DSIN(PHI)
	COSPHI = DCOS(PHI)
	THETA = DATAN2(PP,SINPHI)
	TANTHE = DTAN(THETA)
	PHI = PHI*180.0/3.14159
	THETA = THETA*180.0/3.14159
C	WRITE(6,111) PHI,THETA
C111	FORMAT (' ANGLE FROM TILT AXIS TO ASTAR, AND TILT ANGLE',2F15.5)
C
C  CALCULATE ANGLE TO TILTED ASTAR ... THIS IS THE ANGLE ONE WOULD GET
C  DIRECTLY FROM THE FILM AND IS USED IN PICKTILT.
C
	COSPHI=COSPHI/(DSQRT(1.0+SINPHI*SINPHI*TANTHE*TANTHE))
	PHITLT=DACOS(COSPHI)*180.0/3.14159
C	WRITE(6,112) PHITLT
C112	FORMAT(' ANGLE FROM TILT AXIS TO TILTED ASTAR ON FILM',F15.5)
	TL=PHITLT
	TLTAXA=PHI
	TLTANG=THETA
	RETURN
200	WRITE (6,201)
201	FORMAT (' DISCRIMINANT LESS THAN ZERO')
	STOP
210	WRITE (6,211)
211	FORMAT(' TWO NEGATIVE ROOTS - SOMETHING WRONG')
	STOP
	END
C
C******************************************************************************
C
      SUBROUTINE PLOTOUT(NX,NY,X0,Y0,DX1,DY1,DX2,DY2,NPLATE,NDATA,
     .	INTMAX,NXM,NYM,NXB,NYB,NXMT,NYMT,NBDBCK,NGDBCK,
     .	JH,JK,IPROP,ICORR,IOVER,LPRINT,ROUT,B3,
     .	SINT,BACK,XCOREF,YCOREF,XCOORD,YCOORD)
C
      DIMENSION NRAD(8),THETA(8),DELRAD(8),TEXT(20)
      DIMENSION XCOORD(1),YCOORD(1),XCOREF(1),YCOREF(1)
      DIMENSION SINT(1),BACK(1),ICORR(1),IPROP(1)
      DIMENSION NBDBCK(1),NGDBCK(1),JH(1),JK(1),IOVER(1)
      CHARACTER DAT*24
      LOGICAL LPRINT
CTSH++
	CHARACTER*80 TEXT
CTSH--
C
C     STATISTICS OF CENTRE OF GRAVITY DISPLACEMENTS WILL BE
C     CALCULATED AS A FUNCTION OF RADIUS IN PATTERN FROM XD,YD
C
      PLTSIZ = 260.0      ! old plots were dimensioned in mm
      FONTSIZE = 4.      ! set fontsize to 4mm
      CALL P2K_OUTFILE('PLOTOUT.PS',10)
      CALL P2K_HOME
      CALL P2K_FONT('Courier'//CHAR(0),FONTSIZE)
      CALL P2K_GRID(0.5*PLTSIZ,0.5*PLTSIZ,1.0)
      CALL P2K_ORIGIN(-0.5*PLTSIZ,-0.7*PLTSIZ,0.)
      CALL P2K_COLOUR(0)
      CALL P2K_MOVE(0.,0.,0.)
      XWIDTH = PLTSIZ*FLOAT(NX)/MAX0(NX,NY)
      YHEIGHT = PLTSIZ*FLOAT(NY)/MAX0(NX,NY)
      CALL P2K_DRAW(XWIDTH,0.,0.)
      CALL P2K_DRAW(XWIDTH,YHEIGHT,0.)
      CALL P2K_DRAW(0.,YHEIGHT,0.)
      CALL P2K_DRAW(0.,0.,0.)
C
      CALL P2K_MOVE(XWIDTH-20.0,YHEIGHT,0.)
      CALL P2K_DRAW(XWIDTH-20.0,YHEIGHT-15.0,0.)
      CALL P2K_MOVE(XWIDTH-25.0,YHEIGHT-10.,0.)
      CALL P2K_DRAW(XWIDTH,YHEIGHT-10.0,0.)
      CALL P2K_MOVE(XWIDTH-21.5,YHEIGHT-6.5,0.)
CTSH      ENCODE(6,9300,TEXT)NPLATE
CTSH++
      WRITE(TEXT,9300)NPLATE
CTSH--
9300  FORMAT(I6)
      CALL P2K_STRING(TEXT,6,0.)
      CALL P2K_MOVE(XWIDTH-31.5,YHEIGHT-11.5,0.)
CTSH      ENCODE(1,9309,TEXT)
CTSH++
      WRITE(TEXT,9309)
CTSH--
9309  FORMAT('X')
      CALL P2K_STRING(TEXT,1,0.)
      CALL P2K_MOVE(XWIDTH-21.5,YHEIGHT-21.5,0.)
CTSH      ENCODE(1,9310,TEXT)
CTSH++
      WRITE(TEXT,9310)
CTSH--
9310  FORMAT('Y')
      CALL P2K_STRING(TEXT,1,0.)
      CALL P2K_COLOUR(0)
      CALL P2K_MOVE(XWIDTH/2.0,0.,0.)
      CALL P2K_DRAW(XWIDTH/2.0,YHEIGHT,0.)
      CALL P2K_MOVE(0.,YHEIGHT/2.0,0.)
      CALL P2K_DRAW(XWIDTH,YHEIGHT/2.0,0.)
      SPLOT=PLTSIZ/(NX-1)
      X0PLOT=PLTSIZ-X0*SPLOT
      Y0PLOT=PLTSIZ-Y0*SPLOT
      YOFF=-0.8
      	 X1PLOT=X0PLOT-124.*DX1/SQRT(DX1**2+DY1**2)
      	 Y1PLOT=Y0PLOT-124.*DY1/SQRT(DX1**2+DY1**2)
      	 X2PLOT=X0PLOT-124.*DX2/SQRT(DX2**2+DY2**2)
      	 Y2PLOT=Y0PLOT-124.*DY2/SQRT(DX2**2+DY2**2)
      	 CALL P2K_MOVE(X0PLOT,Y0PLOT,0.)
      	 CALL P2K_DRAW(X1PLOT,Y1PLOT,0.)
      	 CALL P2K_MOVE(X0PLOT,Y0PLOT,0.)
      	 CALL P2K_DRAW(X2PLOT,Y2PLOT,0.)
      	 X1PLOT=X0PLOT-128.5*DX1/SQRT(DX1**2+DY1**2) ! label slightly
      	 Y1PLOT=Y0PLOT-128.5*DY1/SQRT(DX1**2+DY1**2) ! further out
      	 X2PLOT=X0PLOT-128.5*DX2/SQRT(DX2**2+DY2**2)
      	 Y2PLOT=Y0PLOT-128.5*DY2/SQRT(DX2**2+DY2**2)
      CALL P2K_MOVE(X0PLOT,Y0PLOT+YOFF,0.) ! mark centre with asterisk
CTSH      ENCODE (1,9301,TEXT)
CTSH++
      WRITE (TEXT,9301)
CTSH--
      CALL P2K_CSTRING(TEXT,1,0.)
9301  FORMAT('*')
      CALL P2K_MOVE(X1PLOT,Y1PLOT+YOFF,0.) ! mark end of a-axis
CTSH      ENCODE (1,9307,TEXT)
CTSH++
      WRITE (TEXT,9307)
CTSH--
      CALL P2K_CSTRING(TEXT,1,0.)
9307  FORMAT('H')
      CALL P2K_MOVE(X2PLOT,Y2PLOT+YOFF,0.) ! mark end of b-axis
CTSH      ENCODE (1,9308,TEXT)
CTSH++
      WRITE (TEXT,9308)
CTSH--
      CALL P2K_CSTRING(TEXT,1,0.)
9308  FORMAT('K')
      	CALL FDATE(DAT)
      	CALL P2K_MOVE(207.,2.0,0.)
CTSH      	ENCODE(31,9997,TEXT) DAT(5:24)
CTSH++
      	WRITE(TEXT,9997) DAT(5:24)
CTSH--
9997  	FORMAT('pickprofk  ',A20)
      CALL P2K_CSTRING(TEXT,31,0.)
      CALL P2K_COLOUR(0)
      CALL P2K_FONT('Courier'//CHAR(0),0.5*FONTSIZE)
C
      RDEL = ( ROUT )/8.0
      RMST=0.0
      RT=0.0
      NT=0
      INTMAX=0
      DO 307 J=1,8
      DELRAD(J)=0.0
      THETA(J)=0.0
307   NRAD(J)=0
C
      IF(LPRINT) WRITE(6,399)
399   	FORMAT(////)
      IF(LPRINT) WRITE(6,312)
      IF(LPRINT) WRITE(6,9312)
C
      DO 308 J=1,NDATA
      PROP=IPROP(J)/1000.
      NXMRA=PROP*(NXMT-NXM)+FLOAT(NXM)
      NYMRA=PROP*(NYMT-NYM)+FLOAT(NYM)
      ABACK=NXB*NYB - NXMRA*NYMRA
      BACK(J)=BACK(J)/ABACK
C
      IF(INTMAX.LT.ICORR(J).AND.IOVER(J).EQ.0) INTMAX=ICORR(J)
      IF(XCOREF(J).NE.0.0) THEN    ! Centre of Gravity present
      	 CGX=XCOREF(J)-XCOORD(J)
      	 CGY=YCOREF(J)-YCOORD(J)
      	 RMST=RMST+(CGX*CGX+CGY*CGY)
      	 RT=RT+SQRT(CGX*CGX+CGY*CGY)
      	 NT=NT+1
      	 XVECT=XCOORD(J)-X0
      	 YVECT=YCOORD(J)-Y0
      	 RVECT=SQRT(XVECT*XVECT+YVECT*YVECT)
      	 IRSLOT=RVECT/RDEL+1
      	 IF(IRSLOT.GT.8) IRSLOT=8
      	 COSTH=XVECT/RVECT
      	 SINTH=YVECT/RVECT
      	 DELRAD(IRSLOT)=DELRAD(IRSLOT)+CGX*COSTH+CGY*SINTH
      	 THETA(IRSLOT)=THETA(IRSLOT)+CGX*SINTH-CGY*COSTH
      	 NRAD(IRSLOT)=NRAD(IRSLOT)+1
         IF(IOVER(J).NE.0)
     .	 WRITE(6,398) JH(J),JK(J),IOVER(J),XCOREF(J),YCOREF(J)
398	 FORMAT(' C of G present on over- or underloaded spot ,',
     .	 '-  H,K,IOVER,cgx,cgy',3I5,2F10.3)
      ENDIF
      XPLOT=PLTSIZ-XCOORD(J)*SPLOT
      YPLOT=PLTSIZ-YCOORD(J)*SPLOT
      CALL P2K_MOVE(XPLOT,YPLOT+YOFF,0.)
CTSH      ENCODE(1,9302,TEXT)
CTSH++
      WRITE(TEXT,9302)
CTSH--
      IF(IOVER(J).EQ.0) THEN
CTSH      	IF(XCOREF(J).NE.0.0) ENCODE(1,9302,TEXT)	! c.of g. present
CTSH      	IF(XCOREF(J).EQ.0.0) ENCODE(1,9303,TEXT)	! no  c.of g.
CTSH++
      	IF(XCOREF(J).NE.0.0) WRITE(TEXT,9302) ! c.of g. present
      	IF(XCOREF(J).EQ.0.0) WRITE(TEXT,9303) ! no  c.of g.
CTSH++
      ENDIF
CTSH      IF(IOVER(J).EQ.2) ENCODE(1,9304,TEXT)
CTSH      IF(IOVER(J).EQ.1) ENCODE(1,9305,TEXT)
CTSH      IF(IOVER(J).EQ.3) ENCODE(1,9306,TEXT)
CTSH++
      IF(IOVER(J).EQ.2) WRITE(TEXT,9304)
      IF(IOVER(J).EQ.1) WRITE(TEXT,9305)
      IF(IOVER(J).EQ.3) WRITE(TEXT,9306)
CTSH--
      CALL P2K_CSTRING(TEXT,1,0.)
9302  FORMAT('X')
9303  FORMAT('O')
9304  FORMAT('-')
9305  FORMAT('+')
9306  FORMAT('R')
C
C plot c. of g. deviation vectors and any print requested
      IF(XCOREF(J).NE.0.0) THEN
      	XPLOT=260.-XCOORD(J)*SPLOT
      	YPLOT=260.-YCOORD(J)*SPLOT
      	IF(XPLOT.LT.260.AND.XPLOT.GT.0.OR.
     .	   YPLOT.LT.260.AND.YPLOT.GT.0.) THEN ! plot c. of g. deviations
      	 CALL P2K_MOVE(XPLOT,YPLOT,0.)
      	 XPLOT=260.-SPLOT*(20*CGX+XCOORD(J))
      	 YPLOT=260.-SPLOT*(20*CGY+YCOORD(J))
      	 CALL P2K_DRAW(XPLOT,YPLOT,0.)
      	ENDIF
C	NOW SIMPLY A LIST OF ALL NUMBERS TOGETHER WITH CENTRE DEVIATIONS
   	IF(LPRINT) WRITE(6,311) JH(J),JK(J),ICORR(J),IPROP(J),NXMRA,
     $	 NYMRA,SINT(J),BACK(J),NXB,NYB,NBDBCK(J),NGDBCK(J),
     $	 CGX,CGY,XVECT,YVECT
      ELSE
      	IF(LPRINT) THEN
      	   IF(IOVER(J).NE.0.OR.SINT(J).EQ.0.0) THEN
  		WRITE(6,9311)JH(J),JK(J),ICORR(J),IPROP(J),
     $	  NXMRA,NYMRA,SINT(J),BACK(J),NXB,NYB,NBDBCK(J),NGDBCK(J)
      	   ELSE
      	 WRITE(6,311) JH(J),JK(J),ICORR(J),IPROP(J),
     $	  NXMRA,NYMRA,SINT(J),BACK(J),NXB,NYB,NBDBCK(J),NGDBCK(J)
      	   ENDIF
      	ENDIF
      ENDIF
308   CONTINUE
C
      CALL P2K_PAGE
312   FORMAT('   IH   IK     INT   PROP RAST DIM      PEAK      AVRGE (B
     .CK  BCK RAST NBAD NGOOD         CGX   CGY          XVECT     Y',
     .'VECT')
9312  FORMAT('             (CORR)                  (TOTAL OD)   -RADL CO
     .MP)   DIM'//)
311   FORMAT(2I5,I8,I7,1X,'(',I2,',',I2,')',2X,F10.0,F12.1,
     .	5X,'(',I2,',',I2,')',I5,I6,9X,2F6.2,5X,2F10.2)
9311  FORMAT(2I5,I8,I7,1X,'(',I2,',',I2,')**',F10.0,F12.1,
     .	5X,'(',I2,',',I2,')',I5,I6)
C
C
C     OUTPUT STATISTICS OF CENTRE OF GRAVITY
      WRITE(6,321)
      DO 320 J=1,8
      	IF(NRAD(J).EQ.0) GO TO 318
      	THETA(J)=THETA(J)/NRAD(J)
      	DELRAD(J)=DELRAD(J)/NRAD(J)
318   	ROUTPUT=RDEL*J
      	RCORR = B3*ROUTPUT**3
      	WRITE(6,322) ROUTPUT,DELRAD(J),THETA(J),NRAD(J),RCORR
320   CONTINUE
      IF(NT.EQ.0)GO TO 6324
      RT=RT/NT
      RMST=SQRT(RMST/NT)
6324  WRITE(6,323) RT,NT
      WRITE(6,324) RMST,NT
C
324   FORMAT(' OVERALL RMS DEVIATION=',F6.3,'  FOR TOTAL NUMBER=',I5)
323   FORMAT(' OVERALL AVERAGE DEVTN=',F6.3,'  FOR TOTAL NUMBER=',I5)
321   FORMAT(/' DEVIATION OF CENTRE OF GRAVITY FROM PREDICTED POSITION'/
     .'  RADIUS   RADIAL  ANGULAR  NUMBER OF SPOTS   APPROX RADIAL'/
     .'             COMPONENTS                       CORR APPLIED'/)
322   FORMAT(F8.1,2F9.3,I8,12X,F9.3)
C
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE FRIEDEL_OUT(NPLATE,NDATA,FRACT,ABSOL,INTMAX,
     .	NXM,NYM,NXMT,NYMT,LPRINT,TITLE,
     .	JH,JK,ICORR,IPROP,IOVER,SINT)
C
      DIMENSION TOPT(10),BOTT(10),ITABLE(10,12),ITAV(12)
      DIMENSION JH(1),JK(1),ICORR(1),IPROP(1),IOVER(1),SINT(1)
      DIMENSION TITLE(18)
      LOGICAL LPRINT
      MININT=-250

      IF(LPRINT) WRITE(6,399)
399   FORMAT(////)
      NREJECTOUR=0
      NREJECTO=0
      NREJECTOU=0
      NREJECTU=0
      NREJECTR=0
      NMISS=0
      NDISAGREE=0
      TOP=0.0
      BOT=0.0
      NNEG=0
      NPOS=0
      JSPOTS=0
      JAVSTP=INTMAX/10
      JDISTP=INTMAX/100
      DO 350 I1=1,10
      	TOPT(I1)=0.0
      	BOTT(I1)=0.0
      	DO 350 I2=1,12
   	  ITABLE(I1,I2)=0
350   CONTINUE
      WRITE(6,404) NPLATE,TITLE
404   FORMAT(' Title line on output file :  ',I5,18A4)
      WRITE(2,406) NPLATE,TITLE
406   FORMAT(I5,18A4,'***')
      WRITE(6,351)
351   FORMAT(' FRIEDEL PAIRS NOW AVERAGED'//
     1'   IH   IK       JAV     JDIFF           AV/PNT',
     2'   DIF/PNT       RASTER AREAS')
      WRITE(6,355) FRACT,ABSOL
355   FORMAT(45X,' REJECTED IF JDIFF IS GREATER THAN',F5.2,' *JAV, AND',
     1F10.2)
      DO 9402 J=1,NDATA
      IF(JH(J).EQ.0.AND.JK(J).EQ.0)GO TO 9402
      IMATCH=0
      DO 400 K=J,NDATA
      IF(JH(J).NE.-JH(K)) GO TO 400
      IF(JK(J).NE.-JK(K)) GO TO 400
      IF((SINT(J).EQ.0.).OR.(SINT(K).EQ.0.)) GO TO 360
      JDIFF = ICORR(J)-ICORR(K)
      JAV=(ICORR(J)+ICORR(K))/2
      MDIFF=JDIFF
      IF(JDIFF.LT.0) MDIFF=-JDIFF
      JFRACT=FRACT*JAV
      JABSOL=ABSOL
      PROP=IPROP(J)/1000.
      NXMRAJ=PROP*(NXMT-NXM)+NXM
      NYMRAJ=PROP*(NYMT-NYM)+NYM
      AMEASJ=NXMRAJ*NYMRAJ
      PROP=IPROP(K)/1000.
      NXMRAK=PROP*(NXMT-NXM)+NXM
      NYMRAK=PROP*(NYMT-NYM)+NYM
      AMEASK=NXMRAK*NYMRAK
C
      AVPNT=0.5*(ICORR(J)/AMEASJ+ICORR(K)/AMEASK)
      DFPNT=ICORR(J)/AMEASJ-ICORR(K)/AMEASK
      IF((IOVER(J).EQ.1).OR.(IOVER(K).EQ.1)) GO TO 356
      IF((IOVER(J).EQ.2).OR.(IOVER(K).EQ.2)) GO TO 356
      IF((IOVER(J).EQ.3).OR.(IOVER(K).EQ.3)) GO TO 356
      IF((IOVER(J).EQ.4).OR.(IOVER(K).EQ.4)) GO TO 356
      IF((MDIFF.GT.JFRACT).AND.(MDIFF.GT.JABSOL)) GO TO 352
      GO TO 353
360   IF(LPRINT) WRITE(6,361)JH(J),JK(J),ICORR(J),ICORR(K)
361   FORMAT(2I5,20X,'SPOT MISSING, INTENSITIES WERE  ',2I10)
      NMISS=NMISS+1
      GO TO 9404
356   IF(LPRINT) WRITE(6,357) JH(J),JK(J),JAV,JDIFF,IOVER(J),IOVER(K)
357   FORMAT(2I5,2I10,'  REFLECTION REJECTED, OD too high(1) ',
     .	'or low(2), off edge(3) or bad profile(4) on spot',
     .	' or bckgrd',2I2)
        IF((IOVER(J).NE.0).OR.(IOVER(K).NE.0))NREJECTOUR=NREJECTOUR+1
        IF((IOVER(J).EQ.1).OR.(IOVER(K).EQ.1))NREJECTO=NREJECTO+1
        IF((IOVER(J).EQ.2).OR.(IOVER(K).EQ.2))NREJECTU=NREJECTU+1
        IF((IOVER(J).EQ.3).OR.(IOVER(K).EQ.3))NREJECTR=NREJECTR+1
        IF((IOVER(J).EQ.4).OR.(IOVER(K).EQ.4))NREJECTOU=NREJECTOU+1
      GO TO 9404
C
352   WRITE(6,354) JH(J),JK(J),JAV,JDIFF,AVPNT,DFPNT,
     $NXMRAJ,NYMRAJ,NXMRAK,NYMRAK
354   FORMAT(2I5,2I10,5X,2F10.1,2(7X,I2,',',I2),'   REFLECTION REJ',
     1'ECTED  JDIFF TOO BIG')
      NDISAGREE=NDISAGREE+1
      GO TO 9404
353   JSPOTS=JSPOTS+1
      IF(LPRINT) WRITE(6,401) JH(J),JK(J),JAV,JDIFF,AVPNT,DFPNT,
     $NXMRAJ,NYMRAJ,NXMRAK,NYMRAK
401   FORMAT(2I5,2I10,5X,2F10.1,2(7X,I2,',',I2))
      IF(JAV.LE.0) NNEG=NNEG+1
      IF(JAV.GT.0) NPOS=NPOS+1
      IF(JAV.EQ.0) JAV=1
      IF(JAV.LT.MININT) JAV=MININT
      JSAV=JAV
      IF(JSAV.LE.0)JSAV=1
      TOP=TOP+MDIFF
      BOT=BOT+JSAV
      IAVSLT=(JSAV/JAVSTP)+2
      IF(IAVSLT.EQ.2) IAVSLT=((JSAV*2)/JAVSTP)+1
      IDISLT=(MDIFF/JDISTP)+1
      IF(IAVSLT.GT.10) IAVSLT=10
      IF(IDISLT.GT.12) IDISLT=12
      IF(IAVSLT.LT.1) IAVSLT=1
      IF(IDISLT.LT.1)IDISLT=1
      ITABLE(IAVSLT,IDISLT)=ITABLE(IAVSLT,IDISLT)+1
      TOPT(IAVSLT)=TOPT(IAVSLT)+MDIFF
      BOTT(IAVSLT)=BOTT(IAVSLT)+JSAV
      WRITE(2,405) JH(J),JK(J),JAV,JDIFF
405   FORMAT(2I5,2I6)
C
C SET INDICES TO ZERO AFTER REFLECTIONS DEALT WITH
9404  JH(K)=0
      JK(K)=0
      IMATCH=1
      GO TO 9403
400   CONTINUE
9403  IF(IMATCH.EQ.0)GO TO 9402
      JH(J)=0
      JK(J)=0
9402  CONTINUE
      JHTERM=100
      WRITE(2,405) JHTERM,JHTERM,JHTERM,JHTERM
      IF(LPRINT) WRITE(6,401) JHTERM,JHTERM,JHTERM
C
C    SEARCH FOR ANY UNPAIRED REFLECTIONS
      NOPAIR=0
      WRITE(6,9351)
9351  FORMAT(//' REFLECTIONS WITHOUT FRIEDEL PAIRS'//
     1'   IH   IK     ICORR'//)
      DO 9400 J=1, NDATA
      IF (JH(J).EQ.0.AND.JK(J).EQ.0)GO TO 9400
      NOPAIR=NOPAIR+1
      WRITE(6,9401)JH(J),JK(J),ICORR(J)
9401  FORMAT(2I5,I10)
9400  CONTINUE
C
C     OUTPUT OF STATISTICAL TABLES AND R-FACTORS
C
      WRITE(6,506)
506   FORMAT(///' OUTPUT OF STATISTICS AND R-FACTORS'///)
      WRITE(6,500)JDISTP
500   FORMAT(29X,'JDIFF IN GROUPS OF',I6/)
      DO 520 K=1,12
520   ITAV(K)=K*JDISTP
      WRITE(6,501) (ITAV(K),K=1,12)
501   FORMAT(24X,'JAV  ',12I5,'       ALL    RSYM')
      WRITE(6,502)
502   FORMAT(82X,'AND ABOVE'/)
      ITOTAL=0
      DO 550 I1=1,10
      IALL=0
      IF(I1.GT.2) GO TO 522
      IAVB=((I1-1)*JAVSTP)/2
      IAVF=(I1*JAVSTP)/2
      GO TO 523
522   IAVB=(I1-2)*JAVSTP
      IAVF=(I1-1)*JAVSTP
523   CONTINUE
      DO 540 I2=1,12
540   IALL=IALL+ITABLE(I1,I2)
      IF(IALL.EQ.0) GO TO 550
      SYM=TOPT(I1)/BOTT(I1)
      WRITE(6,503)IAVB,IAVF,(ITABLE(I1,K),K=1,12),IALL,SYM
503   FORMAT(5X,I5,5X,'TO',5X,I5,2X,12I5,I10,F9.3)
      ITOTAL=ITOTAL+IALL
550   CONTINUE
      WRITE(6,504)
504   FORMAT(18X,'AND ABOVE')
      DO 560 I2=1,12
      DO 560 I1=2,10
560   ITABLE(1,I2)=ITABLE(1,I2) + ITABLE(I1,I2)
      WRITE(6,505)(ITABLE(1,K),K=1,12),ITOTAL
505   FORMAT(/24X,'ALL  ',12I5,I10)
      WRITE(6,507) NPOS,NNEG
507   FORMAT(//' TOTAL POSITIVE SPOTS=',I5/' TOTAL NEGATIVE SPOTS=',I5)
      WRITE(6,399)
      RSYM=TOP/BOT
      WRITE(6,402) RSYM,JSPOTS
      IF(NREJECTOUR.NE.0) WRITE(6,4407) NREJECTOUR
      IF(NREJECTO.NE.0) WRITE(6,4401) NREJECTO
      IF(NREJECTOU.NE.0) WRITE(6,4410) NREJECTOU
      IF(NREJECTU.NE.0) WRITE(6,4406) NREJECTU
      IF(NREJECTR.NE.0) WRITE(6,4402) NREJECTR
      IF(NDISAGREE.NE.0) WRITE(6,4403) NDISAGREE
      IF(NOPAIR.NE.0) WRITE(6,4404) NOPAIR
      IF(NMISS.NE.0) WRITE(6,4405) NMISS
      IF(NMISS+NOPAIR+NDISAGREE+NREJECTO+NREJECTU+NREJECTR.EQ.0) THEN
      	WRITE(6,4408)
      ELSE
      	WRITE(6,4409)
      ENDIF
402   FORMAT('  OVERALL MEAN DIFFERENCE BET SYM REL SPOTS =  ',F7.3/
     .'  FOR  ',I5,'  SPOTS, with the following not written out')
4401  FORMAT(25X,I5,'  pairs rejected for overloads')
4402  FORMAT(25X,I5,'  pairs rejected beyond radius limits')
4403  FORMAT(20X,I5,'  pairs rejected for disagreement (FRACT,ABSOL)')
4404  FORMAT(20X,I5,'  individual spots with no Friedel pair')
4405  FORMAT(20X,I5,'  unexplained missing pairs')
4406  FORMAT(25X,I5,'  pairs rejected for underloads(pointer?)')
4407  FORMAT(20X,I5,'  pairs rejected for all reasons (O,U,R)')
4408  FORMAT(25X,'  no spots were rejected for any reason'/)
4409  FORMAT(/)
4410  FORMAT(25X,I5,'  pairs rejected : too many pixels in',
     .	 ' profile-fitted raster were under/overloaded')
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE GETBACKGRPOSN(NTYPE,DX1,DY1,DX2,DY2,
     .	  XBA,YBA,XBB,YBB,XBC,YBC)
C left over subroutine_from old version of pickauto,pickycor
      IF(NTYPE.EQ.0) THEN
C     	NTYPE=0
      	XBA=(DX1+DX2)/3
      	YBA=(DY1+DY2)/3
      	XBB=(2*DX1-DX2)/3
      	YBB=(2*DY1-DY2)/3
      	XBC=(DX1-2*DX2)/3
      	YBC=(DY1-2*DY2)/3
      ENDIF
      IF(NTYPE.NE.1) THEN
C     	NTYPE=1
      	XBA=(2*DX1+DX2)/3
      	YBA=(2*DY1+DY2)/3
      	XBB=(DX1+2*DX2)/3
      	YBB=(DY1+2*DY2)/3
      	XBC=(DX1-DX2)/3
      	YBC=(DY1-DY2)/3
      ENDIF
      IF(NTYPE.EQ.2) THEN
C     	NTYPE=2; 4 BACKGROUNDS
      	XBA=(DX1+DX2)/2
      	YBA=(DY1+DY2)/2
      	XBB=(DX1-DX2)/2
      	YBB=(DY1-DY2)/2
      	XBC=0.
      	YBC=0.
      ENDIF
      RETURN
      END
C
C******************************************************************************
C
      SUBROUTINE GETRAST(JSTORE,J,ARRAY,NX,NXST,NYST,NXB,NYB)
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      DIMENSION ARRAY(1),IOVER(1)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST)
      DO 50 IY=1,NYB
      	DO 55 IX=1,NXB
      	INDEX=((NYST+IY-1)-1)*NX+(NXST+IX-1)
      	JSTORE(J,IX,IY)=ARRAY(INDEX)
55    CONTINUE
CC      WRITE(6,52) (JSTORE(J,IX,IY),IX=1,NXB)
52    FORMAT(20I4)
50    CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE INTEGRATE(JSTORE,J,IOVERL,IUNDER,
     .	 NXB,NYB,NXMRA,NYMRA,NXST,NYST,X0,Y0,
     .	 LOOKUP,ODBACK,VARTAB,STNDEV,ODBTOT,YCURVE,
     .	 SINT,VAR,BACK,IOVER,NBDBCK,NGDBCK,IFAIL)
C
C  Calculates three things - SINT,BACK and ODBTOT
C  SINT - integration of densities within peak raster, corrected for OD lookup.
C  BACK - integration of densities within surrounding background raster,
C       - corrected for radial- and y-correction averages.
C  Calculates ODBTOT, the total radial- and y-correction within the peak.
C
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      DIMENSION ODBACK(1),YCURVE(1),STNDEV(1),VAR(1)
      DIMENSION SINT(1),BACK(1),IOVER(1),NBDBCK(1),NGDBCK(1)
      REAL*4 LOOKUP(1),VARTAB(1)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST)
C
      IFAIL=0
      ODBTOT=0.0
      VARBACK=0.0
      NXB_M=(NXB-NXMRA)/2
      NYB_M=(NYB-NYMRA)/2
      DO 100 IX=1,NXB
      	XCOORD = NXST+IX-1-X0
      	XCOSQ = XCOORD**2
      	DO 100 IY=1,NYB
      	 IYCURV=IY-1+NYST
      	 YCOORD=NYST+IY-1-Y0
      	 RAD=1.+SQRT(XCOSQ+YCOORD**2)
      	 IRAD=RAD
      	 DRAD=RAD-IRAD
      	 ODB=ODBACK(IRAD)*(1-DRAD)+ODBACK(IRAD+1)*DRAD+YCURVE(IYCURV)
C			interpolate radial background, lookup ycurve
      	 JLOOKRAD=ODB
      	 IF(JLOOKRAD.GT.IOVERL) IOVER(J)=1
      	 IF(JLOOKRAD.LE.IUNDER) IOVER(J)=2
      	 IF(JLOOKRAD.LE.IUNDER) WRITE(6,49) J,IX,IY,JLOOKRAD
      	 IF(JLOOKRAD.LE.IUNDER) IFAIL=1
      	 IF(JLOOKRAD.LE.0) THEN ! should never be used - LPROF .false.
      	  JLOOKRAD=1
      	  ODB=1.0
      	 ENDIF
      	 DJLOOK=ODB-JLOOKRAD
      	   JLOOK=JSTORE(J,IX,IY)
      	   IF(JLOOK.GT.IOVERL) IOVER(J)=1
      	   IF(JLOOK.LE.IUNDER) IOVER(J)=2
C      	   IF(JLOOK.LE.IUNDER) WRITE(6,48) J,IX,IY,JLOOK
48	FORMAT(' Digitised density LE.IUNDER for J,IX,IY,JLOOK',4I5)
49	FORMAT(' Radial density LE.IUNDER for J,IX,IY,JLOOKRAD',4I5)
      	   IF(JLOOK.LE.0) JLOOK=1  ! underloaded, so not used
C
      	IF(IX.GT.NXB_M.AND.IX.LE.NXMRA+NXB_M.AND.
     .	   IY.GT.NYB_M.AND.IY.LE.NYMRA+NYB_M) THEN ! part of peak
      	 SINT(J) = SINT(J) + LOOKUP(JLOOK)
      	 VAR(J)  = VAR(J) + VARTAB(JLOOK)
      	 ODBTOT  = ODBTOT + LOOKUP(JLOOKRAD)*(1-DJLOOK) +
     .	   LOOKUP(JLOOKRAD+1)*DJLOOK
C		  interpolate lookup table for backgrounds
      	ELSE     ! part of background
		DEV = JLOOK - ODB
		IF(ABS(DEV).GT.3.0*STNDEV(IRAD)) THEN
C			   replace with value from radial density curve
C			   i.e. BACK(J) = BACK(J) + 0
			NBDBCK(J)=NBDBCK(J)+1
		ELSE
      	 BACK(J) = BACK(J) + LOOKUP(JLOOK) -
     .	  LOOKUP(JLOOKRAD)*(1-DJLOOK)-LOOKUP(JLOOKRAD+1)*DJLOOK
			NGDBCK(J)=NGDBCK(J)+1
      	  VARBACK=VARBACK+VARTAB(JLOOK)
		ENDIF
      	ENDIF
100   CONTINUE
      VAR(J) = VAR(J) +
     .	VARBACK*FLOAT((NXMRA*NYMRA)**2)/FLOAT((NXB*NYB-NXMRA*NYMRA)**2)
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE INTERLACE(NX,NY,ARRAY,DMEAN,IEDGE)
C May be specific for 2048x2048 square arrays from Joyce-Loebl densitometer
      PARAMETER  (MAXSIZ=2048)
      DIMENSION ARRAY(1)
      DIMENSION AEVEN(MAXSIZ+2,MAXSIZ/2),AODD(MAXSIZ+2,MAXSIZ/2)
      DATA IFOR/0/,IBAK/1/
      PIBY2=3.1415926/2.0
C
      MAXX=MIN0(NX,MAXSIZ)
      MAXY=MIN0(NY/2,MAXSIZ/2)
      DO 200 L=1,MAXY
      DO 150 J=1,MAXX
      INDEXODD  = J+NX*(2*L-2)  ! first,third line etc
      INDEXEVEN = J+NX*(2*L-1)  ! second,fourth line etc
      AODD(J,L)  = ARRAY(INDEXODD)-DMEAN
150   AEVEN(J,L) = ARRAY(INDEXEVEN)-DMEAN
      AEVEN(MAXX+1,L)=0.
      AEVEN(MAXX+2,L)=0.
      AODD(MAXX+1,L)=0.
200   AODD(MAXX+2,L)=0.
C
      CALL TODFFT(AEVEN,MAXX,MAXY,IFOR)
      CALL TODFFT(AODD,MAXX,MAXY,IFOR)
C  blank out the low resolution part of transform with a cosine bell, as in
C  stand-alone program REMORIG.
      IXZERO=120
      IYZERO=60
      	DO 220 IX= 1,2*IXZERO,2
      	 JX=(IX-1)/2
      	 XFAC=FLOAT(JX)/FLOAT(IXZERO)
      	DO 220 JY= -IYZERO,IYZERO
      	 IF(JY.GE.0)    IY=JY+1
      	    IF(JY.LT.0) IY=JY+1+MAXY
      	 IF(JX**2*IYZERO**2+JY**2*IXZERO**2.LT.IXZERO**2*IYZERO**2) THEN
      	  YFAC=FLOAT(JY)/FLOAT(IYZERO)
      	  CBELL=1.0-(COS(PIBY2*(XFAC**2+YFAC**2)))**2
          AEVEN(IX,IY)  =AEVEN(IX,IY)  * CBELL
      	  AODD(IX,IY)   =AODD(IX,IY)   * CBELL
      	  AEVEN(IX+1,IY)=AEVEN(IX+1,IY)* CBELL
      	  AODD(IX+1,IY) =AODD(IX+1,IY) * CBELL
      	 ENDIF
220   	CONTINUE
      CALL TWOFILEC(AEVEN,AODD,MAXX,MAXY)
      CALL TODFFT(AEVEN,MAXX,MAXY,IBAK)
C
      XCEN=0.
      YCEN=0.
      IXMID=1.0+MAXX/2 ! middle of cross-correlation map should be here.
      IYMID=1.0+MAXY/2 ! after (1/2,1/2) origin shift in twofile.
      	WRITE(6,281)
281   	FORMAT('  Area near cross-correlation peak')
	DO 285 IY=-5,5
285   	WRITE(6,282) (AEVEN(IXMID+IX,IYMID+IY),IX=-4,4)
282   	FORMAT(14F9.0)
      	WRITE(6,283)
283   	FORMAT(40X,'   X        Y')
      DO 300 I=1,5
      	SUM=0.
      	XSUM=0.
      	YSUM=0.
      	IXMIDR=NINT(IXMID+XCEN)
      	IYMIDR=NINT(IYMID+YCEN)
      	DMIN=1.0E15
      	DO 280 IY=-3,3
      	DO 280 IX=-6,6
      	 D = AEVEN(IXMIDR+IX,IYMIDR+IY)
      	 DMIN=AMIN1(DMIN,D)
280   	CONTINUE
      	DO 290 IY=-3,3
      	DO 290 IX=-6,6
      	  D = AEVEN(IXMIDR+IX,IYMIDR+IY) ! - DMIN !speed up convergence ??
      	  SUM  = SUM+D
      	  XSUM = XSUM+D*IX
290   	  YSUM = YSUM+D*IY
      	XCEN=XSUM/SUM + IXMIDR - IXMID
      	YCEN=YSUM/SUM + IYMIDR - IYMID
      	WRITE(6,301) XCEN,YCEN
300   CONTINUE
301   FORMAT(' Offset between odd and even scan lines',2F8.2,' pixels')
      	XCEN=+XCEN ! need odd relative to even
      	YCEN=+YCEN
C
C Apply only nearest integer X-offsets to data in ARRAY.
C
      IXOFFE = NINT(XCEN)/2
      IXOFFO = NINT(XCEN)-IXOFFE
      IEDGE  = MAX0(IABS(IXOFFE),IABS(IXOFFO))
      WRITE(6,305) IXOFFE,IXOFFO,IEDGE
305   FORMAT(' Offsets used to correct data, IXOFFE,IXOFFO,IEDGE',3I4)
      IF(IXOFFE.EQ.0.AND.IXOFFO.EQ.0)  RETURN
      DO 370 IY=1,NY-1,2
       DO 365 IX=1+IEDGE,NX-IEDGE
      	IXN = NX-IX+1
      	IF(XCEN.GT.0.0) THEN
      	 INDEXNEWODDY  = (IY-1)*NX + IX
      	 INDEXNEWEVENY =  IY   *NX + IXN
      	ELSE
      	 INDEXNEWODDY  = (IY-1)*NX + IXN
      	 INDEXNEWEVENY =  IY   *NX + IX
      	ENDIF
      	INDEXOLDODDY  = INDEXNEWODDY  + IXOFFO
      	INDEXOLDEVENY = INDEXNEWEVENY - IXOFFE
      	ARRAY(INDEXNEWODDY)  = ARRAY(INDEXOLDODDY)
      	ARRAY(INDEXNEWEVENY) = ARRAY(INDEXOLDEVENY)
365    CONTINUE
370   CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE TWOFILEC(AEVEN,AODD,MAXX,MAXY)
C
      PARAMETER (MAXSIZ=2048)
      DIMENSION AEVEN(MAXSIZ+2,MAXSIZ/2),AODD(MAXSIZ+2,MAXSIZ/2)
C
C  This version of twofile for cross-correlation, i.e. multiply the first
C    transform by the complex conjugate of the second.
C  Multiply complex AEVEN by complex conjugate of AODD
C
      WRITE(6,1000)
1000  FORMAT(/' ENTERING SUBROUTINE_TWOFILEC from INTERLACE')
C
      	TWOPI=2.0*3.1415926
      	DELPX = -TWOPI * 0.5
      	DELPY = -TWOPI * 0.5
      DO 1100 I=1,MAXX+1,2
      DO 1100 J=1,MAXY
      	 PSHIFT = ((I-1)/2)*DELPX + (J-1)*DELPY
      	 A1=AEVEN(I,J)
      	 B1=AEVEN(I+1,J)
      	 C =COS(PSHIFT)
      	 S =SIN(PSHIFT)
      	 A2=AODD(I,J) * C - AODD(I+1,J) * S
      	 B2=AODD(I,J) * S + AODD(I+1,J) * C
      	 AEVEN(I,J)   = A1*A2 + B1*B2
      	 AEVEN(I+1,J) = A1*B2 - B1*A2
1100  CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE PROFGET(NDATA,JSTORE,NXB,NYB,XCOORD,YCOORD,X0,Y0,
     .	 IOVERL,IUNDER,LOOKUP,ODBACK,YCURVE,JPROF,LPROF)
C
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      DIMENSION XCOORD(1),YCOORD(1),IOVER(1)
      DIMENSION PROFILE(IRAST,IRAST)
      REAL*4 LOOKUP(1),ODBACK(1),YCURVE(1)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST),JPROF(NMAX,IRAST,IRAST)
      LOGICAL LPROF(NMAX,IRAST,IRAST)
C
      WRITE(6,290)
290   FORMAT(/' Entering PROFGET    - gets interpolated raster',
     .	' around each spot')
      	DO 300 J=1,NMAX
      	DO 300 K=1,IRAST
      	DO 300 L=1,IRAST
300   	LPROF(J,K,L)=.TRUE.
      	XMHALF=(NXB-1.0)/2.0
      	YMHALF=(NYB-1.0)/2.0
      DO 500 J=1,NDATA
      	NXST=XCOORD(J)-XMHALF+0.5
      	NYST=YCOORD(J)-YMHALF+0.5
      	DO 400 IX=1,NXB
      	 XCOSQ=(NXST+IX-1-X0)**2
      	DO 400 IY=1,NYB
      	 IYCURV=IY-1+NYST
      	 RAD=1.+SQRT(XCOSQ+(NYST+IY-1-Y0)**2)
      	 IRAD=RAD
      	 DRAD=RAD-IRAD
      	 ODB=ODBACK(IRAD)*(1-DRAD)+ODBACK(IRAD+1)*DRAD+YCURVE(IYCURV)
C			interpolate radial background, lookup ycurve
      	 JLOOKRAD=ODB
      	 JLOOK=JSTORE(J,IX,IY)
      	 IF(JLOOK.GT.IOVERL.OR.JLOOKRAD.GT.IOVERL) LPROF(J,IX,IY)=.FALSE.
      	 IF(JLOOK.LE.IUNDER.OR.JLOOKRAD.LE.IUNDER) LPROF(J,IX,IY)=.FALSE.
      	 IF(JLOOK.LE.0) JLOOK=1    ! should never be used - LPROF .false.
      	    IF(JLOOKRAD.LE.0) THEN  ! should never be used - LPROF .false.
      	  JLOOKRAD=1
      	  ODB=1.0
      	    ENDIF
      	    DJLOOK=ODB-JLOOKRAD
      	 PROFILE(IX,IY) = LOOKUP(JLOOK) -
     .	  LOOKUP(JLOOKRAD) * (1.-DJLOOK) -
     .	  LOOKUP(JLOOKRAD+1) * DJLOOK
400   	CONTINUE
C
C Now interpolate linearly on to exact crystal lattice
C
      	DX=XCOORD(J)-NXST-XMHALF
      	DY=YCOORD(J)-NYST-YMHALF
      	IF(DX.GE.0.) THEN
      	 IDX=1
      	ELSE
      	 IDX=-1
      	 DX=ABS(DX)
      	ENDIF
      	IF(DY.GE.0.) THEN
      	 IDY=1
      	ELSE
      	 IDY=-1
      	 DY=ABS(DY)
      	ENDIF
      	DX1 = 1.-DX
      	DY1 = 1.-DY
      	DO 450 IX=2,NXB-1
      	DO 450 IY=2,NYB-1
      	   JPROF(J,IX,IY) = PROFILE(IX,IY) *DX1*DY1 +
     .	   PROFILE(IX+IDX,IY) *DX*DY1 +
     .	   PROFILE(IX+IDX,IY+IDY) *DX*DY +
     .	   PROFILE(IX,IY+IDY) *DX1*DY
450   	CONTINUE
500   CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE PROFAVER(LPTYPE,LPRANGE,NSPOT,NDATA,JPROF,LPROF,
     .	  APROF,IOVER,ICORR,VAR,XCOORD,YCOORD,X0,Y0,
     .	  JH,JK,TILTAXIS,RHALF,NXB,NYB,LPRINT,
     .	  IFLATTEN,PROFMIN,NAVPROF)
C
      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      DIMENSION XCOORD(1),YCOORD(1),IOVER(1),ICORR(1),VAR(1)
      DIMENSION JH(1),JK(1),APROF(5,IRAST,IRAST)
C*** jms	08.07.2010
c      DIMENSION NSPOTPROF(5),SYMBOL(40),PLOT(IRAST)
      DIMENSION NSPOTPROF(5)
      character SYMBOL(40)
      character PLOT(IRAST)
      INTEGER*2 JPROF(NMAX,IRAST,IRAST)
      LOGICAL LPRINT,KPRINT,IFLATTEN,LPROF(NMAX,IRAST,IRAST)
      DATA SYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
     .	'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
     .	'Q','R','S','T','U','V','W','X','Y','Z','+','*','#','-'/
C***
c      INTEGER*4 TMPPLOT(IRAST)
c        EQUIVALENCE (TMPPLOT,PLOT)
c      INTEGER*4 TMPSYMBOL(40)
c        EQUIVALENCE (TMPSYMBOL,SYMBOL)
c      DATA TMPSYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
c     .	'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
c     .	'Q','R','S','T','U','V','W','X','Y','Z','+','*','#','-'/

      DATA PLOT/IRAST*'0'/
CTSH++
c      DATA TMPPLOT/IRAST*'0'/
CTSH--
C
      IF(LPTYPE.LE.1) WRITE(6,6)
6     FORMAT(/' Entering PROFAVER   - calculates profiles')
      IF(LPRINT.OR.(LPTYPE.EQ.2.AND.NSPOT.EQ.1)) WRITE(6,5) LPRANGE
5     FORMAT(/' Entering PROFAVER   - calculates profile for each spot',
     .	' within radius LPRANGE=',I6)
      	DO 10 J=1,5
      	 NSPOTPROF(J)=0
      	DO 10 K=1,IRAST
      	DO 10 L=1,IRAST
10    	APROF(J,K,L)=0.0
      IF(LPTYPE.EQ.0) THEN
      	DO 100 J=1,NDATA
      	 IF(IOVER(J).EQ.0.AND.ICORR(J).GT.0) THEN
      	  NSPOTPROF(1)=NSPOTPROF(1)+1
      	  DO 150 IX=2,NXB-1
      	  DO 150 IY=2,NYB-1
      	   IF(VAR(J).LE.0.0) STOP ' Negative variance not allowed at 150'
150       APROF(1,IX,IY)=APROF(1,IX,IY)+JPROF(J,IX,IY)*ICORR(J)/VAR(J)
      	 ENDIF
100   	CONTINUE
      	NP=1
      ENDIF
C
      IF(LPTYPE.EQ.1) THEN
C Put in divide into 5 zones calculation
      	DO 200 J=1,NDATA
      	 IF(IOVER(J).EQ.0.AND.ICORR(J).GT.0) THEN
      	  CALL GETZONE(IZ,J,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF)
      	  NSPOTPROF(IZ)=NSPOTPROF(IZ)+1
      	 DO 250 IX=2,NXB-1
      	 DO 250 IY=2,NYB-1
      	  IF(VAR(J).LE.0.0) THEN
      	  WRITE(6,*) J,JH(J),JK(J),IX,IY,ICORR(J),IOVER(J),VAR(J)
      	  STOP ' Negative variance not allowed at 250'
      	  ENDIF
250      APROF(IZ,IX,IY)=APROF(IZ,IX,IY)+JPROF(J,IX,IY)*ICORR(J)/VAR(J)
      	 ENDIF
200   	CONTINUE
      	NP=5
      ENDIF
C
      IF(LPTYPE.EQ.2) THEN
C Include all spots within a radius of LPRANGE
      	DO 300 J=1,NDATA
C calculate distance and test for LPRANGE
      	 IF(J.NE.NSPOT) THEN
      	    LDIST = SQRT((XCOORD(J)-XCOORD(NSPOT))**2 +
     .	   (YCOORD(J)-YCOORD(NSPOT))**2)
      	    IF(IOVER(J).EQ.0.AND.LDIST.LT.LPRANGE.AND.ICORR(J).GT.0)THEN
      	    NSPOTPROF(1)=NSPOTPROF(1)+1
      	    DO 350 IX=2,NXB-1
      	    DO 350 IY=2,NYB-1
      	     IF(VAR(J).LE.0.0) STOP ' Negative variance not allowed, 350'
350         APROF(1,IX,IY)=APROF(1,IX,IY) +
     .	  JPROF(J,IX,IY)*ICORR(J)/VAR(J)
      	    ENDIF
      	 ENDIF
300   	CONTINUE
      	NP=1
      ENDIF
C
C Now calculate perimeter OD, maximum OD and integrated profile OD, then
C  adjust zero-level and print out profiles if LPRINT is .TRUE., and finally
C  normalise so that the sum of each profile = 1.0, above a zero background.
C
      DO 400 N=1,NP
      	NAVPROF=NAVPROF+NSPOTPROF(N) ! number of rasters in profile
      	DPERIM=0.0
      	DMAX=-10000.
      	DO 405 IY=2,NYB-1
405   	 DPERIM=DPERIM + APROF(N,2,IY) + APROF(N,NXB-1,IY)
      	DO 406 IX=3,NXB-2
406   	 DPERIM=DPERIM + APROF(N,IX,2) + APROF(N,IX,NYB-1)
      	DPERIM=DPERIM/(2.*FLOAT(NXB+NYB)-12.) ! get perimeter density
      	 DO 408 IX=2,NXB-1  ! correct for it
      	   DO 408 IY=2,NYB-1
408   	 APROF(N,IX,IY)=APROF(N,IX,IY)-DPERIM
      	TOTALINT=0.0
      	DO 410 IY=2,NYB-1  ! get maximum profile density
      	DO 410 IX=2,NXB-1
410   	DMAX=AMAX1(DMAX,APROF(N,IX,IY))
C    and flatten profile at this stage before normalisation if required
      	APROFMIN=PROFMIN*DMAX  ! before normalisation
      	 IF(LPTYPE.LE.1) KPRINT=.TRUE.
      	 IF(LPTYPE.EQ.2) KPRINT=LPRINT
        IF(IFLATTEN) CALL PROFFLATTEN(N,JH(NSPOT),JK(NSPOT),NXB,NYB,
     .	 APROF,APROFMIN,NSPOT,KPRINT)
C
      	DO 411 IY=2,NYB-1
      	DO 411 IX=2,NXB-1
411     TOTALINT=TOTALINT+APROF(N,IX,IY)
      	TOTALINTRAW=TOTALINT+DPERIM*(NXB-2)*(NYB-2)
      	IF(TOTALINT.LE.0.0) THEN
      	 WRITE(6,412) TOTALINT,DPERIM,DMAX,TOTALINTRAW
412		FORMAT(' ERROR - negative value for TOTALINT ',
     .	  '(c.f.DPERIM,DMAX,TOTALINTRAW)=',4F12.2/
     .	  ' perhaps the edge of the chosen',
     .	  ' profile overlaps adjacent spots')
         STOP
      	ENDIF
C
      	IF(LPRINT.OR.LPTYPE.LE.1) THEN
      	 IF(LPTYPE.EQ.2) THEN
      	  WRITE(6,401) N,NSPOT,JH(NSPOT),JK(NSPOT),NSPOTPROF(N)
401   	  FORMAT(/' Profile',I2,'  spot,index',I5,',',2I4,
     .	  '  includes',I4,' rasters')
      	 ELSE
      	  WRITE(6,402) N,NSPOTPROF(N)
402   	  FORMAT(/' Profile',I3,' includes',I5,' rasters')
      	 ENDIF
      	 IF(.NOT.IFLATTEN)WRITE(6,*)'     This profile was not flattened'
      	 WRITE(6,407) DPERIM,DMAX,DPERIM/DMAX
407   	 FORMAT(' Mean perimeter and maximum OD, ',2F10.1/
     .	  '  background as a fraction of maximum ',F10.4)
      	 DO 420 IY=1,NYB
      	 DO 430 IX=1,NXB
      	  A=APROF(N,IX,IY)
      	  IDATA=1. + 35.*A/DMAX
      	  IF(A.LT.0.0) IDATA=40
      	  PLOT(IX) = SYMBOL(IDATA)
430   	 CONTINUE
      	  WRITE(6,413) (PLOT(IX),IX=1,NXB)
413   	  FORMAT(1X,120A1)
420   	 CONTINUE
      	ENDIF
C
C
C  This statement used for debugging the profile fitting part of the program
C      	IF(JH(NSPOT).EQ.-9.AND.JK(NSPOT).EQ.0)
C     .	CALL DUMP(NSPOT,NXB,NYB,JH(NSPOT),JK(NSPOT),TOTALINT,DPERIM,
C     .	NAVPROF,NSPOTPROF,APROF)
C
C
C  Now normalise profile whether flattened previously or not
C
      	DO 440 IX=2,NXB-1
      	    DO 440 IY=2,NYB-1  ! Normalise to 1.0
440   	APROF(N,IX,IY)=APROF(N,IX,IY)/TOTALINT
C
400   CONTINUE
      NAVPROF=NAVPROF/NP ! number of rasters in profile
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE PROFMEAS(LPTYPE,NDATA,NXB,NYB,NXM,NYM,
     .	JSTORE,JPROF,LPROF,APROF,IPROF,IOVERP,
     .	VARP,VARTAB,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF,
     .	ARRAY,NX,NY,LPRINT,IFLATTEN,TSIGMA,GOODFIT,GOODFITP,GOODFITB,
     .	JH,JK,TPROF,ROUT,PRPMAX,GPBACK,SPVSTORE,SP2VSTORE)
C

      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      REAL*4 TPROF(5,IRAST,IRAST)
      DIMENSION JH(1),JK(1),GPBACK(1)
      DIMENSION XCOORD(1),YCOORD(1),IPROF(1),VARP(1),VARTAB(1)
      DIMENSION IOVERP(1),ARRAY(1),APROF(5,IRAST,IRAST)
      DIMENSION SPVSTORE(1),SP2VSTORE(1)
      INTEGER*2 JSTORE(NMAX,IRAST,IRAST),JPROF(NMAX,IRAST,IRAST)
      LOGICAL LPRINT,IFLATTEN,LPROF(NMAX,IRAST,IRAST)
C
C first determine correct profile for each measurement, one at a time
C  for LPTYPE.eq.2, all at once for others.
C
      IF(LPTYPE.NE.2.OR.(LPTYPE.EQ.2.AND.NDATA.EQ.1).OR.LPRINT)
     .	      WRITE(6,1)
1     FORMAT(/' Entering PROFMEAS : measures intensities by',
     .	' profile fitting (poorly fitting profiles follow)'/)
      TSQ=TSIGMA**2
      NSTART=1
      XMHALF=(NXB-1.0)/2.0
      YMHALF=(NYB-1.0)/2.0
      IF(LPTYPE.EQ.2) NSTART=NDATA
      DO 800 J=NSTART,NDATA
      	IPROF(J)=0.0
      	VARP(J)=0.0
      	BACK=0.0
      	NP=1
      	IF(LPTYPE.EQ.1)
     .	 CALL GETZONE(NP,J,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF)
      	IF(LPTYPE.EQ.3)
     .	 CALL PROFCONV(J,APROF,TPROF,NXB,NYB,XCOORD,YCOORD,
     .	   X0,Y0,TILTAXIS,ROUT,PRPMAX,LPRINT,JH,JK)
C Do two cycles, exclude points more than TSIGMA from background of fitted
C  profile on 2nd pass.
C
      	IF(IFLATTEN) THEN
      	 NPASSES=2 ! to perform sigma checks on profile fit.
      	ELSE
      	 NPASSES=1 ! no sigma checks performed.
      	ENDIF
       DO 760 NPASS=1,NPASSES
      	SIPV = 0.0
      	SPV  = 0.0
      	SV   = 0.0
      	SIV  = 0.0
      	SP2V = 0.0
      	NGOOD=0
      	NBAD =0
      	NGDSIG=0
      	NBDSIG=0
      	SDEVTNP=0.0
      	SSIGMAP=0.0
      	SDEVTNB=0.0
      	SSIGMAB=0.0
      	DO 750 IX=2,NXB-1
C      IF(J.EQ.508) write(6,752)(JPROF(J,IX,IY),IY=2,NYB-1)
C752	FORMAT(30I5)
      	DO 750 IY=2,NYB-1
      	    IDENS=JPROF(J,IX,IY)
      	 JLOOK=JSTORE(J,IX,IY) ! variance only needed approx.
      	 IF(JLOOK.LE.0) JLOOK=1
      	    V = VARTAB(JLOOK)  !
      	    IF(V.EQ.0.0) THEN
      	 WRITE(6,751) J,IX,IY,JLOOK,V
751		FORMAT(' SPOT,IX,IY,DENSITY,VARIANCE =',4I5,F10.2)
      	 STOP ' cannot proceed - variance zero'
      	    ENDIF
      	    P = APROF(NP,IX,IY)
      	    IF(P.NE.0.0) THEN
      	 SSIGMAP = SSIGMAP + SQRT(V)
      	    ELSE
      	 SSIGMAB = SSIGMAB + SQRT(V)
      	    ENDIF
      	    IF(LPROF(J,IX,IY)) THEN
      	 NGOOD=NGOOD+1
      	 IF(NPASS.EQ.1.OR.P.NE.0.0.OR.
     .	   ((IDENS-BACK)**2.LT.TSQ*V.AND.P.EQ.0.0)) THEN ! 2nd pass.
      	  SIPV = SIPV + FLOAT(IDENS)*P/V
      	  SPV  = SPV + P/V
      	  SV   = SV + 1.0/V
      	  SIV  = SIV + FLOAT(IDENS)/V
      	  SP2V = SP2V + P**2/V
      	  NGDSIG=NGDSIG+1
      	 ELSE
      	  NBDSIG=NBDSIG+1
      	 ENDIF
      	    ELSE
      	 NBAD=NBAD+1
      	    ENDIF
750   	CONTINUE
C      	IF(NBAD.GT.NXM*NYM/20) THEN	! use only if less than 5% bad
      	IF(NBAD.GT.0) THEN  ! reject if any bad ones at all
      	 IOVERP(J)=4
      	 KPEAK=0
      	 BACK=0.0
      	 VP=0.0
      	ELSE
      	 IF(SV.LE.0.0) THEN
			WRITE(6,*) J,SIPV,SPV,SV,SIV,SP2V
      	  STOP ' good peak with bad variance - not allowed'
      	 ELSE
      	  DENOM = (SP2V-SPV**2/SV)
      	  IF(DENOM.EQ.0.0) STOP ' DENOM 0.0'
      	  KPEAK = (SIPV-SPV*SIV/SV)/DENOM
      	  BACK = (SIV-SPV*KPEAK)/SV ! BACK used in 2nd pass.
      	  IF(SP2V.EQ.0.0) STOP ' SP2V 0.0'
      	  VP=1.0/SP2V
      	 ENDIF
      	ENDIF
      	IPROF(J) = KPEAK
      	VARP(J) = VP
      	GPBACK(J) = BACK
      	SPVSTORE(J)=SPV  ! store for later use in GLOBAL subroutine.
      	SP2VSTORE(J)=SP2V !
760    CONTINUE
C
C Now subtract fitted profile from input density array.
C
      	NXST=XCOORD(J)-XMHALF+0.5
      	NYST=YCOORD(J)-YMHALF+0.5
      	DX=XCOORD(J)-NXST-XMHALF
      	DY=YCOORD(J)-NYST-YMHALF
      	IF(DX.GE.0.) THEN
      	 IDX=-1
      	ELSE
      	 IDX=+1
      	 DX=ABS(DX)
      	ENDIF
      	IF(DY.GE.0.) THEN
      	 IDY=-1
      	ELSE
      	 IDY=+1
      	 DY=ABS(DY)
      	ENDIF
      	DX1 = 1.-DX
      	DY1 = 1.-DY
      	DO 770 IY=2,NYB-1
      	 INDEXY = (NYST+IY-2)*NX + NXST-1
      	DO 770 IX=2,NXB-1
      	 INDEX = INDEXY+IX
      	 ARRAY(INDEX) = ARRAY(INDEX) -
     .	  IPROF(J)*APROF(NP,IX,IY) *DX1*DY1 -
     .	  IPROF(J)*APROF(NP,IX+IDX,IY) *DX*DY1 -
     .	  IPROF(J)*APROF(NP,IX+IDX,IY+IDY) *DX*DY -
     .	  IPROF(J)*APROF(NP,IX,IY+IDY) *DX1*DY
      	 IF(APROF(NP,IX,IY).NE.0.0) THEN
      	 SDEVTNP=SDEVTNP+ABS(JPROF(J,IX,IY)-IPROF(J)*APROF(NP,IX,IY))
      	 ELSE
      	 SDEVTNB=SDEVTNB+ABS(JPROF(J,IX,IY)-IPROF(J)*APROF(NP,IX,IY))
      	 ENDIF
770   	CONTINUE
      	GOODFIT=GOODFIT+(SDEVTNP+SDEVTNB)/(SSIGMAP+SSIGMAB)
      	SDEVTNP=SDEVTNP/SSIGMAP
      	IF(SSIGMAB.GT.0.0) SDEVTNB=SDEVTNB/SSIGMAB
      	GOODFITP=GOODFITP+SDEVTNP
      	GOODFITB=GOODFITB+SDEVTNB
      	IF(LPRINT.OR.(IOVERP(J).EQ.0.AND.
     .	 (SDEVTNP.GT.2.5.OR.SDEVTNB.GT.2.5))) THEN
      	 WRITE(6,771) J,JH(J),JK(J),
     .	 SDEVTNP,SDEVTNB,NGOOD,NBAD,NGDSIG,NBDSIG,KPEAK,BACK
      	ENDIF
771   	FORMAT(' Spot number',I5,' index (',I3,',',I3,
     .	')  profile goodness of fit(P:B)',2F6.2/
     .	'                       NGOOD,NBAD,NGDSIG,NBDSIG',I6,3I5,
     .	'  peak total, background/pixel',I7,F7.1)
800   CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE GETZONE(IZ,J,XCOORD,YCOORD,X0,Y0,TILTAXIS,RHALF)
C
C  This subroutine_determines the identity (IZ) of the zone in which spot J is
C   located, and returns the value of IZ to the calling program.  Zone 1 is
C   nearest to the centre and zones 2 to 5 are  90-degree sectors.
      DIMENSION XCOORD(1),YCOORD(1)
C      WRITE(6,1)
C1     FORMAT(' Entering GETZONE')
      PI=3.1415926
      XC=XCOORD(J)-X0
      YC=YCOORD(J)-Y0
      ANGSPOT=180.0/PI*ATAN2(YC,XC)
      ANGDIFF=ANGSPOT-TILTAXIS
      IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      RADSQ = YC**2 + XC**2
      IF(RADSQ.LT.RHALF**2) THEN
      	IZ=1
      ELSE
      	IZ=(ANGDIFF+180.0)/45.0
      	IZ=(IZ+5)/2
      	IF(IZ.EQ.6) IZ=2
      ENDIF
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE SUBTRACT(NX,NY,ARRAY,X0,Y0,LOOKUP,ODBACK,YCURVE)
C subtract radial and y-correlated density and correct for look-up table, to
C  create null image suitable for display to examine quality of profiel fit.
      DIMENSION ARRAY(1),ODBACK(1),YCURVE(1)
      REAL*4 LOOKUP(1)
C
      NUNDERRAW=0 ! number of raw zero densities
      NUNDERRAD=0 ! number of radial/ycurve zero densities
      DO 750 IX=1,NX
      	XCOSQ=(IX-X0)**2
      DO 750 IY=1,NY
      	 RAD=1.+SQRT(XCOSQ+(IY-Y0)**2)
      	 IRAD=RAD
      	 DRAD=RAD-IRAD
      	 ODB=ODBACK(IRAD)*(1-DRAD)+ODBACK(IRAD+1)*DRAD+YCURVE(IY)
C			interpolate radial background, lookup ycurve
      	 JLOOKRAD=ODB
      	 IF(JLOOKRAD.LE.0) THEN
      	  NUNDERRAD=NUNDERRAD+1
      	  JLOOKRAD=1   ! only put into ARRAY for cosmetic output
      	  ODB=1.0      !
      	 ENDIF
      	 DJLOOK=ODB-JLOOKRAD
      	 INDEX=(IY-1)*NX+IX
      	 JLOOK=ARRAY(INDEX)
      	 IF(JLOOK.LE.0) THEN
      	  NUNDERRAW=NUNDERRAW+1
      	  JLOOK=1 ! only put into ARRAY for cosmetic output
      	 ENDIF
      	 ARRAY(INDEX) = LOOKUP(JLOOK) -
     .	  LOOKUP(JLOOKRAD) * (1.-DJLOOK) -
     .	  LOOKUP(JLOOKRAD+1) * DJLOOK
750   CONTINUE
      WRITE(6,10) NUNDERRAW,NUNDERRAD
10    FORMAT(/' In subroutine_SUBTRACT during creation of null image'/
     .	' in whole area, there were',I8,
     .	' raw zero density pixels, and',I8,
     .	'  radial/ycurve near-zero densities')
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE PROFFLATTEN(N,IH,IK,NXB,NYB,APROF,APROFMIN,NSPT,LPRINT)
      PARAMETER (IRAST=60)
      DIMENSION APROF(5,IRAST,IRAST),MPROF(IRAST,IRAST)
      LOGICAL LPRINT
C  Set whole profile area to zero except for central connected region with
C   density above APROFMIN.
      IF(LPRINT) WRITE(6,1) N
1     FORMAT(/' Entering PROFFLATTEN, profile number',I3)
      DO 10 I=1,NXB
      DO 10 J=1,NYB
10    MPROF(I,J)=0
C  start in middle with the horizontal line through the centre
      IXS=NXB/2
      IYS=NYB/2
      IF(APROF(N,IXS,IYS).LT.APROFMIN) THEN
      	 DO 5 J=1,NYB
      	 WRITE(6,6) (APROF(N,I,J),I=1,NXB)
6		FORMAT(30F5.0)
5		CONTINUE
      	 WRITE(6,7) NSPT,IH,IK,APROFMIN
7		FORMAT(' for spot number',I6,'  index',2I5,' APROFMIN=',F12.3)
      	 STOP ' PROFILE NO GOOD'
      ENDIF
      MPROF(IXS,IYS)=1
      DO 22 IMOD=-1,1,2
      	DO 20 IPOS=IXS+1,NXB-1
      	 I=(IPOS-IXS)*IMOD+IXS
      	 IF(APROF(N,I,IYS).LT.APROFMIN) GO TO 22
      	IF(MPROF(I+1,IYS).EQ.1.OR.MPROF(I-1,IYS).EQ.1)  MPROF(I,IYS)=1
20	CONTINUE
22    CONTINUE
C
      DO 45 JMOD=-1,1,2
      	DO 40 JPOS=IYS+1,NYB-1
      	 J=(JPOS-IYS)*JMOD+IYS
      	 IF(J.NE.0) THEN
      	    DO 55 IMOD=-1,1,2
      	  DO 50 IPOS=IXS,NXB-1
      	  I=(IPOS-IXS)*IMOD+IXS
      	  IF(I.NE.0) THEN  ! check needed for ODD rasters
      	    IF(APROF(N,I,J).GT.APROFMIN) THEN
      	     IF(I-1.GT.0.AND.I+1.LE.NXB) THEN
      	       IF(MPROF(I,J-JMOD).EQ.1.OR.
     .	        MPROF(I+1,J-JMOD).EQ.1.OR.
     .	        MPROF(I-1,J-JMOD).EQ.1) MPROF(I,J)=1
      	     ELSEIF(I-1.LE.0) THEN
      	       IF(MPROF(I,J-JMOD).EQ.1.OR.
     .	        MPROF(I+1,J-JMOD).EQ.1) MPROF(I,J)=1
      	     ELSE
      	       IF(MPROF(I,J-JMOD).EQ.1.OR.
     .	        MPROF(I-1,J-JMOD).EQ.1) MPROF(I,J)=1
      	     ENDIF
      	    ENDIF
      	  ENDIF
50    	  CONTINUE
55    	    CONTINUE
      	 ENDIF
40    	CONTINUE
45    CONTINUE

      IF(LPRINT) WRITE(6,59)
59    	FORMAT(' Profile flattened using this mask')
      	DO 61 J=1,NYB
      	 DO 60 I=1,NXB
      	  APROF(N,I,J)=APROF(N,I,J)*FLOAT(MPROF(I,J))
60    	CONTINUE
      	 IF(LPRINT) THEN
      	   DO 63 I=1,NXB  ! MPROF integers no longer needed
      	     IF(MPROF(I,J).EQ.0) MPROF(I,J)=ICHAR('o') ! symbols
      	     IF(MPROF(I,J).EQ.1) MPROF(I,J)=ICHAR('X') ! for output
63		  CONTINUE
      	   WRITE(6,62)(MPROF(I,J),I=1,NXB) ! symbols
62    	   FORMAT(1X,100A1)
      	 ENDIF
61    	CONTINUE
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE STATS(NDATA,JH,JK,ICORR,IPROF,IOVER,IPROP,VAR,VARP,
     .	 X0,Y0,XCOORD,YCOORD,ASTAR,AHCORR,ROUT,LPROFIT)
      PARAMETER (MAXSLOTS=6)
      DIMENSION JH(1),JK(1),ICORR(1),IPROF(1),IOVER(1),IPROP(1)
      DIMENSION XCOORD(1),YCOORD(1),VAR(1),VARP(1)
      DIMENSION DSLOTC(MAXSLOTS),DSLOTP(MAXSLOTS),NSLOT(MAXSLOTS)
      DIMENSION TSLOTC(MAXSLOTS),TSLOTP(MAXSLOTS)
      DIMENSION SSLOTC(MAXSLOTS),SSLOTP(MAXSLOTS)
      LOGICAL LPROFIT
C
      SUMICORRNEAR=0.0
      SUMIPROFNEAR=0.0
      SUMBOTH=0.0
      NNEAR=0
      SUMICORRFAR=0.0
      SUMIPROFFAR=0.0
      NFAR=0

      DO 10 I=1,MAXSLOTS
      	NSLOT(I)=0
      	DSLOTC(I)=0.
      	DSLOTP(I)=0.
      	TSLOTC(I)=0.
      	TSLOTP(I)=0.
      	SSLOTC(I)=0.
      	SSLOTP(I)=0.
10    CONTINUE
      NREJECT=0
      DO 90 J=1,1+(NDATA/2)
       DO 80 K=J+1,NDATA
        IF(JH(J).EQ.-JH(K)) THEN
      	  IF(JK(J).EQ.-JK(K)) THEN
      	    IF(IOVER(J).EQ.0.AND.IOVER(K).EQ.0) THEN
      	 RADSQ=(XCOORD(J)-X0)**2+(YCOORD(J)-Y0)**2
      	 ISLOT=0.999+(RADSQ/ROUT**2)*FLOAT(MAXSLOTS)
		IF(ISLOT.LT.1.OR.ISLOT.GT.6) STOP ' STATS resol wrong'
C		WRITE(6,*) J,K,IPROF(J),IPROF(K),ICORR(J),ICORR(K)
C		WRITE(6,*) VAR(J),VAR(K),VARP(J),VARP(K)
      	 NSLOT(ISLOT)=NSLOT(ISLOT)+1
      	 SSLOTC(ISLOT)=SSLOTC(ISLOT)+SQRT(VAR(J)+VAR(K))
      	 SSLOTP(ISLOT)=SSLOTP(ISLOT)+SQRT(VARP(J)+VARP(K))
      	 DSLOTC(ISLOT)=DSLOTC(ISLOT)+ABS(FLOAT(ICORR(J)-ICORR(K)))
      	 DSLOTP(ISLOT)=DSLOTP(ISLOT)+ABS(FLOAT(IPROF(J)-IPROF(K)))
      	 TSLOTC(ISLOT)=TSLOTC(ISLOT)+
     .	  AMAX1(1.,FLOAT(ICORR(J)+ICORR(K))/2.)
      	 TSLOTP(ISLOT)=TSLOTP(ISLOT)+
     .	  AMAX1(1.,FLOAT(IPROF(J)+IPROF(K))/2.)
C		WRITE(6,*)ISLOT,NSLOT(ISLOT),DSLOTC(ISLOT),TSLOTC(ISLOT)
      	 IF(IPROP(J).GE.500) THEN
      	  SUMICORRFAR=SUMICORRFAR+ICORR(J)+ICORR(K)
      	  SUMIPROFFAR=SUMIPROFFAR+IPROF(J)+IPROF(K)
      	  NFAR=NFAR+2
      	 ELSE
      	  SUMICORRNEAR=SUMICORRNEAR+ICORR(J)+ICORR(K)
      	  SUMIPROFNEAR=SUMIPROFNEAR+IPROF(J)+IPROF(K)
      	  NNEAR=NNEAR+2
      	 ENDIF
            ELSE
                NREJECT=NREJECT+1
      	    ENDIF
      	  ENDIF
      	ENDIF
80     CONTINUE
90    CONTINUE
      WRITE(6,99)
99    FORMAT(11X,
     .	's   Resol    N   R_raster R_profile R_theor_r R_theor_p'/)
      NTOT=0
      STOTC=0.
      STOTP=0.
      DTOTC=0.
      DTOTP=0.
      TTOTC=0.
      TTOTP=0.
      DO 100 N=1,MAXSLOTS
       IF(NSLOT(N).GE.1) THEN
      	NTOT=NTOT+NSLOT(N)
      	STOTC=STOTC+SSLOTC(N)
      	STOTP=STOTP+SSLOTP(N)
      	DTOTC=DTOTC+DSLOTC(N)
      	DTOTP=DTOTP+DSLOTP(N)
      	TTOTC=TTOTC+TSLOTC(N)
      	TTOTP=TTOTP+TSLOTP(N)
      	R2=(AHCORR/(ASTAR*ROUT))*SQRT(FLOAT(MAXSLOTS)/FLOAT(N))
      	RFTHEORC=SSLOTC(N)/TSLOTC(N)
      	RFTHEORP=SSLOTP(N)/TSLOTP(N)
      	RFC=DSLOTC(N)/TSLOTC(N)
        RFP=DSLOTP(N)/TSLOTP(N)
      	WRITE(6,101) N,R2,NSLOT(N),RFC,RFP,RFTHEORC,RFTHEORP
       ENDIF
100   CONTINUE
      WRITE(6,102) NTOT,DTOTC/TTOTC,DTOTP/TTOTP,
     .	  STOTC/TTOTC,STOTP/TTOTP,NREJECT
101   FORMAT(I12,F8.2,I5,4F10.3)
102   FORMAT(/13X'OVERALL',I5,4F10.3,'       with',I5,
     .  ' pairs over/underloaded '/)
      IF(LPROFIT) THEN
      	  IF(NNEAR+NFAR.NE.0)
     .	 SUMBOTH=(SUMIPROFNEAR+SUMIPROFFAR)/(SUMICORRNEAR+SUMICORRFAR)
      	  IF(NNEAR.NE.0) SUMIPROFNEAR=SUMIPROFNEAR/SUMICORRNEAR
      	  IF(NFAR.NE.0) SUMIPROFFAR=SUMIPROFFAR/SUMICORRFAR
      	  WRITE(6,103) SUMIPROFNEAR,NNEAR,SUMIPROFFAR,NFAR,SUMBOTH,
     .	 NNEAR+NFAR
      ENDIF
103   FORMAT(' Ratios of total profile to total raster intensities - N'/
     .	     '                       near tiltaxis -------',F7.3,I5/
     .	     '                       far from tiltaxis ---',F7.3,I5/
     .	     '                       all spots -----------',F7.3,I5/)
C
      RETURN
      END
C
C##############################################################################
C
      SUBROUTINE LOSCAL(NDATA,JH,JK,ICORR,IOVER,XCOORD,YCOORD,ILOSC)
      PARAMETER (NMAX=5000)
      DIMENSION JH(1),JK(1),ICORR(1),IOVER(1),XCOORD(1),YCOORD(1)
      DIMENSION ILOSC(1)
      DIMENSION IF(NMAX)
C local scaling of first half of data w.r.t. second, Friedel related half.
      DISTEST=250.**2
      WRITE(6,200)
200   FORMAT(/' Entering LOSCAL   - statistics are calculated but the',
     .	 ' integrated intensities written out are not altered ')
      DO 210 J=1,NDATA
      	ILOSC(J)=0
      	IF(J)=0
      DO 210 K=1,NDATA
      	IF(JH(J).EQ.-JH(K)) THEN
      	   IF(JK(J).EQ.-JK(K)) THEN
      	 IF(J)=K  ! locate the Friedel pair in list
      	   ENDIF
      	ENDIF
210   CONTINUE
      RFA=0.
      RFB=0.
      STOTA=0.
      STOTB=0.
      NAV=0
      ISPOT=0
      DO 230 J=1,(1+NDATA)/2
      	S=0.
      	SF=0.
      	N=0
      	DO 240 K=1,NDATA
      	 IF(J.NE.K.AND.IF(K).NE.0) THEN
      	    IF(IOVER(K).EQ.0.AND.IOVER(IF(K)).EQ.0) THEN
      	  DISTSQ=(XCOORD(J)-XCOORD(K))**2 +
     .	   (YCOORD(J)-YCOORD(K))**2
      	  IF(DISTSQ.LE.DISTEST) THEN
      	   S = S+ICORR(K)
      	   SF= SF+ICORR(IF(K))
      	   N = N+1
      	  ENDIF
      	    ENDIF
      	 ENDIF
240   	CONTINUE
      	IF(N.GE.1) THEN
      	  IF(IF(J).NE.0) THEN
      	    IF(IOVER(J).EQ.0.AND.IOVER(IF(J)).EQ.0) THEN
      	 ILOSC(J)=FLOAT(ICORR(J))*(SF/S)
      	 ILOSC(IF(J))=ICORR(IF(J))
      	 ISPOT=ISPOT+1
      	 NAV=NAV+N
      	 RFA=RFA+ABS(FLOAT(ICORR(J)-ICORR(IF(J))))
      	 RFB=RFB+ABS(FLOAT(ILOSC(J)-ILOSC(IF(J))))
      	 STOTA=STOTA+AMAX1(1.,FLOAT(ICORR(J)+ICORR(IF(J)))/2.)
      	 STOTB=STOTB+AMAX1(1.,FLOAT(ILOSC(J)+ILOSC(IF(J)))/2.)
      	    ENDIF
      	  ENDIF
      	ENDIF
230   CONTINUE
      	IF(ISPOT.GT.0) THEN
      	 NAV=NAV/ISPOT
      	 RFA=RFA/STOTA
      	 RFB=RFB/STOTB
      	ENDIF
      WRITE(6,241) SQRT(DISTEST),NAV,RFA,RFB,ISPOT
241   FORMAT(' Local scaling radius, average number of spots used',
     .	F6.0,I5/' Overall R-factor improved from',F8.3,'  to',F8.3,
     .	'  for',I5,'  spots')
      RETURN
      END
C##############################################################################
      SUBROUTINE DUMP(N,NXB,NYB,IH,IK,TOTALINT,DPERIM,
     .	NAVPROF,NSPOTPROF,APROF)
      PARAMETER (IRAST=60)
C  This subroutine_only used for debugging
      REAL*4 APROF(5,IRAST,IRAST),NSPOTPROF(5)
      WRITE(6,20) N,IH,IK,TOTALINT,DPERIM,NAVPROF,NSPOTPROF(1)
20    FORMAT(/' Diagnostic output for selected spots'/' spot number',I6,
     .	'   index',2I5/' total intensity',F10.1,
     .	'   perimeter intensity',F10.1/' number of profiles so far',I12/
     .	' number of profiles for this spot',5I5)
      DO 30 K=1,NYB
      WRITE(6,21) (APROF(1,J,K),J=1,NXB)
21    FORMAT(30F5.0)
30    CONTINUE
      WRITE(6,22)
22    FORMAT(' ')
      RETURN
      END
C##############################################################################
      SUBROUTINE PROFDECONV(TPROF,NDATA,NXB,NYB,
     .	  XCOORD,YCOORD,X0,Y0,ICORR,VAR,IOVER,IFLATTEN,PROFMIN,
     .	  JPROF,TILTAXIS,RIN,ROUT,RHALF,PRPMAX,JH,JK,NAVPROF)
C  This subroutine_creates 3 profiles by deconvolution from the spots in the
C   original pattern, and stores them initially in RPROF then in TPROF : it
C   is called once only, but in turn calls subroutine_DECONVOLUTE.
C  The spots near the centre are used to create profile 1, the spots at high
C   resolution near the tiltaxis to create profile 2, and the spots
C   perpendicular to the tilt axis to create profile 3.
C	XCOORD,YCOORD are distortion corrected coordinates of all spots.
C	X0,Y0 is the pattern centre.
C	JPROF is an exact interpolated raster of density around each spot.
C	JPTEMP is a temporary version of JPROF used only in the subroutine.
C	TILTAXIS,ROUT,PRPMAX describe the geometry of the diffraction pattern.
C	TPROF stores the final profiles, only 3 at present.
C	RPROF stores the rough profiles during the 3 deconvolution passes.

      PARAMETER (NMAX=5000)
      PARAMETER (IRAST=60)
      INTEGER*2 JPROF(NMAX,IRAST,IRAST)
      REAL*4 TPROF(5,IRAST,IRAST)
      DIMENSION JPTEMP(IRAST,IRAST),RPROF(5,IRAST,IRAST)
      DIMENSION RAWPROF(5,IRAST,IRAST)
      REAL*4 RADGYR(5),RG(5),SHRINKSQ(5),SHRINKN(5) ! radii of gyration and shrinkages
      LOGICAL LPRINT,IFLATTEN
      DIMENSION XCOORD(1),YCOORD(1),ICORR(1),VAR(1),IOVER(1),JH(1),JK(1)
C*** jms	08.07.2010
c      DIMENSION NSPOTPROF(5),SYMBOL(40),PLOT(IRAST)
      DIMENSION NSPOTPROF(5)
      character SYMBOL(40)
      character PLOT(IRAST)
      DATA SYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
     .	'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
     .	'Q','R','S','T','U','V','W','X','Y','Z','+','*','o','-'/
CTSH++
c      INTEGER*4 TMPPLOT(IRAST)
c      EQUIVALENCE (TMPPLOT,PLOT)
c      INTEGER*4 TMPSYMBOL(40)
c      EQUIVALENCE (TMPSYMBOL,SYMBOL)
c      DATA TMPSYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
c     .	'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
c     .	'Q','R','S','T','U','V','W','X','Y','Z','+','*','o','-'/
CTSH--
      DATA LPRINT/.TRUE./
      DATA PLOT/IRAST*'0'/
CTSH++
c      DATA TMPPLOT/IRAST*'0'/
CTSH--
      DATA NSPOTPROF/5*0/
      DATA NPROFS/3/,IDUM/0/
      DATA PROPNEAR/0.3/ ! radius to define near tilt axis

      WRITE(6,1) RIN,ROUT,RHALF,PRPMAX
1     FORMAT(/' Entering PROFDECONV with RIN ROUT RHALF PRPMAX',4F10.1)
      PI=3.1415926
      	XMHALF=(NXB-1.0)/2.0
      	YMHALF=(NYB-1.0)/2.0

      DO 900 IPASS=1,1 ! safer with only no deconvolutions
      	NAVPROF=0
      	DO 5 N=1,5
5	NSPOTPROF(N)=0

      	DO 10 I=1,NPROFS
      	DO 10 J=1,IRAST
      	DO 10 K=1,IRAST
      	RAWPROF(I,J,K)=0.0
10    	RPROF(I,J,K)=0.0

C  First get three rough profiles to get started in pass 1, then carry out
C   proper deconvolution in passes 2 and 3
      DO 80 J=1,NDATA
C      if(ipass.eq.2.and.j.eq.270) 			!
C     .		stop ' test limited o/p of 270 rasters'	! debug
       DO 12 IX=1,NXB
       DO 12 IY=1,NYB
12     JPTEMP(IX,IY)=JPROF(J,IX,IY)
       IF(IOVER(J).EQ.0) THEN
      	IF(IPASS.GE.2) WRITE(6,13) JH(J),JK(J)
13	FORMAT(/'                     profdeconv : spot (',I3,',',I3,')')
      	XC=XCOORD(J)-X0
      	YC=YCOORD(J)-Y0
      	ANGSPOT=180.0/PI*ATAN2(YC,XC)
      	ANGDIFF=ANGSPOT-TILTAXIS
      	IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      	IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      	RADIUS = SQRT(YC**2 + XC**2)
      	ANGDIFF=ANGDIFF*PI/180.0
      	PROPPARA = ABS(COS(ANGDIFF))*RADIUS/ROUT
      	PROPPERP = ABS(SIN(ANGDIFF))*RADIUS/PRPMAX
      	IF(VAR(J).LE.0.0) STOP ' Negative variance STOP in PROFDECONV'
      	IF(PROPPERP.GT.PROPNEAR) THEN  ! PROPNEAR=0.3 is near tiltaxis
      	 IF(PROPPERP.GT.1.0) THEN
      	  WRITE(6,15) PROPPERP,JH(J),JK(J)
15			FORMAT('ERROR IN PROPPERP > 1.0 (H,K)',F10.3,2I5)
      	  STOP
      	 ENDIF
      	    NSPOTPROF(3)=NSPOTPROF(3)+1
      	    IF(IPASS.GE.2) CALL DECONVOLUTE(3,JPTEMP,TPROF,
     .	   PROPPERP,PROPPARA,NXB,NYB)
      	    DO 50 IX=1,NXB
      	 X=IX-1
      	 XPICK = 1.0+XMHALF + (X-XMHALF)*PROPPERP
      	 IXPICK=XPICK
      	 DXPICK=XPICK-IXPICK
      	    DO 50 IY=1,NYB
      	 Y=IY-1
      	 YPICK = 1.0+YMHALF + (Y-YMHALF)*PROPPERP
      	 IYPICK=YPICK
      	 DYPICK=YPICK-IYPICK
	    	RPROF(3,IX,IY)=RPROF(3,IX,IY) +
     .	  (1-DXPICK)*(1-DYPICK)*JPTEMP(IXPICK,IYPICK)*ICORR(J)/VAR(J)+
     .	  DXPICK*(1-DYPICK)*JPTEMP(IXPICK+1,IYPICK)*ICORR(J)/VAR(J)+
     .	  (1-DXPICK)*DYPICK*JPTEMP(IXPICK,IYPICK+1)*ICORR(J)/VAR(J)+
     .	  DXPICK*DYPICK*JPTEMP(IXPICK+1,IYPICK+1)*ICORR(J)/VAR(J)
      	 RAWPROF(3,IX,IY)=RAWPROF(3,IX,IY)+JPTEMP(IX,IY)*ICORR(J)/VAR(J)
50	    CONTINUE
C      write(6,*) '3',JH(J),JK(J)
      	ELSE
      	    IF(PROPPARA*ROUT.GT.RHALF) THEN
      	 IF(PROPPARA.GT.1.0) THEN
      	  WRITE(6,55) PROPPARA,JH(J),JK(J)
55			FORMAT('ERROR IN PROPPARA > 1.0 (H,K)',F10.3,2I5)
      	  STOP
      	 ENDIF
      	 NSPOTPROF(2)=NSPOTPROF(2)+1
      	 IF(IPASS.GE.2) CALL DECONVOLUTE(2,JPTEMP,TPROF,
     .	   PROPPERP,PROPPARA,NXB,NYB)
      	 DO 60 IX=1,NXB
      	 X=IX-1
      	 XPICK = 1.0+XMHALF + (X-XMHALF)*PROPPARA
      	 IXPICK=XPICK
      	 DXPICK=XPICK-IXPICK
      	 DO 60 IY=1,NYB
      	 Y=IY-1
      	 YPICK = 1.0+YMHALF + (Y-YMHALF)*PROPPARA
      	 IYPICK=YPICK
      	 DYPICK=YPICK-IYPICK
	    	RPROF(2,IX,IY)=RPROF(2,IX,IY) +
     .	  (1-DXPICK)*(1-DYPICK)*JPTEMP(IXPICK,IYPICK)*ICORR(J)/VAR(J)+
     .	  DXPICK*(1-DYPICK)*JPTEMP(IXPICK+1,IYPICK)*ICORR(J)/VAR(J)+
     .	  (1-DXPICK)*DYPICK*JPTEMP(IXPICK,IYPICK+1)*ICORR(J)/VAR(J)+
     .	  DXPICK*DYPICK*JPTEMP(IXPICK+1,IYPICK+1)*ICORR(J)/VAR(J)
      	 RAWPROF(2,IX,IY)=RAWPROF(2,IX,IY)+JPTEMP(IX,IY)*ICORR(J)/VAR(J)
60	    CONTINUE
C      write(6,*) '2',JH(J),JK(J)
      	    ELSE
      	     NSPOTPROF(1)=NSPOTPROF(1)+1
      	 IF(IPASS.GE.2) CALL DECONVOLUTE(1,JPTEMP,TPROF,
     .	   PROPPERP,PROPPARA,NXB,NYB)
      	 DO 70 IX=1,NXB
      	 DO 70 IY=1,NYB
      	 RPROF(1,IX,IY)=RPROF(1,IX,IY) + JPTEMP(IX,IY)*ICORR(J)/VAR(J)
      	 RAWPROF(1,IX,IY)=RAWPROF(1,IX,IY)+JPTEMP(IX,IY)*ICORR(J)/VAR(J)
70		CONTINUE
C      write(6,*) '1',JH(J),JK(J)
      	    ENDIF
      	ENDIF
       ENDIF
80    CONTINUE
      	write(6,81) ! comment out later
81	format(' profiles summed')
C
C  Radii of gyration of raw profile averages, needs profile flattening
      	DO 300 N=1,NPROFS
      	 WRITE(6,301) N,RAWPROF(N,NXB/2,NYB/2)
301		FORMAT(' RAW profile for tilted diffraction pattern, zone',I3,
     .	  ' peak density',F12.1)
      	 RPROFMIN=PROFMIN*RAWPROF(N,NXB/2,NYB/2) ! just roughly
         CALL PROFFLATTEN(N,IDUM,IDUM,NXB,NYB,
     .	   RAWPROF,RPROFMIN,IDUM,LPRINT)
      	 CALL PROFSHRINK(N,RAWPROF,NXB,NYB,1.0,RG(N))
300	CONTINUE
C
C  First calculate perimeter OD and maximum OD, then subtract perimeter and
C   flatten profile before calculating radii of gyration
      DO 400 N=1,NPROFS  ! NPROFS started out as  3 in Nov 1997
      	DPERIM=0.0
      	DMAX=-10000.
      	DO 405 IY=2,NYB-1
405   	 DPERIM=DPERIM + RPROF(N,2,IY) + RPROF(N,NXB-1,IY)
      	DO 406 IX=3,NXB-2
406   	 DPERIM=DPERIM + RPROF(N,IX,2) + RPROF(N,IX,NYB-1)
      	DPERIM=DPERIM/(2.*FLOAT(NXB+NYB)-12.) ! get perimeter density
      	 DO 408 IX=2,NXB-1  ! correct for it
      	   DO 408 IY=2,NYB-1
408   	 RPROF(N,IX,IY)=RPROF(N,IX,IY)-DPERIM
      	DO 410 IY=2,NYB-1  ! get maximum profile density
      	DO 410 IX=2,NXB-1
410   	DMAX=AMAX1(DMAX,RPROF(N,IX,IY))
C       and flatten profile at this stage before normalisation if required
      	RPROFMIN=PROFMIN*DMAX  ! before normalisation
      	 WRITE(6,412) N,DMAX
412		FORMAT(' Semi-deconvoluted profile, zone',I3,
     .	  ' peak density',F12.1)
        IF(IFLATTEN) CALL PROFFLATTEN(N,IDUM,IDUM,NXB,NYB,
     .	 RPROF,RPROFMIN,IDUM,LPRINT)
C
C Arbitrary sharpening to fit profile roughly, prior to proper deconvolution
      	IF(IPASS.EQ.1) THEN  ! only for first pass
C      		CALL PROFSHARPEN(N,RPROF,NXB,NYB,2.0)		! sharpen by exp(2) at edge
         DMAX=-10000.
         DO 415 IY=2,NYB-1 ! get maximum density again
         DO 415 IX=2,NXB-1
415      DMAX=AMAX1(DMAX,RPROF(N,IX,IY))
C            and flatten profile at this stage before normalisation if required
         RPROFMIN=PROFMIN*DMAX   ! before normalisation again
C        	IF(IFLATTEN) CALL PROFFLATTEN(N,IDUM,IDUM,NXB,NYB,
C     .		  RPROF,RPROFMIN,IDUM,LPRINT)		! and flatten again
      	 CALL PROFSHRINK(N,RPROF,NXB,NYB,1.0,RADGYR(N))
      	ENDIF
400   CONTINUE
C
C Calculate shrink factors to get three nearly correct profiles.
      	C21=(RHALF+RIN)/(RHALF+ROUT) ! contribution of rawprofile 2 to 1
      	C31=PROPNEAR/(1.0+PROPNEAR) ! contribution of raw profile 3 to 1
      	C32=C31    ! (ditto 3 2) set to 0.23 initially
C      	SHRINKSQ(1)=(1.0-(C21*RG(2)/RG(1))**2-
C     .			(C31*RG(3)/RG(1))**2)/(1.0-C21**2-C31**2)
C      	SHRINKSQ(2) = (1.0-(C32*RG(3)/RG(2))**2)/(1.0-C32**2) -
C     .					(RG(1)/RG(2))**2
C      	SHRINKSQ(3) = 1.0-0.5*(RG(1)**2+RG(2)**2)/RG(3)**2
C
C Try a least squares solution, 3 equations plus 3 unknowns
      	C1=0.7*(4.0*ROUT**2)/((ROUT+RHALF)**2-(RIN+RHALF)**2) ! 0.7 is a fudge
      	SIG2SQ=C1*(RG(2)**2-RG(1)**2)
      	C2=4.0/(1.0+2.0*PROPNEAR)
      	C3=0.8*0.5-((RHALF+RIN)/(2.0*ROUT))**2  ! 0.8 is a fudge
      	SIG3SQ=C2*((RG(3)**2-RG(1)**2)-C3*SIG2SQ)
      	C4=0.8*((RHALF+RIN)/(2.0*ROUT))**2  ! 0.8 is a fudge
      	C5=0.8*(PROPNEAR/2.0)**2   ! 0.8 is a fudge
      	SIG1SQ=RG(1)**2-C4*SIG2SQ-C5*SIG3SQ
      	SHRINKSQ(1)=SIG1SQ/RADGYR(1)**2
      	SHRINKSQ(2)=SIG2SQ/RADGYR(2)**2
      	SHRINKSQ(3)=SIG3SQ/RADGYR(3)**2
C
      	 DO 420 N=1,NPROFS
420   	 IF(SHRINKSQ(N).GT.0.0) SHRINKN(N)=SQRT(SHRINKSQ(N))
      	 SHRINKN(1)=AMAX1(0.80,SHRINKN(1))   ! don't shrink direct beam too much
      	 SHRINKN(2)=AMAX1(0.01,SHRINKN(2))   ! keep at least delta function
      	 SHRINKN(3)=AMAX1(0.01,SHRINKN(3))   ! keep at least delta function
      WRITE(6,421) C21,C31,C32,(SHRINKSQ(N),SHRINKN(N),N=1,3)
421   FORMAT(/' Proportion profile 2 contributing to profile 1 =',F6.3/
     .	      ' Proportion profile 3 contributing to profile 1 =',F6.3/
     .	      ' Proportion profile 3 contributing to profile 2 =',F6.3/
     .	      ' Variance reduction & shrink factor for profile 1 ='2F7.3/
     .	      ' Variance reduction & shrink factor for profile 2 ='2F7.3/
     .	     ' Variance reduction & shrink factor for profile 3 ='2F7.3/)
C
C  Now, shrink, calculate total integrated and new maximum profile OD,
C   normalise and print out profiles, so that the sum of each
C   profile = 1.0, above the zero background.
      DO 450 N=1,NPROFS
      	 CALL PROFSHRINK(N,RPROF,NXB,NYB,SHRINKN(N),RADGYR)
      	TOTALINT=0.0
      	DMAX=-10000.
      	DO 422 IY=2,NYB-1
      	DO 422 IX=2,NXB-1
   	DMAX=AMAX1(DMAX,RPROF(N,IX,IY))
422     TOTALINT=TOTALINT+RPROF(N,IX,IY)
      	IF(TOTALINT.LE.0.0) THEN ! diagnostic
      	 WRITE(6,423) IPASS,N,TOTALINT
423		FORMAT(' PASS',I2,' ERROR in PROFDECONV for profile N =',I2,
     .	  '  - negative value for TOTALINT =',F12.2/
     .	  ' perhaps the edge of the chosen',
     .	  ' profile overlaps adjacent spots'/
     .	  '  or very weak spots - could be more complicated')
         STOP
      	ENDIF
      	NAVPROF=NAVPROF+NSPOTPROF(N)
      	WRITE(6,424) IPASS,N,NSPOTPROF(N)
424   	FORMAT(' PASS',I2,' Rough profile',I3,' includes',I5,' rasters')
C
      	 WRITE(6,426) DPERIM,DMAX,DPERIM/DMAX
426   	 FORMAT(' Mean perimeter and maximum OD, ',2F10.1/
     .	  '  background as a fraction of maximum ',F10.4)
      	 DO 435 IY=1,NYB
      	 DO 430 IX=1,NXB
      	  A=RPROF(N,IX,IY)
      	  IDATA=1. + 20.*A/DMAX ! select symbol to be highest point
      	  IF(A.LT.0.0001) IDATA=39 ! identically zero
      	  IF(A.LT.0.0) IDATA=40
      	  PLOT(IX) = SYMBOL(IDATA)
430   	 CONTINUE
      	  WRITE(6,434) (PLOT(IX),IX=1,NXB)
434   	  FORMAT(1X,120A1)
435   	 CONTINUE
      	 WRITE(6,431)
431		FORMAT(/)
C
C  Now normalise profile whether flattened previously or not
C
      	DO 440 IX=2,NXB-1
      	    DO 440 IY=2,NYB-1  ! Normalise to 1.0
440   	RPROF(N,IX,IY)=RPROF(N,IX,IY)/TOTALINT
C
450   CONTINUE

      DO 500 N=1,NPROFS
      DO 500 IX=1,NXB
      DO 500 IY=1,NYB
500   TPROF(N,IX,IY)=RPROF(N,IX,IY)

900   CONTINUE
      NAVPROF=NAVPROF/NPROFS ! At Nov 1997 there are NPROFS=3 profiles
      RETURN
      END
C##############################################################################
      SUBROUTINE PROFCONV(JS,APROF,TPROF,NXB,NYB,XCOORD,YCOORD,
     .	   X0,Y0,TILTAXIS,ROUT,PRPMAX,LPRINT,JH,JK)
C  This subroutine_creates the required profile by convolution from the
C   three profiles stored in TPROF and places the result in the first
C   location of APROF.  It is called once for each spot to be measured.
      PARAMETER (IRAST=60)
      REAL*4 APROF(5,IRAST,IRAST)
      REAL*4 TPROF(5,IRAST,IRAST)
      REAL*4 S1(IRAST,IRAST),S2(IRAST,IRAST),S3(IRAST,IRAST)
      REAL*4 SPARA(IRAST,IRAST),SPERP(IRAST,IRAST)
      INTEGER IPOUT(IRAST,IRAST)
      LOGICAL LPRINT
      DIMENSION XCOORD(1),YCOORD(1),JH(1),JK(1)
C      WRITE(6,10)
10    FORMAT(/' Entering PROFCONV')

      PI=3.1415926
      XC=XCOORD(JS)-X0
      YC=YCOORD(JS)-Y0
      ANGSPOT=(180.0/PI)*ATAN2(YC,XC)
      ANGDIFF=ANGSPOT-TILTAXIS
      IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      IF(ABS(ANGDIFF).GT.180.0)ANGDIFF=ANGDIFF-360.0*SIGN(1.0,ANGDIFF)
      RADIUS = SQRT(YC**2 + XC**2)
      ANGDIFF=ANGDIFF*PI/180.0
      PROPPARA = ABS(COS(ANGDIFF))*RADIUS/ROUT
      PROPPERP = ABS(SIN(ANGDIFF))*RADIUS/PRPMAX
      XMHALF=(NXB-1.0)/2.0
      YMHALF=(NYB-1.0)/2.0
      XYHALFMIN=AMIN1(XMHALF,YMHALF)
      write(6,11) JS,JH(JS),JK(JS),PROPPARA,PROPPERP
11    format(' Spot',I5,'   Index(',I3,',',I3,')   PROPPARA,PROPPERP ='
     .	 ,2F9.5,'   in subroutine_PROFCONV')
C
C  Convolute profile 1 with a geometrically scaled (by PROPPARA) version
C    of profile 2, then convolute the result with a geometrically scaled
C    (by PROPPERP) version of profile 2,
C
C  First scale and interpolate, centred at XMHALF,YMHALF from the corner.
C  parallel to tilt axis: S2 from TPROF(2,,)
      TOTS1=0.0
      TOTS2=0.0
      TOTS3=0.0
      DO 50 I=1,NXB
      DO 50 J=1,NYB
      	S1(I,J)=TPROF(1,I,J)
      	TOTS1=TOTS1+S1(I,J)
      	X=I-1
      	X=1.0 + XMHALF + (X-XMHALF)/PROPPARA
      	Y=J-1
      	Y=1.0 + YMHALF + (Y-YMHALF)/PROPPARA
      	IX=X
      	IY=Y
      	DX=X-IX
      	DY=Y-IY
      	IF(IX.LT.1.OR.IX.GE.NXB.OR.
     .	   IY.LT.1.OR.IY.GE.NYB)   THEN
      	 S2(I,J)=0.0
      	ELSE
      	 S2(I,J)=(1-DY)*((1-DX)*TPROF(2,IX,IY)+DX*TPROF(2,IX+1,IY))+
     .	   DY*((1-DX)*TPROF(2,IX,IY+1)+DX*TPROF(2,IX+1,IY+1))
      	 TOTS2=TOTS2+S2(I,J)
      	ENDIF
50    CONTINUE
C
C      IF(JS.EQ.1) CALL PROFPRINT(S2,NXB,NYB,2)	! for debug
C
C  perpendicular to tilt axis: S3 from TPROF(3,,)
      DO 60 I=1,NXB
      DO 60 J=1,NYB
        X=I-1
      	X=1.0 + XMHALF + (X-XMHALF)/PROPPERP
      	Y=J-1
      	Y=1.0 + YMHALF + (Y-YMHALF)/PROPPERP
      	IX=X
      	IY=Y
      	DX=X-IX
      	DY=Y-IY
      	IF(IX.LT.1.OR.IX.GE.NXB.OR.
     .	   IY.LT.1.OR.IY.GE.NYB)   THEN
      	 S3(I,J)=0.0
      	ELSE
      	S3(I,J)=TPROF(3,I,J) ! temporary test of S3
C      		S3(I,J)=(1-DY)*((1-DX)*TPROF(3,IX,IY)+DX*TPROF(3,IX+1,IY))+
C     .			 DY*((1-DX)*TPROF(3,IX,IY+1)+DX*TPROF(3,IX+1,IY+1))
      	 TOTS3=TOTS3+S3(I,J)
      	ENDIF
60    CONTINUE
C
C      	IF(JS.EQ.1) CALL PROFPRINT(S3,NXB,NYB,3)	! for debug
C
C
C  Now convolute S1 with S2 and the result with S3, then put into APROF
      IF(TOTS2.GT.0.0) THEN ! not nearly perpendicular to tiltaxis
      	CALL CONVOLUTE(S1,S2,SPARA,NXB,NYB,1)
      ELSE     ! nearly perpendicular to tiltaxis
      	DO 61 I=1,NXB
      	DO 61 J=1,NYB
61	SPARA(I,J)=S1(I,J) ! convolution not necessary
      ENDIF
C      IF(JS.EQ.1) CALL PROFPRINT(SPARA,NXB,NYB,12)	! for debug
      IF(TOTS3.GT.0.0) THEN ! not nearly parallel to tiltaxis
      	CALL CONVOLUTE(SPARA,S3,SPERP,NXB,NYB,2)
      ELSE     ! nearly parallel to tiltaxis
      	DO 62 I=1,NXB
      	DO 62 J=1,NYB
62	SPERP(I,J)=SPARA(I,J) ! convolution not necessary
      ENDIF
C      IF(JS.EQ.1) CALL PROFPRINT(SPERP,NXB,NYB,123)	! for debug
C
C	      calculate renormalisation factor to normalise after convolution
      	 TOTALA=0.0
      	 DO 63 I=2,NXB-1
      	 DO 63 J=2,NYB-1
      	 TOTALA=TOTALA+SPERP(I,J)
63    	 APROF(1,I,J)=SPERP(I,J)
C	      now apply renormalisation
      	 IF(TOTALA.LE.0.0) STOP 'TOTALA.LE.0.0'
      	 DO 64 I=1,NXB
      	 DO 64 J=1,NYB
64    	 APROF(1,I,J)=APROF(1,I,J)/TOTALA
      IF(LPRINT) THEN
      	 PMAX=-5000.0
      	 PMIN= 1000000.0
      	 DO 65 I=1,NXB
      	 DO 65 J=1,NYB
      	  PMAX=AMAX1(PMAX,APROF(1,I,J))
      	  PMIN=AMIN1(PMIN,APROF(1,I,J))
65		CONTINUE
      	 IF(PMAX.LE.0.0) stop 'PMAX.LE.0.0 in subroutine_PROFCONV'
      	 SCALEPROF=16.0/PMAX
      	 WRITE(6,71) JS,JH(JS),JK(JS),TOTALA
71		FORMAT(/' Profile for spot',I5,' with index',2I5,
     .	  '  and integrated intensity',F7.3,
     .	  ' before normalisation to 1.0')
      	 DO 79 J=1,NYB
      	  DO 75 I=1,NXB
      	  IPOUT(I,J)=SCALEPROF*APROF(1,I,J)
75			CONTINUE
      	  WRITE(6,72) (IPOUT(I,J),I=1,NXB)
72			FORMAT(60I2)
79		CONTINUE
      ENDIF
      RETURN
      END
C##############################################################################
      SUBROUTINE CONVOLUTE(A1,A2,A3,NXB,NYB,ID)
      PARAMETER (IRAST=60)
      REAL*4 A1(IRAST,IRAST),A2(IRAST,IRAST),A3(IRAST,IRAST)
      REAL*4 ATEMP(2*IRAST,2*IRAST)
C Convolute A1 with A2 to give A3 with origin at XMHALF,YMHALF from (1,1)
C  first convolute into ATEMP then select central region to copy into A3

      	IF(ID.GE.3) write(6,1) ID ! comment out
1	format(' Entering CONVOLUTE with ID =',I3)

      NXC=2*NXB-1
      NYC=2*NYB-1
      	DO 20 K=1,NXC
      	DO 20 L=1,NYC
20    	ATEMP(K,L)=0.0
C
      DO 100 I1=1,NXB
      DO 100 J1=1,NYB
        DO 50 I2=1,NXB
        DO 50 J2=1,NYB
           K=I1-I2+NXB
           L=J1-J2+NYB
           ATEMP(K,L) = ATEMP(K,L) + A1(I1,J1)*A2(I2,J2)
50      CONTINUE
100   CONTINUE
C
      XMHALF=(NXB-1.0)/2.0
      YMHALF=(NYB-1.0)/2.0
      XT=AMOD(XMHALF,2.0) ! 0.5 if NXB even
      YT=AMOD(YMHALF,2.0) ! 0.5 if NYB even
      IXHALF=XMHALF
      IYHALF=YMHALF
      DO 200 I=1,NXB
      DO 200 J=1,NYB
      	IF(XT.LT.0.1.AND.YT.LT.0.1) THEN ! NXB and NYB odd
      	  A3(I,J) = ATEMP(I+IXHALF,J+IYHALF)
      	ENDIF
      	IF(XT.GT.0.1.AND.YT.LT.0.1) THEN ! NXB odd and NYB even
      	  A3(I,J) = 0.5*(ATEMP(I+IXHALF,J+IYHALF) +
     .	  ATEMP(I+IXHALF+1,J+IYHALF))
      	ENDIF
      	IF(XT.LT.0.1.AND.YT.GT.0.1) THEN ! NXB even and NYB odd
      	  A3(I,J) = 0.5*(ATEMP(I+IXHALF,J+IYHALF) +
     .	  ATEMP(I+IXHALF,J+IYHALF+1))
      	ENDIF
      	IF(XT.GT.0.1.AND.YT.GT.0.1) THEN ! NXB and NYB even
      	  A3(I,J) = 0.25*(ATEMP(I+IXHALF,J+IYHALF) +
     .	  ATEMP(I+IXHALF+1,J+IYHALF) +
     .	  ATEMP(I+IXHALF,J+IYHALF+1) +
     .	  ATEMP(I+IXHALF+1,J+IYHALF+1))
      	ENDIF
200   CONTINUE
C
C Check that A3 has non-zero entries, and write diagnostic/stop if not
      A1MAX=-10000.0
      A2MAX=-10000.0
      A3MAX=-10000.0
      DO 25 I=1,NXB
      DO 25 J=1,NYB
      A1MAX=AMAX1(A1MAX,A1(I,J))
      A2MAX=AMAX1(A2MAX,A2(I,J))
      A3MAX=AMAX1(A3MAX,A3(I,J))
25    CONTINUE
      IF(A3MAX.LE.0.0) THEN
      	WRITE(6,26) A1MAX,A2MAX,A3MAX
26    	FORMAT(' A1MAX,A2MAX,A3MAX =',3F18.3)
      	STOP ' A3 ZERO in subroutine_CONVOLUTE'
      ENDIF

      RETURN
      END
C##############################################################################
      SUBROUTINE PROFPRINT(A1,NXB,NYB,ID)
      PARAMETER (IRAST=60)
      REAL*4 A1(IRAST,IRAST),AOUT(IRAST,IRAST)
C
C This provides a printout useful for debugging only
C
      PMAX=-10000.0
      DO 10 I=1,NXB
      DO 10 J=1,NYB
      PMAX=AMAX1(PMAX,A1(I,J))
10    CONTINUE
      WRITE(6,11) PMAX,ID
11    FORMAT(' PROFPRINT array maximum value =',F20.7,'  Entry',I4)
      IF(PMAX.LE.0.0) STOP ' PMAX.LE.ZERO IN SUBROUTINE_PROFPRINT'
C
      DO 25 J=1,NYB
      DO 22 I=1,NXB
22    AOUT(I,J)=100.0*A1(I,J)/PMAX
      WRITE(6,20) (AOUT(I,J),I=1,NXB)
20    FORMAT(/6(20F6.1/))
25    CONTINUE
C
      RETURN
      END
C##############################################################################
      SUBROUTINE PROFSHRINK(N,RPROF,NXB,NYB,SHRINK,RADGYR)
      PARAMETER (IRAST=60)
      DIMENSION RPROF(5,IRAST,IRAST),RTEMP(IRAST,IRAST)
C Shrink profile after perimeter correction
      write(6,1) N,SHRINK
1     format(' Entering PROFSHRINK N =',I2,'  with SHRINK factor=',F7.3)
      XMHALF=(NXB-1.0)/2.0
      YMHALF=(NYB-1.0)/2.0
      	 DO 20 IX=1,NXB
      	 X=IX-1
      	 XPICK = 1.0+XMHALF + (X-XMHALF)/SHRINK
      	 IXPICK=XPICK
      	 DXPICK=XPICK-IXPICK
      	 DO 20 IY=1,NYB
      	 Y=IY-1
      	 YPICK = 1.0+YMHALF + (Y-YMHALF)/SHRINK
      	 IYPICK=YPICK
      	 DYPICK=YPICK-IYPICK
      	 IF(IXPICK.GE.1.AND.IXPICK.LT.NXB.AND.
     .	  IYPICK.GE.1.AND.IYPICK.LT.NYB) THEN
      	  RTEMP(IX,IY)=(1-DXPICK)*(1-DYPICK)*RPROF(N,IXPICK,IYPICK)+
     .	  DXPICK*(1-DYPICK)*RPROF(N,IXPICK+1,IYPICK)+
     .	  (1-DXPICK)*DYPICK*RPROF(N,IXPICK,IYPICK+1)+
     .	  DXPICK*DYPICK*RPROF(N,IXPICK+1,IYPICK+1)
      	 ELSE
      	  RTEMP(IX,IY)=0.0
      	 ENDIF
20	    CONTINUE

C Put shrunk profile back into starting array
      IF(SHRINK.NE.1.0) THEN  ! leave unchanged if no shrink requested
      	 RMAX=-10000.0
      	 DO 30 IX=1,NXB
      	 DO 30 IY=1,NYB
      	  RMAX=AMAX1(RMAX,RTEMP(IX,IY))
30    	 RPROF(N,IX,IY)=RTEMP(IX,IY)
C        make sure there is at least a delta function at the origin
      	 IF(RMAX.LE.0.0) THEN ! puts total intensity of 1.0 at centre
      	   IXM1=1 + (NXB-1)/2
      	   IYM1=1 + (NYB-1)/2
      	   IXM2=1 + NXB/2  ! IXM1 and IXM2 idential if NXB odd
      	   IYM2=1 + NYB/2
      	   RPROF(N,IXM1,IYM1)=RPROF(N,IXM1,IYM1)+0.25
      	   RPROF(N,IXM1,IYM2)=RPROF(N,IXM1,IYM2)+0.25
      	   RPROF(N,IXM2,IYM1)=RPROF(N,IXM2,IYM1)+0.25
      	   RPROF(N,IXM2,IYM2)=RPROF(N,IXM2,IYM2)+0.25
      	  RTEMP(IXM1,IYM1)=RPROF(N,IXM1,IYM1)
      	  RTEMP(IXM1,IYM2)=RPROF(N,IXM1,IYM2)
      	  RTEMP(IXM2,IYM1)=RPROF(N,IXM2,IYM1)
      	  RTEMP(IXM2,IYM2)=RPROF(N,IXM2,IYM2)
      	 ENDIF
      ENDIF
      CALL FITGAUSS(RTEMP,NXB,NYB,RADGYR,AZERO)  ! radius of gyration for interest

      RETURN
      END
C##############################################################################
      SUBROUTINE GLOBAL(ID,NGLOBL,IPEAK,BACK,IPROP,IOVER,JH,JK,
     .	 NXM,NYM,NXMT,NYMT,NXB,NYB,DX1,DY1,DX2,DY2,NDATA,
     .	 SPVSTORE,SP2VSTORE)
C  carry out wider-range global background correction
C
C  Two situations occur - ID=1 where ICORR and BACK are to be used and requires
C  the sizes of the rasters to be taken into account, or ID=2 where the profile
C  fitted intensity had a background level fitted.
C
      PARAMETER (NMAX=5000)
      DIMENSION IPEAK(1),BACK(1),IPROP(1),IOVER(1),JH(1),JK(1)
      DIMENSION SPVSTORE(1),SP2VSTORE(1)
      DIMENSION XS(NMAX),SCALA(NMAX),SCALB(NMAX)
      DIMENSION IHSTORE(200),IKSTORE(200)
      LOGICAL IFOUND
C
C decrease distance until only NGLOBL neighbours contribute to background
      IF(ID.NE.1.AND.ID.NE.2) STOP ' only ID 1 or 2 allowed'
      DD1=SQRT(DX1**2+DY1**2)
      DD2=SQRT(DX2**2+DY2**2)
      DIST=10.0*AMAX1(DD1,DD2) ! 10x larger lattice parameter
110   NEAR=0
      DO 120 IH=-5,5
      DO 115 IK=-5,5
      	DX=IH*DX1+IK*DX2
      	DY=IH*DY1+IK*DY2
      	DTEST=SQRT(DX**2+DY**2)
      	IF(DTEST.LE.DIST) THEN
      	 NEAR=NEAR+1
      	 IF(NEAR.GT.NGLOBL) THEN
      	  DIST = DIST*0.9
      	  GO TO 110 ! too many spots, try with smaller DIST
      	 ENDIF
      	 IHSTORE(NEAR)=IH
      	 IKSTORE(NEAR)=IK
      	ENDIF
115   CONTINUE
120   CONTINUE
      WRITE(6,121) ID,NGLOBL,NEAR,DIST
121   FORMAT(/' Entering GLOBAL ID =',I2,' with NGLOBL =',I3,
     .	  ' and NEAR =',I3,' for DIST =',F6.1/
     .	' Backgrounds from the following spots averaged')
      DO 122 K=1,NEAR
122   WRITE(6,123) IHSTORE(K),IKSTORE(K)
123   FORMAT(2I6)
C
      DO 140 J=1,NDATA
      	 NXMRA = NXM + (NXMT-NXM)*IPROP(J)/1000.
      	 NYMRA = NYM + (NYMT-NYM)*IPROP(J)/1000.
      	 SCALA(J)=NXMRA*NYMRA ! number of pixels in peak raster
140		SCALB(J)=1.0/(NXB*NYB - NXMRA*NYMRA)  ! background scale factor
      DO 180 J = 1,NDATA
      	 BG1 = 0.0
      	 BG2 = 0.0
      	 NBG= 0
      	 DO 170 K=1,NEAR
      	    IHG = JH(J) + IHSTORE(K)
      	    IKG = JK(J) + IKSTORE(K)
      	    IF(IOVER(J).EQ.0) THEN
      	  DO 142 N=1,NDATA
      	     IF(JH(N).EQ.IHG.AND.JK(N).EQ.IKG) THEN
      	       M=N
      	       IFOUND=.TRUE.
      	       GO TO 145
      	     ENDIF
142			CONTINUE
      	       IFOUND=.FALSE.
145			CONTINUE
C      write(6,*) '                M=',M
      	  IF(IFOUND.AND.IOVER(M).EQ.0) THEN
      	     BG1 = BG1 + BACK(M)*SCALB(M)
      	     BG2 = BG2 + BACK(M)
      	     NBG=NBG + 1
      	  ENDIF
      	    ENDIF
170		CONTINUE
      	 IF(NBG.NE.0) THEN
      	  IF(ID.EQ.1) THEN
      	     XS(J) = SCALA(J)*(BACK(J)*SCALB(J)-BG1/NBG)
      	  ELSE  ! i.e. for ID.EQ.2
      	    IF(SP2VSTORE(J).NE.0.0) THEN
      	     XS(J) =(BACK(J)-BG2/NBG)*SPVSTORE(J)/SP2VSTORE(J)
      	    ELSE
      	     WRITE(6,*) J,ID,NBG,IPEAK(J),IOVER(J),BACK(J)
      	     STOP ' SP2VSTORE zero - J,ID,NBG,IPEAK,IOVER,BACK'
      	    ENDIF
      	  ENDIF
      	 ELSE
      	  XS(J)=0.0 ! no correction possible
      	 ENDIF
      	IPEAK(J) = IPEAK(J) + XS(J)
180   CONTINUE
C
      IF(ID.EQ.2) THEN

      ENDIF
      RETURN
      END
C##############################################################################
      SUBROUTINE DECONVOLUTE(N,JPTEMP,TPROF,PROPPERP,PROPPARA,NXB,NYB)
      PARAMETER (IRAST=60)
      REAL*4 TPROF(5,IRAST,IRAST),TTEMP(5,IRAST,IRAST)
      DIMENSION JPTEMP(IRAST,IRAST),AJPOUT(IRAST,IRAST)
      DIMENSION A1(IRAST,IRAST),A2(IRAST,IRAST),A3(IRAST,IRAST)
      DIMENSION ACONV(IRAST,IRAST)
      DIMENSION AARRAY((IRAST+2)*IRAST),DARRAY((IRAST+2)*IRAST)
      DATA IFOR/0/,IBAK/1/

C The purpose of this subroutine_is to deconvolute a raw spot profile stored
C  in JPTEMP using the three rough scaled profiles stored in TPROF, to produce
C  a better estimate of the profile to be averaged to produce an even better
C  profile for the final spot integration.   The three profiles required are
C  the electron beam profile (N=1), the crystal shape profile (N=2) and the
C  crystal lack-of-flatness profile (N=3).  Two of these profiles in TPROF must
C  first be shrunk using PROPPERP and PROPARA to the right size.
C
C	JPTEMP    - raw spot as input
C	AJPOUT    - spot output after deconvolution
C	TPROF     - best estimates of three profiles available so far
C	TTEMP     - temporary version of TPROF to be shrunk
C	A1,A2,A3  - format conversion to use CONVOLUTE subroutine.
C	ACONV     - calculated profile for deconvolution of this spot
C	AARRAY    - format change of JPTEMP suitable for TODFFT
C	DARRAY    - format change of ACONV suitable for TODFFT
C

C Make local copy of the input profiles
      DO 10 I=1,3 ! only three profiles at present
      DO 10 IX=1,NXB
      DO 10 IY=1,NYB
10    TTEMP(I,IX,IY)=TPROF(I,IX,IY)

C Shrink profile 2 parallel to tiltaxis, and profile 3 perpendicular to it.
      CALL PROFSHRINK(2,TTEMP,NXB,NYB,PROPPARA,RADGYR)
      CALL PROFSHRINK(3,TTEMP,NXB,NYB,PROPPERP,RADGYR)
      DO 20 IX=1,NXB
      DO 20 IY=1,NYB
      if(IX.eq.30.and.IY.eq.20) then   ! debug
      	A1(IX,IY)=1.0    ! debug
      	A2(IX,IY)=1.0    ! debug
      	A3(IX,IY)=1.0    ! debug
      else     ! debug
      	A1(IX,IY)=0.0    ! debug
      	A2(IX,IY)=0.0    ! debug
      	A3(IX,IY)=0.0    ! debug
      endif     ! debug
      	A1(IX,IY)=TTEMP(1,IX,IY)
      	A2(IX,IY)=TTEMP(2,IX,IY)
      	A3(IX,IY)=TTEMP(3,IX,IY)
20    CONTINUE

C First convolute the two profiles whose influence is to be eliminated,
      IF(N.EQ.1) CALL CONVOLUTE(A2,A3,ACONV,NXB,NYB,4)
      IF(N.EQ.2) CALL CONVOLUTE(A1,A3,ACONV,NXB,NYB,5)
      IF(N.EQ.3) CALL CONVOLUTE(A1,A2,ACONV,NXB,NYB,6)
      CALL FITGAUSS(ACONV,NXB,NYB,RADGYR,AZERO)

C  then use the resultant profile to deconvolute the raw observed spot shape.
C    load JPTEMP and ACONV into ARRAY and DARRAY
      	 DO 30 IY=1,NYB
      	   IND=NXB + (IY-1)*(NXB+2)
      	   AARRAY(IND+1)=0.0 ! zero two extra elements at NX+1,NX+2
      	   AARRAY(IND+2)=0.0
      	   DARRAY(IND+1)=0.0 ! zero two extra elements at NX+1,NX+2
      	   DARRAY(IND+2)=0.0
      	 DO 30 IX=1,NXB
      	   IND=IX + (IY-1)*(NXB+2)
      	   AARRAY(IND)=JPTEMP(IX,IY)
      	   DARRAY(IND)=ACONV(IX,IY)  !
30		CONTINUE
C   do both ffts
      CALL TODFFT(AARRAY,NXB,NYB,IFOR)
      CALL TODFFT(DARRAY,NXB,NYB,IFOR)
      CALL TWOFILED(AARRAY,DARRAY,NXB,NYB) ! divide transforms: {JPTEMP}/{ACONV}

C Filter out any noise in the divided transforms, by checking power versus
C  radius in transform - not yet done!!!!!!!

C Back-transform to produce deconvoluted spot profile
      CALL TODFFT(AARRAY,NXB,NYB,IBAK)   ! debug
      	 DO 40 IY=1,NYB
      	 DO 40 IX=1,NXB
      	   IND=IX + (IY-1)*(NXB+2)
      	   AJPOUT(IX,IY)=AARRAY(IND)  ! debug should be AARRAY
40		CONTINUE

C Write out before and after versions of spots to check everything has worked.
      CALL BEFOREAFTER(JPTEMP,AJPOUT,NXB,NYB)
      CALL BEFOREAFTER(JPTEMP,ACONV,NXB,NYB)  ! debug

C Filter out any noise in the final profile - not yet done!!!!!!!!
C and rescale deconvoluted spot to contain same integrated intensity as input.

      ASUMIN=0.0
      ASUMOUT=0.0
      DO 45 IX=1,NXB
      DO 45 IY=1,NYB
      ASUMIN =ASUMIN + JPTEMP(IX,IY)
45    ASUMOUT=ASUMOUT+ AJPOUT(IX,IY)
      IF(ASUMOUT.GT.0.0) THEN
      	SCALE=ASUMIN/ASUMOUT
      ELSE
      	SCALE=1.0
      ENDIF

      DO 50 IX=1,NXB
      DO 50 IY=1,NYB
50    JPTEMP(IX,IY) = SCALE*AJPOUT(IX,IY)

      RETURN
      END
C##############################################################################
C
      SUBROUTINE TWOFILED(AR,DR,NXB,NYB)
      DIMENSION AR(1),DR(1)
C
C  This version of twofile is for deconvolution - divide transforms
C  Divide complex array AR=(a+ib) point-by-point by complex DR=(c+id)
C  result is (a+ib)*(c-id)/(c**2+d**2)=((ac+bd)+i(bc-ad))/(c**2+d**2)
C
      WRITE(6,1000)
1000  FORMAT(' ENTERING SUBROUTINE_TWOFILED from DECONVOLUTE')
C
      NZERO=0
      NTOT1=0
      NTOT2=0
      	TWOPI = 2.0*3.1415926
      	DELPX = -TWOPI * ((NXB/2.0)-0.5)/NXB ! phase shift for centre (0.5)
      	DELPY = -TWOPI * ((NYB/2.0)-0.5)/NYB !
      DO 1100 I=1,NXB,2
      DO 1100 J=1,NYB
      	  NTOT1=NTOT1+1
          IF(I.LE.NXB/2.AND.(J.LT.NYB/4.OR.J.GT.NYB*3/4)) THEN  ! no hi freq
      	 NTOT2=NTOT2+1
C      		PSHIFT = ((I-1)/2)*DELPX + (J-1)*DELPY
      	 PSHIFT=0.0  ! debug symmetrical peak at origin
      	 CS =COS(PSHIFT)
      	 SN =SIN(PSHIFT)
      	 IND=I+(J-1)*(NXB+2)
      	 A=AR(IND)
      	 B=AR(IND+1)
C      		CRAW=DR(IND)
C      		DRAW=DR(IND+1)
      	 CRAW= SQRT(DR(IND)**2+DR(IND+1)**2) ! debug symm peak at orig
      	 DRAW=0.     !
      	 C=CRAW*CS - DRAW*SN
      	 D=CRAW*SN + DRAW*CS

      	 IF((C**2+D**2).NE.0.0) THEN
      	  ATEMP=(A*C+B*D)/(C**2+D**2)
      	  BTEMP=(B*C-A*D)/(C**2+D**2)
      	 ELSE
      	  NZERO=NZERO+1
      	  ATEMP=0.0
      	  BTEMP=0.0
      	 ENDIF
      	 AR(IND)=ATEMP ! replace by the divided transform
      	 AR(IND+1)=BTEMP
      	  ELSE
      	 AR(IND)=0.0
      	 AR(IND+1)=0.0
          ENDIF
1100  CONTINUE
      IF(NZERO.GT.0) WRITE(6,1101) NZERO,NTOT2,NTOT1
1101  FORMAT(' There were',I5,' out of',I5,' low frequency (',I5,
     .	' total) transform points with zero intensity')
      RETURN
      END
C##############################################################################
      SUBROUTINE BEFOREAFTER(JPTEMP,AJPOUT,NXB,NYB)
      PARAMETER (IRAST=60)
      DIMENSION JPTEMP(IRAST,IRAST),AJPOUT(IRAST,IRAST)
C*** jms	08.08.2010
c      DIMENSION PLOTIN(IRAST),PLOTOUT(IRAST),SYMBOL(41)
      character PLOTIN(IRAST)
      character PLOTOUT(IRAST)
      character SYMBOL(41)
      character DUMMY
      DATA SYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
     .  'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
     .  'Q','R','S','T','U','V','W','X','Y','Z','+','*','#','-',' '/
CTSH++
c      INTEGER*4 TMPSYMBOL(41)
c      EQUIVALENCE (TMPSYMBOL,SYMBOL)
c      DATA TMPSYMBOL/'0','1','2','3','4','5','6','7','8','9','A','B',
c     .  'C','D','E','F','G','H','I','J','K','L','M','N','O','P',
c     .  'Q','R','S','T','U','V','W','X','Y','Z','+','*','#','-',' '/
CTSH--
C***
      DUMMY=SYMBOL(41)
      MAXIN=-100000
      AMAXOUT=-100000.0
      DO 12 I=1,NXB
      DO 12 J=1,NYB
      MAXIN=MAX0(MAXIN,JPTEMP(I,J))
12    AMAXOUT=AMAX1(AMAXOUT,AJPOUT(I,J))

      IF(MAXIN.LE.90) RETURN

      WRITE(6,22) MAXIN,AMAXOUT
22    FORMAT('              Raw spot and deconvoluted spot profiles'/
     .	'               input raster peak intensity',I6,
     .	'                  deconvoluted raster peak intensity', F18.3)
      DO 42 J=1,NYB
      DO 32 I=1,NXB
      	IF(MAXIN.GE.35) THEN
      	 IIN = 1 + (20*JPTEMP(I,J))/MAXIN
      	ELSE
      	 IIN = 1 + JPTEMP(I,J)
      	ENDIF
      	 IF(IIN.LE.0) IIN=40
      	IOUT =1.0 + 20.0*AJPOUT(I,J)/AMAXOUT
      	 IF(IOUT.LE.0) IOUT=40
C      WRITE(6,*) IIN,IOUT
      	PLOTIN(I)=SYMBOL(IIN)
32    	PLOTOUT(I)=SYMBOL(IOUT)
      WRITE(6,33) (PLOTIN(K),K=1,NXB),DUMMY,(PLOTOUT(L),L=1,NXB)
33    FORMAT(1X,132A1)
42    CONTINUE
      RETURN
      END
C##############################################################################
      SUBROUTINE PROFSHARPEN(N,RPROF,NXB,NYB,BSHARP)
      PARAMETER (IRAST=60)
      REAL*4 RPROF(5,IRAST,IRAST)
      DIMENSION TIN(IRAST,IRAST),TOUT(IRAST,IRAST)
      DIMENSION AARRAY((IRAST+2)*IRAST)
      DATA IFOR/0/,IBAK/1/
C The purpose of this subroutine_is to sharpen a profile N, stored in RPROF.

C Make local copy of the input profile
      DO 10 IX=1,NXB
      DO 10 IY=1,NYB
10    TIN(IX,IY)=RPROF(N,IX,IY)

C    load TIN into ARRAY
      	 DO 30 IY=1,NYB
      	   IND=NXB + (IY-1)*(NXB+2)
      	   AARRAY(IND+1)=0.0 ! zero two extra elements at NX+1,NX+2
      	   AARRAY(IND+2)=0.0
      	 DO 30 IX=1,NXB
      	   IND=IX + (IY-1)*(NXB+2)
      	   AARRAY(IND)=TIN(IX,IY)
30		CONTINUE

C do fft and sharpen by exp(BSHARP*(x**2+y**2))
      CALL TODFFT(AARRAY,NXB,NYB,IFOR)
      	DO 35 JX=0,NXB/2
      	DO 35 JY=0,NYB-1
      	 IF(JY.LE.NYB/2)THEN
      	  JYY=JY
      	 ELSE
      	  JYY=JY-NYB
      	 ENDIF
      	 FX=FLOAT(JX)/FLOAT(NXB/2)
      	 FY=FLOAT(JYY)/FLOAT(NYB/2)
      	 FACTOR=EXP(BSHARP*(FX**2+FY**2)) ! sharpen for BSHARP positive
      	 IF(FACTOR.GT.5.0) FACTOR=5.0  ! limit to 5-fold sharpening
      	 INDEX = 1 + JX*2 + JY*(NXB+2)
      	 AARRAY(INDEX)=AARRAY(INDEX)*FACTOR
35    	 AARRAY(INDEX+1)=AARRAY(INDEX+1)*FACTOR

C Back-transform to produce deconvoluted spot profile
      CALL TODFFT(AARRAY,NXB,NYB,IBAK)
      	 DO 40 IY=1,NYB
      	 DO 40 IX=1,NXB
      	   IND=IX + (IY-1)*(NXB+2)
      	   TOUT(IX,IY)=AARRAY(IND)
40		CONTINUE

C Rescale deconvoluted spot to contain same integrated intensity as input.
      ASUMIN=0.0
      ASUMOUT=0.0
      DO 45 IX=1,NXB
      DO 45 IY=1,NYB
      ASUMIN=ASUMIN+TIN(IX,IY)
45    ASUMOUT=ASUMOUT+TOUT(IX,IY)
      IF(ASUMOUT.GT.0.0) THEN
      	SCALE=ASUMIN/ASUMOUT
      ELSE
      	SCALE=1.0
      ENDIF
      DO 50 IX=1,NXB
      DO 50 IY=1,NYB
50    RPROF(N,IX,IY) = SCALE*TOUT(IX,IY)

      RETURN
      END
C##############################################################################
      SUBROUTINE FITGAUSS(ACONV,NXB,NYB,RADGYR,AZERO)
      PARAMETER (IRAST=60)
      DIMENSION ACONV(IRAST,IRAST)
C fit a Gaussian to the profile, to eliminate noise
C also calculate radius of gyration as a check
      write(6,*) ' Entering FITGAUSS'
      XMHALF=(NXB-1.0)/2.0
      YMHALF=(NYB-1.0)/2.0

      S=0.
      SX=0.
      SY=0.
      SXX=0.
      SXY=0.
      ERROR=AMAX1(0.10*ACONV(NXB/2,NYB/2),0.00000001)

      DO 40 I=NXB/2,NXB  ! get approximate halfwidth
      IRAD=I-NXB/2
      IF(ACONV(I,NYB/2).LE.0.5*ACONV(NXB/2,NYB/2)) GOTO 41
40    CONTINUE
41    CONTINUE

C first calculate radius of gyration
      	SUMRADSQ=0.0
      	SNRADSQ=0.0
      	DO 45 IX=1,NXB
      	DO 45 IY=1,NYB
         RADSQ = (FLOAT(IX-1)-XMHALF)**2 + (FLOAT(IY-1)-YMHALF)**2
      	 SUMRADSQ=SUMRADSQ+RADSQ*ACONV(IX,IY)
      	 SNRADSQ=SNRADSQ+ACONV(IX,IY)
45    	CONTINUE
      	IF(SNRADSQ.LE.0.0.OR.SUMRADSQ.LE.0.0)
     .	 STOP ' ACONV all zero in FITGAUSS'
      	RADGYR=SQRT(SUMRADSQ/SNRADSQ)
C      write(6,*) ' RADGYR =',RADGYR

C least squares fit to a Gaussian
      ALOGB=ALOG(0.00000001)  ! default value
      ASIGB=ALOG(ERROR+0.00000001)-ALOGB
      ASIGB=ABS(ASIGB)   ! default sigma
      DO 50 IX=NXB/4,(3*NXB)/4  !  use central region only
      DO 50 IY=NYB/4,(3*NYB)/4  !  for Gaussian l.s. fit
      	IF(ACONV(IX,IY).GT.0.00000001) THEN
      	 ALOGA=ALOG(ACONV(IX,IY))
      	 ASIGA=ALOG(ERROR+ACONV(IX,IY))-ALOGA
      	ELSE
      	 ALOGA=ALOGB ! default value
      	 ASIGA=ASIGB ! default value
      	ENDIF
      	RADSQ = (FLOAT(IX-1)-XMHALF)**2 + (FLOAT(IY-1)-YMHALF)**2
      	SIGSQ=ASIGA**2
      	S=S+1./SIGSQ
      	SX=SX+RADSQ/SIGSQ
      	SY=SY+ALOGA/SIGSQ
      	SXX=SXX+RADSQ**2/SIGSQ
      	SXY=SXY+RADSQ*ALOGA/SIGSQ
50    CONTINUE

      DENOM=SXX*S-SX**2
      NUMER=SXY*S-SX*SY
      IF(DENOM.NE.0.) THEN
      	SLOPE=NUMER/DENOM
      	ALOGZERO=(SY-SLOPE*SX)/S
      	AZERO=EXP(ALOGZERO)
      	BFIT=SLOPE
      ELSE
      	AZERO=0.
      	BFIT=0.000001
      ENDIF
C      write(6,*) 'ACONV(NXB/2,NYB/2),AZERO,BFIT,IRAD',
C     .			ACONV(NXB/2,NYB/2),AZERO,BFIT,IRAD
      	IF(BFIT.NE.0.0) THEN
                SIGMA=1.0/SQRT(ABS(BFIT))
      	ELSE
                SIGMA=100.0
      	ENDIF
      	write(6,21) AZERO,SIGMA,RADGYR,ACONV(NXB/2,NYB/2)
21    	format(' Gaussian fit, amplitude =',F10.3,' sigma =',F10.3/
     .	 '      compare this with radius of gyration =',
     .	F10.3/'      and maximum peak height at centre =',F10.3)

      RETURN
      END
C###############################################################################
      SUBROUTINE REINDEX(ATANGL,ATAXIS,DX1,DY1,DX2,DY2,
     .            TL,TAXA,TANG,ASTAR,BSTAR,GMSTAR)
      PARAMETER (NTEST=14)
      DIMENSION TRYDX1(NTEST),TRYDY1(NTEST),TRYDX2(NTEST),TRYDY2(NTEST)
      DIMENSION TRYTL(NTEST),TRYTAXA(NTEST),TRYTANG(NTEST),TRYTXY(NTEST)
      DIMENSION IMAT(4,NTEST)
      DATA IMAT/ 1,0,-3,1, -1,0,-3,1,
     A	  1,0,-2,1, -1,0,-2,1,
     B	  1,0,-1,1, -1,0,-1,1,
     C	  1,0, 0,1, -1,0, 0,1,
     D	  1,0, 1,1, -1,0, 1,1,
     E	  1,0, 2,1, -1,0, 2,1,
     F	  1,0, 3,1, -1,0, 3,1/
C     Try adding +/- three short axes to long one and reversing short axis.
C
C	ATANGL and ATAXIS are approximate target valued for tiltangle and axis
C	DX1,DY1,DX2,DY2 are the lattice parameters which may be changed
C	TL,TAXA,TANG are the values for the crystal tilt to be determined
C	ASTAR, BSTAR and GMSTAR are the untilted reciprocal lattice parameters
C
      PI=3.141592654
      	DX1IN=DX1
      	DY1IN=DY1
      	DX2IN=DX2
      	DY2IN=DY2
C  ATAXIS must be between 0 and 180
      	IF(ATAXIS.LT.0.0) ATAXIS=ATAXIS+180.0
      	IF(ATAXIS.GT.180.0) ATAXIS=ATAXIS-180.0
      	IF(ATAXIS.LT.0.0.OR.ATAXIS.GT.180.0) STOP ' ATAXIS outside range'

C  First calculate possible indexing schemes and corresponding tilt axes/angles
      DO 40 J=1,NTEST
      	TRYDX1(J)=DX1IN*IMAT(1,J) + DX2IN*IMAT(2,J)    ! second part always zero
      	TRYDY1(J)=DY1IN*IMAT(1,J) + DY2IN*IMAT(2,J)
      	TRYDX2(J)=DX1IN*IMAT(3,J) + DX2IN*IMAT(4,J)    ! second part always zero
      	TRYDY2(J)=DY1IN*IMAT(3,J) + DY2IN*IMAT(4,J)
        AT=SQRT(TRYDX1(J)**2+TRYDY1(J)**2)
        BT=SQRT(TRYDX2(J)**2+TRYDY2(J)**2)
      	 ANGA=ATAN2(TRYDY1(J),TRYDX1(J))
      	 ANGB=ATAN2(TRYDY2(J),TRYDX2(J))
      	 DANG=ANGA-ANGB
      	 IF(DANG.GT.PI) DANG=DANG-PI*2.0
      	 IF(DANG.LT.-PI) DANG=DANG+PI*2.0
        GT=DANG*180.0/PI
         IF(GT.LT.0.0) GT=-GT
      	CALL EMTILT(TRYTL(J),TRYTAXA(J),TRYTANG(J),
     .	     ASTAR,BSTAR,GMSTAR,AT,BT,GT)
      	 TRYTXY(J)=ANGA*180.0/PI+TRYTL(J)*SIGN(1.0,DANG)   ! wrt xy-axes
      	 IF(TRYTXY(J).GT.180.0)  TRYTXY(J)=TRYTXY(J)-180.0
      	 IF(TRYTXY(J).LT.-180.0) TRYTXY(J)=TRYTXY(J)+180.0
      	 IF(TRYTXY(J).LT.0.)  TRYTXY(J)=TRYTXY(J)+180.0
      	 IF(TRYTXY(J).LT.0.0.OR.TRYTXY(J).GT.180.0) STOP ' TRTXY'
40    CONTINUE
C
C  then find which one is closest to the approximate given target values
      JBEST=7    ! start with the original indices
      RESIDUAL= ABS(ATANGL-TRYTANG(7)) + ABS(ATAXIS-TRYTXY(7))
      WRITE(6,41)
41    FORMAT(' In subroutine_REINDEX, the reindexing tests gave'/
     .	40X,'    N   Residual  Tiltangle       TAXA  ',
     .	'       Tiltangle relative to XY-axes')
      DO 45 J=1,NTEST
      	RTEST = ABS(ATANGL-TRYTANG(J)) + ABS(ATAXIS-TRYTXY(J))
      	WRITE(6,42) J,RTEST,TRYTANG(J),TRYTAXA(J),TRYTXY(J)
42	FORMAT(40X,I5,3F11.3,F18.3)
      	IF(RTEST.LT.RESIDUAL-0.1) THEN ! must be significantly better
      	 RESIDUAL=RTEST
      	 JBEST=J
      	ENDIF
45    CONTINUE
      TL=TRYTL(JBEST)
      TAXA=TRYTAXA(JBEST)
      TANG=TRYTANG(JBEST)
C
      IF(JBEST.EQ.7) THEN !  no change needed
      	WRITE(6,46)
46	FORMAT('  No reindexing was necessary')
      ELSE
      	DX1 = DX1IN*IMAT(1,JBEST) + DX2IN*IMAT(2,JBEST)
      	DY1 = DY1IN*IMAT(1,JBEST) + DY2IN*IMAT(2,JBEST)
      	DX2 = DX1IN*IMAT(3,JBEST) + DX2IN*IMAT(4,JBEST)
      	DY2 = DY1IN*IMAT(3,JBEST) + DY2IN*IMAT(4,JBEST)
      	WRITE(6,47) DX1,DY1,DX2,DY2,(IMAT(I,JBEST),I=1,4)
47	FORMAT( '  Lattice was therefore reindexed as follows to give ',
     .	 'new starting values for DX1,DY1,DX2,DY2 =  ',4F9.3/
     .	 40X,'   ANEW =',I3,' *AOLD +',I3,' *BOLD'/
     .	 40X,'   BNEW =',I3,' *AOLD +',I3,' *BOLD')
      ENDIF
      RETURN
      END
