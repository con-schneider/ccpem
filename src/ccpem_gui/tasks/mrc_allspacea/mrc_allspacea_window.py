#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Task window for MRC Allspacea program.
'''
from PyQt4 import QtGui
from ccpem_core.tasks.mrc_allspacea import mrc_allspacea_task
from ccpem_gui.utils import window_utils

class MrcAllspaceaWindow(window_utils.CCPEMTaskWindow):
    '''
    Inherits from CCPEMTaskWindow.
    '''
    def __init__(self,
                 task,
                 parent=None):
        super(MrcAllspaceaWindow, self).__init__(parent=parent,
                                                 task=task)

    def set_args(self):
        '''
        Set input arguments.
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)
        # Input datafile
        datafile_arg_input = window_utils.FileArgInput(parent=self,
                                                       required=True,
                                                       arg_name='datafile',
                                                       args=self.args)
        self.args_widget.args_layout.addWidget(datafile_arg_input)
        # Input symmetry
        symm_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='symmetry',
            args=self.args,
            tooltip_text='Define symmetry')
        self.args_widget.args_layout.addWidget(symm_input)
        # Input other args
        self.grid_layout = QtGui.QGridLayout()
        g_row = 3
        for arg in [('search', 'Search'),
                    ('refine', 'Refine'),
                    ('tilt', 'Tilt'),
                    ('ncycle', 'Num cycles'),
                    ('origh', 'Orig h'),
                    ('origk', 'Orig k'),
                    ('tilth', 'Tilt h'),
                    ('tiltk', 'Tilt k'),
                    ('stepsize', 'Step size'),
                    ('phasesize', 'Phases size')]:
            arg_menu = window_utils.NumberArgInput(parent=self,
                                                   arg_name=arg[0],
                                                   label=arg[1],
                                                   args=self.args)
            self.grid_layout.addWidget(arg_menu, g_row, 0)
            g_row += 1
        g_row = 3
        for arg in [('alpha', 'Alpha'),
                    ('beta', 'Beta'),
                    ('gamma', 'Gamma'),
                    ('rin', 'R in'),
                    ('rout', 'R out'),
                    ('cs', 'CS'),
                    ('kv', 'Kv'),
                    ('ilist', 'I list'),
                    ('rot180', 'Rot 180'),
                    ('iqmax', 'Iq max')]:
            arg_menu = window_utils.NumberArgInput(parent=self,
                                                   arg_name=arg[0],
                                                   label=arg[1],
                                                   args=self.args)
            self.grid_layout.addWidget(arg_menu, g_row, 2)
            g_row += 1
        self.args_widget.args_layout.addLayout(self.grid_layout)


def main():
    '''
    For testing
    '''
    window_utils.standalone_window_launch(
#         args_json='./test_data/unittest_args.json',
        task=mrc_allspacea_task.MrcAllspacea,
        window=MrcAllspaceaWindow)


if __name__ == '__main__':
    main()
