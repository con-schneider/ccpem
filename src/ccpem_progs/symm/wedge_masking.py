from math import pi, cos, sin
import numpy as np
from helper_functions.nptomo import NumpyTom, MRCfile
import sys


# defines the missing wedge mask, takes the sampling angles, the path to the missing wedge
# file and the dimensions of the mask (ie the dims of the sampling matrix before analysis
def mask_wedge_tangent(polar_in, wedgepath, mask_dims):

    # define how to open the input files
    if wedgepath[-4:].lower() == ".npy":
        open_tom = NumpyTom
    elif wedgepath[-4:].lower() == ".mrc":
        open_tom = MRCfile
    else:
        print "File format of the missing wedge mask not supported. Please provide a file in .mrc or .npy format."
        sys.exit(1)

    # the incoming data contains tuples of (r,A) for each sampling circle in one row
    # only need to consider one set of angles
    polar_tangent_angles = np.copy(polar_in['angle'][0]) + pi/2

    # convert into cartesian coordinates, setting radius to 1 --> |vector| == 1
    # note that get_all_tom... returns a matrix with index order zyx --> order of cos, sin
    cart_tan_vectors = np.zeros((polar_tangent_angles.shape[0], 3))
    cart_tan_vectors[:, -2:] = np.array([(sin(angle), cos(angle)) for angle in polar_tangent_angles])

    # import the wedgeMask
    with open_tom(wedgepath) as t:
        missing_wedge = t.get_all_tom_density_values()

    # normalise to 1/2 Nyquist
    cart_tan_vectors *= min([f/4. for f in missing_wedge.shape])

    # extend tangent vectors from the middle of the wedge volume
    centre = np.array([x/2 for x in missing_wedge.shape])
    cart_tan_vectors += centre

    # compare to missing wedge mask and set the mask 0 if vector in wedge, 1 if not
    mw_ints = np.around(missing_wedge).astype(np.int)  # round and to int to use as indices
    int_tan_vectors = np.around(cart_tan_vectors).astype(np.int)

    tan_mask = np.array([mw_ints[tuple(int_tan_vectors[i, ])]
                         for i in range(int_tan_vectors.shape[0])])

    # bring the mask into the right shape
    # fill the other 2 dimensions of the outfile with repeats of the tan_mask
    out_mask = np.tile(tan_mask, (mask_dims[2], mask_dims[1], 1))
    out_mask = out_mask.swapaxes(0, 2)

    return out_mask
