#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core import test_data

class TestConkit(unittest.TestCase):
    '''
    Unit test for ConKit.
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_conkit_tools(self):
        # Just test import for now
        try:
            import conkit
            import_ok = True
        except ImportError:
            import_ok = False
        assert import_ok

if __name__ == '__main__':
    unittest.main()
