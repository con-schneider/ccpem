!C******************************************************************************  
!C    Copyright (c) Medical Research Council, Laboratory of Molecular          *  
!C     Biology.    All rights reserved.                                        *  
!C                                                                             *  
!C     All files within this package, unless otherwise stated, are Copyright   *  
!C     (c) Medical Research Council. Redistribution is forbidden.               *  
!C     This program was written by Alan Roseman at the MRC Laboratory of        *  
!C     Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.       *  
!C                                                                             *  
!C     The package and the source are provided without warranty of any kind,    *  
!C     either express or implied, including but not limited to merchantability  *  
!C     or fitness for a particular purpose.                                    *  
!C                                                                             *  
!C     The MRC will not be liable in any way for any losses howsoever caused    *  
!C     by the use of the package or the source, such losses to include but not  *  
!C     be limited to loss of profit, business interruption, loss of business    *  
!C     information, or other pecuniary loss, including but not limited to       *  
!C     special incidental consequential or other damages.  The user agrees      *  
!C     to hold the MRC harmless for any loss, claim, damage, or liability,      *  
!C     of whatsoever kind or nature, which may arise from or in connection      *  
!C     with the use thereof.                                                   *  
!C                                                                              *  
!C     COPYRIGHT 2002                                                          *  
!C                                                                             *  
!C*******************************************************************************  
!DockEM2XpeaksMv2.00, AMR, Jan 2003.  
!DockEM2Xpeaks, extracts the peaks from the map files produced by dockEM2  
! Output is a searchdock file, like the old DockEM produced.  
!AMR 11/2002  
! 12/2002 add real space masking optino to select local region for peak,  
! also peak radius to exclude neadby solutions.
! 1. read map,cccmap, psimap, phimap, thetamap  
! 2. find peaks, mask neighbours, loop  
! 3. output: searchdocnnn.dkm


! changes for SGI compiler 5/3
! more accuracte com , 4/2k4.
! write masked cccmap, 4/2k4.
! correct mask shift, 4/2k4.

!C input:   
!C       Three digit run version key.

        Module image_arrays2
               real, dimension (:,:,:), allocatable ::  rotsobj, rotmask,aa
                real, dimension (:,:,:), allocatable :: lcf, cnv1, cnv2,corr1,v
        End Module image_arrays2
        
        
        
        program DockEM2XpeaksM
        
        use coordinates
        Use  mrc_image
        real, dimension (:,:,:), allocatable :: cccmaxmap,psimap,phimap,thetamap,mask
        
        
        real sampling,domega,omst
        real psi,theta,phi,b,val,blot(99999,3)
        
        
        integer runcode,err
        integer nm,i,bx,by,bz
        integer a,numangles,ji1,ji2,angnum
        integer counter,counter2,acount,bcount
	integer count,comix,comiy,comiz,shx,shy,shz,numatoms
  
        character*80 filename,sobjfilename,maskfilename,anglesfile,junk,pdbfile
!       character*3 num
        logical t,f
        real th,sx,sy,sz,n,j2,peakrad,maskth,peakr,r,sd,mean
        integer vec(3),x,y,z
	real comx,comy,comz
        integer x1,x2,y1,y2,z1,z2,c,ni
   
       
  
        t=.true.
        f=.false.
        blot=0
  
        print*, 'DockEM2XpeaksMV2.02'
        print*, '==================='
        print*,'Enter the sampling of the map (A/pixel)'
        read*, sampling
        print*,'Enter a CCC threshold for peaks to extract, or number of hits to get.'
        read*, n
        write (6,*) 'Enter a run code, for the DockEM in/output files.'
        read (5,*) runcode
30      format (I3)
	
	print*,'Enter a mask file for local solution extraction.'
	read*,maskfilename
	print*,'Enter a mask binary threshold.'
	read*,maskth
	
	print*,'Enter the search object pdb file'
	read*,pdbfile
	print*,' Enter a peak radius (in Angstroems), for exclusion of shoulder solutions'
	read*,peakr
	print*,peakr
	peakr=peakr/sampling
	
!        x1=0;x2=0;y1=0;y2=0;z1=0;z2=0
!	print*,' Enter a x,y,z limits, or 0,0,0,0,0,0 for none. (Xmin,Xmax,Ymin,Ymax,Zmin,Zmax) '
!	read*,x1,x2,y1,y2,z1,z2
	
!C ***********************************************  
!C open input from MRC format files

        CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'.mrc','RO')
	CALL IRDHDR(7,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        CALL IMOPEN(11,'cccmaxmap-masked.mrc','UNKNOWN')
	CALL ITRHDR(11,7)	
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1
        
        nxp1=nx+1
        nxp2=nx+2
       
!C allocate  image_files 

        allocate (cccmaxmap(0:nxp1,0:nym1,0:nzm1),phimap(0:nxp1,0:nym1,0:nzm1))
        allocate (thetamap(0:nxp1,0:nym1,0:nzm1), psimap(0:nxp1,0:nym1,0:nzm1))  
        allocate (mask(0:nxp1,0:nym1,0:nzm1))
        
                CALL IMOPEN(8,'phimap'//num(runcode)//'.mrc','RO')
        	CALL IRDHDR(8,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
                CALL IMOPEN(9,'thetamap'//num(runcode)//'.mrc','RO')
		CALL IRDHDR(9,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
                CALL IMOPEN(10,'psimap'//num(runcode)//'.mrc','RO')
                CALL IRDHDR(10,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
                
                
         filename= 'cccmaxmap'//num(runcode)//'.mrc'
        CALL READMRCMAP(7,filename,cccmaxmap,nx,ny,nz,T,T,T,err) !read, is open,  close,      
          filename= 'phimap'//num(runcode)//'.mrc'
        CALL READMRCMAP(8,filename,phimap,nx,ny,nz,T,T,T,err) !read, is open,  close, add 2 bytes
         filename= 'thetamap'//num(runcode)//'.mrc'
        CALL READMRCMAP(9,filename,thetamap,nx,ny,nz,T,T,T,err) !read,  open,  close, add 2 bytes         
         filename= 'psimap'//num(runcode)//'.mrc'
        CALL READMRCMAP(10,filename,psimap,nx,ny,nz,T,T,T,err) !read,  open,  close, add 2 bytes  
        
	                     
        CALL IMOPEN(10,maskfilename,'RO')
        CALL IRDHDR(10,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        CALL READMRCMAP(10,maskfilename,mask,nx,ny,nz,.FALSE.,T,T,err) !read,  open,  close, add 2 bytes  
	

	
	where (mask.ge.maskth)
			mask=1
	elsewhere
			mask=0
	endwhere
	
	nm=sum(mask(:,:,:))
	
	print*,'Number in mask =',nm
	ni=nx*ny*nz
	call msd(cccmaxmap,nx,ny,nz,ni,mean,sd,err)
! apply mask
	
	call readpdb(pdbfile,numatoms)
	
	call com(comx,comy,comz,numatoms)
	print*,comx,comy,comz
	comix=int((comx/sampling)+0.5)
	comiy=int((comy/sampling)+0.5)
	comiz=int((comz/sampling)+0.5)
	print*,comix,comiy,comiz
!	shiftmask
!	shx=comix-nx
!	shy=comiy-ny
!	shz=comiz-nz
	shx=comix
	shy=comiy
	shz=comiz
	
	mask=cshift(mask,shx,1)
	mask=cshift(mask,shy,2)
	mask=cshift(mask,shz,3)

	cccmaxmap=cccmaxmap*mask
	CALL writeMRCMAP(11,cccmaxmap,nx,ny,nz,sampling,err)  
	
! apply given boundaries.  
!	 If ((x1.ne.0).and.(x2.ne.0)) then
!                                 cccmaxmap(0:x1-1,:,:)=0
!                                  cccmaxmap(x2+1:nxm1,:,:)=0
!                                  cccmaxmap(:,0:y1-1,:)=0
!                                  cccmaxmap(:,y2+1:nym1,:)=0
!                                  cccmaxmap(:,:,0:z1-1)=0
!                                  cccmaxmap(:,:,z2+1:nzm1)=0
!        endif
        
         
! create blot for peakshoulders

	count=0
	do ix=-peakr,peakr
		do iy=-peakr,peakr
			do iz=-peakr,peakr
				r=rad(ix,iy,iz,nx,ny,nz)
				if (r.lt.peakr) then
					count=count+1
					blot(count,1)=ix
					blot(count,2)=iy
					blot(count,3)=iz
				endif
			enddo
		enddo
	enddo


      open(4,file='searchdoc'//num(runcode)//'.dkm',status='UNKNOWN')
        !loop over cccmaxmap
        
        th=-9999.
        if (n.lt.1) then
        		th=n
        		n=10000.
        endif
        
        cx=int(float(nx)/2.)
        cy=int(float(ny)/2.)
        cz=int(float(nz)/2.)
        
            
        do i=1,n
!        	print*,i,n,psi,theta,phi
		vec=maxloc(cccmaxmap)
		x=vec(1)-1
		y=vec(2)-1
		z=vec(3)-1
		val=cccmaxmap(x,y,z)
		if (val.eq.-2) cycle
		if (val.lt.th) exit
		cccmaxmap(x,y,z)=-2
	
		!mask neighbours too  
		do c=1,count
			bx=x+blot(c,1)
			by=y+blot(c,2)
			bz=z+blot(c,3)
			if (bx.gt.nxm1) bx=nxm1
			if (by.gt.nym1) by=nym1
			if (bz.gt.nzm1) bz=nzm1
			if (bx.lt.0) bx=0
			if (by.lt.0) by=0
			if (bz.lt.0) bz=0
			cccmaxmap(bx,by,bz)=-1.3
		enddo
		
!		print*,x,y,z,val,vec

                psi = psimap(x,y,z)
		theta = thetamap(x,y,z)
		phi = phimap(x,y,z)
		
!		print*,x,y,z,val,phi,theta,psi
  
		j2=0
		b=0
		angnum=0
		
		sx=x
		sy=y
		sz=z
		
		if (sx.gt.cx) sx=sx-nx
		if (sy.gt.cy) sy=sy-ny
		if (sz.gt.cz) sz=sz-nz
		
		sx = sx    !*sampling
		sy = sy    !*sampling
		sz = sz    !*sampling
		
		j2=(val-mean)/sd


		write (4,510) i,sx,sy,sz,angnum,b,psi,theta,phi,j2,val
			  
	enddo
	
	
510             format (1x,I8,3F8.2,I8,F8.3,3F10.2,2x,2G12.6)
  
         STOP
              contains
              
              
        function num(number)
        character(len=3) num
    
        
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)
  
        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'
  
        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
       print *,hun,ten,units
  
  
        num=digit(hun)//digit(ten)//digit(units)  
        return
        end function num
          
          
        SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
   
        Use mrc_image
   
         INTEGER  X,Y,Z,err
         real map(0:X+1,0:Y-1,0:Z-1)
         INTEGER  stream, IX,IY,IZ
      
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
              
          if(.not. open) then
                       CALL IMOPEN(stream,filename,'RO')
                       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
          endif
            
         
      
      
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
!            print*,ix,ALINE(IX+1)
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
        if(add2) then  
                       map(X+0,iy,iz)=0
                       map(X+1,iy,iz)=0
        endif
       
!          print*,ix,iy,iz,ALINE(IX+1)  
350   CONTINUE

        if (close) then
                CALL IMCLOSE(stream)
                print*,'closed ',stream
        endif

      return
998   STOP 'Error on file read.'
     
        END SUBROUTINE READMRCMAP
        
      SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         

      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZM1
      DO 450 IY = 0,NYM1
         
            DO 400 IX = 0,NXM1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP


        
        
        
	real function rad(x,y,z,nx,ny,nz)
	integer x,y,z,nx,ny,nz,cx,cy,cz
	real r
	
	r=(x)**2+(y)**2+(z)**2
	rad=sqrt(r)
	end function rad
	
      SUBROUTINE MSD(map,nx,ny,nz,nm,val,STD,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
        real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th,STD
      integer nm
        real*8 nm8
     
   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
        nm8=nm
      SD =-9999999
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))
      mean=lsum/nm8

        map8=map
        map8=map8*map8
       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
 
         lsum=lsum*lsum
         sum_sqs=nm8*sum_sqs
         sq=((sum_sqs-lsum))
         sq=sq/(nm8*nm8)
    


       if (sq.lt.0) stop 'sd lt zero in msd.'

       th=0.00001
      if (sq.gt.th) then
                sd = sqrt(sq)
                val= -mean/sd
                err=0
        elseif (sq.le.0) then
                err=1
                print*,'le0'
                stop
        elseif (sq.le.th) then 
                !map=(map-mean)
                print*,'at threshold'
                err=0
                stop
        endif

        VAL=MEAN
        STD=SD
       return
       end subroutine MSD

	end program DockEM2XpeaksM
