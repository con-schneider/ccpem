#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from ccpem_core.mmdb_coord_tools.ext import mmdb_coord_tools as mmdb


class mmdb_selection_type(object):

    def undefined(self):
        return mmdb.STYPE_UNDEFINED

    def atom(self):
        return mmdb.STYPE_ATOM

    def residue(self):
        return mmdb.STYPE_RESIDUE

    def chain(self):
        return mmdb.STYPE_CHAIN

    def model(self):
        return mmdb.STYPE_MODEL


class mmdb_selection_key(object):

    def new(self):
        return mmdb.SKEY_NEW

    def or_key(self):          # or reserved
        return mmdb.SKEY_OR

    def and_key(self):         # and reserved
        return mmdb.SKEY_AND

    def xor(self):
        return mmdb.SKEY_XOR

    def clr(self):
        return mmdb.SKEY_CLR

    def xand(self):
        return mmdb.SKEY_xand

    # Wild card to select any res
    def any_res(self):
        return mmdb.ANY_RES


class mmdb_selection_property(object):

    def solvent(self):
        return mmdb.SELPROP_Solvent

    def aminoacid(self):
        return mmdb.SELPROP_Aminoacid

    def nucleotide(self):
        return mmdb.SELPROP_Nucleotide

    def sugar(self):
        return mmdb.SELPROP_Sugar

    def modres(self):
        return mmdb.SELPROP_ModRes


def mmdb_copy_and_paste(mol_copy_atom_selection, mol_copy, mol_paste):
    mmdb.AtomCopyPaste(mol_copy_atom_selection, mol_copy, mol_paste)


class manager(object):
    def __init__(self, filename_open=None):
        self.filename_open = filename_open
        if self.filename_open is not None:
            self.mol = mmdb.mmdbManager(filename_open)
        else:
            self.mol = mmdb.mmdbManager()

    def make_empty_selection(self):
        '''
        Make selection (stored as selection number in mmdb)
        '''
        sel_num = self.mol.NewSelection()
        return sel_num

    def make_selection(
            self,
            sel_num=None,
            sel_type=mmdb_selection_type().residue(),  # selection type
            model=0,                                   # 0=any model
            chain="*",                                 # Comma seperated string for multiple, * for any
            res_min=mmdb_selection_key().any_res(),    # min residue number, any_res for any residue
            ins_min="*",                               # min insertion number, any_res for any residue
            res_max=mmdb_selection_key().any_res(),    # max residue number
            ins_max="*",                               # max insertion number
            res_name="*",                              # residue name
            atom_name="*",                             # atom name
            element="*",                               # chemical element
            alt_loc="*",                               # alternative location indicator
            sel_key=mmdb_selection_key().new()):       # mmdb selection key

        if sel_num is None:
            sel_num = self.make_empty_selection()

        self.mol.Select(sel_num,
                        sel_type,
                        model,
                        chain,
                        res_min,
                        ins_min,
                        res_max,
                        ins_max,
                        res_name,
                        atom_name,
                        element,
                        alt_loc,
                        sel_key)
        return sel_num

    def get_atom_coords(self, sel_num=None):
        '''
        Default select all atoms
        '''
        if sel_num is None:
            sel_num = self.make_selection(
                sel_type=mmdb_selection_type().atom())
        return self.mol.GetAtomCoords(sel_num)

    def set_atom_coords(self, coords, sel_num):
        self.mol.SetAtomCoords(coords, sel_num)

    def generate_symmetry_mates(self):
        '''
        Calls function, non zero returns 
        '''
        rc = self.mol.GenerateSymMates()
        if rc != 0:
            print ('Symmetry mate generation not possible due to missing '
                   'operators and/or cell parameters')

    def apply_rot_trans(self, rot_mat=None, trans_mat=None):
        if rot_mat is None:
            rot_mat = ((1, 0, 0),
                       (0, 1, 0),
                       (0, 0, 1))
        assert len(rot_mat) == 3
        assert len(rot_mat[0]) == 3
        if trans_mat is None:
            trans_mat = (0, 0, 0)
        assert len(trans_mat) == 3
        self.mol.ApplyTransform(rot_mat, trans_mat)

    def rename_chain(self, current_chain_id, new_chain_id):
        sel_num = self.make_selection(sel_type=mmdb_selection_type().chain(),
                                      chain=current_chain_id)
        self.mol.RenameChain(sel_num, new_chain_id)

    def delete_selected(self,
                        sel_num):
        '''
        Delete selected objects
        '''
        self.mol.DeleteSelectedObjects(sel_num)

    def delete_solvent(self):
        '''
        Remove solvent
        '''
        self.mol.DeleteSolvent()

    def delete_alternative_locations(self):
        '''
        Remove solvent atoms
        '''
        self.mol.DeleteAltLocs()

    def delete_high_bfactor_atoms(self, bmax=100):
        '''
        Atoms with bfactor greater than bmax removed
        '''
        self.mol.DeleteAtomBFactorMax(bmax)

    def delete_high_bfactor_residues(self, bmax=100):
        '''
        Residues with an atom great than bmax deleted
        '''
        self.mol.DeleteResidueBFactorMax(bmax)

    def write_pdb_file(self,
                       filename):
        '''
        Output put file.
        '''
        self.mol.WritePDB(filename)
