#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import unittest
import os
import shutil
import json
import tempfile
from ccpem_progs.ribfind import run_ribfind
from ccpem_core import ccpem_utils


class Test(unittest.TestCase):
    '''
    Unit test for ribfind.
    '''
    def setUp(self):
        '''
        Always run at start of test, e.g. for creating directory to store
        temporary test data producing during unit test.
        '''
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()
        self.test_pdb_path = self.test_data + '/1akeA.pdb'
        assert os.path.exists(self.test_pdb_path)
        self.test_dssp_path = self.test_data + '/1akeA.dssp'
        assert os.path.exists(self.test_dssp_path)

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

#     def test_ribfind_integration(self):
#         ccpem_utils.print_header(message='Unit test - Ribfind program')
#         params = {'input_pdb': self.test_pdb_path,
#                   'output_dssp': self.test_dssp_path,
#                   'contact_distance': 10.0,  # 6.5 A default, 10.0 A faster
#                   'job_location': self.test_output}
#         args_path = os.path.join(self.test_output,
#                                  'ribfind_params.json')
#         json.dump(params, open(args_path, 'w'))
#         run_ribfind.main(args_path=args_path)

if __name__ == '__main__':
    unittest.main()
