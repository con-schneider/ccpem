#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---For Option 2---

# (i)  will read in a target sequence and a template structure file
# (ii) will create a specified number of homology models

from modeller import *
from modeller.scripts import complete_pdb
from modeller.automodel import *

import sys
import os

#--------------------------------------------------------------------------------------
#---------------read in data via keyboard----------------------------------------------
#--------------------------------------------------------------------------------------

# read in number of models to create from keyboard:
if len(sys.argv) != 2:  
    print "usage: filename.py [number of suboptimal alignments]"
    sys.exit()

# alignment file is named alignment.ali

no_models	= 1
no_loopModels   = 10
no_alignments   = int(sys.argv[1])-1              # number of models to create


#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------


#--------------------------------------------------------------------------------------
# align target sequence and template structure

log.verbose()
env = environ()
env.libs.topology.read(file='$(LIB)/top_heav.lib')
env.libs.parameters.read(file='$(LIB)/par.lib')

aln = alignment(env)

#--------------------------------------------------------------------------------------
# read template structure from "template.pdb"

mdl = model(env)
mdl.read(file="template")
aln.append_model(mdl, align_codes = "template", atom_files = "template")

#--------------------------------------------------------------------------------------
# read target sequence from "target.fa"
aln.append(file="target.fa", align_codes=("target"), alignment_format = 'fasta')

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# OPTION 1
# This is supposed to be the first option...just making one alignment (align2D) and creating models from it

#--------------------------------------
# salign (align2D)

aln.salign(rr_file='$(LIB)/as1.sim.mat',  # Substitution matrix used
           output='',
           max_gap_length=20,
           gap_function=True,#False, #True,              # If False then align2d not done
           feature_weights=(1., 0., 0., 0., 0., 0.),
           gap_penalties_1d=(-600, -400), #(-100, 0),
           gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
           # d.p. score matrix
           #write_weights=True, output_weights_file='salign.mtx'
           similarity_flag=True)   # Ensuring that the dynamic programming
                                   # matrix is not scaled to a difference matrix

# two different output files
pir_file   = "target_template.ali"
fasta_file = "target_template.fa"

aln.write(file = pir_file,   alignment_format='pir')
aln.write(file = fasta_file, alignment_format='fasta')

#-------------------------------------------------------------------------
# store sequence identity of the alignment in file "Sequence_identity.txt"

s1 = aln[0]
s2 = aln[1]
f = open("Sequence_identity.txt","w")
f.write("%s and %s have %d equivalences, and are %.2f%% identical"
        % (s1, s2, s1.get_num_equiv(s2), s1.get_sequence_identity(s2)))
f.close()


#--------------------------------------------------------------------------------------
# create number of specified models via automodel()---------------------
a = automodel(env, alnfile=pir_file,                        #---target file should be named "target.fa" by the preprocess script "isFASTA.py"
              knowns='template', sequence='target',         #---the structure file should be named "template.pdb" the target sequence file "target.fa" by the webserver
              assess_methods=(assess.normalized_dope))      #---score the created models according to their normalized_dope score
a.starting_model = 1
a.ending_model = no_models                                  #---number of models to create
a.make()
#---------------------------------------------------------------------- a.make()

# list saving all produced models
ok_models = list()

# Get a list of all successfully built models from a.outputs (align2d models)
align2d_models = filter(lambda x: x['failure'] is None, a.outputs)
i = 1
for elem in align2d_models:
##    new_name = elem['name'].rstrip('.pdb') + '_%d.pdb' %i
    new_name = elem['name'][:len(elem['name'])-8] + '_%d.pdb' %i    
    i += 1
    os.system( 'mv %s %s' %( elem['name'], new_name ) )
    elem['name'] = new_name
    ok_models.append( elem )


# Create a new class based on 'dope_loopmodel' so that we can redefine
# select_loop_atoms
#a = complete_pdb(env,'target.B9999_1.pdb')
class MyLoop(dope_loopmodel):
    # This routine picks the residues to be refined by loop modeling
    def select_loop_atoms(self):
        # Two residue ranges (both will be refined simultaneously)
        return selection(self.residue_range('45:', '52:'),
                       self.residue_range('266:', '279:'))

#a = MyLoop(env,
#           alnfile  = pir_file,      # alignment filename
#           knowns   = 'template',               # codes of the templates
#           sequence = 'target')#,               # code of the target
#           loop_assess_methods=assess.DOPE) # assess each loop with DOPE
a = MyLoop(env,
           inimodel='target.B9999_1.pdb', # initial model of the target
           sequence='target',               # code of the target
           loop_assess_methods=assess.DOPE)     # gives DOPE energy readouts at                                                 # bottom of log file FOR LOOPS

#a.starting_model= 1     # index of the first model
#a.ending_model  = 1     # index of the last model

a.loop.starting_model = 1                       # First loop model
#a.loop.ending_model   = no_loopModels           # Last loop model
a.loop.ending_model = 5
a.loop.md_level = refine.very_fast  # loop refinement method
a.make()                            # do modeling and loop refinement
                        
loop_models = list()
print len(loop_models)

align2DLoopModels = filter(lambda x: x['failure'] is None, a.outputs)
for elem in align2DLoopModels:
    print elem['name']

    
sys.exit("END")
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# OPTION 2
# make sub-optimal alignments to get a variaty of models
# use instead of normal align2D sub-optimal alignments
aln.salign(rr_file='$(LIB)/as1.sim.mat',  # Substitution matrix used
           max_gap_length=20,
           gap_function=True,#False, #True,              # If False then align2d not done
           feature_weights=(1., 0., 0., 0., 0., 0.),
           gap_penalties_1d=(-600, -400), #(-100, 0),
           gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
#           n_subopt = 5, subopt_offset = 15,
	   n_subopt = no_alignments, subopt_offset = 15,
           similarity_flag=True)   # Ensuring that the dynamic programming
                                   # matrix is not scaled to a difference matrix

# Convert suboptimal alignment output file into actual alignments
#ok_models = list()
f = open('suboptimal_alignments2d.out')
i = no_models + 1
for (n, aln) in enumerate(aln.get_suboptimals(f)):
    alignment = 'target_template%d.ali' %(i-no_models)
    aln.write(file = alignment)

    # create number of specified models via automodel()
    a = automodel(env, alnfile=alignment,                       #---target file should be named "target.fa" by the preprocess script "isFASTA.py"
                  knowns='template', sequence='target',         #---the structure file should be named "template.pdb" the target sequence file "target.fa" by the webserver
                  assess_methods=(assess.normalized_dope))      #---score the created models according to their normalized_dope score
    a.starting_model = 1
    a.ending_model = no_models                                  #---number of models to create
    a.make()

    # (i)   get list with successfully built models
    new_models = filter(lambda x: x['failure'] is None, a.outputs)
    j = 1
    for elem in new_models:
##        new_name = elem['name'].rstrip('.pdb') + '_%d.pdb' %i
        new_name = elem['name'][:len(elem['name'])-8] + '_%d.pdb' %i
        i += 1
        j += 1
        os.system( 'mv %s %s' %( elem['name'], new_name ) )
        elem['name'] = new_name
        ok_models.append( elem )
    
f.close()


# Sort the models by Normalized_Dope score
key = 'Normalized DOPE score'                                                                
ok_models.sort(lambda a,b: cmp(a[key], b[key]))                                     

# Get the best model (lowest Normalized_Dope score) and store it in 'Top_model' file
m = ok_models[0]
##print "Top model: %s (DOPE score %.3f)" % (m['name'], m[key])                       
f = open("Top_model.txt","w")
f.write(m['name'])
f.close()

# store ranked list of models in file "Models_summary"
# and store names of produced models in File "Model_names"
g = open("Models_summary.txt","w")
g.write("Filename Normalized_DOPE\n")
i = 0
while i < len(ok_models):
    mn = ok_models[i]
    g.write("%s %f\n" % (mn['name'], mn[key]))
    i += 1

g.close()

