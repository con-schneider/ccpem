#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import sys
import tempfile
from PyQt4 import QtGui
from ccpem_gui.project_database import sqlite_project_database
from ccpem_gui.project_database import database_widget

'''
N.B. Be careful that database is made a unique test_output directory, e.g.
if multiple unit test create and delete same directory containing DB this can
cause problems with parallel testing.
'''

class Test(unittest.TestCase):
    '''
    Unit test for sqlite database.
    '''
    def setUp(self):
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_create_database(self):
        database_path = os.path.join(
            self.test_output,
            'pyqt4_projectmanager.sqlite')
        db = sqlite_project_database.CCPEMDatabase(database_path=database_path)
        # Test programs table functions
        db.insert_new_program(
            program_id='tst_prog-0.3',
            name='tst_prog',
            module='',
            version='0.3',
            description='a test entry',
            author='tom')
        db.insert_new_program(
            program_id='tst_prog-0.1',
            name='tst_prog',
            module='',
            version='0.1',
            description='a test entry',
            author='tom')
        db.insert_new_program(
            program_id='tst_prog-0.2',
            name='tst_prog',
            module='',
            version='0.2',
            description='a test entry2',
            author='tom')
        assert db.get_latest_program_id(program_name='tst_prog')\
            == 'tst_prog-0.3'

        # Test new job functions
        job_a = db.insert_new_job(program='tst_prog-0.1',
                                  job_location=self.test_output + '/tst1',
                                  title='tst1',
                                  mode='TEST')
        assert job_a == 1
        job_b = db.insert_new_job(program='tst_prog-0.1',
                                  job_location=self.test_output + '/tst2',
                                  title='tst2')
        assert job_b == 2
        job_c = db.insert_new_job(program='tst_prog-xx',
                                  job_location=self.test_output + '/tst3',
                                  title='tst3')
        assert job_c == 3
        db.show_all_jobs()
        assert db.get_status_of_jobs()[2] == 'ready'
        db.update_job_start(job_id=job_b)
        assert db.get_status_of_jobs()[2] == 'started'
        db.update_job_end(job_id=job_b)
        assert db.get_status_of_jobs()[2] == 'finished'
        job_b_info = db.get_job_information(job_id=job_b)
        job_b_path = job_b_info['dirpath']
        print job_b_path
        assert os.path.exists(job_b_path)
        db.delete_job(job_id=job_b, remove_data=True)
        assert len(db.get_status_of_jobs().keys()) == 2
        assert not os.path.exists(job_b_path)

        print db.get_status_of_jobs()

        job_id = db.insert_new_job('tst_prog-0.1',
            job_location=self.test_output + '/tst_update',
            title='tst2')

        # Insert job with fixed job_id
        job_id = db.insert_new_job(
            'tst_prog-0.1',
            job_id=4,
            job_location=self.test_output + '/tst_update',
            title='tst2')

        # Show RDV in pop-up window
        if False:
            app = QtGui.QApplication(sys.argv)
            project_view = database_widget.CCPEMProjectTableView(
                database=db)
            project_view.show()
            app.exec_()

#     def test_project_manager(self):
#         ccpem_utils.print_header(message='Unit test - Project Manager')
#         path = os.path.dirname(os.path.realpath(__file__))
#         p1 = project_manager.CCPEMProject(user='Dave',
#                                           name='test1',
#                                           path=path)
#         p2 = project_manager.CCPEMProject(user='Bob',
#                                           name='test2',
#                                           path=path+'/..')
#         filename = os.path.join(self.test_output, 'projects.json')
#         pros = project_manager.CCPEMProjectContainer(filename=filename)
#         pros.add_project(project=p1)
#         pros.add_project(project=p2)
#         for p in pros:
#             print p.path
#             if p.user == 'Bob':
#                 assert p.name == 'test2'
#         pros.save()
#         del pros
#         pros_2 = project_manager.CCPEMProjectContainer(filename=filename)
#         for p in pros_2:
#             if p.user == 'Bob':
#                 print p.name
#                 assert p.name == 'test2'
#             pros_2.remove_project(project=p)
#         for p in pros_2:
#             assert p.name != 'test2'

if __name__ == '__main__':
    unittest.main()
